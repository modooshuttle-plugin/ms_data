/**
 * 오늘날짜에 대한 문자열을 입력한 포맷으로 반환한다.
 * ex)
 * MMMM Do YYYY, h:mm:ss a'); // October 17th 2019, 11:54:20 am
 * 'dddd'                    // Thursday
 * "MMM Do YY"               // Oct 17th 19
 * 'YYYY [escaped] YYYY'     // 2019 escaped 2019
 * @param format string 포맷
 */
export declare const mFormat: (format: string) => any;
export declare const mmt: {
    intervalDay: (day: number) => any;
    makeBoardPeriod: (day: string) => {
        startDay: any;
        endDay: any;
    };
    momentTz: (date?: string | number | undefined) => any;
    mFormat: (format: string) => any;
};

export declare const toCamelCase: (col: string) => string;
export declare const objToCamelCase: (obj: any) => any;
export declare const searchText: (text: string) => string;
export declare const snakeToCamel: (text: string) => string;
export declare const objSnakeToCamel: (obj: any) => any;
export declare const camelToSnake: (s: any) => any;

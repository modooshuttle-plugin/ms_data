/**
 *   active record
 *      created by KPS
 */
declare type Tcol = "func";
export declare class Ar {
    private col;
    private cond;
    private icond;
    private hcond;
    private tables;
    private order;
    private group;
    private table;
    private paging;
    /**
     * 컬럼 선택 목록
     * @param col
     */
    select(col: string): void;
    /**
     * and 조건문 목록
     * @param cond
     */
    where(cond: string): void;
    /**
     * or 조건문 목록
     * @param cond
     */
    or_where(cond: string): void;
    /**
     *
     * @param having
     */
    having(cond: string): void;
    /**
     *  테이블 선택
     * @param table
     */
    from(table: string): void;
    /**
     *  join 하는 테이블 목록
     * @param table
     */
    join(table: string, value?: string): void;
    /**
     *  groupby 목록
     * @param group
     */
    groupby(group: string): void;
    /**
     *  orderby 목록
     * @param order
     */
    orderby(order: string): void;
    /**
     *  paging
     * @param paging
     */
    limit(paging: string): void;
    /**
     *  select 종합
     */
    private cSelect;
    private cSelectCnt;
    /**
     *  join 종합
     */
    private cJoin;
    private cWhere;
    private cInnerWhere;
    private cGroup;
    private cHaving;
    private cOrder;
    getSql: () => string;
    getRowSql(): string;
    getCountSql(): string;
    input: any;
    set(key: string, value: any, type?: Tcol): void;
    setCnt(): any;
    setObject(obj: any): void;
    insertSql(): string;
    insertSelectSql(): string;
    private parseValue;
    deleteSql(): string;
    updateSql(): string;
    getResToJson(result: any[]): any[];
}
export {};

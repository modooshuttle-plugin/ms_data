import { Ar } from "../../../util";
declare class DrTermsAgreeQuery {
    /**
     * 선택된 테이블 및 컬럼 정보를 불러온다.
     * @param alias true일 경우에 dr_terms_agree객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
     * @returns
     */
    select: (alias?: boolean | undefined) => Ar;
    /**
     * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
     */
    selectAlias: (ar: Ar, alias?: string | undefined) => void;
    /**
     * set 입력 | 변경할 컬럼을 설정한다.
     * @form
     */
    set: (form: any) => Ar;
    /**
     *  @deprecated set을 사용하자
     */
    insert: (form: any) => Ar;
    /**
     * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
     */
    table: () => Ar;
    /**
     *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
     */
    col: {
        drTermsAgreeId: string;
        drId: string;
        agreeYn: string;
        createdAt: string;
        updatedAt: string;
    };
    /**
     * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
     */
    tb: () => Ar;
    /**
     * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
     */
    check: () => Ar;
}
export declare const DrTermsAgreeSql: DrTermsAgreeQuery;
export {};

export interface ILogEvt {
    logEvtId: number;
    evtId: string;
    obj: string;
    objDtl: string;
    action: string;
    target: string;
    targetId: string;
    createdAt: string;
}

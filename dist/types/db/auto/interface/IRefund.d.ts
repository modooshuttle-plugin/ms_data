export interface IRefund {
    /**
     * 환불 ID
     */
    refundId: number;
    /**
     * 결제완료 ID
     */
    paidId: number;
    /**
     * 결제 ID
     */
    payId: number;
    /**
     * 주문번호
     */
    orderNo: string;
    /**
     * 탑승 ID
     */
    boardId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 기업 구분
     */
    compnCd: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

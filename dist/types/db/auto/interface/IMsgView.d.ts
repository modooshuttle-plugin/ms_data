export interface IMsgView {
    /**
     * 메세지 뷰 ID
     */
    msgViewId: number;
    /**
     * 경로구분
     */
    rtCd: string;
    /**
     * 제목
     */
    title: string;
    /**
     * 탬플릿 리스트
     */
    tplList: any;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * idx
     */
    idx: number;
}

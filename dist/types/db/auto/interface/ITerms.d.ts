export interface ITerms {
    /**
     * 정책 ID
     */
    termsId: number;
    /**
     * 제목
     */
    title: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

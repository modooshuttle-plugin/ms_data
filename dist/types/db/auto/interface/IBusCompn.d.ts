export interface IBusCompn {
    /**
     * 버스 업체 ID
     */
    busCompnId: number;
    /**
     * 이름
     */
    nm: string;
    /**
     * 사업자 명
     */
    bizNm: string;
    /**
     * 이메일
     */
    email: string;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 사업자 등록번호
     */
    bizRegNo: string;
    /**
     * 사업자 주소
     */
    bizAddr: string;
    /**
     * 은행이름
     */
    bankNm: string;
    /**
     * 은행계좌
     */
    bankAccount: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

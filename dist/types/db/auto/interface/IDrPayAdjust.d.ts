export interface IDrPayAdjust {
    /**
     * 기사 조정금액 ID
     */
    drPayAdjustId: number;
    /**
     * 기사 계약금액 ID
     */
    drPayId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 정산 적용 시작월
     */
    setlStartMonth: string;
    /**
     * 정산 적용 종료월
     */
    setlEndMonth: string;
    /**
     * 기사 ID
     */
    drId: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * 계약 조건 Id
     */
    drPayCondId: number;
}

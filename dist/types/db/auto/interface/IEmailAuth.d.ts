export interface IEmailAuth {
    /**
     * 이메일 인증 ID
     */
    emailAuthId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 인증 ID
     */
    authId: number;
    /**
     * 이메일 구분
     */
    emailCd: string;
    /**
     * 인증여부
     */
    useYn: number;
    /**
     * 쿠폰 ID
     */
    cpnId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 기업명
     */
    orgNm: string;
    /**
     * 이메일 도메인
     */
    domain: string;
    /**
     * 경로 구분
     */
    rtCd: string;
    /**
     * 이메일 주소
     */
    emailAddr: string;
    /**
     * 기업 유저 인증 ID
     */
    orgUserAuthId: number;
    /**
     * 결제 ID
     */
    payId: number;
}

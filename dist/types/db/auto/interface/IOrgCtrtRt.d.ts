export interface IOrgCtrtRt {
    /**
     * 기업 계약 경로 ID
     */
    orgCtrtRtId: number;
    /**
     * 기업 계약 ID
     */
    orgCtrtId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

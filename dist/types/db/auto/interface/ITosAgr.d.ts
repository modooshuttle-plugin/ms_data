export interface ITosAgr {
    tosAgrId: number;
    userId: string;
    loginId: string;
    phone: string;
    gender: string;
    ageRange: string;
    birthYear: string;
    birthMonth: string;
    birthDay: string;
    agrChannel: string;
    deleteYn: string;
    createdAt: string;
    updatedAt: string;
}

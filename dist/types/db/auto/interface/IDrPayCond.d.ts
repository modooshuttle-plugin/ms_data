export interface IDrPayCond {
    /**
     * 계약 조건 Id
     */
    drPayCondId: number;
    /**
     * 계약 Id
     */
    drPayId: number;
    /**
     * 배차 Id
     */
    dispatchId: number;
    /**
     * 시작 정산월
     */
    startSetlMonth: string;
    /**
     * 계약조건
     */
    condCd: string;
    /**
     * 조건이 적용되는 최소 인원
     */
    minCnt: number;
    /**
     * 적용금액
     */
    amount: number;
    /**
     * 등록일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 Id
     */
    adminId: number;
    /**
     * 메모
     */
    comment: string;
}

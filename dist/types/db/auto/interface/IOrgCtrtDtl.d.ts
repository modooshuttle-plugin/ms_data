export interface IOrgCtrtDtl {
    /**
     * 기업 계약 상세 ID
     */
    orgCtrtDtlId: number;
    /**
     * 기업 상세 ID
     */
    orgCtrtId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 계약상세 제목
     */
    title: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 종료일
     */
    endDay: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 정산 구분
     */
    setlCd: number;
    /**
     * 비즈니스 구분
     */
    bizCd: number;
    /**
     * 상품 구분
     */
    prodsCd: number;
    /**
     * 구독 구분
     */
    subscribeCd: number;
    /**
     * 계약서 주소
     */
    url: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

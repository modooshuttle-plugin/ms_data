export interface IDr {
    /**
     * 기사정보 ID
     */
    drInfoId: number;
    /**
     * 기사 ID
     */
    drId: string;
    /**
     * 이름
     */
    nm: string;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 전화수신 구분
     */
    phoneAccess: number;
    /**
     * 기사 구분
     */
    statusCd: string;
    /**
     * 기사 계정 구분
     */
    drCd: string;
    /**
     * 계약서 작성 여부
     */
    ctrtYn: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
}

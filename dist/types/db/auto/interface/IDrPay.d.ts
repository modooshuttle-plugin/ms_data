export interface IDrPay {
    /**
     * 기사 계약금액 ID
     */
    drPayId: number;
    /**
     * 배차 ID
     */
    dispatchId: number;
    /**
     * 시작 정산월
     */
    startSetlMonth: string;
    /**
     * 정산 시작일자
     */
    setlStartDay: string;
    /**
     * 정산 일자 구분
     */
    setlDayCd: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 기사 ID
     */
    drId: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 버스 업체 ID
     */
    busCompnId: number;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 대차 구분
     */
    borrowSetlCd: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
}

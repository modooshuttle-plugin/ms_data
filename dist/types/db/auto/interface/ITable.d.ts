export interface ITable {
    /**
     * 테이블  Id
     */
    tableId: number;
    /**
     * 구분
     */
    targetCd: string;
    /**
     * 테이블명
     */
    nm: string;
    /**
     * 이전 이름
     */
    oldNm: string;
    /**
     * 테이블명 (한국어)
     */
    korNm: string;
    /**
     * 버전
     */
    ver: string;
    /**
     * db구분
     */
    db: string;
    /**
     * 코멘트
     */
    comment: string;
}

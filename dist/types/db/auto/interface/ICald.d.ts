export interface ICald {
    /**
     * 달력 ID
     */
    caldId: number;
    /**
     * 운행구분
     */
    runnCd: number;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * 날짜
     */
    day: string;
}

export interface IDrAppPush {
    /**
     * 기사앱 푸시 ID
     */
    drAppPushId: number;
    /**
     * 기사 ID
     */
    drId: string;
    /**
     * 앱 푸시 토큰
     */
    appPushToken: string;
    /**
     * 기기 ID
     */
    uuid: string;
    /**
     * 플랫폼
     */
    platform: string;
    /**
     * os ver
     */
    osVer: string;
    /**
     * 기기 모델
     */
    model: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 네이티브 버전
     */
    nativeVer: string;
}

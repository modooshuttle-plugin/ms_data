export interface ITime {
    /**
     * 시간 ID
     */
    timeId: string;
    /**
     * idx
     */
    idx: number;
    /**
     * 시간
     */
    time: string;
    /**
     * 신청시 시간
     */
    timeApply: string;
    /**
     * 검색시 시간
     */
    timeSearch: string;
    /**
     * 시간 구분
     */
    timeCd: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 출퇴근 구분
     */
    commuteCd: number;
}

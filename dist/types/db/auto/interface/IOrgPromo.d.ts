export interface IOrgPromo {
    /**
     * 기업 프로모션 ID
     */
    orgPromoId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 기업 계약 ID
     */
    orgCtrtId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface ISt {
    /**
     * 정류장 ID
     */
    stId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 구역 구분
     */
    sectionCd: string;
    /**
     * idx
     */
    idx: number;
    /**
     * path ver
     */
    pathVer: number;
    /**
     * 정류장 코드
     */
    stCd: string;
    /**
     * 주소
     */
    addr: string;
    /**
     * 별칭
     */
    alias: string;
    /**
     * 위도
     */
    lat: number;
    /**
     * 경도
     */
    lng: number;
    /**
     * 정차시간
     */
    time: string;
    /**
     * 월요일 정차시간
     */
    timeMon: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 읍면동 코드
     */
    emdCd: string;
    /**
     * 수평각도
     */
    pan: number;
    /**
     * 수평각 위도
     */
    panLat: number;
    /**
     * 수평각 경도
     */
    panLng: number;
}

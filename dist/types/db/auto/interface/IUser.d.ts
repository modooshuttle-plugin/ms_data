export interface IUser {
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 이름
     */
    nm: string;
    /**
     * 유저 구분
     */
    userCd: string;
    /**
     * 닉네임
     */
    nick: string;
    /**
     * 활성 구분
     */
    activeCd: string;
    /**
     * 약관 체크
     */
    termsCheck: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 추천인 코드
     */
    refCd: string;
    /**
     * 이메일
     */
    orgEmail: string;
    /**
     * 기업 ID
     */
    orgId: string;
    /**
     * 성별
     */
    gender: string;
    /**
     * 생년
     */
    birthYear: string;
    /**
     * 생월
     */
    birthMonth: string;
    /**
     * 생일
     */
    birthDay: string;
    /**
     * 국가코드
     */
    countryCd: string;
}

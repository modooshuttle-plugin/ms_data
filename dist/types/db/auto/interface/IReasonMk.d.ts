export interface IReasonMk {
    /**
     * 제작 취소 사유 ID
     */
    reasonMkId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 유저제작 ID
     */
    makeId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 경로 구분
     */
    rtCd: string;
    /**
     * 취소 사유 구분
     */
    reasonCd: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

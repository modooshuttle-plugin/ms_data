export interface IHold {
    /**
     * 홀딩 ID
     */
    holdId: number;
    /**
     * 구독 ID
     */
    subscribeId: string;
    /**
     * 쿠폰 ID
     */
    cpnId: number;
    /**
     * 신청 ID
     */
    applyId: number;
    /**
     * 탑승 ID
     */
    boardId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 홀딩 구분
     */
    holdCd: string;
    /**
     * 쿠폰 발행 구분
     */
    cpnPblshCd: number;
    /**
     * 홀딩 상태 구분
     */
    statusCd: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface IPathLine {
    /**
     * path line ID
     */
    pathLineId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * path ver
     */
    pathVer: number;
    /**
     * 구역 구분
     */
    sectionCd: string;
    /**
     * linestring
     */
    geom: any;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface IDrDoc {
    /**
     * 기사문서 ID
     */
    drDocId: number;
    /**
     * 기사 ID
     */
    drId: string;
    /**
     * 파일위치
     */
    pathFolder: string;
    /**
     * 기사문서구분
     */
    drDocCd: string;
    /**
     * 적용시작일
     */
    startDay: string;
    /**
     * 적용종료일
     */
    endDay: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 종료시간
     */
    updatedAt: string;
}

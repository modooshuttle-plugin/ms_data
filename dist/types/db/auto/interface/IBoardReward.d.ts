export interface IBoardReward {
    /**
     * 탑승 보상 ID
     */
    boardRewardId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 운행 이슈 ID
     */
    runnIssId: number;
    /**
     * 탑승 이슈 ID
     */
    boardIssId: number;
    /**
     * 탑승 ID
     */
    boardId: string;
    /**
     * 상태 구분
     */
    statusCd: number;
    /**
     * 보상 구분
     */
    rewardCd: string;
    /**
     * 보상방법 구분
     */
    methodCd: string;
    /**
     * 내용 상세 (json)
     */
    rewardDtl: any;
    /**
     * 이미 파일 경로
     */
    pathFolder: any;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface IBizOrderDtl {
    /**
     * 기업 주문 상세 Id
     */
    bizOrderDtlId: number;
    /**
     * 기업 주문  Id
     */
    bizOrderId: number;
    /**
     * 버스 구분
     */
    busCd: string;
    /**
     * 버스 인승
     */
    busCnt: string;
    /**
     * 시간 범위
     */
    timeRange: string;
    /**
     * 운행 대수
     */
    runnCnt: string;
    /**
     * 운행예정일
     */
    runnSchdDay: string;
    /**
     * 출발지
     */
    start: string;
    /**
     * 경유지
     */
    move: string;
    /**
     * 도착지
     */
    end: string;
    /**
     * 메모
     */
    comment: string;
    /**
     * 퇴근 경로에 대한 질문
     */
    endComment: string;
    /**
     * 작성시간
     */
    createdAt: string;
    /**
     * 구분
     */
    sectionCd: string;
    /**
     * 출발 시간
     */
    startTime: string;
    /**
     * 도착시간
     */
    endTime: string;
    /**
     * 탑승자 평균 나이
     */
    age: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

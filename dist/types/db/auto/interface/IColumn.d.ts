export interface IColumn {
    /**
     * 컬럼Id
     */
    columnId: number;
    /**
     * 테이블  Id
     */
    tableId: number;
    /**
     * 컬럼명
     */
    nm: string;
    /**
     * 이전 컬럼명
     */
    oldNm: string;
    /**
     * 데이터 타입
     */
    type: string;
    /**
     * 컬렴명 (한국어)
     */
    korNm: string;
    /**
     * 버전
     */
    ver: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 데이터베이스 구분
     */
    db: string;
}

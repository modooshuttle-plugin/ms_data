export interface IPayTry {
    /**
     * 결제 시도 ID
     */
    payTryId: number;
    /**
     * 주문번호
     */
    orderNo: string;
    /**
     * 결제 ID
     */
    payId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 결제방법 구분
     */
    methodCd: string;
    /**
     * 현금 영수증 발급 여부
     */
    cashReceiptYn: string;
    /**
     * 상태 구분
     */
    statusCd: string;
    /**
     * 채널 구분
     */
    channelCd: string;
    /**
     * 아임포트 ID
     */
    impUid: string;
    /**
     * 결제 제공자 구분
     */
    providerCd: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 현금영수증 발급 일시
     */
    cashReceiptAt: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

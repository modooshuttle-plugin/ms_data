export interface IEvent {
    /**
     * 이벤트 ID
     */
    eventId: number;
    /**
     * 제목
     */
    title: string;
    /**
     * 부제목
     */
    titleSub: string;
    /**
     * pc이미지
     */
    imagePc: string;
    /**
     * 모바일 이미지
     */
    imageMobile: string;
    /**
     * 상세1 사용 구분
     */
    detail_1Use: string;
    /**
     * 상세1 제목
     */
    detail_1Title: string;
    /**
     * 상세1 내용
     */
    detail_1Content: string;
    /**
     * 상세2 사용 구분
     */
    detail_2Use: string;
    /**
     * 상세2 제목
     */
    detail_2Title: string;
    /**
     * 상세2 내용
     */
    detail_2Content: string;
    /**
     * 상세3 사용 구분
     */
    detail_3Use: string;
    /**
     * 상세3 제목
     */
    detail_3Title: string;
    /**
     * 상세3 내용
     */
    detail_3Content: string;
    /**
     * 버튼 사용 구분
     */
    btnUse: string;
    /**
     * 버튼 텍스트
     */
    btnText: string;
    /**
     * 버튼 링크
     */
    btnLink: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 종료일
     */
    endDay: string;
}

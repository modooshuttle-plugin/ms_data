export interface IRef {
    /**
     * 추천 ID
     */
    refId: number;
    /**
     * 탑승 ID
     */
    boardId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 다른 유저 ID
     */
    otherUserId: string;
    /**
     * 쿠폰 ID
     */
    cpnId: number;
    /**
     * 다른 쿠폰 ID
     */
    otherCpnId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 배포구분
     */
    deployCd: number;
}

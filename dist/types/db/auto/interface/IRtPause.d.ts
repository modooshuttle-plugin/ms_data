export interface IRtPause {
    /**
     * 경로 일시중단 ID
     */
    rtPauseId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 일시중단 구분
     */
    pauseCd: number;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 종료일
     */
    endDay: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

export interface IProds {
    /**
     * 상품 번호
     */
    prodsNo: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 상품 ID
     */
    prodsId: string;
    /**
     * 결제금액
     */
    amount: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 기본금액
     */
    defaultAmount: number;
    /**
     * 배포 여부
     */
    deployYn: number;
    /**
     * 관리자 ID
     */
    adminId: number;
}

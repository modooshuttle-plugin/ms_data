export interface ILogBoard {
    logBoardId: number;
    evtId: string;
    boardId: number;
    applyId: string;
    userId: string;
    rtId: string;
    prodsId: string;
    startStCd: string;
    startStVer: string;
    endStCd: string;
    endStVer: string;
    boardCd: string;
    boardShapeCd: number;
    seatId: string;
    startDay: string;
    endDay: string;
    createdAt: string;
}

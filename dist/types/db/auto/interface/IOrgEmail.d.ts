export interface IOrgEmail {
    /**
     * 기업 이메일 ID
     */
    orgEmailId: number;
    /**
     * 기업 유저 인증 ID
     */
    orgUserAuthId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 기업 계약 ID
     */
    orgCtrtId: number;
    /**
     * 이메일 도메인
     */
    domain: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

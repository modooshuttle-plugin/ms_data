export interface IDic {
    /**
     * 용어사전 id
     */
    dicId: number;
    /**
     * 논리명
     */
    logical: string;
    /**
     * 물리명
     */
    physical: string;
    /**
     * 표준용어
     */
    standardEn: string;
    logicalEqualKo: string;
    physicalEqualEn: string;
    dicCd: string;
    deployYn: number;
    createdAt: string;
    updatedAt: string;
    adminId: number;
}

export interface IOrg {
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 이름
     */
    nm: string;
    /**
     * 사업자명
     */
    bizNm: string;
    /**
     * 사업자 등록번호
     */
    bizRegNo: string;
    /**
     * 사업자 주소
     */
    bizAddr: string;
    /**
     * 기업 구분
     */
    orgCd: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

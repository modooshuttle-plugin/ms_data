export interface IOrgCtrt {
    /**
     * 기업 게약 ID
     */
    orgCtrtId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 계약 주체 구분
     */
    ctrtSubjCd: string;
    /**
     * 제목
     */
    title: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 ID
     */
    adminId: number;
}

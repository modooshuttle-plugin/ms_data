export interface IAdmin {
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 비밀번호
     */
    pw: string;
    /**
     * 이름
     */
    nm: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * 이메일주소
     */
    email: string;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 노션 유저 Id
     */
    notionUserId: string;
    /**
     * 관리자 구분
     */
    adminCd: string;
}

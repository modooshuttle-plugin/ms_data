export interface ILogPay {
    logPayId: number;
    evtId: string;
    actionCd: string;
    statusCd: string;
    channelCd: string;
    impUid: string;
    orderNo: string;
    methodCd: string;
    userId: string;
    payId: number;
    amount: number;
    createdAt: string;
}

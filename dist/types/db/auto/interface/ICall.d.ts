export interface ICall {
    /**
     * 콜 ID
     */
    callId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 이전 경로 ID
     */
    beforeRtId: string;
    /**
     * 추천 경로 ID
     */
    rtId: string;
    /**
     * 추천 경로 구분
     */
    rtCd: string;
    /**
     * 콜 구분
     */
    callCd: string;
    /**
     * 탑승 ID
     */
    boardId: string;
    /**
     * idx
     */
    idx: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 결과 구분
     */
    resultCd: string;
    /**
     * 관리자 ID
     */
    adminId: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 탑승취소|중단 ID
     */
    reasonId: string;
}

export interface IRtTaskReq {
    /**
     * 경로 업무 요청 ID
     */
    rtTaskReqId: number;
    /**
     * 경로 업무 ID
     */
    rtTaskId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 요청 구분
     */
    reqCd: string;
    /**
     * 제목
     */
    title: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 요청 상태 구분
     */
    reqStatusCd: number;
    adminIds: any;
}

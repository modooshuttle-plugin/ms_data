export interface IOfferBus {
    /**
     * 제안 셔틀 조건 Id
     */
    offerBusId: number;
    /**
     * 제안 기사 Id
     */
    offerDrId: number;
    /**
     * bep
     */
    bep: number;
    /**
     * 연비
     */
    fuelEfcnc: number;
    /**
     * 추가금액
     */
    addAmount: number;
    /**
     * 보험금
     */
    insrAmount: number;
    /**
     * 지입료
     */
    paperAmount: number;
    /**
     * 관제비
     */
    controlAmount: number;
    /**
     * 기타 금액
     */
    expendAmount: number;
    /**
     * 차량 금액
     */
    amount: string;
    /**
     * 관리자 Id
     */
    adminId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 버스 구분
     */
    busCd: string;
}

export interface IPath {
    /**
     * path ID
     */
    pathId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * path ver
     */
    pathVer: number;
    /**
     * 출발지 구분
     */
    startCd: string;
    /**
     * 도착지 구분
     */
    endCd: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 거리
     */
    dist: number;
    /**
     * polyline 삭제예정
     */
    googlePl: string;
}

export interface IMsg {
    /**
     * 메세지 ID
     */
    msgId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    prodsId: string;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 이름
     */
    nm: string;
    /**
     * 발송구분
     */
    msgCd: string;
    /**
     * 경로구분
     */
    rtCd: string;
    /**
     * 메세지 제목
     */
    subject: string;
    /**
     * 메세지 내용
     */
    comment: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 예약시간
     */
    reservAt: string;
    /**
     * 발송결과
     */
    result: string;
    /**
     * 문자 ID
     */
    sendId: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 문자업체  구분
     */
    msgCompnCd: string;
    /**
     * 문자탬플릿 ID
     */
    msgTplId: string;
    /**
     * 발송 구분
     */
    mbrCd: string;
    /**
     * 발송상태 구분
     */
    statusCd: string;
}

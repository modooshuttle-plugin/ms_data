export interface ICat {
    /**
     * 카테고리 ID
     */
    catId: number;
    /**
     * 카테고리 구분
     */
    catCd: string;
    /**
     * 구간 구분
     */
    sectionCd: string;
    /**
     * 별칭
     */
    alias: string;
    /**
     * 키워드
     */
    keyword: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
    /**
     * 출퇴근구분
     */
    commuteCd: string;
}

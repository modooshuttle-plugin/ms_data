export interface ILogApply {
    logApplyId: number;
    evtId: string;
    applyId: string;
    userId: string;
    subscribeId: string;
    prodsCd: number;
    startDay: string;
    endDay: string;
    createdAt: string;
}

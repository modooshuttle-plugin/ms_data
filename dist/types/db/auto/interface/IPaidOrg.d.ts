export interface IPaidOrg {
    /**
     * 완료된 기업 할인 ID
     */
    paidOrgId: number;
    /**
     * 기업 할인 ID
     */
    payOrgId: number;
    /**
     * 기업 할인 구분
     */
    payOrgCd: string;
    /**
     * 결제완료 ID
     */
    paidId: number;
    /**
     * 결제 ID
     */
    payId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 탑승 ID
     */
    boardId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 기업 계약 ID
     */
    orgCtrtId: number;
    /**
     * 기업 유저 인증 ID
     */
    orgUserAuthId: number;
    /**
     * 이메일 인증 ID
     */
    emailAuthId: number;
    /**
     * 주문번호
     */
    orderNo: string;
    /**
     * 금액
     */
    amount: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

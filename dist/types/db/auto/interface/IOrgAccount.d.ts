export interface IOrgAccount {
    /**
     * 기업 계정 ID
     */
    orgAccountId: number;
    /**
     * 기업 ID
     */
    orgId: number;
    /**
     * 기업 영문명
     */
    corp: string;
    /**
     * 인증 ID
     */
    id: string;
    /**
     * 인증 PW
     */
    pw: string;
    /**
     * 기업 한글명
     */
    ko: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

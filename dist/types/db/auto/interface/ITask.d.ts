export interface ITask {
    /**
     * 업무 ID
     */
    taskId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 업무 구분
     */
    taskCd: string;
    /**
     * 제목
     */
    title: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 상태 구분
     */
    statusCd: number;
    /**
     * 담당자 리스트
     */
    adminIds: any;
    /**
     * 시작일시
     */
    startDay: string;
    /**
     * 종료일시
     */
    endDay: string;
    /**
     * 노션 페이지 id
     */
    notionPageId: string;
    /**
     * 목표일
     */
    targetDay: string;
    /**
     * 수정한 관리자 Id
     */
    updatedId: number;
}

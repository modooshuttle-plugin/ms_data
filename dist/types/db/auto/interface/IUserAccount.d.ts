export interface IUserAccount {
    /**
     * 유저 계정 ID
     */
    userAccountId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * ID
     */
    id: string;
    /**
     * 비밀번호
     */
    pw: string;
    /**
     * 제공자 구분
     */
    providerCd: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface IBoardIss {
    /**
     * 탑승 이슈 ID
     */
    boardIssId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 탑승 ID
     */
    boardId: number;
    /**
     * 유저 ID
     */
    userId: string;
    /**
     * 운행 이슈 ID
     */
    runnIssId: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

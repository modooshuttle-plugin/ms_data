export interface IOfferDrPay {
    /**
     * 기사 계약금 제안액 Id
     */
    offerDrPayId: number;
    /**
     * 기사 계약금 제안 Id
     */
    offerDrId: number;
    /**
     * 기사 계약금 Id
     */
    drPayId: number;
    /**
     * 제안 구분
     */
    condCd: any;
    /**
     * 월회비
     */
    prodsAmount: number;
    /**
     * 할인액(부가세포함, 원)
     */
    discountAmount: number;
    /**
     * 할인 월회비(부가세포함, 원)
     */
    discountProdsAmount: number;
    /**
     * 결제자수
     */
    paidCnt: number;
    /**
     * 제안액 (부가세별도, 원)
     */
    drPayAmount: number;
    /**
     * 차액 (부가세별도, 원)
     */
    diffAmount: number;
    /**
     * 인센티브설정인원수(명)
     */
    incntCnt: number;
    /**
     * 필요 결제자수
     */
    minPaidCnt: number;
    /**
     * 경로 Id
     */
    rtId: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
    /**
     * 관리자 Id
     */
    adminId: number;
    /**
     * 메모
     */
    comment: string;
    /**
     * 5차_1인당인센티브(원)
     */
    incntAmount: number;
    /**
     * 최소기사비
     */
    drAmount: number;
}

export interface IRunnIss {
    /**
     * 운행 이슈 ID
     */
    runnIssId: number;
    /**
     * 경로 ID
     */
    rtId: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 종료일
     */
    endDay: string;
    /**
     * 정산월
     */
    setlMonth: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 이슈 구분
     */
    issCd: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

export interface IDrAuthMsg {
    /**
     * 기사 인증 문자 ID
     */
    drAuthMsgId: number;
    /**
     * 전화번호
     */
    phone: string;
    /**
     * 인증번호
     */
    authNo: string;
    /**
     * 발송 ID
     */
    sendId: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
}

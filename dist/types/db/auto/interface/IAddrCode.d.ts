export interface IAddrCode {
    addrCodeId: number;
    ctpKorNm: string;
    ctprvnCd: string;
    siKorNm: string;
    siCd: string;
    sigKorNm: string;
    sigCd: string;
    emdKorNm: string;
    emdCd: string;
}

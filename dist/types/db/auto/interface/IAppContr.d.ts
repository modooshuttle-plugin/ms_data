export interface IAppContr {
    /**
     * 앱 관리 Id
     */
    appContrId: number;
    /**
     * 타켓 구분
     */
    targetCd: string;
    /**
     * 웹 버전
     */
    ver: string;
    /**
     * 네이티브 버전
     */
    nativeVer: string;
    /**
     * os 구분
     */
    osCd: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

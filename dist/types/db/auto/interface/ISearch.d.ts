export interface ISearch {
    searchId: number;
    userId: string;
    startAddr: string;
    startLat: number;
    startLng: number;
    endAddr: string;
    endLat: number;
    endLng: number;
    createdAt: string;
    updatedAt: string;
    timeId: any;
    startEmdCd: string;
    endEmdCd: string;
}

export interface IPopup {
    /**
     * 팝업 ID
     */
    popupId: number;
    /**
     * 제목
     */
    title: string;
    /**
     * 시작일
     */
    startDay: string;
    /**
     * 변경일
     */
    endDay: string;
    /**
     * 이미지
     */
    img: string;
    /**
     * 이동 링크
     */
    link: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 배포 구분
     */
    deployCd: number;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

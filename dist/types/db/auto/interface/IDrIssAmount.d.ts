export interface IDrIssAmount {
    /**
     * 기사 이슈 정산 ID
     */
    drIssAmountId: number;
    /**
     * 기사 이슈 ID
     */
    drIssId: number;
    /**
     * 부호
     */
    sign: number;
    /**
     * 금액
     */
    amount: number;
    /**
     * 이슈 금액 구분
     */
    issAmountCd: string;
    /**
     * 관리자 ID
     */
    adminId: number;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 변경시간
     */
    updatedAt: string;
}

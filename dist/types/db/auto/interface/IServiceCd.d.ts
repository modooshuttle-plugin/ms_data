export interface IServiceCd {
    /**
     * 서비스 코드 ID
     */
    serviceCdId: number;
    /**
     * 테이블
     */
    tb: string;
    /**
     * 컬럼
     */
    col: string;
    /**
     * 관리 ver
     */
    ver: string;
    /**
     * 값 json_array
     */
    value: any;
    /**
     * 조건
     */
    wh: any;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 생성일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

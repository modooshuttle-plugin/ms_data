export interface ITrkEvt {
    /**
     * 트래킹 이벤트 Id
     */
    trkEvtId: number;
    /**
     * 타겟 구분
     */
    targetCd: string;
    /**
     * 트래킹 이벤트 구분
     */
    trkEvtCd: string;
    /**
     * 이벤트 이름 (eng)
     */
    evtNm: string;
    /**
     * 코멘트
     */
    comment: string;
    /**
     * 파라미터
     */
    params: any;
    /**
     * 등록일시
     */
    createdAt: string;
    /**
     * 변경일시
     */
    updatedAt: string;
}

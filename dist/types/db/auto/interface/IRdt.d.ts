export interface IRdt {
    /**
     * 리다이렉트 ID
     */
    rdtId: number;
    /**
     * url
     */
    url: string;
    /**
     * 생성시간
     */
    createdAt: string;
    /**
     * 행동구분
     */
    actionCd: string;
}

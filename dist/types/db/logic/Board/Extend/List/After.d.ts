import { Ar } from "../../../../../util";
/**
 *  연장 복사된 탑승정보 쿼리
 */
declare class After {
    /**
     * 연장 복사자
     * @param col
     * @returns
     */
    getCopiedBoard: (col: boolean) => Ar;
}
export declare const ExtendAfterListService: After;
export {};

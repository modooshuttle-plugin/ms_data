import { Ar } from "../../../../../util";
declare class BeforeList {
    /**
     * 탑승자 정보
     * @returns
     */
    getBoardInfo: (col: boolean) => Ar;
}
export declare const ExtendBeforeListService: BeforeList;
export {};

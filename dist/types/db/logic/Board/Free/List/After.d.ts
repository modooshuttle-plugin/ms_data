import { Ar } from "../../../../../util";
/**
 *  정식 복사된 정보
 */
declare class After {
    getFreeCopyBoard: (col: boolean) => Ar;
}
export declare const FreeAfterListService: After;
export {};

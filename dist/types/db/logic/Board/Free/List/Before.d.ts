import { Ar } from "../../../../../util";
declare class BeforeList {
    /**
     * 무료탑승자
     * @returns
     */
    getFreeBoard: (col: boolean) => Ar;
}
export declare const FreeBeforeListService: BeforeList;
export {};

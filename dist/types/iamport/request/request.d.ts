import { IBoard, IPay, IPayTry, IRt, IUser } from "../../db";
export interface IImp {
    pg: string;
    pay_method: string;
    merchant_uid: string;
    name: string;
    amount: number;
    buyer_name: string;
    buyer_tel: string;
    currency: "KRW";
    escrow: boolean;
    notice_url: string;
    company: "주식회사 모두의셔틀";
    card_quota: any[];
    custom_data: {
        mno: number;
        no: number;
    };
    vbank_due?: string;
    tax_free?: 0;
    naverProducts?: INaverProducts[];
    naverPopupMode?: boolean;
}
interface INaverProducts {
    categoryType: "ETC";
    categoryId: "ETC";
    uid: string;
    name: string;
    count: number;
}
export declare type TypeImpRequestObj = {
    board: IBoard;
    payTry: IPayTry;
    rt: IRt;
    user: IUser;
    webhookUrl: string;
    pay: IPay;
    vbankDue?: string;
    isMobile: boolean;
};
export declare const makeRequestObj: (obj: TypeImpRequestObj) => IImp;
export declare const naverPay: (obj: TypeImpRequestObj, value: any) => any;
export {};

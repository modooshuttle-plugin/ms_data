import { IPaid, IRefund, IUserBankAccount } from "../../db";
interface IBankServiceCd {
    key: string;
    ko: string;
    kcp: string;
}
export declare type TypeIamportRefundObj = {
    paid: IPaid;
    refund: IRefund;
    bank?: {
        userBankAccount?: IUserBankAccount;
        list: IBankServiceCd[];
    };
};
export declare const makeRefundObj: ({ paid, refund, bank }: TypeIamportRefundObj) => any;
export {};

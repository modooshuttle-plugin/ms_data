import { Iamport } from ".";
import { PaymentsByImpUidDto, PaymentsByOrderNoDto, PaymentsCancelDto } from "../interface";
export declare class PaymentsRepsoitory extends Iamport {
    /**
     * imp_uid로 내용 검색
     * @param param0
     * @returns
     */
    findOneByImpUid: ({ impUid }: PaymentsByImpUidDto) => Promise<any>;
    /**
     * merchant_uid (orderNo) 로 내용 검색
     * @param param0
     * @returns
     */
    findResultByOrderNo: ({ orderNo }: PaymentsByOrderNoDto) => Promise<any>;
    /**
     * 환불처리
     * @param obj
     * @returns
     */
    cancel: (obj: PaymentsCancelDto) => Promise<import("axios").AxiosResponse<any, any>>;
}

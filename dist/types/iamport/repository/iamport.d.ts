interface IamportConfig {
    impKey: string;
    impSecret: string;
}
export declare class Iamport {
    _iamportConfig: any;
    constructor(obj: IamportConfig);
    getData: () => Promise<any>;
    getToken: () => Promise<any>;
}
export {};

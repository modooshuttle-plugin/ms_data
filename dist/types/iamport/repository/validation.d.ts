import { Iamport } from ".";
import { PaymentPrepareAnnotationDto } from "../interface";
export declare class ValidationRepository extends Iamport {
    /**
     * validation요청
     * @param params
     */
    prepare: (params: PaymentPrepareAnnotationDto) => Promise<import("axios").AxiosResponse<any, any>>;
}

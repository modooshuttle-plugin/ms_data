import { Iamport } from ".";
export declare class VbankRepository extends Iamport {
    /**
     * 가상계좌 삭제
     * @param impUid
     */
    deleteVbank: (impUid: string) => Promise<import("axios").AxiosResponse<any, any>>;
    /**
     * 은행코드/계좌번호로 통장 예금주를 확인한다.
     * @param accountNo
     * @param bankCd
     */
    vbankHolder: (accountNo: string, bankCd: string) => Promise<any>;
}

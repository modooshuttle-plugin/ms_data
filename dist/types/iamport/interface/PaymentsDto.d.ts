import { IUserBankAccount } from "../../db";
export interface PaymentsByImpUidDto {
    impUid: string;
}
export interface PaymentsByOrderNoDto {
    orderNo: string;
}
export interface PaymentsCancelDto {
    impUid: string;
    orderNo: string;
    amount: number;
    reason: string;
    bankAccount?: Pick<IUserBankAccount, "accountNo" | "bankCd" | "nm">;
}

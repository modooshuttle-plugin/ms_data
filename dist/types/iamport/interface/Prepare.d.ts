export interface PaymentPrepareAnnotation {
    merchant_uid: string;
    amount: number;
}
export interface PaymentPrepareAnnotationDto {
    orderNo: string;
    amount: number;
}

import { InputCal, InputEl } from "./Input";
import { BusCal, BusEl } from "./Bus";
export declare class FirstCal {
    private _input;
    private _busCal;
    private _vatRatio;
    constructor(busCal: BusCal, input: InputCal);
    get busCal(): BusEl;
    get input(): InputEl;
    get vatRatio(): number;
    get oil(): {
        day: number;
        month: null;
        monthWeek: number;
    };
    get interestRatio(): {
        month: number;
        day: number;
        monthWeek: number;
    };
    get expendAmount(): {
        month: number;
        day: number;
        monthWeek: number;
    };
    get insrAmount(): {
        month: number;
        day: number;
        monthWeek: number;
    };
    get paperAmount(): {
        month: number;
        day: number;
        monthWeek: number;
    };
    get controlAmount(): {
        month: number;
        day: number;
        monthWeek: number;
    };
    get tollgateAmount(): {
        day: number;
        monthWeek: number;
    };
}

export interface InputEl {
    /**
     * 유가_경유(원/l)
     */
    oilAmount: number;
    /**
     * 톨게이트 비용(원)
     */
    tollgateAmount: number;
    /**
     * 월 운행일수(일)
     */
    runnDayCnt: number;
    /**
     * 주말 운행일수(일)
     */
    weekdayCnt: number;
    /**
     * 편동 운행횟수(일, 회)
     */
    repeatRunnCnt: number;
    /**
     * 편도 운행거리(일, km)
     */
    runnDist: number;
    /**
     * 편도 운행시간(일, 분)
     */
    runnTime: number;
    /**
     * 출근 도착시간
     */
    timeId: string;
    /**
     * 순환셔틀 여부
     */
    isCircular: boolean;
    /**
     * 연 할부이율(%)
     */
    interestRatio: number;
    /**
     * 운행지수(km/지수)
     */
    runnConstant: number;
    /**
     * 일 기타 추가비(원)
     */
    addAmountPerDay: number;
    /**
     * 주말 추가비(원)
     */
    weekDayAddAmount: number;
}
export declare class InputCal {
    _inputEl: InputEl;
    constructor(obj: InputEl);
    get inputEl(): InputEl;
    getRunnConstant: () => void;
}

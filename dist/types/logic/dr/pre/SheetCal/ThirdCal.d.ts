import { SecondCal } from "./SecondCal";
interface IThirdResult {
    avg?: number;
    avgMin?: number;
    avgMax?: number;
    time?: number;
}
export declare class ThirdCal {
    _secondCal: SecondCal;
    constructor(secondCal: SecondCal);
    timeCal: (time: string) => 1 | 1.1;
    get calDrPay(): {
        month: IThirdResult;
        day: IThirdResult;
        vat: IThirdResult;
    };
    get calProdsAmount(): {
        day: any;
        month: any;
        vat: any;
    };
    get calDrPayCd(): {
        month: {
            time: {
                ko: string;
                comment: string;
            };
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
        day: {
            time: {
                ko: string;
                comment: string;
            };
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
        vat: {
            time: {
                ko: string;
                comment: string;
            };
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
    };
    get calProdsAmountCd(): {
        day: {
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
        month: {
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
        vat: {
            avg: {
                ko: string;
                comment: string;
            };
            avgMin: {
                ko: string;
                comment: string;
            };
            avgMax: {
                ko: string;
                comment: string;
            };
        };
    };
}
export {};

export interface BusEl {
    busCd: string;
    /**
     * 버스 인승
     * 기사비 변동 적용값
     */
    cnt: string;
    /**
     * bep인원수 (int)
     * 기준데이터
     */
    bep: number;
    /**
     * 연비 (km / l)
     * # 기준데이터 # 기사 운행 고정 적용값
     */
    fuelEfcnc: number;
    /**
     * 적용금액(추가비)
     * # 기준데이터 적용값
     */
    addAmount: number;
    /**
     * 보험료 (1month)
     * # 기준데이터
     */
    insrAmount: number;
    /**
     * 지입료 (1month)
     * # 기준데이터
     */
    paperAmount: number;
    /**
     * 관제비 (1month)
     *	# 기준데이터
     */
    controlAmount: number;
    /**
     * 소모품비 (1month)
     *	# 기준데이터
     */
    expendAmount: number;
    /**
     * 차량원금
     * # 기사 운행 고정 적용값
     */
    amount: number;
}
export declare class BusCal {
    _busEl: BusEl;
    constructor(obj: BusEl);
    get busEl(): BusEl;
}

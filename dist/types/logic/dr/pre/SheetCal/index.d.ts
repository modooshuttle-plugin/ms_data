import { BusCal, BusEl } from "./Bus";
import { InputCal, InputEl } from "./Input";
import { FirstCal } from "./FirstCal";
import { SecondCal } from "./SecondCal";
import { ThirdCal } from "./ThirdCal";
export declare const sheetClass: {
    busCal: typeof BusCal;
    inputCal: typeof InputCal;
};
export declare type sheetType = {
    busEl: BusEl;
    inputEl: InputEl;
};
export declare const sheetData: {
    commuteCd: {
        a: string;
        b: string;
        c: string;
    };
    koDic: {
        cnt: string;
        bep: string;
        fuelEfcnc: string;
        addAmount: string;
        insrAmount: string;
        paperAmount: string;
        controlAmount: string;
        expendablesAmount: string;
        amount: string;
    };
    busList: BusEl[];
};
export declare class SheetCal {
    _firstCal: FirstCal;
    _secondCal: SecondCal;
    _thirdCal: ThirdCal;
    constructor(bus: BusCal, input: InputCal);
    get firstCal(): FirstCal;
    get secondCal(): SecondCal;
    get thirdCal(): ThirdCal;
}

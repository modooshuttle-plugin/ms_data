import { BusEl } from "./Bus";
export declare const commuteCd: {
    a: string;
    b: string;
    c: string;
};
export declare const koDic: {
    cnt: string;
    bep: string;
    fuelEfcnc: string;
    addAmount: string;
    insrAmount: string;
    paperAmount: string;
    controlAmount: string;
    expendablesAmount: string;
    amount: string;
};
export declare const busList: BusEl[];

import { FirstCal } from "./FirstCal";
export declare class SecondCal {
    _firstCal: FirstCal;
    constructor(firstCal: FirstCal);
    get firstCal(): FirstCal;
    /**
     *  기사 고정 지출비
     */
    get minusAmount(): {
        month: number;
        day: number;
    };
    /**
     *  평균 운행 추가비
     */
    get addAmount(): {
        day: number;
        week: number;
        month: number;
        monthWeek: number;
    };
    get minusAmountCd(): {
        day: {
            ko: string;
            comment: string;
        };
        month: {
            ko: string;
            comment: string;
        };
    };
    get addAmountCd(): {
        day: {
            ko: string;
            comment: string;
        };
        week: {
            ko: string;
            comment: string;
        };
        month: {
            ko: string;
            comment: string;
        };
        monthWeek: {
            ko: string;
            comment: string;
        };
    };
}

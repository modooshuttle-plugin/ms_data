import { IOfferDrPay } from "../../../db/auto/interface";
export declare const simltnKo: {
    prodsAmount: string;
    discountAmount: string;
    discountProdsAmount: string;
    paidCnt: string;
    drPayAmount: string;
    drAmount: string;
    diffAmount: string;
    incntCnt: string;
    incntCntPerUser: string;
    incntCntPerUserPerDiscountProdsAmount: string;
    minPaidCnt: string;
};
export interface SimltnInput extends Pick<IOfferDrPay, "prodsAmount" | "discountAmount" | "paidCnt" | "incntCnt" | "drAmount"> {
}
export declare class SmltnCal {
    _simltn: SimltnInput;
    constructor(simltn: SimltnInput);
    get calData(): {
        prodsAmount: number;
        discountAmount: number;
        discountProdsAmount: number;
        paidCnt: number;
        drPayAmount: number;
        drAmount: number;
        diffAmount: number;
        minPaidCnt: number;
        incntCnt: number;
        incntCntPerUser: number;
        incntCntPerUserPerDiscountProdsAmount: number;
    };
}

import { IMsgMake } from "./text";
/**
 * 문자내용 체크
 * @param text 탬플릿 내용
 * @returns
 */
export declare const makeMsgObj: (obj: IMsgMake, tplTxt: string) => {
    subject: any;
    phone: any;
    rtId: string | null;
    reservAt: any;
    mbrCd: any;
    msgCompnCd: any;
    userId: string;
    msgTplId: any;
    comment: string;
    createdAt: any;
};
export interface ICoolsms {
    from: string;
    text: string;
    to: string;
    type: string;
    subject?: string;
}
/**
 * coolsms 발송 객체
 * @param obj
 * @param tplTxt
 * @returns
 */
export declare const makeCoolsms: (obj: IMsgMake, tplTxt: string) => ICoolsms;

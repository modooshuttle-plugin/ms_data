import { IApply, IBoard, ICat, IMake, IMsgTpl, IPaid, IPay, IPayTry, IRt, ITime, IUser } from "../../db";
export declare type TUrl = {
    USER: string;
    AUTH: string;
    PAY: string;
    ROUTE: string;
    MAKE: string;
};
declare type TVbank = {
    vbankDate: string;
    vbankHolder: string;
    vbankCode: string;
    vbankName: string;
    vbankNum: string;
};
export interface IMsgComp {
    user?: IUser;
    apply?: IApply;
    board?: IBoard;
    make?: IMake;
    pay?: IPay;
    payTry?: IPayTry;
    paid?: IPaid;
    rt?: IRt;
    vbank?: TVbank;
}
export interface IMsgMake extends IMsgComp {
    msg: any;
    msgTpl: IMsgTpl;
    timeList: ITime[];
    catList: ICat[];
    url: TUrl;
}
export declare class MakeMsgText {
    _obj: IMsgMake;
    constructor(obj: IMsgMake);
    get makeText(): {
        check: {
            blank: boolean;
            rt: boolean;
            user: boolean;
            board: boolean;
            pay: boolean;
            vbank: boolean;
            make: boolean;
        };
        tplTxt: string;
        msg: {
            subject: any;
            phone: any;
            rtId: string | null;
            reservAt: any;
            mbrCd: any;
            msgCompnCd: any;
            userId: string;
            msgTplId: any;
            comment: string;
            createdAt: any;
        };
        coolsms: import("./makeMsg").ICoolsms;
    } | {
        check: {
            blank: boolean;
            rt: boolean;
            user: boolean;
            board: boolean;
            pay: boolean;
            vbank: boolean;
            make: boolean;
        };
        tplTxt?: undefined;
        msg?: undefined;
        coolsms?: undefined;
    };
}
export {};

export declare const tplCheck: (text: string) => {
    blank: boolean;
    rt: boolean;
    user: boolean;
    board: boolean;
    pay: boolean;
    vbank: boolean;
    make: boolean;
};
/**
 * 탬플릿에 변수가 전부 치환되었는지 체크함.
 * @param text
 * @returns
 */
export declare const hasNotValue: (text: string) => boolean;

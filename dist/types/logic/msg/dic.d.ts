export declare const msgDic: () => {
    type: string;
    code: string;
    ko: string;
    editable: boolean;
}[];
export declare const msgDicCheck: (type: string, tpl: string) => boolean;

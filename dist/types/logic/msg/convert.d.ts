import { IMsgMake } from "./text";
export declare const rtCvt: (msg: IMsgMake, text: string) => {
    reason: string[];
    text: string;
    valid: boolean;
};
export declare const userCvt: (msg: IMsgMake, text: string) => {
    valid: boolean;
    text: string;
    reason: any[];
};
export declare const applyCvt: (msg: IMsgMake, text: string) => {
    valid: boolean;
    text: string;
    reason: any[];
    apply: import("../..").IApply | undefined;
};
export declare const vbankCvt: (msg: IMsgMake, text: string) => {
    valid: boolean;
    text: string;
    reason: string[];
    vbank: {
        vbankDate: string;
        vbankHolder: string;
        vbankCode: string;
        vbankName: string;
        vbankNum: string;
    } | undefined;
};
export declare const payCvt: (msg: IMsgMake, text: string) => {
    valid: boolean;
    text: string;
    reason: any[];
    pay: import("../..").IPay | undefined;
};
export declare const makeCvt: (msg: IMsgMake, text: string) => {
    valid: boolean;
    text: string;
    reason: any[];
    make: import("../..").IMake | undefined;
};

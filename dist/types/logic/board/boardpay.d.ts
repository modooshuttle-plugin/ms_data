import { IApply, IBoard, IPaid, IPay, IRt, IServiceCd } from "../../db";
declare type BoardPay = {
    rt: IRt;
    board: IBoard;
    pay: IPay;
    apply: IApply;
    paid?: IPaid;
    serviceCd: IServiceCd;
    runnDay?: string;
};
export declare class BoardPayLogic {
    _obj: BoardPay;
    constructor(obj: BoardPay);
    get isNeed(): boolean;
    get isPaid(): boolean;
    get payDay(): string | null;
}
export {};

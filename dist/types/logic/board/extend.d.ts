import { IApply, IBoard, IProds, IUser } from "../../db";
interface IExtendBoard {
    user: IUser;
    board: IBoard;
    apply: IApply;
    prods: IProds;
    crossBoard?: IBoard;
    crossProds?: IProds;
}
export declare class ExtendBoard {
    _obj: IExtendBoard;
    constructor(obj: IExtendBoard);
    makeExtendBoard: () => any;
}
export {};

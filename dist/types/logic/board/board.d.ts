import { IBoard, IRt, IServiceCd } from "../../db";
export declare const periodCdBoard: {
    key: string;
    ko: string;
    status: string;
    detail: string;
}[];
export declare const periodCdStop: {
    key: string;
    ko: string;
    status: string;
    detail: string;
}[];
export declare const bizCd: never[];
export declare class BoardLogic {
    _rt: IRt;
    _board: IBoard;
    _serviceCd: IServiceCd;
    constructor(rt: IRt, board: IBoard, serviceCd: IServiceCd);
    get boardCd(): {
        boardCd: any;
    } | undefined;
    get runnCd(): {
        boardCd: any;
        periodCd?: any;
    };
    get schdCd(): {
        boardCd: any;
    };
    get periodCd(): {
        key: string;
        ko: string;
        status: string;
        detail: string;
    };
    /**
     * 탑승 종료일 계산
     */
    get boardEndDay(): string;
    get constList(): {
        key: string;
        ko: string;
        status: string;
        detail: string;
    }[];
    get isPossUpdate(): boolean;
    get isPossCancel(): boolean;
    get isClose(): boolean;
}

import { ICpn, IPaidCpn, ICpnConn, ICpnLimit, IServiceCd, IRt, IPay } from "../../db";
export interface ICpnObj {
    cpn: ICpn;
    paidCpn?: IPaidCpn;
    cpnConn?: ICpnConn;
    cpnLimit?: ICpnLimit;
}
export interface IServiceCdObj {
    serviceCd: {
        limitCd: IServiceCd;
        rtCd: IServiceCd;
    };
}
export interface ICpnLogic extends ICpnObj, IServiceCdObj {
}
export declare class CpnLogic {
    _obj: ICpnLogic;
    constructor(obj: ICpnLogic);
    get cpnStatus(): {
        txt: string;
        key: string;
        ko: string;
        use: boolean;
    } | {
        txt: string;
        period: string;
        ko: string;
        use: boolean;
    } | {
        txt: string;
        period: null;
        ko: string;
        use?: undefined;
    } | undefined;
    get period(): {
        key: string;
        ko: string;
        use: boolean;
    };
    get usePoss(): {
        txt: string;
        key: string;
        ko: string;
        use: boolean;
    } | undefined;
    get useImposs(): {
        txt: string;
        ko: string;
        use: boolean;
        key: string;
        period?: undefined;
    } | {
        txt: string;
        period: string;
        ko: string;
        use: boolean;
    } | {
        txt: string;
        period: null;
        ko: string;
        use?: undefined;
    } | undefined;
    get condition(): {
        cpnConn: ICpnConn | undefined;
        cpnLimit: any;
    };
    get periodTxt(): string;
    connDetail: (payId: number) => {
        isConn: boolean;
        msg: string;
    };
    limitDetail: (rt: IRt, pay: IPay) => {
        isPush: boolean;
        msg: string;
        title: string;
        type: string;
    };
}

import { IRt } from "../../db";
export declare const rtRunnCd: {
    key: string;
    ko: string;
    detail: string;
}[];
export declare const rtSchdCd: {
    key: string;
    ko: string;
    detail: string;
}[];
export declare class RtLogic {
    _rt: IRt;
    constructor(rt: IRt);
    getRtCdEx: () => {
        key: string;
        ko: string;
        detail: string;
    };
    get runnStatus(): {
        key: string;
        ko: string;
        detail: string;
    };
    get schdStatus(): {
        key: string;
        ko: string;
        detail: string;
    };
    get isStop(): boolean;
}

import { IOrgEmail, IOrgPayDcTpl, IOrgPayTpl, IOrgUserAuth } from "../../../db";
export declare type TOrg = {
    orgUserAuth?: IOrgUserAuth;
    orgPayTpl?: IOrgPayTpl;
    orgPayDcTpl?: IOrgPayDcTpl;
    orgEmailList: IOrgEmail[];
};
export declare class CalOrgPay {
    private _org;
    constructor(obj: TOrg);
    get payOrgObj(): {
        payDc: {
            orgPayDcTplId?: number | undefined;
            orgUserAuthId?: number | undefined;
            orgId?: number | undefined;
            orgCtrtId?: number | undefined;
            nm?: string | undefined;
            amount?: number | undefined;
            createdAt?: string | undefined;
            updatedAt?: string | undefined;
            adminId?: number | undefined;
        } | undefined;
        payOrg: {
            orgPayDcTplId?: number | undefined;
            orgUserAuthId?: number | undefined;
            orgId?: number | undefined;
            orgCtrtId?: number | undefined;
            nm?: string | undefined;
            amount?: number | undefined;
            createdAt?: string | undefined;
            updatedAt?: string | undefined;
            adminId?: number | undefined;
        } | undefined;
    };
    get validate(): boolean;
    emailAuth: (emailAddr: string) => {
        emailAuth: {
            domain: string | undefined;
            emailAddr: string;
            useYn: number;
        };
        success: boolean;
        msg: string;
    } | {
        msg: string;
        success: boolean;
        emailAuth?: undefined;
    };
}

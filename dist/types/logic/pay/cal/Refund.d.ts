import { IPaid, IRefund } from "../../../db";
export declare class CalRefund {
    private _paid;
    private _refundList;
    constructor(paid: IPaid, refundList: IRefund[]);
    get paid(): IPaid;
    get refundList(): IRefund[];
    get cal(): {
        amount: number;
        statusCd: string;
    };
}

import { ICpnUse, IPayAdjust } from "../../../../db";
declare type TMshuttleCal = {
    defaultAmount: number;
    paidId: number;
    cpnUseList: ICpnUse[];
    payAdjustList: IPayAdjust[];
};
export declare class MshuttleCalTry {
    _obj: TMshuttleCal;
    constructor(obj: TMshuttleCal);
    cal: () => {
        paidCpnList: import("../../../../db").IPaidCpn[];
        cpnUseList: ICpnUse[];
        cpnAmount: number;
        paidAdjustList: import("../../../../db").IPaidAdjust[];
        payAdjustAmount: number;
        totalAmount: number;
    };
}
export {};

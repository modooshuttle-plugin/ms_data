import { IOutput, TPayOrg } from "../Cal";
declare type TMshuttleCal = {
    calObj: IOutput;
    payObj: TPayOrg;
};
export declare class MshuttleCal {
    _obj: TMshuttleCal;
    constructor(obj: TMshuttleCal);
    get cal(): IOutput;
}
export {};

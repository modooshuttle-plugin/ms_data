import { ICpnUse, IPaidCpn } from "../../../db";
export declare class CalCpnUseList {
    private _cpnUseList;
    constructor(cpnUseList: ICpnUse[], paidId: number);
    get cpnUseList(): ICpnUse[];
    get cal(): {
        paidCpnList: IPaidCpn[];
        amount: number;
    };
}

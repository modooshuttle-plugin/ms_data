import { IOutput, TPayOrg } from "../Cal";
declare type TMshuttleCal = {
    calObj: IOutput;
    payObj: TPayOrg;
};
export declare class BeforeCal {
    _obj: TMshuttleCal;
    constructor(obj: TMshuttleCal);
    firstStep: () => IOutput;
    private calA;
    private calB;
    private calC;
}
export {};

import { TCpn } from "./Cal";
export declare class CalCpnList {
    private _cpnList;
    private _defaultAmount;
    constructor(cpnList: TCpn[], defaultAmount: number);
    get cpnList(): TCpn[];
    get defaultAmount(): number;
    get cal(): {
        cpnUseList: any[];
        amount: number;
        paidCpnList: any[];
    };
}

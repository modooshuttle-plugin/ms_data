import { IOutput, TPayOrg } from "../Cal";
declare type TMshuttleCal = {
    calObj: IOutput;
    payObj: TPayOrg;
};
export declare class Cleanning {
    _obj: TMshuttleCal;
    constructor(obj: TMshuttleCal);
    get clean(): IOutput;
}
export {};

import { ICpn, ICpnConn, ICpnLimit, ICpnUse, IOrgUserAuth, IPaidDc, IPaidOrg, IPay, IPayAdjust, IPayDc, IPayOrg } from "../../../db";
export declare type TCpn = {
    cpn: ICpn;
    cpnUse?: ICpnUse;
    cpnConn?: ICpnConn;
    cpnLimit?: ICpnLimit;
};
export declare type TPayOrg = {
    orderNo?: string;
    pay: IPay;
    orgUserAuth?: IOrgUserAuth;
    payOrg?: IPayOrg;
    payDc?: IPayDc;
    cpnList: TCpn[];
    payAdjustList: IPayAdjust[];
    payTry: any;
};
export interface IOutput {
    methodCd: string | null;
    amount: {
        defaultAmount: number;
        payOrgAmount: number;
        cpnAmount: number;
        payAdjustAmount: number;
        totalAmount: number;
    };
    ratio: string;
    output: {
        payTry: any;
        paidOrg?: IPaidOrg;
        paidDc?: IPaidDc;
        cpnUseList: any[];
        paidCpnList: any[];
        paidAdjustList: any[];
    };
}
export declare class Cal {
    private _org;
    constructor(obj: TPayOrg);
    get calculate(): IOutput;
}

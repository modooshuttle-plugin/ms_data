import { IOutput, TPayOrg } from "../Cal";
declare type TMshuttleCal = {
    calObj: IOutput;
    payObj: TPayOrg;
};
export declare class AfterCal {
    _obj: TMshuttleCal;
    constructor(obj: TMshuttleCal);
    get thirdStep(): IOutput;
}
export {};

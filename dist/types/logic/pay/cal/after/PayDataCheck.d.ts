import { IPaid, IPayTry } from "../../../../db";
declare type TPayDataCheck = {
    payTry: IPayTry;
    paidTemp: IPaid;
};
export declare class PayDataCheck {
    _obj: TPayDataCheck;
    constructor(obj: TPayDataCheck);
    get isForgery(): boolean;
    get isFailed(): boolean;
    get isVbank(): boolean;
}
export {};

import { IPaidAdjust, IPayAdjust } from "../../../db";
export declare class CalPayAdjustList {
    private _payAdjustList;
    constructor(obj: IPayAdjust[]);
    get payAdjustList(): IPayAdjust[];
    get cal(): {
        paidAdjustList: IPaidAdjust[];
        amount: number;
    };
}

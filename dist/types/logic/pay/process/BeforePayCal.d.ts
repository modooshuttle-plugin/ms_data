import { ICpn, ICpnConn, IEmailAuth, IPay, IPayAdjust, IPayDc, IPayOrg } from "../../../db/auto/interface";
export declare type TBeforePayCal = {
    pay: IPay;
    payOrg: IPayOrg;
    payDcList: IPayDc[];
    payAdjustList: IPayAdjust[];
    cpnList: ICpn[];
    emailAuth: IEmailAuth;
    cpnConnList: ICpnConn[];
};
declare class BeforePayCal {
    private _payCal;
    constructor(obj: TBeforePayCal);
    get payCal(): TBeforePayCal;
}
export default BeforePayCal;

import { ICpn, IPay } from "../../db/auto/interface";
export declare const calCoupon: (defaultAmount: number, cpn: ICpn) => any;
export declare const getValidate: (cpnList: ICpn[], cpnConnect: any) => {
    dupeYn: number;
};
export declare const getOrderNo: (pay: IPay) => string;

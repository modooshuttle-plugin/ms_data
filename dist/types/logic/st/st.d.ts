import { IBoard, IPath, ISt } from "../../db";
export declare type TBoardingSt = {
    start: ISt | undefined;
    end: ISt | undefined;
};
export declare type TIsMonday = {
    start: boolean;
    end: boolean;
    rt: boolean;
};
export declare type TStLogic = {
    path: IPath;
    stList: ISt[];
    board?: IBoard;
};
export declare class StLogic {
    _data: TStLogic;
    constructor(obj: TStLogic);
    /**
     * 탑승정류장
     */
    get boardingSt(): TBoardingSt;
    /**
     * 월요일 운핸여부
     */
    get isMonday(): TIsMonday;
}

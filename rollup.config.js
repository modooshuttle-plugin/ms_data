import resolve from "@rollup/plugin-node-resolve";
import babel from "@rollup/plugin-babel";
// import typescript from '@rollup/plugin-typescript';
import typescript from "rollup-plugin-typescript2";
import image from "@rollup/plugin-image";
import pkg from "./package.json";
import commonjs from "@rollup/plugin-commonjs";
import peerDepsExternal from "rollup-plugin-peer-deps-external";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

process.env.BABEL_ENV = "production";

export default {
  input: "./src/index.ts",
  external: ["styled-components", "react", "react-dom"],
  // output: [
  //   {
  //     dir: "build",
  //     format: "esm",
  //     exports: "named",
  //     sourcemap: true,
  //   },
  // ],
  // preserveModules: true,
  plugins: [
    peerDepsExternal(),
    image(),
    // typescript({ typescript: require('typescript') }),
    typescript({ useTsconfigDeclarationDir: true }),
    resolve({ extensions }),
    commonjs({ include: "/node_modules/" }),
    babel({
      extensions,
      include: ["src/**/*"],
      exclude: "node_modules/**",
      babelHelpers: "runtime",
    }),
  ],
  output: [
    { file: pkg.module, format: "es" },
    { file: pkg.main, format: "cjs" },
  ],
};

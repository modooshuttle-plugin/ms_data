const moment = require("moment-timezone");

/**
 * moment 객체 (서울)
 * @param date
 * @returns
 */
const momentTz = (date?: string | number) => {
  return moment(date).tz("Asia/Seoul");
};

/**
 * 오늘날짜에 대한 문자열을 입력한 포맷으로 반환한다.
 * ex)
 * MMMM Do YYYY, h:mm:ss a'); // October 17th 2019, 11:54:20 am
 * 'dddd'                    // Thursday
 * "MMM Do YY"               // Oct 17th 19
 * 'YYYY [escaped] YYYY'     // 2019 escaped 2019
 * @param format string 포맷
 */

export const mFormat = (format: string) => {
  const m = momentTz();
  // m.locale('ko');
  return m.format(format);
};

/**
 * 탑승기간 계산 (1달)
 * @param day
 * @returns
 */
const makeBoardPeriod = (day: string) => {
  const m = moment(day).tz("Asia/Seoul");
  m.add(1, "day");
  const startDay = m.format("YYYY-MM-DD");
  const endDay = makeEndDay(startDay);

  return {
    startDay,
    endDay,
  };
};

const makeEndDay = (sday: string) => {
  const eday = moment(sday)
    .tz("Asia/Seoul")
    .add(1, "month")
    .add(-1, "day")
    .format();
  return eday;
};

/**
 * 오늘 기준 차이나는 일자 계산
 * @param day
 * @returns
 */
const intervalDay = (day: number) => {
  const date = moment().tz("Asia/Seoul").add(day, "days").format(`YYYY-MM-DD`);
  return date;
};

export const mmt = {
  intervalDay,
  makeBoardPeriod,
  momentTz,
  mFormat,
};

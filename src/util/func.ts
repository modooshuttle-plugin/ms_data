export const nullCheck = (value: any) => {
  let res = false;
  if (value !== undefined && value !== null) {
    if (typeof value === "string") {
      if (value !== "") {
        res = true;
      }
    } else {
      res = true;
    }
  }
  return res;
};

export const mComma = (num: number) => {
  let res: string = "0";
  try {
    res = new Intl.NumberFormat("ko-KR").format(num);
  } catch (error) {
    res = "0";
  }
  return res;
};

export const makeid = (num: number) => {
  let text = "";
  const possible =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (let i = 0; i < num; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
};

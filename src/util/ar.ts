/**
 *   active record
 *      created by KPS
 */

type Tcol = "func";

export class Ar {
  private col: string[] = [];
  private cond: any[] = [];
  private icond: any[] = [];
  private hcond: any[] = [];
  private tables: any[] = [];
  private order: string[] = [];
  private group: string[] = [];

  private table: string = "";
  private paging: string = "";

  /**
   * 컬럼 선택 목록
   * @param col
   */
  public select(col: string) {
    this.col.push(col);
  }

  /**
   * and 조건문 목록
   * @param cond
   */
  public where(cond: string) {
    this.cond.push({ cond, connect: " and " });
  }

  /**
   * or 조건문 목록
   * @param cond
   */
  public or_where(cond: string) {
    this.cond.push({ cond, connect: " or " });
  }

  /**
   *
   * @param having
   */
  public having(cond: string) {
    this.hcond.push({ cond, connect: " and " });
  }

  /**
   *  테이블 선택
   * @param table
   */
  public from(table: string) {
    this.table = table;
  }

  /**
   *  join 하는 테이블 목록
   * @param table
   */
  public join(table: string, value?: string) {
    this.tables.push({ table, value });
  }

  /**
   *  groupby 목록
   * @param group
   */
  public groupby(group: string) {
    this.group.push(group);
  }

  /**
   *  orderby 목록
   * @param order
   */
  public orderby(order: string) {
    this.order.push(order);
  }

  /**
   *  paging
   * @param paging
   */
  public limit(paging: string) {
    this.paging = " Limit " + paging + " ";
  }

  // // // // // // // // // // // // //

  /**
   *  select 종합
   */
  private cSelect = () => {
    let col: string = ` select `;
    if (this.col.length > 0) {
      // col = ` `;
      for (let i = 0; i < this.col.length; i++) {
        const e = this.col[i];
        col += e;
        if (i !== this.col.length - 1) {
          col += `, `;
        } else {
          col += ` `;
        }
      }
    } else {
      col += ` * `;
    }
    return col;
  };

  private cSelectCnt = () => {
    let col: string = ` select `;
    if (this.col.length > 0) {
      // col = ` `;
      for (let i = 0; i < this.col.length; i++) {
        const e = this.col[i];
        col += e;
        if (i !== this.col.length - 1) {
          col += `, `;
        } else {
          col += ` `;
        }
      }
    } else {
      col += ` count(*) as cnt `;
    }
    return col;
  };

  /**
   *  join 종합
   */
  private cJoin = () => {
    // join
    let table: string = `  `;
    if (this.tables.length > 0) {
      for (let i = 0; i < this.tables.length; i++) {
        const e = this.tables[i];
        table += ` ${e.value ? e.value : ""} join ` + e.table;
      }
    }
    return table;
  };

  private cWhere = () => {
    let cond: string = `  `;
    if (this.cond.length > 0) {
      cond += ` where `;
      for (let i = 0; i < this.cond.length; i++) {
        const e = this.cond[i];
        cond += e.cond;
        if (i !== this.cond.length - 1) {
          cond += e.connect;
        } else {
          cond += `  `;
        }
      }
      // sql += cond;
    }
    return cond;
  };

  private cInnerWhere = () => {
    let cond: string = `  `;
    if (this.icond.length > 0) {
      cond += ` where `;
      for (let i = 0; i < this.icond.length; i++) {
        const e = this.icond[i];
        cond += e.cond;
        if (i !== this.icond.length - 1) {
          cond += e.connect;
        } else {
          cond += `  `;
        }
      }
      // sql += cond;
    }
    return cond;
  };

  private cGroup = () => {
    let group: string = ` `;
    if (this.group.length > 0) {
      group += ` group by `;
      for (let i = 0; i < this.group.length; i++) {
        const e = this.group[i];
        group += e;
        if (i !== this.group.length - 1) {
          group += ` , `;
        } else {
          group += ` `;
        }
      }
      // sql += group;
    }
    return group;
  };

  private cHaving = () => {
    let cond: string = `  `;
    if (this.hcond.length > 0) {
      cond += ` having `;
      for (let i = 0; i < this.hcond.length; i++) {
        const e = this.hcond[i];
        cond += e.cond;
        if (i !== this.hcond.length - 1) {
          cond += e.connect;
        } else {
          cond += `  `;
        }
      }
      // sql += cond;
    }
    return cond;
  };

  private cOrder = () => {
    let order: string = ``;
    if (this.order.length > 0) {
      order = ` order by `;
      for (let i = 0; i < this.order.length; i++) {
        const e = this.order[i];
        order += e;
        if (i !== this.order.length - 1) {
          order += `, `;
        } else {
          order += ` `;
        }
      }
    }
    return order;
  };

  // select 문
  public getSql = () => {
    let sql = "";
    const from: string = this.table;
    // select
    sql += this.cSelect();
    //  from
    sql += ` from ` + from;

    // join
    sql += this.cJoin();

    // inner where
    sql += this.cInnerWhere();
    // if (type) {
    //   if (type === true) {
    //     sql = `select * from (${sql}) ${uniqid(`inner`)} `;
    //   }
    // }
    // where
    sql += this.cWhere();

    // group
    sql += this.cGroup();

    sql += this.cHaving();
    // order
    sql += this.cOrder();

    sql += this.paging;
    // sql += `;`;

    return sql;
  };

  public getRowSql() {
    this.limit(`0, 1`);
    const sql = this.getSql();
    return sql;
  }

  public getCountSql() {
    this.limit(`0, 1`);
    let sql = "";
    const from: string = this.table;
    sql += this.cSelectCnt();
    //  from
    sql += " from " + from;
    sql += this.cJoin();

    // inner where
    sql += this.cInnerWhere();
    sql += this.cWhere();
    sql += this.cGroup();
    sql += this.cHaving();
    sql += this.paging;
    return sql;
  }

  public input: any = [];

  public set(key: string, value: any, type?: Tcol) {
    console.log(key, value);
    console.log({ [key]: { value, type } });
    this.input.push({ [key]: { value, type } });
  }

  public setCnt() {
    return this.input.length;
  }

  public setObject(obj: any) {
    for (let i = 0; i < Object.keys(obj).length; i++) {
      const key = Object.keys(obj)[i];
      this.set(key, obj[key]);
    }
  }

  public insertSql() {
    let sql = "insert into ";
    const from: string = this.table;
    sql += from + " ";

    let cols = "(";
    let values = " values (";
    if (this.input.length > 0) {
      for (let i = 0; i < this.input.length; i++) {
        const key: any = Object.keys(this.input[i]);
        cols += key;

        values += this.parseValue(this.input[i][key]);

        if (i !== this.input.length - 1) {
          cols += ", ";
          values += ", ";
        } else {
          cols += " )";
          values += ")";
        }
      }
    }
    // console.log(cols);
    const q = sql + cols + values + ";";
    // console.log(q);
    return q;
  }

  public insertSelectSql() {
    let sql = "insert into ";
    const from: string = this.table;
    sql += from + " ";

    let cols = "(";
    let values = " select ";
    if (this.input.length > 0) {
      for (let i = 0; i < this.input.length; i++) {
        const key: any = Object.keys(this.input[i]);
        cols += key;
        values += this.parseValue(this.input[i][key]);

        if (i !== this.input.length - 1) {
          cols += ", ";
          values += ", ";
        } else {
          cols += " )";
          values += " ";
        }
      }
      values += " from dual ";
      values += this.cWhere();
    }
    // console.log(cols);
    const q = sql + cols + values + ";";
    // console.log(q);
    return q;
  }

  private parseValue = (item: { value: any; type?: string }) => {
    const { value, type } = item;

    console.log(value);
    console.log(type);

    let txt = "";
    if (typeof value === "string") {
      // 따옴표, 쌍따옴표 처리

      if (type !== undefined) {
        txt += " " + item.value + " ";
      } else {
        console.log(item.value);
        item.value = item.value.replace(/"/g, `\\"`);
        item.value = item.value.replace(/'/g, `\\'`);
        if (item.value === "?") {
          txt += " " + item.value + " ";
        } else {
          if (item.value.indexOf(`"`) > -1) {
            txt += `'` + item.value + `'`;
          } else if (item.value.indexOf(`'`) > -1) {
            txt += `"` + item.value + `"`;
          } else {
            txt += `'` + item.value + `'`;
          }
        }
      }
    } else {
      txt += " " + item.value + " ";
    }
    return txt;
  };

  public deleteSql() {
    let sql = "delete from ";
    const from: string = this.table;
    let cond: string = " where ";
    sql += from;

    if (this.cond.length > 0) {
      for (let i = 0; i < this.cond.length; i++) {
        const e = this.cond[i];
        cond += e.cond;
        if (i !== this.cond.length - 1) {
          cond += e.connect;
        } else {
          cond += " ";
        }
      }
      sql += cond;
    }
    return sql;
  }

  public updateSql() {
    let sql = "update ";
    let from: string = this.table + " ";

    from += this.cJoin();

    let cond: string = " where ";
    sql += from;

    let cols = " set ";

    if (this.input.length > 0) {
      for (let i = 0; i < this.input.length; i++) {
        cols += Object.keys(this.input[i]);
        const v: any = Object.keys(this.input[i]);

        let value = this.input[i][v].value;
        if (typeof value === "string") {
          let type = this.input[i][v].type;
          if (type !== undefined) {
            cols += `= ` + value + ` `;
          } else {
            value = value.replace(/"/g, `\\"`);
            value = value.replace(/'/g, `\\'`);

            if (value === "?") {
              cols += "=" + value + " ";
            } else {
              if (value.indexOf(`"`) > -1) {
                cols += `='` + value + `'`;
              } else if (value.indexOf(`'`) > -1) {
                cols += `="` + value + `"`;
              } else {
                cols += `='` + value + `'`;
              }
            }
          }
        } else {
          if (value !== undefined) {
            cols += "= " + value + " ";
          }
        }

        if (i !== this.input.length - 1) {
          cols += ", ";
        }
      }
    }

    sql += cols;

    if (this.cond.length > 0) {
      for (let i = 0; i < this.cond.length; i++) {
        const e = this.cond[i];
        cond += e.cond;
        if (i !== this.cond.length - 1) {
          cond += e.connect;
        } else {
          cond += " ";
        }
      }
      sql += cond;
    }
    sql += ";";
    console.log(sql);
    return sql;
  }

  public getResToJson(result: any[]) {
    const list = [];
    for (let i = 0; i < result.length; i++) {
      const el = result[i];

      const keys = Object.keys(el);
      const obj: any = {};

      keys.map((c, k) => {
        const spel = c.split(".");

        if (spel.length > 1) {
          if (obj[spel[0]] === undefined) {
            obj[spel[0]] = {
              [spel[1]]: el[c],
            };
          } else {
            obj[spel[0]][spel[1]] = el[c];
          }
        } else {
          obj[c] = el[c];
        }
      });
      list.push(obj);
    }

    return list;
  }
}

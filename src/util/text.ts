import { nullCheck } from "./func";

export const toCamelCase = (col: string) => {
  const c = col.split("_");
  let column = "";
  c.map((str: string, i: number) => {
    if (i === 0) {
      column += str.toLowerCase();
    } else {
      column +=
        str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }
  });

  // console.log(column);
  return column;
};

export const objToCamelCase = (obj: any) => {
  let mobj: any = null;
  if (nullCheck(obj) === true) {
    mobj = {};
    Object.keys(obj).map((key, i) => {
      if (typeof obj[key] === "object") {
        if (Array.isArray(obj[key])) {
          mobj[toCamelCase(key)] = obj[key];
        } else {
          mobj[toCamelCase(key)] = objToCamelCase(obj[key]);
        }
      } else {
        mobj[toCamelCase(key)] = obj[key];
      }
    });
  }

  return mobj;
};

export const searchText = (text: string) => {
  const deny_char = /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|\*]+$/;
  let search = "";
  for (let i = 0; i < text.length; i++) {
    if (deny_char.test(text[i]) === true) {
      search += text[i];
    }
  }

  return search;
};

export const snakeToCamel = (text: string) => {
  return text.replace(/([-_][a-z])/gi, ($1) => {
    return $1.toUpperCase().replace("-", "").replace("_", "");
  });
};

export const objSnakeToCamel = (obj: any) => {
  let new_obj: any = {};
  Object.keys(obj).map((key: string) => {
    new_obj[snakeToCamel(key)] = obj[key];
  });
  return new_obj;
};

export const camelToSnake = (s: any) => {
  return s
    .split(/(?=[A-Z])/)
    .join("_")
    .toLowerCase();
};

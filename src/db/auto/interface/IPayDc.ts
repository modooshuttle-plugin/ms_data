export interface IPayDc {

    /**
     * 결제 할인 ID
     */
	payDcId: number;
 
    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 기업 유저 인증 ID
     */
	orgUserAuthId: number;
 
    /**
     * 이메일 인증 ID
     */
	emailAuthId: number;
 
    /**
     * 이름
     */
	nm: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
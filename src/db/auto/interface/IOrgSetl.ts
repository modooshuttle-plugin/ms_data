export interface IOrgSetl {

    /**
     * 기업 정산 ID
     */
	orgSetlId: number;
 
    /**
     * 기업 계약 ID
     */
	orgCtrtId: number;
 
    /**
     * 기업 계약 상세 ID
     */
	orgCtrtDtlId: number;
 
    /**
     * 기업 ID
     */
	orgId: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 정산월
     */
	setlMonth: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 세금계산서 발행일
     */
	taxIssDay: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
export interface ILogDr {
	logDrId: number;
 	evtId: string;
 	drId: string;
 	nm: string;
 	phone: string;
 	phoneAccess: number;
 	statusCd: string;
 	drCd: string;
 	ctrtYn: number;
 	createdAt: string;
 
}
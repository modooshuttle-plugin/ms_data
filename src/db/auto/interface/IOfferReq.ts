export interface IOfferReq {
	offerReqId: number;
 	taskId: number;
 	rtId: string;
 	fromCd: string;
 	toCd: string;
 	fromId: string;
 	toId: string;
 	reqCd: string;
 	createdAt: string;
 	updatedAt: string;
 	comment: string;
 	adminId: number;
 	offerDrId: number;
 
}
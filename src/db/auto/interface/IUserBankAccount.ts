export interface IUserBankAccount {

    /**
     * 유저 은행 계좌 ID
     */
	userBankAccountId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 은행 구분
     */
	bankCd: string;
 
    /**
     * 계좌번호
     */
	accountNo: string;
 
    /**
     * 이름
     */
	nm: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경리시
     */
	updatedAt: string;
 
}
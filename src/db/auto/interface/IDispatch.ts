export interface IDispatch {

    /**
     * 배차 ID
     */
	dispatchId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 시작일시
     */
	startDay: string;
 
    /**
     * 변경일시
     */
	endDay: string;
 
    /**
     * 배차 구분
     */
	dispatchCd: string;
 
    /**
     * 배포 여부
     */
	deployYn: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
export interface IPromoAmount {

    /**
     * 프로모션 금액 ID
     */
	promoAmountId: number;
 
    /**
     * 프로모션 코드 ID
     */
	promoCdId: number;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * idx
     */
	idx: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
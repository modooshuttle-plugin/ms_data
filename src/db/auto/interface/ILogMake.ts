export interface ILogMake {
	logMakeId: number;
 	evtId: string;
 	makeId: string;
 	userId: string;
 	startAddr: string;
 	startLat: string;
 	startLng: string;
 	endAddr: string;
 	endLat: string;
 	endLng: string;
 	timeId: string;
 	startTitle: string;
 	endTitle: string;
 	comment: string;
 	email: string;
 	orgId: string;
 	startCatCd: string;
 	endCatCd: string;
 	createdAt: string;
 
}
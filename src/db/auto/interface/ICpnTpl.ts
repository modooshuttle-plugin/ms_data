export interface ICpnTpl {

    /**
     * 쿠폰 탬플릿 ID
     */
	cpnTplId: number;
 
    /**
     * 이름
     */
	nm: string;
 
    /**
     * 계산 구분
     */
	calCd: number;
 
    /**
     * 함수
     */
	func: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 부호
     */
	signCd: number;
 
    /**
     * 관리자 업데이트 구분
     */
	adminUpdateCd: number;
 
    /**
     * 중복 여부
     */
	dupeYn: number;
 
}
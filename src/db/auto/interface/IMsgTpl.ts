export interface IMsgTpl {

    /**
     * 메세지 탬플릿 ID
     */
	msgTplId: number;
 
    /**
     * 경로 구분
     */
	rtCd: string;
 
    /**
     * 제목
     */
	title: string;
 
    /**
     * 부제목
     */
	subject: string;
 
    /**
     * 내용
     */
	content: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 알림톡 ID
     */
	ataId: string;
 
    /**
     * 알림톡 탬플릿 ID
     */
	ataTplId: string;
 
    /**
     * utm_source
     */
	utmSource: string;
 
    /**
     * utm_medium
     */
	utmMedium: string;
 
    /**
     * utm_campaign
     */
	utmCampaign: string;
 
    /**
     * use_yn
     */
	useYn: number;
 
    /**
     * kakao_opt
     */
	kakaoOpt: any;
 
}
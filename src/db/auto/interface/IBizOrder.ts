export interface IBizOrder {

    /**
     * 기업 주문 Id
     */
	bizOrderId: number;
 
    /**
     * 노션 링크
     */
	notion: string;
 
    /**
     * 등록일시
     */
	createdAt: string;
 
    /**
     * 신청자
     */
	apply: string;
 
    /**
     * 전화번호
     */
	phone: string;
 
    /**
     * 이메일
     */
	email: string;
 
    /**
     * 구분
     */
	orderCd: string;
 
    /**
     * 경로 구분
     */
	pathCd: string;
 
    /**
     * 경로 메모
     */
	pathComment: string;
 	methodCd: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
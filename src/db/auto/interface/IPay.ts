export interface IPay {

    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 상품 ID
     */
	prodsId: string;
 
    /**
     * 결제 시도 ID
     */
	payTryId: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 결제 상태 구분
     */
	statusCd: string;
 
}
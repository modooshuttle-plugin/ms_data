export interface IBoard {

    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 상품 ID
     */
	prodsId: string;
 
    /**
     * 시작 정류장 CD
     */
	startStCd: string;
 
    /**
     * 도착 정류장 CD
     */
	endStCd: string;
 
    /**
     * 시작 정류장 VER
     */
	startStVer: number;
 
    /**
     * 도착 정류장 VER
     */
	endStVer: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 탑승 구분
     */
	boardCd: string;
 
    /**
     * 탑승 형태 구분
     */
	boardShapeCd: number;
 
    /**
     * 좌석 ID
     */
	seatId: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 신청 ID
     */
	applyId: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 경로 구분
     */
	rtCd: string;
 
    /**
     * 신청 타겟 구분
     */
	targetCd: string;
 
}
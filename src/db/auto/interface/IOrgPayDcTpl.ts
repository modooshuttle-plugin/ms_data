export interface IOrgPayDcTpl {

    /**
     * 기업 결제 할인 탬플릿 ID
     */
	orgPayDcTplId: number;
 
    /**
     * 기업 유저 인증 ID
     */
	orgUserAuthId: number;
 
    /**
     * 기업 ID
     */
	orgId: number;
 
    /**
     * 기업 계약 ID
     */
	orgCtrtId: number;
 
    /**
     * 제목
     */
	nm: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
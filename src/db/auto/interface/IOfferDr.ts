export interface IOfferDr {

    /**
     * 기사 계약금 제안 Id
     */
	offerDrId: number;
 
    /**
     * 업무  Id
     */
	taskId: number;
 
    /**
     * 유류비
     */
	oilAmount: number;
 
    /**
     * 톨게이트 비
     */
	tollgateAmount: number;
 
    /**
     * 운행일수
     */
	runnDayCnt: number;
 
    /**
     * 주말운행일수
     */
	weekdayCnt: number;
 
    /**
     * 반복운행횟수
     */
	repeatRunnCnt: number;
 
    /**
     * 운행거리
     */
	runnDist: number;
 
    /**
     * 운행시간 (분)
     */
	runnTime: number;
 
    /**
     * 시간 Id
     */
	timeId: string;
 
    /**
     * 연할부이율(%)
     */
	interestRatio: number;
 
    /**
     * 경로 Id
     */
	rtId: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 메모
     */
	comment: string;
 
    /**
     * 관리자 Id
     */
	adminId: string;
 
}
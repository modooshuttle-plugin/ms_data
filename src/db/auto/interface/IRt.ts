export interface IRt {

    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 시작정보
     */
	startInfo: string;
 
    /**
     * 도착정보
     */
	endInfo: string;
 
    /**
     * 시작태그
     */
	startTag: string;
 
    /**
     * 도착태그
     */
	endTag: string;
 
    /**
     * 시작 카테고리 고드
     */
	startCatCd: string;
 
    /**
     * 도착 카테고리 코드
     */
	endCatCd: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 시간 ID
     */
	timeId: string;
 
    /**
     * 경로 구분
     */
	rtCd: string;
 
    /**
     * 경로 상태 구분
     */
	rtStatusCd: string;
 
    /**
     * 사업 구분
     */
	bizCd: number;
 
    /**
     * 출퇴근 구분
     */
	commuteCd: number;
 
    /**
     * 활성 구분
     */
	activateCd: number;
 
    /**
     * 좌석 구분
     */
	seatCd: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 최소 모집 인원
     */
	minUserCnt: number;
 
    /**
     * 폐지일
     */
	closeDay: string;
 
    /**
     * 운행에정 url
     */
	schdUrl: string;
 
    /**
     * 운행중 url
     */
	runnUrl: string;
 
    /**
     * 검색 구분
     */
	searchCd: number;
 
}
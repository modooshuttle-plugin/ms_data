export interface ICpnLimit {

    /**
     * 쿠폰제한 ID
     */
	cpnLimitId: number;
 
    /**
     * 쿠폰 ID
     */
	cpnId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 제한 구분
     */
	limitCd: string;
 
    /**
     * 제한 구분에 따른 타켓 ID
     */
	targetId: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
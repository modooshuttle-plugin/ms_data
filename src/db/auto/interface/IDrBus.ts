export interface IDrBus {

    /**
     * 기사-버스 ID
     */
	drBusId: number;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 버스 ID
     */
	busId: string;
 
    /**
     * 연식
     */
	year: string;
 
    /**
     * 차량정보
     */
	info: string;
 
    /**
     * 차량번호
     */
	busNo: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
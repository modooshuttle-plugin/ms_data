export interface IMsgEvt {

    /**
     * 메세지이벤트 ID
     */
	msgEvtId: number;
 
    /**
     * 제목
     */
	title: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 메세지 이벤트 구분
     */
	msgEvtCd: string;
 
    /**
     * 배포 구분
     */
	deployCd: number;
 
    /**
     * 타겟
     */
	target: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
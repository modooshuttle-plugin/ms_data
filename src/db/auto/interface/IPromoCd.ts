export interface IPromoCd {

    /**
     * 프로모션 코드 ID
     */
	promoCdId: number;
 
    /**
     * 프로모션 코드
     */
	promoCd: string;
 
    /**
     * 기업 ID
     */
	orgId: string;
 
    /**
     * 시작일시
     */
	startDay: string;
 
    /**
     * 종료일시
     */
	endDay: string;
 
    /**
     * 제목
     */
	title: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 사용 시작일시
     */
	useStartDay: string;
 
    /**
     * 사용 종료일시
     */
	useEndDay: string;
 
    /**
     * 쿠폰 탬플릿 ID
     */
	cpnTplId: number;
 
    /**
     * 상태 구분
     */
	promoStatusCd: number;
 
    /**
     * 계약 ID
     */
	orgCtrtId: number;
 
    /**
     * 계약상세 ID
     */
	orgCtrtDtlId: number;
 
}
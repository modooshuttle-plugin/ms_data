export interface IBus {

    /**
     * 버스 ID
     */
	busId: number;
 
    /**
     * 좌석배치도
     */
	seat: any;
 
    /**
     * 이름
     */
	nm: string;
 
    /**
     * 좌석수
     */
	seatCnt: number;
 
    /**
     * 출력 좌석수
     */
	viewSeatCnt: number;
 
    /**
     * 설명
     */
	comment: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
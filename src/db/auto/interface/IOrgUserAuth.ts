export interface IOrgUserAuth {

    /**
     * 기업 유저 인증 ID
     */
	orgUserAuthId: number;
 
    /**
     * 기업 ID
     */
	orgId: number;
 
    /**
     * 기업 계약 ID
     */
	orgCtrtId: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 방법 구분
     */
	methodCd: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
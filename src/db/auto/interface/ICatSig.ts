export interface ICatSig {

    /**
     * 카테고리 시군구 ID
     */
	catSigId: number;
 
    /**
     * 카테고리 ID
     */
	catId: string;
 
    /**
     * 시군구 CD
     */
	sigCd: string;
 
    /**
     * 별칭
     */
	alias: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
export interface IRtTask {

    /**
     * 경로 업무 ID
     */
	rtTaskId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 제목
     */
	title: string;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 업무 구분
     */
	taskCd: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
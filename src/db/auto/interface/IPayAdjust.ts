export interface IPayAdjust {

    /**
     * 결제 조정금액 ID
     */
	payAdjustId: number;
 
    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: number;
 
    /**
     * 연장 구분
     */
	extendCd: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 주문번호
     */
	orderNo: string;
 
    /**
     * 이름
     */
	nm: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: string;
 
}
export interface IDrSetl {

    /**
     * 기사정산 ID
     */
	drSetlId: number;
 
    /**
     * 기사계약금 ID
     */
	drPayId: number;
 
    /**
     * 비즈니스 구분
     */
	bizCd: number;
 
    /**
     * 운행일 수
     */
	runnDayCnt: number;
 
    /**
     * 미운행일 수
     */
	notRunnDayCnt: number;
 
    /**
     * 예외일 수
     */
	exceptDayCnt: number;
 
    /**
     * 기본금액
     */
	defaultAmount: number;
 
    /**
     * 계약금액
     */
	payAmount: number;
 
    /**
     * 미운행 금액
     */
	notRunnAmount: number;
 
    /**
     * 추가 금액
     */
	addAmount: number;
 
    /**
     * 이슈 추가액
     */
	issAddAmount: number;
 
    /**
     * 이슈 제외액
     */
	issRemoveAmount: number;
 
    /**
     * 공급가액
     */
	supplyAmount: number;
 
    /**
     * 수쇼료
     */
	vatAmount: number;
 
    /**
     * 기타 금액
     */
	etcAmount: number;
 
    /**
     * 추천 금액
     */
	refAmount: number;
 
    /**
     * 총운행비
     */
	totalRunnAmount: number;
 
    /**
     * 수수료 구분
     */
	vatCd: string;
 
    /**
     * 정산 구분
     */
	setlCd: string;
 
    /**
     * 경로ID
     */
	rtId: string;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 버스업체 ID
     */
	busCompnId: number;
 
    /**
     * 배차 ID
     */
	dispatchId: number;
 
    /**
     * 정산월
     */
	setlMonth: string;
 
    /**
     * 정산시작일
     */
	startDay: string;
 
    /**
     * 정산종료일
     */
	endDay: string;
 
    /**
     * 이체여부
     */
	transferYn: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 추천인 ID
     */
	drRefId: number;
 
    /**
     * 코멘트
     */
	comment: string;
 
}
export interface IScontr {
	scontrId: number;
 	info: string;
 	platform: string;
 	ver: string;
 	modal: string;
 	etc: string;
 	start: string;
 	end: string;
 
}
export interface IConfirmDrReq {

    /**
     * 기사 확인 요청 Id
     */
	confirmDrReqId: number;
 
    /**
     * 경로 Id
     */
	rtId: string;
 
    /**
     * 발송 구분
     */
	fromCd: string;
 
    /**
     * 수신 구분
     */
	toCd: string;
 
    /**
     * 발신 id
     */
	fromId: string;
 
    /**
     * 수신 id
     */
	toId: string;
 
    /**
     * 요청 구분
     */
	reqCd: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 메모
     */
	comment: string;
 
    /**
     * 관리자 Id
     */
	adminId: number;
 
}
export interface IPaidCpn {

    /**
     * 결제된 쿠폰 ID
     */
	paidCpnId: number;
 
    /**
     * 쿠폰 탬플릿 ID
     */
	cpnTplId: number;
 
    /**
     * 결제 완료 ID
     */
	paidId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 쿠폰 ID
     */
	cpnId: number;
 
    /**
     * 쿠폰 사용 ID
     */
	cpnUseId: number;
 
    /**
     * 주문번호
     */
	orderNo: string;
 
    /**
     * 사용 가능 여부
     */
	useYn: number;
 
    /**
     * 적용 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
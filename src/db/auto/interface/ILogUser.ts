export interface ILogUser {
	logUserId: number;
 	evtId: string;
 	userId: string;
 	phone: string;
 	nm: string;
 	email: string;
 	orgId: string;
 	createdAt: string;
 	userCd: string;
 	activeCd: string;
 
}
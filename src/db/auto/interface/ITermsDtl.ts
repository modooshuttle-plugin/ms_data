export interface ITermsDtl {

    /**
     * 정책 상세 ID
     */
	termsDtlId: number;
 
    /**
     * 정책 ID
     */
	termsId: string;
 
    /**
     * 배포 구분
     */
	deployYn: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
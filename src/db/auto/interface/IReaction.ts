export interface IReaction {

    /**
     * 리액션 ID
     */
	reactionId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 탑승 ID
     */
	boardId: string;
 
    /**
     * 리액션 구분
     */
	reactionCd: string;
 
    /**
     * 경로 구분
     */
	rtId: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
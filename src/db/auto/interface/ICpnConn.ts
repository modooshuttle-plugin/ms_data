export interface ICpnConn {

    /**
     * 쿠폰 연결 ID
     */
	cpnConnId: number;
 
    /**
     * 쿠폰 ID
     */
	cpnId: number;
 
    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
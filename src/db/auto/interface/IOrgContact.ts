export interface IOrgContact {

    /**
     * 기업 연락처 ID
     */
	orgContactId: number;
 
    /**
     * 기업 ID
     */
	orgId: number;
 
    /**
     * 기업 계약 ID
     */
	orgCtrtId: number;
 
    /**
     * 이름
     */
	contactNm: string;
 
    /**
     * 직급, 직책
     */
	contactManager: string;
 
    /**
     * 전화
     */
	contactPhone: string;
 
    /**
     * 이메일
     */
	contactEmail: string;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 사용 구분
     */
	onCd: number;
 
}
export interface ILogRt {
	logRtId: number;
 	evtId: string;
 	rtId: string;
 	startInfo: string;
 	endInfo: string;
 	startTag: string;
 	endTag: string;
 	startCatCd: string;
 	endCatCd: string;
 	timeId: string;
 	rtCd: string;
 	rtStatusCd: string;
 	bizCd: string;
 	commuteCd: string;
 	activateCd: number;
 	seatCd: string;
 	startDay: string;
 	endDay: string;
 	closeDay: string;
 	minUserCnt: number;
 	createdAt: string;
 	searchCd: number;
 
}
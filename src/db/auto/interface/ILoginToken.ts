export interface ILoginToken {

    /**
     * 로그인 토큰 ID
     */
	loginTokenId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 로그인 제공자 구분
     */
	providerCd: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * ip주소
     */
	ipAddr: string;
 
    /**
     * url
     */
	url: string;
 
    /**
     * 디바이스
     */
	device: string;
 
    /**
     * OS
     */
	os: string;
 
    /**
     * 브라우저
     */
	browser: string;
 
    /**
     * 리다이렉트 주소
     */
	redirect: string;
 
    /**
     * 토큰
     */
	token: string;
 
    /**
     * uuid
     */
	uuid: string;
 
    /**
     * 암호화 secret
     */
	secret: string;
 
    /**
     * 만료일
     */
	expire: number;
 
}
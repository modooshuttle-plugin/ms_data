export interface IMemo {

    /**
     * 메모 ID
     */
	memoId: number;
 
    /**
     * 메모 구분 코드
     */
	memoCd: string;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 경로 코드
     */
	rtCd: string;
 
    /**
     * 타겟 ID
     */
	userId: string;
 
    /**
     * 메모 내용
     */
	comment: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
    /**
     * 생성한 관리자 no (admin의 ad_no)
     */
	adminId: number;
 
}
export interface IMake {

    /**
     * 유저제작 ID
     */
	makeId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 출발지 주소
     */
	startAddr: string;
 
    /**
     * 출발지 위도
     */
	startLat: number;
 
    /**
     * 출발지 경도
     */
	startLng: number;
 
    /**
     * 도착지 주소
     */
	endAddr: string;
 
    /**
     * 도착지 위도
     */
	endLat: number;
 
    /**
     * 도착지 경도
     */
	endLng: number;
 
    /**
     * 시간 ID
     */
	timeId: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 출발지 제목
     */
	startTitle: string;
 
    /**
     * 도착지 제목
     */
	endTitle: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 이메일
     */
	email: string;
 
    /**
     * 회사 ID
     */
	orgId: string;
 
    /**
     * 출발지 카테고리 코드
     */
	startCatCd: string;
 
    /**
     * 도착지 카테고리 코드
     */
	endCatCd: string;
 
    /**
     * ????
     */
	confirmYn: string;
 
    /**
     * 출발지 읍면동 코드
     */
	startEmdCd: string;
 
    /**
     * 도착지 읍면동 코드
     */
	endEmdCd: string;
 
}
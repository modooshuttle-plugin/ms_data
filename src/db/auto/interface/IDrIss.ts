export interface IDrIss {

    /**
     * 기사 이슈 ID
     */
	drIssId: number;
 
    /**
     * 운행 이슈 ID
     */
	runnIssId: number;
 
    /**
     * 배차 ID
     */
	dispatchId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 관리자 체크 여부
     */
	adminCheckCd: string;
 
    /**
     * 이슈시작일
     */
	startDay: string;
 
    /**
     * 이슈종료일
     */
	endDay: string;
 
    /**
     * 정산월
     */
	setlMonth: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 이슈 구분
     */
	issCd: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
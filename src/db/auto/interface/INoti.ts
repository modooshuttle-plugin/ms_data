export interface INoti {

    /**
     * 공지사항 ID
     */
	notiId: number;
 
    /**
     * 제목
     */
	title: string;
 
    /**
     * 내용
     */
	content: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
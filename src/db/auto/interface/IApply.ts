export interface IApply {

    /**
     * 신청 ID
     */
	applyId: number;
 
    /**
     * 구독 ID
     */
	subscribeId: string;
 
    /**
     * 상품 구분
     */
	prodsCd: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 초기 시작일
     */
	initStartDay: string;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 종료일
     */
	endDay: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 신청 구분
     */
	applyCd: string;
 
    /**
     * 이전 신청 ID
     */
	prevApplyId: number;
 
}
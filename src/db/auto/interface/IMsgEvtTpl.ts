export interface IMsgEvtTpl {

    /**
     * 메세지 이벤트 탬플릿 ID
     */
	msgEvtTplId: number;
 
    /**
     * 메세지 이벤트 ID
     */
	msgEvtId: number;
 
    /**
     * 시작일
     */
	startDay: string;
 
    /**
     * 메세지 탬플릿 ID
     */
	msgTplId: number;
 
    /**
     * 메세지 구분
     */
	msgCd: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
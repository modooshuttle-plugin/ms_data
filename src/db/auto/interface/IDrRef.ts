export interface IDrRef {

    /**
     * 기사 추천 ID
     */
	drRefId: number;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 정산월
     */
	setlMonth: string;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 추천 기사 ID
     */
	refDrId: string;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
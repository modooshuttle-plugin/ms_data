export interface IRtMember {

    /**
     * 경로 유저 ID
     */
	rtMemberId: number;
 
    /**
     * 경로 업무 ID
     */
	rtTaskId: number;
 
    /**
     * 경로 업무 요청 ID
     */
	rtTaskReqId: number;
 
    /**
     * 이전 탑승 ID
     */
	preBoardId: number;
 
    /**
     * 이전 경로 ID
     */
	preRtId: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 이전 경로 구분
     */
	preRtCd: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 업무 Id
     */
	taskId: number;
 
}
export interface IReason {

    /**
     * 취소중단 사유 ID
     */
	reasonId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 탑승 구분
     */
	boardCd: string;
 
    /**
     * 취소중단 사유 구분
     */
	reasonCd: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
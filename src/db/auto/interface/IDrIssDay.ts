export interface IDrIssDay {

    /**
     * 기사 이슈 일자 ID
     */
	drIssDayId: number;
 
    /**
     * 기사 이슈 ID
     */
	drIssId: number;
 
    /**
     * 이슈 일자
     */
	day: number;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
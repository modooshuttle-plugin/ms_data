export interface IRunnCald {

    /**
     * 운행일정  Id
     */
	runnCaldId: number;
 
    /**
     * 경로 Id
     */
	rtId: string;
 
    /**
     * 운행구분
     */
	runnCd: string;
 
    /**
     * 시간 구분
     */
	timeCd: string;
 
    /**
     * 운행일자
     */
	day: string;
 
    /**
     * 메모
     */
	comment: string;
 
    /**
     * 등록일시
     */
	createdAt: string;
 
    /**
     * 수정일시
     */
	updatedAt: string;
 
    /**
     * 관리자 Id
     */
	adminId: number;
 
}
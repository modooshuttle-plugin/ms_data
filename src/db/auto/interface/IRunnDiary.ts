export interface IRunnDiary {

    /**
     * 운행일지 ID
     */
	runnDiaryId: number;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 연
     */
	year: number;
 
    /**
     * 월
     */
	month: number;
 
    /**
     * 일
     */
	day: number;
 
    /**
     * 시작시간
     */
	startedAt: string;
 
    /**
     * 도착시간
     */
	endedAt: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 운행구분
     */
	runnCd: number;
 
}
export interface IDrTermsAgree {

    /**
     * 기사 약관동의 ID
     */
	drTermsAgreeId: number;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 동의 여부
     */
	agreeYn: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
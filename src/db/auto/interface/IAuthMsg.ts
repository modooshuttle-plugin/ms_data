export interface IAuthMsg {

    /**
     * 인증 메세지 ID
     */
	authMsgId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 전화번호
     */
	phone: string;
 
    /**
     * 인증번호
     */
	authNo: string;
 
    /**
     * 인증구분
     */
	authCd: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 결과
     */
	resultCd: string;
 
    /**
     * 발송 ID
     */
	sendId: string;
 
}
export interface ICpn {

    /**
     * 쿠폰 ID
     */
	cpnId: number;
 
    /**
     * 탬플릿 ID
     */
	cpnTplId: number;
 
    /**
     * 쿠폰명
     */
	nm: string;
 
    /**
     * 수식 코드
     */
	calCd: number;
 
    /**
     * 사용 코드
     */
	statusCd: number;
 
    /**
     * 수식
     */
	func: string;
 
    /**
     * 값
     */
	amount: number;
 
    /**
     * 부호
     */
	signCd: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 프로모션 코드
     */
	promoCd: string;
 
    /**
     * 쿠폰 적용 시작일
     */
	startDay: string;
 
    /**
     * 쿠폰 적용 종료일
     */
	endDay: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
    /**
     * 중복 허용 여부
     */
	dupeYn: number;
 
    /**
     * 최소 사용 가능 기본금액
     */
	minDefaultAmount: number;
 
    /**
     * 사용 가능 여부
     */
	useCd: number;
 
}
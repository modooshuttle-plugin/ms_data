export interface IRtPath {

    /**
     * 경로 geom ID
     */
	rtPathId: number;
 
    /**
     * 경로 Id
     */
	rtId: string;
 
    /**
     * ver
     */
	pathVer: string;
 
    /**
     * 구간 구분
     */
	sectionCd: string;
 
    /**
     * 위치정보
     */
	geom: any;
 
    /**
     * 생성일시
     */
	createdAt: any;
 
    /**
     * 변경일시
     */
	updatedAt: any;
 
}
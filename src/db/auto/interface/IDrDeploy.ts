export interface IDrDeploy {

    /**
     * 기사배포물 ID
     */
	drDeployId: number;
 
    /**
     * 기사 ID
     */
	drId: string;
 
    /**
     * 배포물 종류
     */
	deployCd: string;
 
    /**
     * 상태구분
     */
	statusCd: string;
 
    /**
     * 생성시간
     */
	createdAt: string;
 
    /**
     * 변경시간
     */
	updatedAt: string;
 
}
export interface IOrgTransfer {

    /**
     * 기업 이체 ID
     */
	orgTransferId: number;
 
    /**
     * 상품 구분
     */
	prodsCd: number;
 
    /**
     * 기업 정산|정산 멤버십 ID
     */
	orgSetlId: number;
 
    /**
     * 기업 ID
     */
	orgId: number;
 
    /**
     * 이체 일시
     */
	transferAt: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
export interface IQuestion {

    /**
     * 문의사항 ID
     */
	questionId: number;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 코멘트
     */
	comment: string;
 
    /**
     * 빈영구분
     */
	confirmCd: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
    /**
     * 문의사항 구분
     */
	questionCd: string;
 
    /**
     * 기사 확인 구분
     */
	drConfirmCd: number;
 
}
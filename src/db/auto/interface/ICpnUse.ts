export interface ICpnUse {

    /**
     * 쿠폰 사용 ID
     */
	cpnUseId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 결제 ID
     */
	payId: number;
 
    /**
     * 쿠폰 ID
     */
	cpnId: number;
 
    /**
     * 주문번호
     */
	orderNo: string;
 
    /**
     * 사용여부
     */
	useYn: number;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
    /**
     * 관리자 ID
     */
	adminId: number;
 
}
export interface IPaidAdjust {

    /**
     * 완료된 결제 조정금액 ID
     */
	paidAdjustId: number;
 
    /**
     * 결제 조정금액 ID
     */
	payAdjustId: number;
 
    /**
     * 결제완료 ID
     */
	paidId: number;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 경로 ID
     */
	rtId: string;
 
    /**
     * 연장 구분
     */
	extendCd: string;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 주문 번호
     */
	orderNo: string;
 
    /**
     * 금액
     */
	amount: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
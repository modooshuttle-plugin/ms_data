export interface IHoldDay {

    /**
     * 유저 중단일 ID
     */
	holdDayId: number;
 
    /**
     * 홀딩 ID
     */
	holdId: number;
 
    /**
     * 유저 중단일
     */
	day: string;
 
    /**
     * 탑승 ID
     */
	boardId: number;
 
    /**
     * 유저 ID
     */
	userId: string;
 
    /**
     * 상태 구분
     */
	statusCd: number;
 
    /**
     * 생성일시
     */
	createdAt: string;
 
    /**
     * 변경일시
     */
	updatedAt: string;
 
}
export interface IOfferRes {
	offerResId: number;
 	offerReqId: number;
 	taskId: number;
 	rtId: string;
 	amount: string;
 	createdAt: string;
 	updatedAt: string;
 	comment: string;
 	adminId: string;
 	nm: string;
 	phone: string;
 	drId: string;
 	dispatchId: number;
 	drPayId: number;
 	drPayCondId: number;
 
}
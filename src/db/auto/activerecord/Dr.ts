import { Ar, nullCheck } from "../../../util";
 
  import { IDr } from "../interface";


  class DrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_view dr');

    
      if (alias === true) {
        ar.select("dr.dr_info_id as 'dr.dr_info_id'" );

      } else{
        ar.select("dr.dr_info_id");

      }
      
      if (alias === true) {
        ar.select("dr.dr_id as 'dr.dr_id'" );

      } else{
        ar.select("dr.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr.nm as 'dr.nm'" );

      } else{
        ar.select("dr.nm");

      }
      
      if (alias === true) {
        ar.select("dr.phone as 'dr.phone'" );

      } else{
        ar.select("dr.phone");

      }
      
      if (alias === true) {
        ar.select("dr.phone_access as 'dr.phone_access'" );

      } else{
        ar.select("dr.phone_access");

      }
      
      if (alias === true) {
        ar.select("dr.status_cd as 'dr.status_cd'" );

      } else{
        ar.select("dr.status_cd");

      }
      
      if (alias === true) {
        ar.select("dr.dr_cd as 'dr.dr_cd'" );

      } else{
        ar.select("dr.dr_cd");

      }
      
      if (alias === true) {
        ar.select("dr.ctrt_yn as 'dr.ctrt_yn'" );

      } else{
        ar.select("dr.ctrt_yn");

      }
      
      if (alias === true) {
        ar.select("dr.created_at as 'dr.created_at'" );

      } else{
        ar.select("dr.created_at");

      }
      
      if (alias === true) {
        ar.select("dr.updated_at as 'dr.updated_at'" );

      } else{
        ar.select("dr.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_info_id = 'dr.dr_info_id';
        if (alias !== undefined) {

          col_dr_info_id = `${alias}.dr_info_id`;

        }

        ar.select(`${col_dr_info_id} as '${col_dr_info_id}' `);

         
        let col_dr_id = 'dr.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_nm = 'dr.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_phone = 'dr.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_phone_access = 'dr.phone_access';
        if (alias !== undefined) {

          col_phone_access = `${alias}.phone_access`;

        }

        ar.select(`${col_phone_access} as '${col_phone_access}' `);

         
        let col_status_cd = 'dr.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_dr_cd = 'dr.dr_cd';
        if (alias !== undefined) {

          col_dr_cd = `${alias}.dr_cd`;

        }

        ar.select(`${col_dr_cd} as '${col_dr_cd}' `);

         
        let col_ctrt_yn = 'dr.ctrt_yn';
        if (alias !== undefined) {

          col_ctrt_yn = `${alias}.ctrt_yn`;

        }

        ar.select(`${col_ctrt_yn} as '${col_ctrt_yn}' `);

         
        let col_created_at = 'dr.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_info');
    
    if (nullCheck(form.drInfoId) === true) {
      ar.set("d_no", form.drInfoId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("d_name", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("d_phone", form.phone);
    } 
    

    if (nullCheck(form.phoneAccess) === true) {
      ar.set("d_phone_access", form.phoneAccess);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("d_type", form.statusCd);
    } 
    

    if (nullCheck(form.drCd) === true) {
      ar.set("d_auth", form.drCd);
    } 
    

    if (nullCheck(form.ctrtYn) === true) {
      ar.set("d_contract", form.ctrtYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_info');
    
    if (nullCheck(form.drInfoId) === true) {
      ar.set("d_no", form.drInfoId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("d_name", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("d_phone", form.phone);
    } 
    

    if (nullCheck(form.phoneAccess) === true) {
      ar.set("d_phone_access", form.phoneAccess);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("d_type", form.statusCd);
    } 
    

    if (nullCheck(form.drCd) === true) {
      ar.set("d_auth", form.drCd);
    } 
    

    if (nullCheck(form.ctrtYn) === true) {
      ar.set("d_contract", form.ctrtYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_view dr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drInfoId: 'd_no'
    , 

    drId: 'd_did'
    , 

    nm: 'd_name'
    , 

    phone: 'd_phone'
    , 

    phoneAccess: 'd_phone_access'
    , 

    statusCd: 'd_type'
    , 

    drCd: 'd_auth'
    , 

    ctrtYn: 'd_contract'
    , 

    createdAt: 'd_timestamp'
    , 

    updatedAt: 'd_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_info');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_view dr');
    
      ar.select("dr.dr_info_id");

    
    
      ar.select("dr.dr_id");

    
    
      ar.select("dr.nm");

    
    
      ar.select("dr.phone");

    
    
      ar.select("dr.phone_access");

    
    
      ar.select("dr.status_cd");

    
    
      ar.select("dr.dr_cd");

    
    
      ar.select("dr.ctrt_yn");

    
    
      ar.select("dr.created_at");

    
    
      ar.select("dr.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrSql =  new DrQuery()

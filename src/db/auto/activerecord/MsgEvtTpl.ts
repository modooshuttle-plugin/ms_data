import { Ar, nullCheck } from "../../../util";
 
  import { IMsgEvtTpl } from "../interface";


  class MsgEvtTplQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 msg_evt_tpl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_tpl_view msg_evt_tpl');

    
      if (alias === true) {
        ar.select("msg_evt_tpl.msg_evt_tpl_id as 'msg_evt_tpl.msg_evt_tpl_id'" );

      } else{
        ar.select("msg_evt_tpl.msg_evt_tpl_id");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.msg_evt_id as 'msg_evt_tpl.msg_evt_id'" );

      } else{
        ar.select("msg_evt_tpl.msg_evt_id");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.start_day as 'msg_evt_tpl.start_day'" );

      } else{
        ar.select("msg_evt_tpl.start_day");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.msg_tpl_id as 'msg_evt_tpl.msg_tpl_id'" );

      } else{
        ar.select("msg_evt_tpl.msg_tpl_id");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.msg_cd as 'msg_evt_tpl.msg_cd'" );

      } else{
        ar.select("msg_evt_tpl.msg_cd");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.comment as 'msg_evt_tpl.comment'" );

      } else{
        ar.select("msg_evt_tpl.comment");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.created_at as 'msg_evt_tpl.created_at'" );

      } else{
        ar.select("msg_evt_tpl.created_at");

      }
      
      if (alias === true) {
        ar.select("msg_evt_tpl.updated_at as 'msg_evt_tpl.updated_at'" );

      } else{
        ar.select("msg_evt_tpl.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_msg_evt_tpl_id = 'msg_evt_tpl.msg_evt_tpl_id';
        if (alias !== undefined) {

          col_msg_evt_tpl_id = `${alias}.msg_evt_tpl_id`;

        }

        ar.select(`${col_msg_evt_tpl_id} as '${col_msg_evt_tpl_id}' `);

         
        let col_msg_evt_id = 'msg_evt_tpl.msg_evt_id';
        if (alias !== undefined) {

          col_msg_evt_id = `${alias}.msg_evt_id`;

        }

        ar.select(`${col_msg_evt_id} as '${col_msg_evt_id}' `);

         
        let col_start_day = 'msg_evt_tpl.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_msg_tpl_id = 'msg_evt_tpl.msg_tpl_id';
        if (alias !== undefined) {

          col_msg_tpl_id = `${alias}.msg_tpl_id`;

        }

        ar.select(`${col_msg_tpl_id} as '${col_msg_tpl_id}' `);

         
        let col_msg_cd = 'msg_evt_tpl.msg_cd';
        if (alias !== undefined) {

          col_msg_cd = `${alias}.msg_cd`;

        }

        ar.select(`${col_msg_cd} as '${col_msg_cd}' `);

         
        let col_comment = 'msg_evt_tpl.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'msg_evt_tpl.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'msg_evt_tpl.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_tpl');
    
    if (nullCheck(form.msgEvtTplId) === true) {
      ar.set("msg_evt_tpl_id", form.msgEvtTplId);
    } 
    

    if (nullCheck(form.msgEvtId) === true) {
      ar.set("msg_evt_id", form.msgEvtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.msgTplId) === true) {
      ar.set("msg_tpl_id", form.msgTplId);
    } 
    

    if (nullCheck(form.msgCd) === true) {
      ar.set("msg_cd", form.msgCd);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_tpl');
    
    if (nullCheck(form.msgEvtTplId) === true) {
      ar.set("msg_evt_tpl_id", form.msgEvtTplId);
    } 
    

    if (nullCheck(form.msgEvtId) === true) {
      ar.set("msg_evt_id", form.msgEvtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.msgTplId) === true) {
      ar.set("msg_tpl_id", form.msgTplId);
    } 
    

    if (nullCheck(form.msgCd) === true) {
      ar.set("msg_cd", form.msgCd);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_tpl_view msg_evt_tpl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    msgEvtTplId: 'msg_evt_tpl_id'
    , 

    msgEvtId: 'msg_evt_id'
    , 

    startDay: 'start_day'
    , 

    msgTplId: 'msg_tpl_id'
    , 

    msgCd: 'msg_cd'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('msg_evt_tpl');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_tpl_view msg_evt_tpl');
    
      ar.select("msg_evt_tpl.msg_evt_tpl_id");

    
    
      ar.select("msg_evt_tpl.msg_evt_id");

    
    
      ar.select("msg_evt_tpl.start_day");

    
    
      ar.select("msg_evt_tpl.msg_tpl_id");

    
    
      ar.select("msg_evt_tpl.msg_cd");

    
    
      ar.select("msg_evt_tpl.comment");

    
    
      ar.select("msg_evt_tpl.created_at");

    
    
      ar.select("msg_evt_tpl.updated_at");

    
    
    return ar;
  }

  

  
}
export const MsgEvtTplSql =  new MsgEvtTplQuery()

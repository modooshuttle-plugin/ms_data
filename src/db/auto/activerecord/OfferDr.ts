import { Ar, nullCheck } from "../../../util";
 
  import { IOfferDr } from "../interface";


  class OfferDrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 offer_dr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_view offer_dr');

    
      if (alias === true) {
        ar.select("offer_dr.offer_dr_id as 'offer_dr.offer_dr_id'" );

      } else{
        ar.select("offer_dr.offer_dr_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr.task_id as 'offer_dr.task_id'" );

      } else{
        ar.select("offer_dr.task_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr.oil_amount as 'offer_dr.oil_amount'" );

      } else{
        ar.select("offer_dr.oil_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr.tollgate_amount as 'offer_dr.tollgate_amount'" );

      } else{
        ar.select("offer_dr.tollgate_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr.runn_day_cnt as 'offer_dr.runn_day_cnt'" );

      } else{
        ar.select("offer_dr.runn_day_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr.weekday_cnt as 'offer_dr.weekday_cnt'" );

      } else{
        ar.select("offer_dr.weekday_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr.repeat_runn_cnt as 'offer_dr.repeat_runn_cnt'" );

      } else{
        ar.select("offer_dr.repeat_runn_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr.runn_dist as 'offer_dr.runn_dist'" );

      } else{
        ar.select("offer_dr.runn_dist");

      }
      
      if (alias === true) {
        ar.select("offer_dr.runn_time as 'offer_dr.runn_time'" );

      } else{
        ar.select("offer_dr.runn_time");

      }
      
      if (alias === true) {
        ar.select("offer_dr.time_id as 'offer_dr.time_id'" );

      } else{
        ar.select("offer_dr.time_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr.interest_ratio as 'offer_dr.interest_ratio'" );

      } else{
        ar.select("offer_dr.interest_ratio");

      }
      
      if (alias === true) {
        ar.select("offer_dr.rt_id as 'offer_dr.rt_id'" );

      } else{
        ar.select("offer_dr.rt_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr.created_at as 'offer_dr.created_at'" );

      } else{
        ar.select("offer_dr.created_at");

      }
      
      if (alias === true) {
        ar.select("offer_dr.updated_at as 'offer_dr.updated_at'" );

      } else{
        ar.select("offer_dr.updated_at");

      }
      
      if (alias === true) {
        ar.select("offer_dr.comment as 'offer_dr.comment'" );

      } else{
        ar.select("offer_dr.comment");

      }
      
      if (alias === true) {
        ar.select("offer_dr.admin_id as 'offer_dr.admin_id'" );

      } else{
        ar.select("offer_dr.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_offer_dr_id = 'offer_dr.offer_dr_id';
        if (alias !== undefined) {

          col_offer_dr_id = `${alias}.offer_dr_id`;

        }

        ar.select(`${col_offer_dr_id} as '${col_offer_dr_id}' `);

         
        let col_task_id = 'offer_dr.task_id';
        if (alias !== undefined) {

          col_task_id = `${alias}.task_id`;

        }

        ar.select(`${col_task_id} as '${col_task_id}' `);

         
        let col_oil_amount = 'offer_dr.oil_amount';
        if (alias !== undefined) {

          col_oil_amount = `${alias}.oil_amount`;

        }

        ar.select(`${col_oil_amount} as '${col_oil_amount}' `);

         
        let col_tollgate_amount = 'offer_dr.tollgate_amount';
        if (alias !== undefined) {

          col_tollgate_amount = `${alias}.tollgate_amount`;

        }

        ar.select(`${col_tollgate_amount} as '${col_tollgate_amount}' `);

         
        let col_runn_day_cnt = 'offer_dr.runn_day_cnt';
        if (alias !== undefined) {

          col_runn_day_cnt = `${alias}.runn_day_cnt`;

        }

        ar.select(`${col_runn_day_cnt} as '${col_runn_day_cnt}' `);

         
        let col_weekday_cnt = 'offer_dr.weekday_cnt';
        if (alias !== undefined) {

          col_weekday_cnt = `${alias}.weekday_cnt`;

        }

        ar.select(`${col_weekday_cnt} as '${col_weekday_cnt}' `);

         
        let col_repeat_runn_cnt = 'offer_dr.repeat_runn_cnt';
        if (alias !== undefined) {

          col_repeat_runn_cnt = `${alias}.repeat_runn_cnt`;

        }

        ar.select(`${col_repeat_runn_cnt} as '${col_repeat_runn_cnt}' `);

         
        let col_runn_dist = 'offer_dr.runn_dist';
        if (alias !== undefined) {

          col_runn_dist = `${alias}.runn_dist`;

        }

        ar.select(`${col_runn_dist} as '${col_runn_dist}' `);

         
        let col_runn_time = 'offer_dr.runn_time';
        if (alias !== undefined) {

          col_runn_time = `${alias}.runn_time`;

        }

        ar.select(`${col_runn_time} as '${col_runn_time}' `);

         
        let col_time_id = 'offer_dr.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_interest_ratio = 'offer_dr.interest_ratio';
        if (alias !== undefined) {

          col_interest_ratio = `${alias}.interest_ratio`;

        }

        ar.select(`${col_interest_ratio} as '${col_interest_ratio}' `);

         
        let col_rt_id = 'offer_dr.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_created_at = 'offer_dr.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'offer_dr.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_comment = 'offer_dr.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'offer_dr.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr');
    
    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.oilAmount) === true) {
      ar.set("oil_amount", form.oilAmount);
    } 
    

    if (nullCheck(form.tollgateAmount) === true) {
      ar.set("tollgate_amount", form.tollgateAmount);
    } 
    

    if (nullCheck(form.runnDayCnt) === true) {
      ar.set("runn_day_cnt", form.runnDayCnt);
    } 
    

    if (nullCheck(form.weekdayCnt) === true) {
      ar.set("weekday_cnt", form.weekdayCnt);
    } 
    

    if (nullCheck(form.repeatRunnCnt) === true) {
      ar.set("repeat_runn_cnt", form.repeatRunnCnt);
    } 
    

    if (nullCheck(form.runnDist) === true) {
      ar.set("runn_dist", form.runnDist);
    } 
    

    if (nullCheck(form.runnTime) === true) {
      ar.set("runn_time", form.runnTime);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("time_id", form.timeId);
    } 
    

    if (nullCheck(form.interestRatio) === true) {
      ar.set("interest_ratio", form.interestRatio);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr');
    
    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.oilAmount) === true) {
      ar.set("oil_amount", form.oilAmount);
    } 
    

    if (nullCheck(form.tollgateAmount) === true) {
      ar.set("tollgate_amount", form.tollgateAmount);
    } 
    

    if (nullCheck(form.runnDayCnt) === true) {
      ar.set("runn_day_cnt", form.runnDayCnt);
    } 
    

    if (nullCheck(form.weekdayCnt) === true) {
      ar.set("weekday_cnt", form.weekdayCnt);
    } 
    

    if (nullCheck(form.repeatRunnCnt) === true) {
      ar.set("repeat_runn_cnt", form.repeatRunnCnt);
    } 
    

    if (nullCheck(form.runnDist) === true) {
      ar.set("runn_dist", form.runnDist);
    } 
    

    if (nullCheck(form.runnTime) === true) {
      ar.set("runn_time", form.runnTime);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("time_id", form.timeId);
    } 
    

    if (nullCheck(form.interestRatio) === true) {
      ar.set("interest_ratio", form.interestRatio);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_view offer_dr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    offerDrId: 'offer_dr_id'
    , 

    taskId: 'task_id'
    , 

    oilAmount: 'oil_amount'
    , 

    tollgateAmount: 'tollgate_amount'
    , 

    runnDayCnt: 'runn_day_cnt'
    , 

    weekdayCnt: 'weekday_cnt'
    , 

    repeatRunnCnt: 'repeat_runn_cnt'
    , 

    runnDist: 'runn_dist'
    , 

    runnTime: 'runn_time'
    , 

    timeId: 'time_id'
    , 

    interestRatio: 'interest_ratio'
    , 

    rtId: 'rt_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('offer_dr');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_view offer_dr');
    
      ar.select("offer_dr.offer_dr_id");

    
    
      ar.select("offer_dr.task_id");

    
    
      ar.select("offer_dr.oil_amount");

    
    
      ar.select("offer_dr.tollgate_amount");

    
    
      ar.select("offer_dr.runn_day_cnt");

    
    
      ar.select("offer_dr.weekday_cnt");

    
    
      ar.select("offer_dr.repeat_runn_cnt");

    
    
      ar.select("offer_dr.runn_dist");

    
    
      ar.select("offer_dr.runn_time");

    
    
      ar.select("offer_dr.time_id");

    
    
      ar.select("offer_dr.interest_ratio");

    
    
      ar.select("offer_dr.rt_id");

    
    
      ar.select("offer_dr.created_at");

    
    
      ar.select("offer_dr.updated_at");

    
    
      ar.select("offer_dr.comment");

    
    
      ar.select("offer_dr.admin_id");

    
    
    return ar;
  }

  

  
}
export const OfferDrSql =  new OfferDrQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IOfferRes } from "../interface";


  class OfferResQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 offer_res객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' offer_res_view offer_res');

    
      if (alias === true) {
        ar.select("offer_res.offer_res_id as 'offer_res.offer_res_id'" );

      } else{
        ar.select("offer_res.offer_res_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.offer_req_id as 'offer_res.offer_req_id'" );

      } else{
        ar.select("offer_res.offer_req_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.task_id as 'offer_res.task_id'" );

      } else{
        ar.select("offer_res.task_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.rt_id as 'offer_res.rt_id'" );

      } else{
        ar.select("offer_res.rt_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.amount as 'offer_res.amount'" );

      } else{
        ar.select("offer_res.amount");

      }
      
      if (alias === true) {
        ar.select("offer_res.created_at as 'offer_res.created_at'" );

      } else{
        ar.select("offer_res.created_at");

      }
      
      if (alias === true) {
        ar.select("offer_res.updated_at as 'offer_res.updated_at'" );

      } else{
        ar.select("offer_res.updated_at");

      }
      
      if (alias === true) {
        ar.select("offer_res.comment as 'offer_res.comment'" );

      } else{
        ar.select("offer_res.comment");

      }
      
      if (alias === true) {
        ar.select("offer_res.admin_id as 'offer_res.admin_id'" );

      } else{
        ar.select("offer_res.admin_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.nm as 'offer_res.nm'" );

      } else{
        ar.select("offer_res.nm");

      }
      
      if (alias === true) {
        ar.select("offer_res.phone as 'offer_res.phone'" );

      } else{
        ar.select("offer_res.phone");

      }
      
      if (alias === true) {
        ar.select("offer_res.dr_id as 'offer_res.dr_id'" );

      } else{
        ar.select("offer_res.dr_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.dispatch_id as 'offer_res.dispatch_id'" );

      } else{
        ar.select("offer_res.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.dr_pay_id as 'offer_res.dr_pay_id'" );

      } else{
        ar.select("offer_res.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("offer_res.dr_pay_cond_id as 'offer_res.dr_pay_cond_id'" );

      } else{
        ar.select("offer_res.dr_pay_cond_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_offer_res_id = 'offer_res.offer_res_id';
        if (alias !== undefined) {

          col_offer_res_id = `${alias}.offer_res_id`;

        }

        ar.select(`${col_offer_res_id} as '${col_offer_res_id}' `);

         
        let col_offer_req_id = 'offer_res.offer_req_id';
        if (alias !== undefined) {

          col_offer_req_id = `${alias}.offer_req_id`;

        }

        ar.select(`${col_offer_req_id} as '${col_offer_req_id}' `);

         
        let col_task_id = 'offer_res.task_id';
        if (alias !== undefined) {

          col_task_id = `${alias}.task_id`;

        }

        ar.select(`${col_task_id} as '${col_task_id}' `);

         
        let col_rt_id = 'offer_res.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_amount = 'offer_res.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'offer_res.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'offer_res.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_comment = 'offer_res.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'offer_res.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_nm = 'offer_res.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_phone = 'offer_res.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_dr_id = 'offer_res.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_dispatch_id = 'offer_res.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_dr_pay_id = 'offer_res.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_dr_pay_cond_id = 'offer_res.dr_pay_cond_id';
        if (alias !== undefined) {

          col_dr_pay_cond_id = `${alias}.dr_pay_cond_id`;

        }

        ar.select(`${col_dr_pay_cond_id} as '${col_dr_pay_cond_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_res');
    
    if (nullCheck(form.offerResId) === true) {
      ar.set("offer_res_id", form.offerResId);
    } 
    

    if (nullCheck(form.offerReqId) === true) {
      ar.set("offer_req_id", form.offerReqId);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_res');
    
    if (nullCheck(form.offerResId) === true) {
      ar.set("offer_res_id", form.offerResId);
    } 
    

    if (nullCheck(form.offerReqId) === true) {
      ar.set("offer_req_id", form.offerReqId);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_res_view offer_res');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    offerResId: 'offer_res_id'
    , 

    offerReqId: 'offer_req_id'
    , 

    taskId: 'task_id'
    , 

    rtId: 'rt_id'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    , 

    nm: 'nm'
    , 

    phone: 'phone'
    , 

    drId: 'dr_id'
    , 

    dispatchId: 'dispatch_id'
    , 

    drPayId: 'dr_pay_id'
    , 

    drPayCondId: 'dr_pay_cond_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('offer_res');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_res_view offer_res');
    
      ar.select("offer_res.offer_res_id");

    
    
      ar.select("offer_res.offer_req_id");

    
    
      ar.select("offer_res.task_id");

    
    
      ar.select("offer_res.rt_id");

    
    
      ar.select("offer_res.amount");

    
    
      ar.select("offer_res.created_at");

    
    
      ar.select("offer_res.updated_at");

    
    
      ar.select("offer_res.comment");

    
    
      ar.select("offer_res.admin_id");

    
    
      ar.select("offer_res.nm");

    
    
      ar.select("offer_res.phone");

    
    
      ar.select("offer_res.dr_id");

    
    
      ar.select("offer_res.dispatch_id");

    
    
      ar.select("offer_res.dr_pay_id");

    
    
      ar.select("offer_res.dr_pay_cond_id");

    
    
    return ar;
  }

  

  
}
export const OfferResSql =  new OfferResQuery()

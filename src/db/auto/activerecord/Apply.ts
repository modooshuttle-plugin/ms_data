import { Ar, nullCheck } from "../../../util";
 
  import { IApply } from "../interface";


  class ApplyQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 apply객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' apply_view apply');

    
      if (alias === true) {
        ar.select("apply.apply_id as 'apply.apply_id'" );

      } else{
        ar.select("apply.apply_id");

      }
      
      if (alias === true) {
        ar.select("apply.subscribe_id as 'apply.subscribe_id'" );

      } else{
        ar.select("apply.subscribe_id");

      }
      
      if (alias === true) {
        ar.select("apply.prods_cd as 'apply.prods_cd'" );

      } else{
        ar.select("apply.prods_cd");

      }
      
      if (alias === true) {
        ar.select("apply.user_id as 'apply.user_id'" );

      } else{
        ar.select("apply.user_id");

      }
      
      if (alias === true) {
        ar.select("apply.init_start_day as 'apply.init_start_day'" );

      } else{
        ar.select("apply.init_start_day");

      }
      
      if (alias === true) {
        ar.select("apply.start_day as 'apply.start_day'" );

      } else{
        ar.select("apply.start_day");

      }
      
      if (alias === true) {
        ar.select("apply.end_day as 'apply.end_day'" );

      } else{
        ar.select("apply.end_day");

      }
      
      if (alias === true) {
        ar.select("apply.created_at as 'apply.created_at'" );

      } else{
        ar.select("apply.created_at");

      }
      
      if (alias === true) {
        ar.select("apply.updated_at as 'apply.updated_at'" );

      } else{
        ar.select("apply.updated_at");

      }
      
      if (alias === true) {
        ar.select("apply.apply_cd as 'apply.apply_cd'" );

      } else{
        ar.select("apply.apply_cd");

      }
      
      if (alias === true) {
        ar.select("apply.prev_apply_id as 'apply.prev_apply_id'" );

      } else{
        ar.select("apply.prev_apply_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_apply_id = 'apply.apply_id';
        if (alias !== undefined) {

          col_apply_id = `${alias}.apply_id`;

        }

        ar.select(`${col_apply_id} as '${col_apply_id}' `);

         
        let col_subscribe_id = 'apply.subscribe_id';
        if (alias !== undefined) {

          col_subscribe_id = `${alias}.subscribe_id`;

        }

        ar.select(`${col_subscribe_id} as '${col_subscribe_id}' `);

         
        let col_prods_cd = 'apply.prods_cd';
        if (alias !== undefined) {

          col_prods_cd = `${alias}.prods_cd`;

        }

        ar.select(`${col_prods_cd} as '${col_prods_cd}' `);

         
        let col_user_id = 'apply.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_init_start_day = 'apply.init_start_day';
        if (alias !== undefined) {

          col_init_start_day = `${alias}.init_start_day`;

        }

        ar.select(`${col_init_start_day} as '${col_init_start_day}' `);

         
        let col_start_day = 'apply.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'apply.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_created_at = 'apply.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'apply.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_apply_cd = 'apply.apply_cd';
        if (alias !== undefined) {

          col_apply_cd = `${alias}.apply_cd`;

        }

        ar.select(`${col_apply_cd} as '${col_apply_cd}' `);

         
        let col_prev_apply_id = 'apply.prev_apply_id';
        if (alias !== undefined) {

          col_prev_apply_id = `${alias}.prev_apply_id`;

        }

        ar.select(`${col_prev_apply_id} as '${col_prev_apply_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_apply');
    
    if (nullCheck(form.applyId) === true) {
      ar.set("o_no", form.applyId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("o_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("o_rptype", form.prodsCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.initStartDay) === true) {
      ar.set("o_start_day_init", form.initStartDay);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("o_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("o_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.applyCd) === true) {
      ar.set("apply_ty", form.applyCd);
    } 
    

    if (nullCheck(form.prevApplyId) === true) {
      ar.set("prev_aid", form.prevApplyId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_apply');
    
    if (nullCheck(form.applyId) === true) {
      ar.set("o_no", form.applyId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("o_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("o_rptype", form.prodsCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.initStartDay) === true) {
      ar.set("o_start_day_init", form.initStartDay);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("o_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("o_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.applyCd) === true) {
      ar.set("apply_ty", form.applyCd);
    } 
    

    if (nullCheck(form.prevApplyId) === true) {
      ar.set("prev_aid", form.prevApplyId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' apply_view apply');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    applyId: 'o_no'
    , 

    subscribeId: 'o_sid'
    , 

    prodsCd: 'o_rptype'
    , 

    userId: 'o_mid'
    , 

    initStartDay: 'o_start_day_init'
    , 

    startDay: 'o_start_day'
    , 

    endDay: 'o_end_day'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    applyCd: 'apply_ty'
    , 

    prevApplyId: 'prev_aid'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_apply');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' apply_view apply');
    
      ar.select("apply.apply_id");

    
    
      ar.select("apply.subscribe_id");

    
    
      ar.select("apply.prods_cd");

    
    
      ar.select("apply.user_id");

    
    
      ar.select("apply.init_start_day");

    
    
      ar.select("apply.start_day");

    
    
      ar.select("apply.end_day");

    
    
      ar.select("apply.created_at");

    
    
      ar.select("apply.updated_at");

    
    
      ar.select("apply.apply_cd");

    
    
      ar.select("apply.prev_apply_id");

    
    
    return ar;
  }

  

  
}
export const ApplySql =  new ApplyQuery()

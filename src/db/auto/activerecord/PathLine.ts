import { Ar, nullCheck } from "../../../util";
 
  import { IPathLine } from "../interface";


  class PathLineQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 path_line객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' path_line_view path_line');

    
      if (alias === true) {
        ar.select("path_line.path_line_id as 'path_line.path_line_id'" );

      } else{
        ar.select("path_line.path_line_id");

      }
      
      if (alias === true) {
        ar.select("path_line.rt_id as 'path_line.rt_id'" );

      } else{
        ar.select("path_line.rt_id");

      }
      
      if (alias === true) {
        ar.select("path_line.path_ver as 'path_line.path_ver'" );

      } else{
        ar.select("path_line.path_ver");

      }
      
      if (alias === true) {
        ar.select("path_line.section_cd as 'path_line.section_cd'" );

      } else{
        ar.select("path_line.section_cd");

      }
      
      if (alias === true) {
        ar.select("path_line.geom as 'path_line.geom'" );

      } else{
        ar.select("path_line.geom");

      }
      
      if (alias === true) {
        ar.select("path_line.created_at as 'path_line.created_at'" );

      } else{
        ar.select("path_line.created_at");

      }
      
      if (alias === true) {
        ar.select("path_line.updated_at as 'path_line.updated_at'" );

      } else{
        ar.select("path_line.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_path_line_id = 'path_line.path_line_id';
        if (alias !== undefined) {

          col_path_line_id = `${alias}.path_line_id`;

        }

        ar.select(`${col_path_line_id} as '${col_path_line_id}' `);

         
        let col_rt_id = 'path_line.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_path_ver = 'path_line.path_ver';
        if (alias !== undefined) {

          col_path_ver = `${alias}.path_ver`;

        }

        ar.select(`${col_path_ver} as '${col_path_ver}' `);

         
        let col_section_cd = 'path_line.section_cd';
        if (alias !== undefined) {

          col_section_cd = `${alias}.section_cd`;

        }

        ar.select(`${col_section_cd} as '${col_section_cd}' `);

         
        let col_geom = 'path_line.geom';
        if (alias !== undefined) {

          col_geom = `${alias}.geom`;

        }

        ar.select(`${col_geom} as '${col_geom}' `);

         
        let col_created_at = 'path_line.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'path_line.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_path');
    
    if (nullCheck(form.pathLineId) === true) {
      ar.set("rt_path_id", form.pathLineId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rid", form.rtId);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("ver", form.pathVer);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section", form.sectionCd);
    } 
    

    if (nullCheck(form.geom) === true) {
      ar.set("geom", form.geom);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_path');
    
    if (nullCheck(form.pathLineId) === true) {
      ar.set("rt_path_id", form.pathLineId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rid", form.rtId);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("ver", form.pathVer);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section", form.sectionCd);
    } 
    

    if (nullCheck(form.geom) === true) {
      ar.set("geom", form.geom);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' path_line_view path_line');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    pathLineId: 'rt_path_id'
    , 

    rtId: 'rid'
    , 

    pathVer: 'ver'
    , 

    sectionCd: 'section'
    , 

    geom: 'geom'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_path');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' path_line_view path_line');
    
      ar.select("path_line.path_line_id");

    
    
      ar.select("path_line.rt_id");

    
    
      ar.select("path_line.path_ver");

    
    
      ar.select("path_line.section_cd");

    
    
      ar.select("path_line.geom");

    
    
      ar.select("path_line.created_at");

    
    
      ar.select("path_line.updated_at");

    
    
    return ar;
  }

  

  
}
export const PathLineSql =  new PathLineQuery()

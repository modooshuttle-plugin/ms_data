import { Ar, nullCheck } from "../../../util";
 
  import { IDispatch } from "../interface";


  class DispatchQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dispatch객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dispatch_view dispatch');

    
      if (alias === true) {
        ar.select("dispatch.dispatch_id as 'dispatch.dispatch_id'" );

      } else{
        ar.select("dispatch.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("dispatch.rt_id as 'dispatch.rt_id'" );

      } else{
        ar.select("dispatch.rt_id");

      }
      
      if (alias === true) {
        ar.select("dispatch.dr_id as 'dispatch.dr_id'" );

      } else{
        ar.select("dispatch.dr_id");

      }
      
      if (alias === true) {
        ar.select("dispatch.start_day as 'dispatch.start_day'" );

      } else{
        ar.select("dispatch.start_day");

      }
      
      if (alias === true) {
        ar.select("dispatch.end_day as 'dispatch.end_day'" );

      } else{
        ar.select("dispatch.end_day");

      }
      
      if (alias === true) {
        ar.select("dispatch.dispatch_cd as 'dispatch.dispatch_cd'" );

      } else{
        ar.select("dispatch.dispatch_cd");

      }
      
      if (alias === true) {
        ar.select("dispatch.deploy_yn as 'dispatch.deploy_yn'" );

      } else{
        ar.select("dispatch.deploy_yn");

      }
      
      if (alias === true) {
        ar.select("dispatch.created_at as 'dispatch.created_at'" );

      } else{
        ar.select("dispatch.created_at");

      }
      
      if (alias === true) {
        ar.select("dispatch.updated_at as 'dispatch.updated_at'" );

      } else{
        ar.select("dispatch.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dispatch_id = 'dispatch.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_rt_id = 'dispatch.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_dr_id = 'dispatch.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_start_day = 'dispatch.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'dispatch.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_dispatch_cd = 'dispatch.dispatch_cd';
        if (alias !== undefined) {

          col_dispatch_cd = `${alias}.dispatch_cd`;

        }

        ar.select(`${col_dispatch_cd} as '${col_dispatch_cd}' `);

         
        let col_deploy_yn = 'dispatch.deploy_yn';
        if (alias !== undefined) {

          col_deploy_yn = `${alias}.deploy_yn`;

        }

        ar.select(`${col_deploy_yn} as '${col_deploy_yn}' `);

         
        let col_created_at = 'dispatch.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dispatch.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_driver');
    
    if (nullCheck(form.dispatchId) === true) {
      ar.set("r_no", form.dispatchId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("r_did", form.drId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("r_end_day", form.endDay);
    } 
    

    if (nullCheck(form.dispatchCd) === true) {
      ar.set("r_type", form.dispatchCd);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("r_deploy", form.deployYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_driver');
    
    if (nullCheck(form.dispatchId) === true) {
      ar.set("r_no", form.dispatchId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("r_did", form.drId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("r_end_day", form.endDay);
    } 
    

    if (nullCheck(form.dispatchCd) === true) {
      ar.set("r_type", form.dispatchCd);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("r_deploy", form.deployYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dispatch_view dispatch');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    dispatchId: 'r_no'
    , 

    rtId: 'r_rid'
    , 

    drId: 'r_did'
    , 

    startDay: 'r_start_day'
    , 

    endDay: 'r_end_day'
    , 

    dispatchCd: 'r_type'
    , 

    deployYn: 'r_deploy'
    , 

    createdAt: 'r_timestamp'
    , 

    updatedAt: 'r_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('r_driver');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dispatch_view dispatch');
    
      ar.select("dispatch.dispatch_id");

    
    
      ar.select("dispatch.rt_id");

    
    
      ar.select("dispatch.dr_id");

    
    
      ar.select("dispatch.start_day");

    
    
      ar.select("dispatch.end_day");

    
    
      ar.select("dispatch.dispatch_cd");

    
    
      ar.select("dispatch.deploy_yn");

    
    
      ar.select("dispatch.created_at");

    
    
      ar.select("dispatch.updated_at");

    
    
    return ar;
  }

  

  
}
export const DispatchSql =  new DispatchQuery()

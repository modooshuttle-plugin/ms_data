import { Ar, nullCheck } from "../../../util";
 
  import { IRunnCald } from "../interface";


  class RunnCaldQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 runn_cald객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' runn_cald_view runn_cald');

    
      if (alias === true) {
        ar.select("runn_cald.runn_cald_id as 'runn_cald.runn_cald_id'" );

      } else{
        ar.select("runn_cald.runn_cald_id");

      }
      
      if (alias === true) {
        ar.select("runn_cald.rt_id as 'runn_cald.rt_id'" );

      } else{
        ar.select("runn_cald.rt_id");

      }
      
      if (alias === true) {
        ar.select("runn_cald.runn_cd as 'runn_cald.runn_cd'" );

      } else{
        ar.select("runn_cald.runn_cd");

      }
      
      if (alias === true) {
        ar.select("runn_cald.time_cd as 'runn_cald.time_cd'" );

      } else{
        ar.select("runn_cald.time_cd");

      }
      
      if (alias === true) {
        ar.select("runn_cald.day as 'runn_cald.day'" );

      } else{
        ar.select("runn_cald.day");

      }
      
      if (alias === true) {
        ar.select("runn_cald.comment as 'runn_cald.comment'" );

      } else{
        ar.select("runn_cald.comment");

      }
      
      if (alias === true) {
        ar.select("runn_cald.created_at as 'runn_cald.created_at'" );

      } else{
        ar.select("runn_cald.created_at");

      }
      
      if (alias === true) {
        ar.select("runn_cald.updated_at as 'runn_cald.updated_at'" );

      } else{
        ar.select("runn_cald.updated_at");

      }
      
      if (alias === true) {
        ar.select("runn_cald.admin_id as 'runn_cald.admin_id'" );

      } else{
        ar.select("runn_cald.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_runn_cald_id = 'runn_cald.runn_cald_id';
        if (alias !== undefined) {

          col_runn_cald_id = `${alias}.runn_cald_id`;

        }

        ar.select(`${col_runn_cald_id} as '${col_runn_cald_id}' `);

         
        let col_rt_id = 'runn_cald.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_runn_cd = 'runn_cald.runn_cd';
        if (alias !== undefined) {

          col_runn_cd = `${alias}.runn_cd`;

        }

        ar.select(`${col_runn_cd} as '${col_runn_cd}' `);

         
        let col_time_cd = 'runn_cald.time_cd';
        if (alias !== undefined) {

          col_time_cd = `${alias}.time_cd`;

        }

        ar.select(`${col_time_cd} as '${col_time_cd}' `);

         
        let col_day = 'runn_cald.day';
        if (alias !== undefined) {

          col_day = `${alias}.day`;

        }

        ar.select(`${col_day} as '${col_day}' `);

         
        let col_comment = 'runn_cald.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'runn_cald.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'runn_cald.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'runn_cald.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' runn_cald');
    
    if (nullCheck(form.runnCaldId) === true) {
      ar.set("runn_cald_id", form.runnCaldId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("runn_cd", form.runnCd);
    } 
    

    if (nullCheck(form.timeCd) === true) {
      ar.set("time_cd", form.timeCd);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("day", form.day);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' runn_cald');
    
    if (nullCheck(form.runnCaldId) === true) {
      ar.set("runn_cald_id", form.runnCaldId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("runn_cd", form.runnCd);
    } 
    

    if (nullCheck(form.timeCd) === true) {
      ar.set("time_cd", form.timeCd);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("day", form.day);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_cald_view runn_cald');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    runnCaldId: 'runn_cald_id'
    , 

    rtId: 'rt_id'
    , 

    runnCd: 'runn_cd'
    , 

    timeCd: 'time_cd'
    , 

    day: 'day'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('runn_cald');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_cald_view runn_cald');
    
      ar.select("runn_cald.runn_cald_id");

    
    
      ar.select("runn_cald.rt_id");

    
    
      ar.select("runn_cald.runn_cd");

    
    
      ar.select("runn_cald.time_cd");

    
    
      ar.select("runn_cald.day");

    
    
      ar.select("runn_cald.comment");

    
    
      ar.select("runn_cald.created_at");

    
    
      ar.select("runn_cald.updated_at");

    
    
      ar.select("runn_cald.admin_id");

    
    
    return ar;
  }

  

  
}
export const RunnCaldSql =  new RunnCaldQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IProds } from "../interface";


  class ProdsQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 prods객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' prods_view prods');

    
      if (alias === true) {
        ar.select("prods.prods_no as 'prods.prods_no'" );

      } else{
        ar.select("prods.prods_no");

      }
      
      if (alias === true) {
        ar.select("prods.rt_id as 'prods.rt_id'" );

      } else{
        ar.select("prods.rt_id");

      }
      
      if (alias === true) {
        ar.select("prods.prods_id as 'prods.prods_id'" );

      } else{
        ar.select("prods.prods_id");

      }
      
      if (alias === true) {
        ar.select("prods.amount as 'prods.amount'" );

      } else{
        ar.select("prods.amount");

      }
      
      if (alias === true) {
        ar.select("prods.created_at as 'prods.created_at'" );

      } else{
        ar.select("prods.created_at");

      }
      
      if (alias === true) {
        ar.select("prods.updated_at as 'prods.updated_at'" );

      } else{
        ar.select("prods.updated_at");

      }
      
      if (alias === true) {
        ar.select("prods.start_day as 'prods.start_day'" );

      } else{
        ar.select("prods.start_day");

      }
      
      if (alias === true) {
        ar.select("prods.default_amount as 'prods.default_amount'" );

      } else{
        ar.select("prods.default_amount");

      }
      
      if (alias === true) {
        ar.select("prods.deploy_yn as 'prods.deploy_yn'" );

      } else{
        ar.select("prods.deploy_yn");

      }
      
      if (alias === true) {
        ar.select("prods.admin_id as 'prods.admin_id'" );

      } else{
        ar.select("prods.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_prods_no = 'prods.prods_no';
        if (alias !== undefined) {

          col_prods_no = `${alias}.prods_no`;

        }

        ar.select(`${col_prods_no} as '${col_prods_no}' `);

         
        let col_rt_id = 'prods.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_prods_id = 'prods.prods_id';
        if (alias !== undefined) {

          col_prods_id = `${alias}.prods_id`;

        }

        ar.select(`${col_prods_id} as '${col_prods_id}' `);

         
        let col_amount = 'prods.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'prods.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'prods.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_start_day = 'prods.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_default_amount = 'prods.default_amount';
        if (alias !== undefined) {

          col_default_amount = `${alias}.default_amount`;

        }

        ar.select(`${col_default_amount} as '${col_default_amount}' `);

         
        let col_deploy_yn = 'prods.deploy_yn';
        if (alias !== undefined) {

          col_deploy_yn = `${alias}.deploy_yn`;

        }

        ar.select(`${col_deploy_yn} as '${col_deploy_yn}' `);

         
        let col_admin_id = 'prods.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_product');
    
    if (nullCheck(form.prodsNo) === true) {
      ar.set("r_no", form.prodsNo);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("r_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("r_price", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.defaultAmount) === true) {
      ar.set("r_price_default", form.defaultAmount);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("r_deploy", form.deployYn);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("r_admin", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_product');
    
    if (nullCheck(form.prodsNo) === true) {
      ar.set("r_no", form.prodsNo);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("r_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("r_price", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.defaultAmount) === true) {
      ar.set("r_price_default", form.defaultAmount);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("r_deploy", form.deployYn);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("r_admin", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' prods_view prods');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    prodsNo: 'r_no'
    , 

    rtId: 'r_rid'
    , 

    prodsId: 'r_rpid'
    , 

    amount: 'r_price'
    , 

    createdAt: 'r_timestamp'
    , 

    updatedAt: 'r_timestamp_u'
    , 

    startDay: 'r_start_day'
    , 

    defaultAmount: 'r_price_default'
    , 

    deployYn: 'r_deploy'
    , 

    adminId: 'r_admin'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('r_product');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' prods_view prods');
    
      ar.select("prods.prods_no");

    
    
      ar.select("prods.rt_id");

    
    
      ar.select("prods.prods_id");

    
    
      ar.select("prods.amount");

    
    
      ar.select("prods.created_at");

    
    
      ar.select("prods.updated_at");

    
    
      ar.select("prods.start_day");

    
    
      ar.select("prods.default_amount");

    
    
      ar.select("prods.deploy_yn");

    
    
      ar.select("prods.admin_id");

    
    
    return ar;
  }

  

  
}
export const ProdsSql =  new ProdsQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IOrgEmail } from "../interface";


  class OrgEmailQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_email객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_email_view org_email');

    
      if (alias === true) {
        ar.select("org_email.org_email_id as 'org_email.org_email_id'" );

      } else{
        ar.select("org_email.org_email_id");

      }
      
      if (alias === true) {
        ar.select("org_email.org_user_auth_id as 'org_email.org_user_auth_id'" );

      } else{
        ar.select("org_email.org_user_auth_id");

      }
      
      if (alias === true) {
        ar.select("org_email.org_id as 'org_email.org_id'" );

      } else{
        ar.select("org_email.org_id");

      }
      
      if (alias === true) {
        ar.select("org_email.org_ctrt_id as 'org_email.org_ctrt_id'" );

      } else{
        ar.select("org_email.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_email.domain as 'org_email.domain'" );

      } else{
        ar.select("org_email.domain");

      }
      
      if (alias === true) {
        ar.select("org_email.created_at as 'org_email.created_at'" );

      } else{
        ar.select("org_email.created_at");

      }
      
      if (alias === true) {
        ar.select("org_email.updated_at as 'org_email.updated_at'" );

      } else{
        ar.select("org_email.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_email_id = 'org_email.org_email_id';
        if (alias !== undefined) {

          col_org_email_id = `${alias}.org_email_id`;

        }

        ar.select(`${col_org_email_id} as '${col_org_email_id}' `);

         
        let col_org_user_auth_id = 'org_email.org_user_auth_id';
        if (alias !== undefined) {

          col_org_user_auth_id = `${alias}.org_user_auth_id`;

        }

        ar.select(`${col_org_user_auth_id} as '${col_org_user_auth_id}' `);

         
        let col_org_id = 'org_email.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_org_ctrt_id = 'org_email.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_domain = 'org_email.domain';
        if (alias !== undefined) {

          col_domain = `${alias}.domain`;

        }

        ar.select(`${col_domain} as '${col_domain}' `);

         
        let col_created_at = 'org_email.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_email.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_email');
    
    if (nullCheck(form.orgEmailId) === true) {
      ar.set("org_email_id", form.orgEmailId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.domain) === true) {
      ar.set("domain", form.domain);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_email');
    
    if (nullCheck(form.orgEmailId) === true) {
      ar.set("org_email_id", form.orgEmailId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.domain) === true) {
      ar.set("domain", form.domain);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_email_view org_email');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgEmailId: 'org_email_id'
    , 

    orgUserAuthId: 'org_user_auth_id'
    , 

    orgId: 'org_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    domain: 'domain'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_email');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_email_view org_email');
    
      ar.select("org_email.org_email_id");

    
    
      ar.select("org_email.org_user_auth_id");

    
    
      ar.select("org_email.org_id");

    
    
      ar.select("org_email.org_ctrt_id");

    
    
      ar.select("org_email.domain");

    
    
      ar.select("org_email.created_at");

    
    
      ar.select("org_email.updated_at");

    
    
    return ar;
  }

  

  
}
export const OrgEmailSql =  new OrgEmailQuery()

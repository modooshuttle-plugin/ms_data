import { Ar, nullCheck } from "../../../util";
 
  import { ITrkEvt } from "../interface";


  class TrkEvtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 trk_evt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' trk_evt_view trk_evt');

    
      if (alias === true) {
        ar.select("trk_evt.trk_evt_id as 'trk_evt.trk_evt_id'" );

      } else{
        ar.select("trk_evt.trk_evt_id");

      }
      
      if (alias === true) {
        ar.select("trk_evt.target_cd as 'trk_evt.target_cd'" );

      } else{
        ar.select("trk_evt.target_cd");

      }
      
      if (alias === true) {
        ar.select("trk_evt.trk_evt_cd as 'trk_evt.trk_evt_cd'" );

      } else{
        ar.select("trk_evt.trk_evt_cd");

      }
      
      if (alias === true) {
        ar.select("trk_evt.evt_nm as 'trk_evt.evt_nm'" );

      } else{
        ar.select("trk_evt.evt_nm");

      }
      
      if (alias === true) {
        ar.select("trk_evt.comment as 'trk_evt.comment'" );

      } else{
        ar.select("trk_evt.comment");

      }
      
      if (alias === true) {
        ar.select("trk_evt.params as 'trk_evt.params'" );

      } else{
        ar.select("trk_evt.params");

      }
      
      if (alias === true) {
        ar.select("trk_evt.created_at as 'trk_evt.created_at'" );

      } else{
        ar.select("trk_evt.created_at");

      }
      
      if (alias === true) {
        ar.select("trk_evt.updated_at as 'trk_evt.updated_at'" );

      } else{
        ar.select("trk_evt.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_trk_evt_id = 'trk_evt.trk_evt_id';
        if (alias !== undefined) {

          col_trk_evt_id = `${alias}.trk_evt_id`;

        }

        ar.select(`${col_trk_evt_id} as '${col_trk_evt_id}' `);

         
        let col_target_cd = 'trk_evt.target_cd';
        if (alias !== undefined) {

          col_target_cd = `${alias}.target_cd`;

        }

        ar.select(`${col_target_cd} as '${col_target_cd}' `);

         
        let col_trk_evt_cd = 'trk_evt.trk_evt_cd';
        if (alias !== undefined) {

          col_trk_evt_cd = `${alias}.trk_evt_cd`;

        }

        ar.select(`${col_trk_evt_cd} as '${col_trk_evt_cd}' `);

         
        let col_evt_nm = 'trk_evt.evt_nm';
        if (alias !== undefined) {

          col_evt_nm = `${alias}.evt_nm`;

        }

        ar.select(`${col_evt_nm} as '${col_evt_nm}' `);

         
        let col_comment = 'trk_evt.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_params = 'trk_evt.params';
        if (alias !== undefined) {

          col_params = `${alias}.params`;

        }

        ar.select(`${col_params} as '${col_params}' `);

         
        let col_created_at = 'trk_evt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'trk_evt.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' trk_evt');
    
    if (nullCheck(form.trkEvtId) === true) {
      ar.set("trk_evt_id", form.trkEvtId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.trkEvtCd) === true) {
      ar.set("trk_evt_cd", form.trkEvtCd);
    } 
    

    if (nullCheck(form.evtNm) === true) {
      ar.set("evt_nm", form.evtNm);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.params) === true) {
      ar.set("params", form.params);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' trk_evt');
    
    if (nullCheck(form.trkEvtId) === true) {
      ar.set("trk_evt_id", form.trkEvtId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.trkEvtCd) === true) {
      ar.set("trk_evt_cd", form.trkEvtCd);
    } 
    

    if (nullCheck(form.evtNm) === true) {
      ar.set("evt_nm", form.evtNm);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.params) === true) {
      ar.set("params", form.params);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' trk_evt_view trk_evt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    trkEvtId: 'trk_evt_id'
    , 

    targetCd: 'target_cd'
    , 

    trkEvtCd: 'trk_evt_cd'
    , 

    evtNm: 'evt_nm'
    , 

    comment: 'comment'
    , 

    params: 'params'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('trk_evt');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' trk_evt_view trk_evt');
    
      ar.select("trk_evt.trk_evt_id");

    
    
      ar.select("trk_evt.target_cd");

    
    
      ar.select("trk_evt.trk_evt_cd");

    
    
      ar.select("trk_evt.evt_nm");

    
    
      ar.select("trk_evt.comment");

    
    
      ar.select("trk_evt.params");

    
    
      ar.select("trk_evt.created_at");

    
    
      ar.select("trk_evt.updated_at");

    
    
    return ar;
  }

  

  
}
export const TrkEvtSql =  new TrkEvtQuery()

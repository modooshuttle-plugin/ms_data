import { Ar, nullCheck } from "../../../util";
 
  import { IDrIssAmount } from "../interface";


  class DrIssAmountQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_iss_amount객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_amount_view dr_iss_amount');

    
      if (alias === true) {
        ar.select("dr_iss_amount.dr_iss_amount_id as 'dr_iss_amount.dr_iss_amount_id'" );

      } else{
        ar.select("dr_iss_amount.dr_iss_amount_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.dr_iss_id as 'dr_iss_amount.dr_iss_id'" );

      } else{
        ar.select("dr_iss_amount.dr_iss_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.sign as 'dr_iss_amount.sign'" );

      } else{
        ar.select("dr_iss_amount.sign");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.amount as 'dr_iss_amount.amount'" );

      } else{
        ar.select("dr_iss_amount.amount");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.iss_amount_cd as 'dr_iss_amount.iss_amount_cd'" );

      } else{
        ar.select("dr_iss_amount.iss_amount_cd");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.admin_id as 'dr_iss_amount.admin_id'" );

      } else{
        ar.select("dr_iss_amount.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.created_at as 'dr_iss_amount.created_at'" );

      } else{
        ar.select("dr_iss_amount.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_iss_amount.updated_at as 'dr_iss_amount.updated_at'" );

      } else{
        ar.select("dr_iss_amount.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_iss_amount_id = 'dr_iss_amount.dr_iss_amount_id';
        if (alias !== undefined) {

          col_dr_iss_amount_id = `${alias}.dr_iss_amount_id`;

        }

        ar.select(`${col_dr_iss_amount_id} as '${col_dr_iss_amount_id}' `);

         
        let col_dr_iss_id = 'dr_iss_amount.dr_iss_id';
        if (alias !== undefined) {

          col_dr_iss_id = `${alias}.dr_iss_id`;

        }

        ar.select(`${col_dr_iss_id} as '${col_dr_iss_id}' `);

         
        let col_sign = 'dr_iss_amount.sign';
        if (alias !== undefined) {

          col_sign = `${alias}.sign`;

        }

        ar.select(`${col_sign} as '${col_sign}' `);

         
        let col_amount = 'dr_iss_amount.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_iss_amount_cd = 'dr_iss_amount.iss_amount_cd';
        if (alias !== undefined) {

          col_iss_amount_cd = `${alias}.iss_amount_cd`;

        }

        ar.select(`${col_iss_amount_cd} as '${col_iss_amount_cd}' `);

         
        let col_admin_id = 'dr_iss_amount.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'dr_iss_amount.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_iss_amount.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_amount');
    
    if (nullCheck(form.drIssAmountId) === true) {
      ar.set("dr_iss_amount_id", form.drIssAmountId);
    } 
    

    if (nullCheck(form.drIssId) === true) {
      ar.set("dr_iss_id", form.drIssId);
    } 
    

    if (nullCheck(form.sign) === true) {
      ar.set("sign", form.sign);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.issAmountCd) === true) {
      ar.set("iss_amount_cd", form.issAmountCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_amount');
    
    if (nullCheck(form.drIssAmountId) === true) {
      ar.set("dr_iss_amount_id", form.drIssAmountId);
    } 
    

    if (nullCheck(form.drIssId) === true) {
      ar.set("dr_iss_id", form.drIssId);
    } 
    

    if (nullCheck(form.sign) === true) {
      ar.set("sign", form.sign);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.issAmountCd) === true) {
      ar.set("iss_amount_cd", form.issAmountCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_amount_view dr_iss_amount');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drIssAmountId: 'dr_iss_amount_id'
    , 

    drIssId: 'dr_iss_id'
    , 

    sign: 'sign'
    , 

    amount: 'amount'
    , 

    issAmountCd: 'iss_amount_cd'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_iss_amount');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_amount_view dr_iss_amount');
    
      ar.select("dr_iss_amount.dr_iss_amount_id");

    
    
      ar.select("dr_iss_amount.dr_iss_id");

    
    
      ar.select("dr_iss_amount.sign");

    
    
      ar.select("dr_iss_amount.amount");

    
    
      ar.select("dr_iss_amount.iss_amount_cd");

    
    
      ar.select("dr_iss_amount.admin_id");

    
    
      ar.select("dr_iss_amount.created_at");

    
    
      ar.select("dr_iss_amount.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrIssAmountSql =  new DrIssAmountQuery()

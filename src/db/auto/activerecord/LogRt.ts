import { Ar, nullCheck } from "../../../util";
 
  import { ILogRt } from "../interface";


  class LogRtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_rt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_rt_view log_rt');

    
      if (alias === true) {
        ar.select("log_rt.log_rt_id as 'log_rt.log_rt_id'" );

      } else{
        ar.select("log_rt.log_rt_id");

      }
      
      if (alias === true) {
        ar.select("log_rt.evt_id as 'log_rt.evt_id'" );

      } else{
        ar.select("log_rt.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_rt.rt_id as 'log_rt.rt_id'" );

      } else{
        ar.select("log_rt.rt_id");

      }
      
      if (alias === true) {
        ar.select("log_rt.start_info as 'log_rt.start_info'" );

      } else{
        ar.select("log_rt.start_info");

      }
      
      if (alias === true) {
        ar.select("log_rt.end_info as 'log_rt.end_info'" );

      } else{
        ar.select("log_rt.end_info");

      }
      
      if (alias === true) {
        ar.select("log_rt.start_tag as 'log_rt.start_tag'" );

      } else{
        ar.select("log_rt.start_tag");

      }
      
      if (alias === true) {
        ar.select("log_rt.end_tag as 'log_rt.end_tag'" );

      } else{
        ar.select("log_rt.end_tag");

      }
      
      if (alias === true) {
        ar.select("log_rt.start_cat_cd as 'log_rt.start_cat_cd'" );

      } else{
        ar.select("log_rt.start_cat_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.end_cat_cd as 'log_rt.end_cat_cd'" );

      } else{
        ar.select("log_rt.end_cat_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.time_id as 'log_rt.time_id'" );

      } else{
        ar.select("log_rt.time_id");

      }
      
      if (alias === true) {
        ar.select("log_rt.rt_cd as 'log_rt.rt_cd'" );

      } else{
        ar.select("log_rt.rt_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.rt_status_cd as 'log_rt.rt_status_cd'" );

      } else{
        ar.select("log_rt.rt_status_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.biz_cd as 'log_rt.biz_cd'" );

      } else{
        ar.select("log_rt.biz_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.commute_cd as 'log_rt.commute_cd'" );

      } else{
        ar.select("log_rt.commute_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.activate_cd as 'log_rt.activate_cd'" );

      } else{
        ar.select("log_rt.activate_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.seat_cd as 'log_rt.seat_cd'" );

      } else{
        ar.select("log_rt.seat_cd");

      }
      
      if (alias === true) {
        ar.select("log_rt.start_day as 'log_rt.start_day'" );

      } else{
        ar.select("log_rt.start_day");

      }
      
      if (alias === true) {
        ar.select("log_rt.end_day as 'log_rt.end_day'" );

      } else{
        ar.select("log_rt.end_day");

      }
      
      if (alias === true) {
        ar.select("log_rt.close_day as 'log_rt.close_day'" );

      } else{
        ar.select("log_rt.close_day");

      }
      
      if (alias === true) {
        ar.select("log_rt.min_user_cnt as 'log_rt.min_user_cnt'" );

      } else{
        ar.select("log_rt.min_user_cnt");

      }
      
      if (alias === true) {
        ar.select("log_rt.created_at as 'log_rt.created_at'" );

      } else{
        ar.select("log_rt.created_at");

      }
      
      if (alias === true) {
        ar.select("log_rt.search_cd as 'log_rt.search_cd'" );

      } else{
        ar.select("log_rt.search_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_rt_id = 'log_rt.log_rt_id';
        if (alias !== undefined) {

          col_log_rt_id = `${alias}.log_rt_id`;

        }

        ar.select(`${col_log_rt_id} as '${col_log_rt_id}' `);

         
        let col_evt_id = 'log_rt.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_rt_id = 'log_rt.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_start_info = 'log_rt.start_info';
        if (alias !== undefined) {

          col_start_info = `${alias}.start_info`;

        }

        ar.select(`${col_start_info} as '${col_start_info}' `);

         
        let col_end_info = 'log_rt.end_info';
        if (alias !== undefined) {

          col_end_info = `${alias}.end_info`;

        }

        ar.select(`${col_end_info} as '${col_end_info}' `);

         
        let col_start_tag = 'log_rt.start_tag';
        if (alias !== undefined) {

          col_start_tag = `${alias}.start_tag`;

        }

        ar.select(`${col_start_tag} as '${col_start_tag}' `);

         
        let col_end_tag = 'log_rt.end_tag';
        if (alias !== undefined) {

          col_end_tag = `${alias}.end_tag`;

        }

        ar.select(`${col_end_tag} as '${col_end_tag}' `);

         
        let col_start_cat_cd = 'log_rt.start_cat_cd';
        if (alias !== undefined) {

          col_start_cat_cd = `${alias}.start_cat_cd`;

        }

        ar.select(`${col_start_cat_cd} as '${col_start_cat_cd}' `);

         
        let col_end_cat_cd = 'log_rt.end_cat_cd';
        if (alias !== undefined) {

          col_end_cat_cd = `${alias}.end_cat_cd`;

        }

        ar.select(`${col_end_cat_cd} as '${col_end_cat_cd}' `);

         
        let col_time_id = 'log_rt.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_rt_cd = 'log_rt.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_rt_status_cd = 'log_rt.rt_status_cd';
        if (alias !== undefined) {

          col_rt_status_cd = `${alias}.rt_status_cd`;

        }

        ar.select(`${col_rt_status_cd} as '${col_rt_status_cd}' `);

         
        let col_biz_cd = 'log_rt.biz_cd';
        if (alias !== undefined) {

          col_biz_cd = `${alias}.biz_cd`;

        }

        ar.select(`${col_biz_cd} as '${col_biz_cd}' `);

         
        let col_commute_cd = 'log_rt.commute_cd';
        if (alias !== undefined) {

          col_commute_cd = `${alias}.commute_cd`;

        }

        ar.select(`${col_commute_cd} as '${col_commute_cd}' `);

         
        let col_activate_cd = 'log_rt.activate_cd';
        if (alias !== undefined) {

          col_activate_cd = `${alias}.activate_cd`;

        }

        ar.select(`${col_activate_cd} as '${col_activate_cd}' `);

         
        let col_seat_cd = 'log_rt.seat_cd';
        if (alias !== undefined) {

          col_seat_cd = `${alias}.seat_cd`;

        }

        ar.select(`${col_seat_cd} as '${col_seat_cd}' `);

         
        let col_start_day = 'log_rt.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'log_rt.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_close_day = 'log_rt.close_day';
        if (alias !== undefined) {

          col_close_day = `${alias}.close_day`;

        }

        ar.select(`${col_close_day} as '${col_close_day}' `);

         
        let col_min_user_cnt = 'log_rt.min_user_cnt';
        if (alias !== undefined) {

          col_min_user_cnt = `${alias}.min_user_cnt`;

        }

        ar.select(`${col_min_user_cnt} as '${col_min_user_cnt}' `);

         
        let col_created_at = 'log_rt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_search_cd = 'log_rt.search_cd';
        if (alias !== undefined) {

          col_search_cd = `${alias}.search_cd`;

        }

        ar.select(`${col_search_cd} as '${col_search_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_line');
    
    if (nullCheck(form.logRtId) === true) {
      ar.set("l_no", form.logRtId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("l_rid", form.rtId);
    } 
    

    if (nullCheck(form.startInfo) === true) {
      ar.set("l_home", form.startInfo);
    } 
    

    if (nullCheck(form.endInfo) === true) {
      ar.set("l_work", form.endInfo);
    } 
    

    if (nullCheck(form.startTag) === true) {
      ar.set("l_home_detail", form.startTag);
    } 
    

    if (nullCheck(form.endTag) === true) {
      ar.set("l_work_detail", form.endTag);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("l_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("l_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("l_tid", form.timeId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("l_type", form.rtCd);
    } 
    

    if (nullCheck(form.rtStatusCd) === true) {
      ar.set("l_status", form.rtStatusCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("l_business", form.bizCd);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("l_commute", form.commuteCd);
    } 
    

    if (nullCheck(form.activateCd) === true) {
      ar.set("l_activation", form.activateCd);
    } 
    

    if (nullCheck(form.seatCd) === true) {
      ar.set("l_seat", form.seatCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.closeDay) === true) {
      ar.set("l_close_day", form.closeDay);
    } 
    

    if (nullCheck(form.minUserCnt) === true) {
      ar.set("l_max_person", form.minUserCnt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.searchCd) === true) {
      ar.set("l_search", form.searchCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_line');
    
    if (nullCheck(form.logRtId) === true) {
      ar.set("l_no", form.logRtId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("l_rid", form.rtId);
    } 
    

    if (nullCheck(form.startInfo) === true) {
      ar.set("l_home", form.startInfo);
    } 
    

    if (nullCheck(form.endInfo) === true) {
      ar.set("l_work", form.endInfo);
    } 
    

    if (nullCheck(form.startTag) === true) {
      ar.set("l_home_detail", form.startTag);
    } 
    

    if (nullCheck(form.endTag) === true) {
      ar.set("l_work_detail", form.endTag);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("l_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("l_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("l_tid", form.timeId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("l_type", form.rtCd);
    } 
    

    if (nullCheck(form.rtStatusCd) === true) {
      ar.set("l_status", form.rtStatusCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("l_business", form.bizCd);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("l_commute", form.commuteCd);
    } 
    

    if (nullCheck(form.activateCd) === true) {
      ar.set("l_activation", form.activateCd);
    } 
    

    if (nullCheck(form.seatCd) === true) {
      ar.set("l_seat", form.seatCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.closeDay) === true) {
      ar.set("l_close_day", form.closeDay);
    } 
    

    if (nullCheck(form.minUserCnt) === true) {
      ar.set("l_max_person", form.minUserCnt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.searchCd) === true) {
      ar.set("l_search", form.searchCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_rt_view log_rt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logRtId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    rtId: 'l_rid'
    , 

    startInfo: 'l_home'
    , 

    endInfo: 'l_work'
    , 

    startTag: 'l_home_detail'
    , 

    endTag: 'l_work_detail'
    , 

    startCatCd: 'l_home_c'
    , 

    endCatCd: 'l_work_c'
    , 

    timeId: 'l_tid'
    , 

    rtCd: 'l_type'
    , 

    rtStatusCd: 'l_status'
    , 

    bizCd: 'l_business'
    , 

    commuteCd: 'l_commute'
    , 

    activateCd: 'l_activation'
    , 

    seatCd: 'l_seat'
    , 

    startDay: 'l_start_day'
    , 

    endDay: 'l_end_day'
    , 

    closeDay: 'l_close_day'
    , 

    minUserCnt: 'l_max_person'
    , 

    createdAt: 'l_timestamp'
    , 

    searchCd: 'l_search'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_line');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_rt_view log_rt');
    
      ar.select("log_rt.log_rt_id");

    
    
      ar.select("log_rt.evt_id");

    
    
      ar.select("log_rt.rt_id");

    
    
      ar.select("log_rt.start_info");

    
    
      ar.select("log_rt.end_info");

    
    
      ar.select("log_rt.start_tag");

    
    
      ar.select("log_rt.end_tag");

    
    
      ar.select("log_rt.start_cat_cd");

    
    
      ar.select("log_rt.end_cat_cd");

    
    
      ar.select("log_rt.time_id");

    
    
      ar.select("log_rt.rt_cd");

    
    
      ar.select("log_rt.rt_status_cd");

    
    
      ar.select("log_rt.biz_cd");

    
    
      ar.select("log_rt.commute_cd");

    
    
      ar.select("log_rt.activate_cd");

    
    
      ar.select("log_rt.seat_cd");

    
    
      ar.select("log_rt.start_day");

    
    
      ar.select("log_rt.end_day");

    
    
      ar.select("log_rt.close_day");

    
    
      ar.select("log_rt.min_user_cnt");

    
    
      ar.select("log_rt.created_at");

    
    
      ar.select("log_rt.search_cd");

    
    
    return ar;
  }

  

  
}
export const LogRtSql =  new LogRtQuery()

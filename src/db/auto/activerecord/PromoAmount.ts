import { Ar, nullCheck } from "../../../util";
 
  import { IPromoAmount } from "../interface";


  class PromoAmountQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 promo_amount객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' promo_amount_view promo_amount');

    
      if (alias === true) {
        ar.select("promo_amount.promo_amount_id as 'promo_amount.promo_amount_id'" );

      } else{
        ar.select("promo_amount.promo_amount_id");

      }
      
      if (alias === true) {
        ar.select("promo_amount.promo_cd_id as 'promo_amount.promo_cd_id'" );

      } else{
        ar.select("promo_amount.promo_cd_id");

      }
      
      if (alias === true) {
        ar.select("promo_amount.amount as 'promo_amount.amount'" );

      } else{
        ar.select("promo_amount.amount");

      }
      
      if (alias === true) {
        ar.select("promo_amount.idx as 'promo_amount.idx'" );

      } else{
        ar.select("promo_amount.idx");

      }
      
      if (alias === true) {
        ar.select("promo_amount.created_at as 'promo_amount.created_at'" );

      } else{
        ar.select("promo_amount.created_at");

      }
      
      if (alias === true) {
        ar.select("promo_amount.updated_at as 'promo_amount.updated_at'" );

      } else{
        ar.select("promo_amount.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_promo_amount_id = 'promo_amount.promo_amount_id';
        if (alias !== undefined) {

          col_promo_amount_id = `${alias}.promo_amount_id`;

        }

        ar.select(`${col_promo_amount_id} as '${col_promo_amount_id}' `);

         
        let col_promo_cd_id = 'promo_amount.promo_cd_id';
        if (alias !== undefined) {

          col_promo_cd_id = `${alias}.promo_cd_id`;

        }

        ar.select(`${col_promo_cd_id} as '${col_promo_cd_id}' `);

         
        let col_amount = 'promo_amount.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_idx = 'promo_amount.idx';
        if (alias !== undefined) {

          col_idx = `${alias}.idx`;

        }

        ar.select(`${col_idx} as '${col_idx}' `);

         
        let col_created_at = 'promo_amount.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'promo_amount.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mp_price');
    
    if (nullCheck(form.promoAmountId) === true) {
      ar.set("mp_no", form.promoAmountId);
    } 
    

    if (nullCheck(form.promoCdId) === true) {
      ar.set("mp_cid", form.promoCdId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("mp_price", form.amount);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("mp_idx", form.idx);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mp_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mp_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mp_price');
    
    if (nullCheck(form.promoAmountId) === true) {
      ar.set("mp_no", form.promoAmountId);
    } 
    

    if (nullCheck(form.promoCdId) === true) {
      ar.set("mp_cid", form.promoCdId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("mp_price", form.amount);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("mp_idx", form.idx);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mp_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mp_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' promo_amount_view promo_amount');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    promoAmountId: 'mp_no'
    , 

    promoCdId: 'mp_cid'
    , 

    amount: 'mp_price'
    , 

    idx: 'mp_idx'
    , 

    createdAt: 'mp_timestamp'
    , 

    updatedAt: 'mp_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('mp_price');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' promo_amount_view promo_amount');
    
      ar.select("promo_amount.promo_amount_id");

    
    
      ar.select("promo_amount.promo_cd_id");

    
    
      ar.select("promo_amount.amount");

    
    
      ar.select("promo_amount.idx");

    
    
      ar.select("promo_amount.created_at");

    
    
      ar.select("promo_amount.updated_at");

    
    
    return ar;
  }

  

  
}
export const PromoAmountSql =  new PromoAmountQuery()

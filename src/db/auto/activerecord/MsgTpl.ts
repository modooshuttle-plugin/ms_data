import { Ar, nullCheck } from "../../../util";
 
  import { IMsgTpl } from "../interface";


  class MsgTplQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 msg_tpl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' msg_tpl_view msg_tpl');

    
      if (alias === true) {
        ar.select("msg_tpl.msg_tpl_id as 'msg_tpl.msg_tpl_id'" );

      } else{
        ar.select("msg_tpl.msg_tpl_id");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.rt_cd as 'msg_tpl.rt_cd'" );

      } else{
        ar.select("msg_tpl.rt_cd");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.title as 'msg_tpl.title'" );

      } else{
        ar.select("msg_tpl.title");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.subject as 'msg_tpl.subject'" );

      } else{
        ar.select("msg_tpl.subject");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.content as 'msg_tpl.content'" );

      } else{
        ar.select("msg_tpl.content");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.created_at as 'msg_tpl.created_at'" );

      } else{
        ar.select("msg_tpl.created_at");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.updated_at as 'msg_tpl.updated_at'" );

      } else{
        ar.select("msg_tpl.updated_at");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.ata_id as 'msg_tpl.ata_id'" );

      } else{
        ar.select("msg_tpl.ata_id");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.ata_tpl_id as 'msg_tpl.ata_tpl_id'" );

      } else{
        ar.select("msg_tpl.ata_tpl_id");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.utm_source as 'msg_tpl.utm_source'" );

      } else{
        ar.select("msg_tpl.utm_source");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.utm_medium as 'msg_tpl.utm_medium'" );

      } else{
        ar.select("msg_tpl.utm_medium");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.utm_campaign as 'msg_tpl.utm_campaign'" );

      } else{
        ar.select("msg_tpl.utm_campaign");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.use_yn as 'msg_tpl.use_yn'" );

      } else{
        ar.select("msg_tpl.use_yn");

      }
      
      if (alias === true) {
        ar.select("msg_tpl.kakao_opt as 'msg_tpl.kakao_opt'" );

      } else{
        ar.select("msg_tpl.kakao_opt");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_msg_tpl_id = 'msg_tpl.msg_tpl_id';
        if (alias !== undefined) {

          col_msg_tpl_id = `${alias}.msg_tpl_id`;

        }

        ar.select(`${col_msg_tpl_id} as '${col_msg_tpl_id}' `);

         
        let col_rt_cd = 'msg_tpl.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_title = 'msg_tpl.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_subject = 'msg_tpl.subject';
        if (alias !== undefined) {

          col_subject = `${alias}.subject`;

        }

        ar.select(`${col_subject} as '${col_subject}' `);

         
        let col_content = 'msg_tpl.content';
        if (alias !== undefined) {

          col_content = `${alias}.content`;

        }

        ar.select(`${col_content} as '${col_content}' `);

         
        let col_created_at = 'msg_tpl.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'msg_tpl.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_ata_id = 'msg_tpl.ata_id';
        if (alias !== undefined) {

          col_ata_id = `${alias}.ata_id`;

        }

        ar.select(`${col_ata_id} as '${col_ata_id}' `);

         
        let col_ata_tpl_id = 'msg_tpl.ata_tpl_id';
        if (alias !== undefined) {

          col_ata_tpl_id = `${alias}.ata_tpl_id`;

        }

        ar.select(`${col_ata_tpl_id} as '${col_ata_tpl_id}' `);

         
        let col_utm_source = 'msg_tpl.utm_source';
        if (alias !== undefined) {

          col_utm_source = `${alias}.utm_source`;

        }

        ar.select(`${col_utm_source} as '${col_utm_source}' `);

         
        let col_utm_medium = 'msg_tpl.utm_medium';
        if (alias !== undefined) {

          col_utm_medium = `${alias}.utm_medium`;

        }

        ar.select(`${col_utm_medium} as '${col_utm_medium}' `);

         
        let col_utm_campaign = 'msg_tpl.utm_campaign';
        if (alias !== undefined) {

          col_utm_campaign = `${alias}.utm_campaign`;

        }

        ar.select(`${col_utm_campaign} as '${col_utm_campaign}' `);

         
        let col_use_yn = 'msg_tpl.use_yn';
        if (alias !== undefined) {

          col_use_yn = `${alias}.use_yn`;

        }

        ar.select(`${col_use_yn} as '${col_use_yn}' `);

         
        let col_kakao_opt = 'msg_tpl.kakao_opt';
        if (alias !== undefined) {

          col_kakao_opt = `${alias}.kakao_opt`;

        }

        ar.select(`${col_kakao_opt} as '${col_kakao_opt}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_sms');
    
    if (nullCheck(form.msgTplId) === true) {
      ar.set("t_no", form.msgTplId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("t_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("t_title", form.title);
    } 
    

    if (nullCheck(form.subject) === true) {
      ar.set("t_subject", form.subject);
    } 
    

    if (nullCheck(form.content) === true) {
      ar.set("t_content", form.content);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("t_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("t_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.ataId) === true) {
      ar.set("t_ata_id", form.ataId);
    } 
    

    if (nullCheck(form.ataTplId) === true) {
      ar.set("ata_tpl_id", form.ataTplId);
    } 
    

    if (nullCheck(form.utmSource) === true) {
      ar.set("utm_source", form.utmSource);
    } 
    

    if (nullCheck(form.utmMedium) === true) {
      ar.set("utm_medium", form.utmMedium);
    } 
    

    if (nullCheck(form.utmCampaign) === true) {
      ar.set("utm_campaign", form.utmCampaign);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("use_yn", form.useYn);
    } 
    

    if (nullCheck(form.kakaoOpt) === true) {
      ar.set("kakao_opt", form.kakaoOpt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_sms');
    
    if (nullCheck(form.msgTplId) === true) {
      ar.set("t_no", form.msgTplId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("t_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("t_title", form.title);
    } 
    

    if (nullCheck(form.subject) === true) {
      ar.set("t_subject", form.subject);
    } 
    

    if (nullCheck(form.content) === true) {
      ar.set("t_content", form.content);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("t_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("t_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.ataId) === true) {
      ar.set("t_ata_id", form.ataId);
    } 
    

    if (nullCheck(form.ataTplId) === true) {
      ar.set("ata_tpl_id", form.ataTplId);
    } 
    

    if (nullCheck(form.utmSource) === true) {
      ar.set("utm_source", form.utmSource);
    } 
    

    if (nullCheck(form.utmMedium) === true) {
      ar.set("utm_medium", form.utmMedium);
    } 
    

    if (nullCheck(form.utmCampaign) === true) {
      ar.set("utm_campaign", form.utmCampaign);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("use_yn", form.useYn);
    } 
    

    if (nullCheck(form.kakaoOpt) === true) {
      ar.set("kakao_opt", form.kakaoOpt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_tpl_view msg_tpl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    msgTplId: 't_no'
    , 

    rtCd: 't_rtype'
    , 

    title: 't_title'
    , 

    subject: 't_subject'
    , 

    content: 't_content'
    , 

    createdAt: 't_timestamp'
    , 

    updatedAt: 't_timestamp_u'
    , 

    ataId: 't_ata_id'
    , 

    ataTplId: 'ata_tpl_id'
    , 

    utmSource: 'utm_source'
    , 

    utmMedium: 'utm_medium'
    , 

    utmCampaign: 'utm_campaign'
    , 

    useYn: 'use_yn'
    , 

    kakaoOpt: 'kakao_opt'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('t_sms');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_tpl_view msg_tpl');
    
      ar.select("msg_tpl.msg_tpl_id");

    
    
      ar.select("msg_tpl.rt_cd");

    
    
      ar.select("msg_tpl.title");

    
    
      ar.select("msg_tpl.subject");

    
    
      ar.select("msg_tpl.content");

    
    
      ar.select("msg_tpl.created_at");

    
    
      ar.select("msg_tpl.updated_at");

    
    
      ar.select("msg_tpl.ata_id");

    
    
      ar.select("msg_tpl.ata_tpl_id");

    
    
      ar.select("msg_tpl.utm_source");

    
    
      ar.select("msg_tpl.utm_medium");

    
    
      ar.select("msg_tpl.utm_campaign");

    
    
      ar.select("msg_tpl.use_yn");

    
    
      ar.select("msg_tpl.kakao_opt");

    
    
    return ar;
  }

  

  
}
export const MsgTplSql =  new MsgTplQuery()

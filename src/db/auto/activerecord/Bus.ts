import { Ar, nullCheck } from "../../../util";
 
  import { IBus } from "../interface";


  class BusQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 bus객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' bus_view bus');

    
      if (alias === true) {
        ar.select("bus.bus_id as 'bus.bus_id'" );

      } else{
        ar.select("bus.bus_id");

      }
      
      if (alias === true) {
        ar.select("bus.seat as 'bus.seat'" );

      } else{
        ar.select("bus.seat");

      }
      
      if (alias === true) {
        ar.select("bus.nm as 'bus.nm'" );

      } else{
        ar.select("bus.nm");

      }
      
      if (alias === true) {
        ar.select("bus.seat_cnt as 'bus.seat_cnt'" );

      } else{
        ar.select("bus.seat_cnt");

      }
      
      if (alias === true) {
        ar.select("bus.view_seat_cnt as 'bus.view_seat_cnt'" );

      } else{
        ar.select("bus.view_seat_cnt");

      }
      
      if (alias === true) {
        ar.select("bus.comment as 'bus.comment'" );

      } else{
        ar.select("bus.comment");

      }
      
      if (alias === true) {
        ar.select("bus.created_at as 'bus.created_at'" );

      } else{
        ar.select("bus.created_at");

      }
      
      if (alias === true) {
        ar.select("bus.updated_at as 'bus.updated_at'" );

      } else{
        ar.select("bus.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_bus_id = 'bus.bus_id';
        if (alias !== undefined) {

          col_bus_id = `${alias}.bus_id`;

        }

        ar.select(`${col_bus_id} as '${col_bus_id}' `);

         
        let col_seat = 'bus.seat';
        if (alias !== undefined) {

          col_seat = `${alias}.seat`;

        }

        ar.select(`${col_seat} as '${col_seat}' `);

         
        let col_nm = 'bus.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_seat_cnt = 'bus.seat_cnt';
        if (alias !== undefined) {

          col_seat_cnt = `${alias}.seat_cnt`;

        }

        ar.select(`${col_seat_cnt} as '${col_seat_cnt}' `);

         
        let col_view_seat_cnt = 'bus.view_seat_cnt';
        if (alias !== undefined) {

          col_view_seat_cnt = `${alias}.view_seat_cnt`;

        }

        ar.select(`${col_view_seat_cnt} as '${col_view_seat_cnt}' `);

         
        let col_comment = 'bus.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'bus.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'bus.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' b_bus');
    
    if (nullCheck(form.busId) === true) {
      ar.set("b_no", form.busId);
    } 
    

    if (nullCheck(form.seat) === true) {
      ar.set("b_seat", form.seat);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("b_name", form.nm);
    } 
    

    if (nullCheck(form.seatCnt) === true) {
      ar.set("b_cnt", form.seatCnt);
    } 
    

    if (nullCheck(form.viewSeatCnt) === true) {
      ar.set("b_cnt_view", form.viewSeatCnt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("b_etc", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("b_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("b_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' b_bus');
    
    if (nullCheck(form.busId) === true) {
      ar.set("b_no", form.busId);
    } 
    

    if (nullCheck(form.seat) === true) {
      ar.set("b_seat", form.seat);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("b_name", form.nm);
    } 
    

    if (nullCheck(form.seatCnt) === true) {
      ar.set("b_cnt", form.seatCnt);
    } 
    

    if (nullCheck(form.viewSeatCnt) === true) {
      ar.set("b_cnt_view", form.viewSeatCnt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("b_etc", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("b_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("b_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' bus_view bus');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    busId: 'b_no'
    , 

    seat: 'b_seat'
    , 

    nm: 'b_name'
    , 

    seatCnt: 'b_cnt'
    , 

    viewSeatCnt: 'b_cnt_view'
    , 

    comment: 'b_etc'
    , 

    createdAt: 'b_timestamp'
    , 

    updatedAt: 'b_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('b_bus');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' bus_view bus');
    
      ar.select("bus.bus_id");

    
    
      ar.select("bus.seat");

    
    
      ar.select("bus.nm");

    
    
      ar.select("bus.seat_cnt");

    
    
      ar.select("bus.view_seat_cnt");

    
    
      ar.select("bus.comment");

    
    
      ar.select("bus.created_at");

    
    
      ar.select("bus.updated_at");

    
    
    return ar;
  }

  

  
}
export const BusSql =  new BusQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IRdt } from "../interface";


  class RdtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rdt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rdt_view rdt');

    
      if (alias === true) {
        ar.select("rdt.rdt_id as 'rdt.rdt_id'" );

      } else{
        ar.select("rdt.rdt_id");

      }
      
      if (alias === true) {
        ar.select("rdt.url as 'rdt.url'" );

      } else{
        ar.select("rdt.url");

      }
      
      if (alias === true) {
        ar.select("rdt.created_at as 'rdt.created_at'" );

      } else{
        ar.select("rdt.created_at");

      }
      
      if (alias === true) {
        ar.select("rdt.action_cd as 'rdt.action_cd'" );

      } else{
        ar.select("rdt.action_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rdt_id = 'rdt.rdt_id';
        if (alias !== undefined) {

          col_rdt_id = `${alias}.rdt_id`;

        }

        ar.select(`${col_rdt_id} as '${col_rdt_id}' `);

         
        let col_url = 'rdt.url';
        if (alias !== undefined) {

          col_url = `${alias}.url`;

        }

        ar.select(`${col_url} as '${col_url}' `);

         
        let col_created_at = 'rdt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_action_cd = 'rdt.action_cd';
        if (alias !== undefined) {

          col_action_cd = `${alias}.action_cd`;

        }

        ar.select(`${col_action_cd} as '${col_action_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_redirect');
    
    if (nullCheck(form.rdtId) === true) {
      ar.set("a_no", form.rdtId);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("a_url", form.url);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.actionCd) === true) {
      ar.set("a_action", form.actionCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_redirect');
    
    if (nullCheck(form.rdtId) === true) {
      ar.set("a_no", form.rdtId);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("a_url", form.url);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.actionCd) === true) {
      ar.set("a_action", form.actionCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rdt_view rdt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rdtId: 'a_no'
    , 

    url: 'a_url'
    , 

    createdAt: 'a_timestamp'
    , 

    actionCd: 'a_action'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_redirect');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rdt_view rdt');
    
      ar.select("rdt.rdt_id");

    
    
      ar.select("rdt.url");

    
    
      ar.select("rdt.created_at");

    
    
      ar.select("rdt.action_cd");

    
    
    return ar;
  }

  

  
}
export const RdtSql =  new RdtQuery()

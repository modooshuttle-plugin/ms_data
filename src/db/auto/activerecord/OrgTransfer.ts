import { Ar, nullCheck } from "../../../util";
 
  import { IOrgTransfer } from "../interface";


  class OrgTransferQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_transfer객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_transfer_view org_transfer');

    
      if (alias === true) {
        ar.select("org_transfer.org_transfer_id as 'org_transfer.org_transfer_id'" );

      } else{
        ar.select("org_transfer.org_transfer_id");

      }
      
      if (alias === true) {
        ar.select("org_transfer.prods_cd as 'org_transfer.prods_cd'" );

      } else{
        ar.select("org_transfer.prods_cd");

      }
      
      if (alias === true) {
        ar.select("org_transfer.org_setl_id as 'org_transfer.org_setl_id'" );

      } else{
        ar.select("org_transfer.org_setl_id");

      }
      
      if (alias === true) {
        ar.select("org_transfer.org_id as 'org_transfer.org_id'" );

      } else{
        ar.select("org_transfer.org_id");

      }
      
      if (alias === true) {
        ar.select("org_transfer.transfer_at as 'org_transfer.transfer_at'" );

      } else{
        ar.select("org_transfer.transfer_at");

      }
      
      if (alias === true) {
        ar.select("org_transfer.amount as 'org_transfer.amount'" );

      } else{
        ar.select("org_transfer.amount");

      }
      
      if (alias === true) {
        ar.select("org_transfer.created_at as 'org_transfer.created_at'" );

      } else{
        ar.select("org_transfer.created_at");

      }
      
      if (alias === true) {
        ar.select("org_transfer.updated_at as 'org_transfer.updated_at'" );

      } else{
        ar.select("org_transfer.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_transfer.admin_id as 'org_transfer.admin_id'" );

      } else{
        ar.select("org_transfer.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_transfer_id = 'org_transfer.org_transfer_id';
        if (alias !== undefined) {

          col_org_transfer_id = `${alias}.org_transfer_id`;

        }

        ar.select(`${col_org_transfer_id} as '${col_org_transfer_id}' `);

         
        let col_prods_cd = 'org_transfer.prods_cd';
        if (alias !== undefined) {

          col_prods_cd = `${alias}.prods_cd`;

        }

        ar.select(`${col_prods_cd} as '${col_prods_cd}' `);

         
        let col_org_setl_id = 'org_transfer.org_setl_id';
        if (alias !== undefined) {

          col_org_setl_id = `${alias}.org_setl_id`;

        }

        ar.select(`${col_org_setl_id} as '${col_org_setl_id}' `);

         
        let col_org_id = 'org_transfer.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_transfer_at = 'org_transfer.transfer_at';
        if (alias !== undefined) {

          col_transfer_at = `${alias}.transfer_at`;

        }

        ar.select(`${col_transfer_at} as '${col_transfer_at}' `);

         
        let col_amount = 'org_transfer.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'org_transfer.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_transfer.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org_transfer.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_transfer');
    
    if (nullCheck(form.orgTransferId) === true) {
      ar.set("org_transfer_id", form.orgTransferId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("prods_cd", form.prodsCd);
    } 
    

    if (nullCheck(form.orgSetlId) === true) {
      ar.set("org_setl_id", form.orgSetlId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.transferAt) === true) {
      ar.set("transfer_at", form.transferAt);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_transfer');
    
    if (nullCheck(form.orgTransferId) === true) {
      ar.set("org_transfer_id", form.orgTransferId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("prods_cd", form.prodsCd);
    } 
    

    if (nullCheck(form.orgSetlId) === true) {
      ar.set("org_setl_id", form.orgSetlId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.transferAt) === true) {
      ar.set("transfer_at", form.transferAt);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_transfer_view org_transfer');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgTransferId: 'org_transfer_id'
    , 

    prodsCd: 'prods_cd'
    , 

    orgSetlId: 'org_setl_id'
    , 

    orgId: 'org_id'
    , 

    transferAt: 'transfer_at'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_transfer');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_transfer_view org_transfer');
    
      ar.select("org_transfer.org_transfer_id");

    
    
      ar.select("org_transfer.prods_cd");

    
    
      ar.select("org_transfer.org_setl_id");

    
    
      ar.select("org_transfer.org_id");

    
    
      ar.select("org_transfer.transfer_at");

    
    
      ar.select("org_transfer.amount");

    
    
      ar.select("org_transfer.created_at");

    
    
      ar.select("org_transfer.updated_at");

    
    
      ar.select("org_transfer.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgTransferSql =  new OrgTransferQuery()

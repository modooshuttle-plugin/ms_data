import { Ar, nullCheck } from "../../../util";
 
  import { IPaidCpn } from "../interface";


  class PaidCpnQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 paid_cpn객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' paid_cpn_view paid_cpn');

    
      if (alias === true) {
        ar.select("paid_cpn.paid_cpn_id as 'paid_cpn.paid_cpn_id'" );

      } else{
        ar.select("paid_cpn.paid_cpn_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.cpn_tpl_id as 'paid_cpn.cpn_tpl_id'" );

      } else{
        ar.select("paid_cpn.cpn_tpl_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.paid_id as 'paid_cpn.paid_id'" );

      } else{
        ar.select("paid_cpn.paid_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.rt_id as 'paid_cpn.rt_id'" );

      } else{
        ar.select("paid_cpn.rt_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.user_id as 'paid_cpn.user_id'" );

      } else{
        ar.select("paid_cpn.user_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.board_id as 'paid_cpn.board_id'" );

      } else{
        ar.select("paid_cpn.board_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.pay_id as 'paid_cpn.pay_id'" );

      } else{
        ar.select("paid_cpn.pay_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.cpn_id as 'paid_cpn.cpn_id'" );

      } else{
        ar.select("paid_cpn.cpn_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.cpn_use_id as 'paid_cpn.cpn_use_id'" );

      } else{
        ar.select("paid_cpn.cpn_use_id");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.order_no as 'paid_cpn.order_no'" );

      } else{
        ar.select("paid_cpn.order_no");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.use_yn as 'paid_cpn.use_yn'" );

      } else{
        ar.select("paid_cpn.use_yn");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.amount as 'paid_cpn.amount'" );

      } else{
        ar.select("paid_cpn.amount");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.created_at as 'paid_cpn.created_at'" );

      } else{
        ar.select("paid_cpn.created_at");

      }
      
      if (alias === true) {
        ar.select("paid_cpn.updated_at as 'paid_cpn.updated_at'" );

      } else{
        ar.select("paid_cpn.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_paid_cpn_id = 'paid_cpn.paid_cpn_id';
        if (alias !== undefined) {

          col_paid_cpn_id = `${alias}.paid_cpn_id`;

        }

        ar.select(`${col_paid_cpn_id} as '${col_paid_cpn_id}' `);

         
        let col_cpn_tpl_id = 'paid_cpn.cpn_tpl_id';
        if (alias !== undefined) {

          col_cpn_tpl_id = `${alias}.cpn_tpl_id`;

        }

        ar.select(`${col_cpn_tpl_id} as '${col_cpn_tpl_id}' `);

         
        let col_paid_id = 'paid_cpn.paid_id';
        if (alias !== undefined) {

          col_paid_id = `${alias}.paid_id`;

        }

        ar.select(`${col_paid_id} as '${col_paid_id}' `);

         
        let col_rt_id = 'paid_cpn.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'paid_cpn.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_board_id = 'paid_cpn.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_pay_id = 'paid_cpn.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_cpn_id = 'paid_cpn.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_cpn_use_id = 'paid_cpn.cpn_use_id';
        if (alias !== undefined) {

          col_cpn_use_id = `${alias}.cpn_use_id`;

        }

        ar.select(`${col_cpn_use_id} as '${col_cpn_use_id}' `);

         
        let col_order_no = 'paid_cpn.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_use_yn = 'paid_cpn.use_yn';
        if (alias !== undefined) {

          col_use_yn = `${alias}.use_yn`;

        }

        ar.select(`${col_use_yn} as '${col_use_yn}' `);

         
        let col_amount = 'paid_cpn.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'paid_cpn.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'paid_cpn.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid_cpn');
    
    if (nullCheck(form.paidCpnId) === true) {
      ar.set("paid_cpn_id", form.paidCpnId);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("cpn_tpl_id", form.cpnTplId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("cpn_id", form.cpnId);
    } 
    

    if (nullCheck(form.cpnUseId) === true) {
      ar.set("cpn_use_id", form.cpnUseId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("use_yn", form.useYn);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid_cpn');
    
    if (nullCheck(form.paidCpnId) === true) {
      ar.set("paid_cpn_id", form.paidCpnId);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("cpn_tpl_id", form.cpnTplId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("cpn_id", form.cpnId);
    } 
    

    if (nullCheck(form.cpnUseId) === true) {
      ar.set("cpn_use_id", form.cpnUseId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("use_yn", form.useYn);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_cpn_view paid_cpn');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    paidCpnId: 'paid_cpn_id'
    , 

    cpnTplId: 'cpn_tpl_id'
    , 

    paidId: 'paid_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    boardId: 'board_id'
    , 

    payId: 'pay_id'
    , 

    cpnId: 'cpn_id'
    , 

    cpnUseId: 'cpn_use_id'
    , 

    orderNo: 'order_no'
    , 

    useYn: 'use_yn'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('paid_cpn');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_cpn_view paid_cpn');
    
      ar.select("paid_cpn.paid_cpn_id");

    
    
      ar.select("paid_cpn.cpn_tpl_id");

    
    
      ar.select("paid_cpn.paid_id");

    
    
      ar.select("paid_cpn.rt_id");

    
    
      ar.select("paid_cpn.user_id");

    
    
      ar.select("paid_cpn.board_id");

    
    
      ar.select("paid_cpn.pay_id");

    
    
      ar.select("paid_cpn.cpn_id");

    
    
      ar.select("paid_cpn.cpn_use_id");

    
    
      ar.select("paid_cpn.order_no");

    
    
      ar.select("paid_cpn.use_yn");

    
    
      ar.select("paid_cpn.amount");

    
    
      ar.select("paid_cpn.created_at");

    
    
      ar.select("paid_cpn.updated_at");

    
    
    return ar;
  }

  

  
}
export const PaidCpnSql =  new PaidCpnQuery()

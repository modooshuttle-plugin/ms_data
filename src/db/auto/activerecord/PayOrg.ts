import { Ar, nullCheck } from "../../../util";
 
  import { IPayOrg } from "../interface";


  class PayOrgQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 pay_org객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' pay_org_view pay_org');

    
      if (alias === true) {
        ar.select("pay_org.pay_org_id as 'pay_org.pay_org_id'" );

      } else{
        ar.select("pay_org.pay_org_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.pay_org_cd as 'pay_org.pay_org_cd'" );

      } else{
        ar.select("pay_org.pay_org_cd");

      }
      
      if (alias === true) {
        ar.select("pay_org.pay_id as 'pay_org.pay_id'" );

      } else{
        ar.select("pay_org.pay_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.rt_id as 'pay_org.rt_id'" );

      } else{
        ar.select("pay_org.rt_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.user_id as 'pay_org.user_id'" );

      } else{
        ar.select("pay_org.user_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.board_id as 'pay_org.board_id'" );

      } else{
        ar.select("pay_org.board_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.org_id as 'pay_org.org_id'" );

      } else{
        ar.select("pay_org.org_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.org_ctrt_id as 'pay_org.org_ctrt_id'" );

      } else{
        ar.select("pay_org.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.org_user_auth_id as 'pay_org.org_user_auth_id'" );

      } else{
        ar.select("pay_org.org_user_auth_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.email_auth_id as 'pay_org.email_auth_id'" );

      } else{
        ar.select("pay_org.email_auth_id");

      }
      
      if (alias === true) {
        ar.select("pay_org.order_no as 'pay_org.order_no'" );

      } else{
        ar.select("pay_org.order_no");

      }
      
      if (alias === true) {
        ar.select("pay_org.ratio as 'pay_org.ratio'" );

      } else{
        ar.select("pay_org.ratio");

      }
      
      if (alias === true) {
        ar.select("pay_org.amount as 'pay_org.amount'" );

      } else{
        ar.select("pay_org.amount");

      }
      
      if (alias === true) {
        ar.select("pay_org.created_at as 'pay_org.created_at'" );

      } else{
        ar.select("pay_org.created_at");

      }
      
      if (alias === true) {
        ar.select("pay_org.updated_at as 'pay_org.updated_at'" );

      } else{
        ar.select("pay_org.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_pay_org_id = 'pay_org.pay_org_id';
        if (alias !== undefined) {

          col_pay_org_id = `${alias}.pay_org_id`;

        }

        ar.select(`${col_pay_org_id} as '${col_pay_org_id}' `);

         
        let col_pay_org_cd = 'pay_org.pay_org_cd';
        if (alias !== undefined) {

          col_pay_org_cd = `${alias}.pay_org_cd`;

        }

        ar.select(`${col_pay_org_cd} as '${col_pay_org_cd}' `);

         
        let col_pay_id = 'pay_org.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_rt_id = 'pay_org.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'pay_org.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_board_id = 'pay_org.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_org_id = 'pay_org.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_org_ctrt_id = 'pay_org.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_org_user_auth_id = 'pay_org.org_user_auth_id';
        if (alias !== undefined) {

          col_org_user_auth_id = `${alias}.org_user_auth_id`;

        }

        ar.select(`${col_org_user_auth_id} as '${col_org_user_auth_id}' `);

         
        let col_email_auth_id = 'pay_org.email_auth_id';
        if (alias !== undefined) {

          col_email_auth_id = `${alias}.email_auth_id`;

        }

        ar.select(`${col_email_auth_id} as '${col_email_auth_id}' `);

         
        let col_order_no = 'pay_org.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_ratio = 'pay_org.ratio';
        if (alias !== undefined) {

          col_ratio = `${alias}.ratio`;

        }

        ar.select(`${col_ratio} as '${col_ratio}' `);

         
        let col_amount = 'pay_org.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'pay_org.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'pay_org.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' pay_org');
    
    if (nullCheck(form.payOrgId) === true) {
      ar.set("pay_org_id", form.payOrgId);
    } 
    

    if (nullCheck(form.payOrgCd) === true) {
      ar.set("pay_org_cd", form.payOrgCd);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.emailAuthId) === true) {
      ar.set("email_auth_id", form.emailAuthId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.ratio) === true) {
      ar.set("ratio", form.ratio);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' pay_org');
    
    if (nullCheck(form.payOrgId) === true) {
      ar.set("pay_org_id", form.payOrgId);
    } 
    

    if (nullCheck(form.payOrgCd) === true) {
      ar.set("pay_org_cd", form.payOrgCd);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.emailAuthId) === true) {
      ar.set("email_auth_id", form.emailAuthId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.ratio) === true) {
      ar.set("ratio", form.ratio);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_org_view pay_org');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    payOrgId: 'pay_org_id'
    , 

    payOrgCd: 'pay_org_cd'
    , 

    payId: 'pay_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    boardId: 'board_id'
    , 

    orgId: 'org_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    orgUserAuthId: 'org_user_auth_id'
    , 

    emailAuthId: 'email_auth_id'
    , 

    orderNo: 'order_no'
    , 

    ratio: 'ratio'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('pay_org');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_org_view pay_org');
    
      ar.select("pay_org.pay_org_id");

    
    
      ar.select("pay_org.pay_org_cd");

    
    
      ar.select("pay_org.pay_id");

    
    
      ar.select("pay_org.rt_id");

    
    
      ar.select("pay_org.user_id");

    
    
      ar.select("pay_org.board_id");

    
    
      ar.select("pay_org.org_id");

    
    
      ar.select("pay_org.org_ctrt_id");

    
    
      ar.select("pay_org.org_user_auth_id");

    
    
      ar.select("pay_org.email_auth_id");

    
    
      ar.select("pay_org.order_no");

    
    
      ar.select("pay_org.ratio");

    
    
      ar.select("pay_org.amount");

    
    
      ar.select("pay_org.created_at");

    
    
      ar.select("pay_org.updated_at");

    
    
    return ar;
  }

  

  
}
export const PayOrgSql =  new PayOrgQuery()

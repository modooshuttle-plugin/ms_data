import { Ar, nullCheck } from "../../../util";
 
  import { ICpn } from "../interface";


  class CpnQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cpn객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cpn_view cpn');

    
      if (alias === true) {
        ar.select("cpn.cpn_id as 'cpn.cpn_id'" );

      } else{
        ar.select("cpn.cpn_id");

      }
      
      if (alias === true) {
        ar.select("cpn.cpn_tpl_id as 'cpn.cpn_tpl_id'" );

      } else{
        ar.select("cpn.cpn_tpl_id");

      }
      
      if (alias === true) {
        ar.select("cpn.nm as 'cpn.nm'" );

      } else{
        ar.select("cpn.nm");

      }
      
      if (alias === true) {
        ar.select("cpn.cal_cd as 'cpn.cal_cd'" );

      } else{
        ar.select("cpn.cal_cd");

      }
      
      if (alias === true) {
        ar.select("cpn.status_cd as 'cpn.status_cd'" );

      } else{
        ar.select("cpn.status_cd");

      }
      
      if (alias === true) {
        ar.select("cpn.func as 'cpn.func'" );

      } else{
        ar.select("cpn.func");

      }
      
      if (alias === true) {
        ar.select("cpn.amount as 'cpn.amount'" );

      } else{
        ar.select("cpn.amount");

      }
      
      if (alias === true) {
        ar.select("cpn.sign_cd as 'cpn.sign_cd'" );

      } else{
        ar.select("cpn.sign_cd");

      }
      
      if (alias === true) {
        ar.select("cpn.user_id as 'cpn.user_id'" );

      } else{
        ar.select("cpn.user_id");

      }
      
      if (alias === true) {
        ar.select("cpn.admin_id as 'cpn.admin_id'" );

      } else{
        ar.select("cpn.admin_id");

      }
      
      if (alias === true) {
        ar.select("cpn.promo_cd as 'cpn.promo_cd'" );

      } else{
        ar.select("cpn.promo_cd");

      }
      
      if (alias === true) {
        ar.select("cpn.start_day as 'cpn.start_day'" );

      } else{
        ar.select("cpn.start_day");

      }
      
      if (alias === true) {
        ar.select("cpn.end_day as 'cpn.end_day'" );

      } else{
        ar.select("cpn.end_day");

      }
      
      if (alias === true) {
        ar.select("cpn.created_at as 'cpn.created_at'" );

      } else{
        ar.select("cpn.created_at");

      }
      
      if (alias === true) {
        ar.select("cpn.updated_at as 'cpn.updated_at'" );

      } else{
        ar.select("cpn.updated_at");

      }
      
      if (alias === true) {
        ar.select("cpn.dupe_yn as 'cpn.dupe_yn'" );

      } else{
        ar.select("cpn.dupe_yn");

      }
      
      if (alias === true) {
        ar.select("cpn.min_default_amount as 'cpn.min_default_amount'" );

      } else{
        ar.select("cpn.min_default_amount");

      }
      
      if (alias === true) {
        ar.select("cpn.use_cd as 'cpn.use_cd'" );

      } else{
        ar.select("cpn.use_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cpn_id = 'cpn.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_cpn_tpl_id = 'cpn.cpn_tpl_id';
        if (alias !== undefined) {

          col_cpn_tpl_id = `${alias}.cpn_tpl_id`;

        }

        ar.select(`${col_cpn_tpl_id} as '${col_cpn_tpl_id}' `);

         
        let col_nm = 'cpn.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_cal_cd = 'cpn.cal_cd';
        if (alias !== undefined) {

          col_cal_cd = `${alias}.cal_cd`;

        }

        ar.select(`${col_cal_cd} as '${col_cal_cd}' `);

         
        let col_status_cd = 'cpn.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_func = 'cpn.func';
        if (alias !== undefined) {

          col_func = `${alias}.func`;

        }

        ar.select(`${col_func} as '${col_func}' `);

         
        let col_amount = 'cpn.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_sign_cd = 'cpn.sign_cd';
        if (alias !== undefined) {

          col_sign_cd = `${alias}.sign_cd`;

        }

        ar.select(`${col_sign_cd} as '${col_sign_cd}' `);

         
        let col_user_id = 'cpn.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_admin_id = 'cpn.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_promo_cd = 'cpn.promo_cd';
        if (alias !== undefined) {

          col_promo_cd = `${alias}.promo_cd`;

        }

        ar.select(`${col_promo_cd} as '${col_promo_cd}' `);

         
        let col_start_day = 'cpn.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'cpn.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_created_at = 'cpn.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cpn.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_dupe_yn = 'cpn.dupe_yn';
        if (alias !== undefined) {

          col_dupe_yn = `${alias}.dupe_yn`;

        }

        ar.select(`${col_dupe_yn} as '${col_dupe_yn}' `);

         
        let col_min_default_amount = 'cpn.min_default_amount';
        if (alias !== undefined) {

          col_min_default_amount = `${alias}.min_default_amount`;

        }

        ar.select(`${col_min_default_amount} as '${col_min_default_amount}' `);

         
        let col_use_cd = 'cpn.use_cd';
        if (alias !== undefined) {

          col_use_cd = `${alias}.use_cd`;

        }

        ar.select(`${col_use_cd} as '${col_use_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_coupon');
    
    if (nullCheck(form.cpnId) === true) {
      ar.set("a_no", form.cpnId);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("a_temp", form.cpnTplId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("a_name", form.nm);
    } 
    

    if (nullCheck(form.calCd) === true) {
      ar.set("a_type", form.calCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("a_status", form.statusCd);
    } 
    

    if (nullCheck(form.func) === true) {
      ar.set("a_func", form.func);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("a_value", form.amount);
    } 
    

    if (nullCheck(form.signCd) === true) {
      ar.set("a_sign", form.signCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    

    if (nullCheck(form.promoCd) === true) {
      ar.set("a_mp_code", form.promoCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("a_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("a_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.dupeYn) === true) {
      ar.set("dupe_yn", form.dupeYn);
    } 
    

    if (nullCheck(form.minDefaultAmount) === true) {
      ar.set("min_default_amount", form.minDefaultAmount);
    } 
    

    if (nullCheck(form.useCd) === true) {
      ar.set("use_cd", form.useCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_coupon');
    
    if (nullCheck(form.cpnId) === true) {
      ar.set("a_no", form.cpnId);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("a_temp", form.cpnTplId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("a_name", form.nm);
    } 
    

    if (nullCheck(form.calCd) === true) {
      ar.set("a_type", form.calCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("a_status", form.statusCd);
    } 
    

    if (nullCheck(form.func) === true) {
      ar.set("a_func", form.func);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("a_value", form.amount);
    } 
    

    if (nullCheck(form.signCd) === true) {
      ar.set("a_sign", form.signCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    

    if (nullCheck(form.promoCd) === true) {
      ar.set("a_mp_code", form.promoCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("a_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("a_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.dupeYn) === true) {
      ar.set("dupe_yn", form.dupeYn);
    } 
    

    if (nullCheck(form.minDefaultAmount) === true) {
      ar.set("min_default_amount", form.minDefaultAmount);
    } 
    

    if (nullCheck(form.useCd) === true) {
      ar.set("use_cd", form.useCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_view cpn');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    cpnId: 'a_no'
    , 

    cpnTplId: 'a_temp'
    , 

    nm: 'a_name'
    , 

    calCd: 'a_type'
    , 

    statusCd: 'a_status'
    , 

    func: 'a_func'
    , 

    amount: 'a_value'
    , 

    signCd: 'a_sign'
    , 

    userId: 'a_mid'
    , 

    adminId: 'a_admin'
    , 

    promoCd: 'a_mp_code'
    , 

    startDay: 'a_start_day'
    , 

    endDay: 'a_end_day'
    , 

    createdAt: 'a_timestamp'
    , 

    updatedAt: 'a_timestamp_u'
    , 

    dupeYn: 'dupe_yn'
    , 

    minDefaultAmount: 'min_default_amount'
    , 

    useCd: 'use_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_coupon');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_view cpn');
    
      ar.select("cpn.cpn_id");

    
    
      ar.select("cpn.cpn_tpl_id");

    
    
      ar.select("cpn.nm");

    
    
      ar.select("cpn.cal_cd");

    
    
      ar.select("cpn.status_cd");

    
    
      ar.select("cpn.func");

    
    
      ar.select("cpn.amount");

    
    
      ar.select("cpn.sign_cd");

    
    
      ar.select("cpn.user_id");

    
    
      ar.select("cpn.admin_id");

    
    
      ar.select("cpn.promo_cd");

    
    
      ar.select("cpn.start_day");

    
    
      ar.select("cpn.end_day");

    
    
      ar.select("cpn.created_at");

    
    
      ar.select("cpn.updated_at");

    
    
      ar.select("cpn.dupe_yn");

    
    
      ar.select("cpn.min_default_amount");

    
    
      ar.select("cpn.use_cd");

    
    
    return ar;
  }

  

  
}
export const CpnSql =  new CpnQuery()

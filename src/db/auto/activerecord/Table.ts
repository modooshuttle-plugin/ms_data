import { Ar, nullCheck } from "../../../util";
 
  import { ITable } from "../interface";


  class TableQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 table객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' table_view table');

    
      if (alias === true) {
        ar.select("table.table_id as 'table.table_id'" );

      } else{
        ar.select("table.table_id");

      }
      
      if (alias === true) {
        ar.select("table.target_cd as 'table.target_cd'" );

      } else{
        ar.select("table.target_cd");

      }
      
      if (alias === true) {
        ar.select("table.nm as 'table.nm'" );

      } else{
        ar.select("table.nm");

      }
      
      if (alias === true) {
        ar.select("table.old_nm as 'table.old_nm'" );

      } else{
        ar.select("table.old_nm");

      }
      
      if (alias === true) {
        ar.select("table.kor_nm as 'table.kor_nm'" );

      } else{
        ar.select("table.kor_nm");

      }
      
      if (alias === true) {
        ar.select("table.ver as 'table.ver'" );

      } else{
        ar.select("table.ver");

      }
      
      if (alias === true) {
        ar.select("table.db as 'table.db'" );

      } else{
        ar.select("table.db");

      }
      
      if (alias === true) {
        ar.select("table.comment as 'table.comment'" );

      } else{
        ar.select("table.comment");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_table_id = 'table.table_id';
        if (alias !== undefined) {

          col_table_id = `${alias}.table_id`;

        }

        ar.select(`${col_table_id} as '${col_table_id}' `);

         
        let col_target_cd = 'table.target_cd';
        if (alias !== undefined) {

          col_target_cd = `${alias}.target_cd`;

        }

        ar.select(`${col_target_cd} as '${col_target_cd}' `);

         
        let col_nm = 'table.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_old_nm = 'table.old_nm';
        if (alias !== undefined) {

          col_old_nm = `${alias}.old_nm`;

        }

        ar.select(`${col_old_nm} as '${col_old_nm}' `);

         
        let col_kor_nm = 'table.kor_nm';
        if (alias !== undefined) {

          col_kor_nm = `${alias}.kor_nm`;

        }

        ar.select(`${col_kor_nm} as '${col_kor_nm}' `);

         
        let col_ver = 'table.ver';
        if (alias !== undefined) {

          col_ver = `${alias}.ver`;

        }

        ar.select(`${col_ver} as '${col_ver}' `);

         
        let col_db = 'table.db';
        if (alias !== undefined) {

          col_db = `${alias}.db`;

        }

        ar.select(`${col_db} as '${col_db}' `);

         
        let col_comment = 'table.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' table');
    
    if (nullCheck(form.tableId) === true) {
      ar.set("table_id", form.tableId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.oldNm) === true) {
      ar.set("old_nm", form.oldNm);
    } 
    

    if (nullCheck(form.korNm) === true) {
      ar.set("kor_nm", form.korNm);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.db) === true) {
      ar.set("db", form.db);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' table');
    
    if (nullCheck(form.tableId) === true) {
      ar.set("table_id", form.tableId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.oldNm) === true) {
      ar.set("old_nm", form.oldNm);
    } 
    

    if (nullCheck(form.korNm) === true) {
      ar.set("kor_nm", form.korNm);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.db) === true) {
      ar.set("db", form.db);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' table_view table');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    tableId: 'table_id'
    , 

    targetCd: 'target_cd'
    , 

    nm: 'nm'
    , 

    oldNm: 'old_nm'
    , 

    korNm: 'kor_nm'
    , 

    ver: 'ver'
    , 

    db: 'db'
    , 

    comment: 'comment'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('table');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' table_view table');
    
      ar.select("table.table_id");

    
    
      ar.select("table.target_cd");

    
    
      ar.select("table.nm");

    
    
      ar.select("table.old_nm");

    
    
      ar.select("table.kor_nm");

    
    
      ar.select("table.ver");

    
    
      ar.select("table.db");

    
    
      ar.select("table.comment");

    
    
    return ar;
  }

  

  
}
export const TableSql =  new TableQuery()

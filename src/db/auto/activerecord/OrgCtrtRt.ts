import { Ar, nullCheck } from "../../../util";
 
  import { IOrgCtrtRt } from "../interface";


  class OrgCtrtRtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_ctrt_rt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_rt_view org_ctrt_rt');

    
      if (alias === true) {
        ar.select("org_ctrt_rt.org_ctrt_rt_id as 'org_ctrt_rt.org_ctrt_rt_id'" );

      } else{
        ar.select("org_ctrt_rt.org_ctrt_rt_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.org_ctrt_id as 'org_ctrt_rt.org_ctrt_id'" );

      } else{
        ar.select("org_ctrt_rt.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.org_id as 'org_ctrt_rt.org_id'" );

      } else{
        ar.select("org_ctrt_rt.org_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.rt_id as 'org_ctrt_rt.rt_id'" );

      } else{
        ar.select("org_ctrt_rt.rt_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.comment as 'org_ctrt_rt.comment'" );

      } else{
        ar.select("org_ctrt_rt.comment");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.created_at as 'org_ctrt_rt.created_at'" );

      } else{
        ar.select("org_ctrt_rt.created_at");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.updated_at as 'org_ctrt_rt.updated_at'" );

      } else{
        ar.select("org_ctrt_rt.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_rt.admin_id as 'org_ctrt_rt.admin_id'" );

      } else{
        ar.select("org_ctrt_rt.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_ctrt_rt_id = 'org_ctrt_rt.org_ctrt_rt_id';
        if (alias !== undefined) {

          col_org_ctrt_rt_id = `${alias}.org_ctrt_rt_id`;

        }

        ar.select(`${col_org_ctrt_rt_id} as '${col_org_ctrt_rt_id}' `);

         
        let col_org_ctrt_id = 'org_ctrt_rt.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_org_id = 'org_ctrt_rt.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_rt_id = 'org_ctrt_rt.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_comment = 'org_ctrt_rt.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'org_ctrt_rt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_ctrt_rt.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org_ctrt_rt.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_rt');
    
    if (nullCheck(form.orgCtrtRtId) === true) {
      ar.set("org_ctrt_rt_id", form.orgCtrtRtId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_rt');
    
    if (nullCheck(form.orgCtrtRtId) === true) {
      ar.set("org_ctrt_rt_id", form.orgCtrtRtId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_rt_view org_ctrt_rt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgCtrtRtId: 'org_ctrt_rt_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    orgId: 'org_id'
    , 

    rtId: 'rt_id'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_ctrt_rt');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_rt_view org_ctrt_rt');
    
      ar.select("org_ctrt_rt.org_ctrt_rt_id");

    
    
      ar.select("org_ctrt_rt.org_ctrt_id");

    
    
      ar.select("org_ctrt_rt.org_id");

    
    
      ar.select("org_ctrt_rt.rt_id");

    
    
      ar.select("org_ctrt_rt.comment");

    
    
      ar.select("org_ctrt_rt.created_at");

    
    
      ar.select("org_ctrt_rt.updated_at");

    
    
      ar.select("org_ctrt_rt.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgCtrtRtSql =  new OrgCtrtRtQuery()

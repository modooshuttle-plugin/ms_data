import { Ar, nullCheck } from "../../../util";
 
  import { IBizOrderDtl } from "../interface";


  class BizOrderDtlQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 biz_order_dtl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_dtl_view biz_order_dtl');

    
      if (alias === true) {
        ar.select("biz_order_dtl.biz_order_dtl_id as 'biz_order_dtl.biz_order_dtl_id'" );

      } else{
        ar.select("biz_order_dtl.biz_order_dtl_id");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.biz_order_id as 'biz_order_dtl.biz_order_id'" );

      } else{
        ar.select("biz_order_dtl.biz_order_id");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.bus_cd as 'biz_order_dtl.bus_cd'" );

      } else{
        ar.select("biz_order_dtl.bus_cd");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.bus_cnt as 'biz_order_dtl.bus_cnt'" );

      } else{
        ar.select("biz_order_dtl.bus_cnt");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.time_range as 'biz_order_dtl.time_range'" );

      } else{
        ar.select("biz_order_dtl.time_range");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.runn_cnt as 'biz_order_dtl.runn_cnt'" );

      } else{
        ar.select("biz_order_dtl.runn_cnt");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.runn_schd_day as 'biz_order_dtl.runn_schd_day'" );

      } else{
        ar.select("biz_order_dtl.runn_schd_day");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.start as 'biz_order_dtl.start'" );

      } else{
        ar.select("biz_order_dtl.start");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.move as 'biz_order_dtl.move'" );

      } else{
        ar.select("biz_order_dtl.move");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.end as 'biz_order_dtl.end'" );

      } else{
        ar.select("biz_order_dtl.end");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.comment as 'biz_order_dtl.comment'" );

      } else{
        ar.select("biz_order_dtl.comment");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.end_comment as 'biz_order_dtl.end_comment'" );

      } else{
        ar.select("biz_order_dtl.end_comment");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.created_at as 'biz_order_dtl.created_at'" );

      } else{
        ar.select("biz_order_dtl.created_at");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.section_cd as 'biz_order_dtl.section_cd'" );

      } else{
        ar.select("biz_order_dtl.section_cd");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.start_time as 'biz_order_dtl.start_time'" );

      } else{
        ar.select("biz_order_dtl.start_time");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.end_time as 'biz_order_dtl.end_time'" );

      } else{
        ar.select("biz_order_dtl.end_time");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.age as 'biz_order_dtl.age'" );

      } else{
        ar.select("biz_order_dtl.age");

      }
      
      if (alias === true) {
        ar.select("biz_order_dtl.updated_at as 'biz_order_dtl.updated_at'" );

      } else{
        ar.select("biz_order_dtl.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_biz_order_dtl_id = 'biz_order_dtl.biz_order_dtl_id';
        if (alias !== undefined) {

          col_biz_order_dtl_id = `${alias}.biz_order_dtl_id`;

        }

        ar.select(`${col_biz_order_dtl_id} as '${col_biz_order_dtl_id}' `);

         
        let col_biz_order_id = 'biz_order_dtl.biz_order_id';
        if (alias !== undefined) {

          col_biz_order_id = `${alias}.biz_order_id`;

        }

        ar.select(`${col_biz_order_id} as '${col_biz_order_id}' `);

         
        let col_bus_cd = 'biz_order_dtl.bus_cd';
        if (alias !== undefined) {

          col_bus_cd = `${alias}.bus_cd`;

        }

        ar.select(`${col_bus_cd} as '${col_bus_cd}' `);

         
        let col_bus_cnt = 'biz_order_dtl.bus_cnt';
        if (alias !== undefined) {

          col_bus_cnt = `${alias}.bus_cnt`;

        }

        ar.select(`${col_bus_cnt} as '${col_bus_cnt}' `);

         
        let col_time_range = 'biz_order_dtl.time_range';
        if (alias !== undefined) {

          col_time_range = `${alias}.time_range`;

        }

        ar.select(`${col_time_range} as '${col_time_range}' `);

         
        let col_runn_cnt = 'biz_order_dtl.runn_cnt';
        if (alias !== undefined) {

          col_runn_cnt = `${alias}.runn_cnt`;

        }

        ar.select(`${col_runn_cnt} as '${col_runn_cnt}' `);

         
        let col_runn_schd_day = 'biz_order_dtl.runn_schd_day';
        if (alias !== undefined) {

          col_runn_schd_day = `${alias}.runn_schd_day`;

        }

        ar.select(`${col_runn_schd_day} as '${col_runn_schd_day}' `);

         
        let col_start = 'biz_order_dtl.start';
        if (alias !== undefined) {

          col_start = `${alias}.start`;

        }

        ar.select(`${col_start} as '${col_start}' `);

         
        let col_move = 'biz_order_dtl.move';
        if (alias !== undefined) {

          col_move = `${alias}.move`;

        }

        ar.select(`${col_move} as '${col_move}' `);

         
        let col_end = 'biz_order_dtl.end';
        if (alias !== undefined) {

          col_end = `${alias}.end`;

        }

        ar.select(`${col_end} as '${col_end}' `);

         
        let col_comment = 'biz_order_dtl.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_end_comment = 'biz_order_dtl.end_comment';
        if (alias !== undefined) {

          col_end_comment = `${alias}.end_comment`;

        }

        ar.select(`${col_end_comment} as '${col_end_comment}' `);

         
        let col_created_at = 'biz_order_dtl.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_section_cd = 'biz_order_dtl.section_cd';
        if (alias !== undefined) {

          col_section_cd = `${alias}.section_cd`;

        }

        ar.select(`${col_section_cd} as '${col_section_cd}' `);

         
        let col_start_time = 'biz_order_dtl.start_time';
        if (alias !== undefined) {

          col_start_time = `${alias}.start_time`;

        }

        ar.select(`${col_start_time} as '${col_start_time}' `);

         
        let col_end_time = 'biz_order_dtl.end_time';
        if (alias !== undefined) {

          col_end_time = `${alias}.end_time`;

        }

        ar.select(`${col_end_time} as '${col_end_time}' `);

         
        let col_age = 'biz_order_dtl.age';
        if (alias !== undefined) {

          col_age = `${alias}.age`;

        }

        ar.select(`${col_age} as '${col_age}' `);

         
        let col_updated_at = 'biz_order_dtl.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_dtl');
    
    if (nullCheck(form.bizOrderDtlId) === true) {
      ar.set("biz_order_dtl_id", form.bizOrderDtlId);
    } 
    

    if (nullCheck(form.bizOrderId) === true) {
      ar.set("biz_order_id", form.bizOrderId);
    } 
    

    if (nullCheck(form.busCd) === true) {
      ar.set("bus_cd", form.busCd);
    } 
    

    if (nullCheck(form.busCnt) === true) {
      ar.set("bus_cnt", form.busCnt);
    } 
    

    if (nullCheck(form.timeRange) === true) {
      ar.set("time_range", form.timeRange);
    } 
    

    if (nullCheck(form.runnCnt) === true) {
      ar.set("runn_cnt", form.runnCnt);
    } 
    

    if (nullCheck(form.runnSchdDay) === true) {
      ar.set("runn_schd_day", form.runnSchdDay);
    } 
    

    if (nullCheck(form.start) === true) {
      ar.set("start", form.start);
    } 
    

    if (nullCheck(form.move) === true) {
      ar.set("move", form.move);
    } 
    

    if (nullCheck(form.end) === true) {
      ar.set("end", form.end);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.endComment) === true) {
      ar.set("end_comment", form.endComment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section_cd", form.sectionCd);
    } 
    

    if (nullCheck(form.startTime) === true) {
      ar.set("start_time", form.startTime);
    } 
    

    if (nullCheck(form.endTime) === true) {
      ar.set("end_time", form.endTime);
    } 
    

    if (nullCheck(form.age) === true) {
      ar.set("age", form.age);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_dtl');
    
    if (nullCheck(form.bizOrderDtlId) === true) {
      ar.set("biz_order_dtl_id", form.bizOrderDtlId);
    } 
    

    if (nullCheck(form.bizOrderId) === true) {
      ar.set("biz_order_id", form.bizOrderId);
    } 
    

    if (nullCheck(form.busCd) === true) {
      ar.set("bus_cd", form.busCd);
    } 
    

    if (nullCheck(form.busCnt) === true) {
      ar.set("bus_cnt", form.busCnt);
    } 
    

    if (nullCheck(form.timeRange) === true) {
      ar.set("time_range", form.timeRange);
    } 
    

    if (nullCheck(form.runnCnt) === true) {
      ar.set("runn_cnt", form.runnCnt);
    } 
    

    if (nullCheck(form.runnSchdDay) === true) {
      ar.set("runn_schd_day", form.runnSchdDay);
    } 
    

    if (nullCheck(form.start) === true) {
      ar.set("start", form.start);
    } 
    

    if (nullCheck(form.move) === true) {
      ar.set("move", form.move);
    } 
    

    if (nullCheck(form.end) === true) {
      ar.set("end", form.end);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.endComment) === true) {
      ar.set("end_comment", form.endComment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section_cd", form.sectionCd);
    } 
    

    if (nullCheck(form.startTime) === true) {
      ar.set("start_time", form.startTime);
    } 
    

    if (nullCheck(form.endTime) === true) {
      ar.set("end_time", form.endTime);
    } 
    

    if (nullCheck(form.age) === true) {
      ar.set("age", form.age);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_dtl_view biz_order_dtl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    bizOrderDtlId: 'biz_order_dtl_id'
    , 

    bizOrderId: 'biz_order_id'
    , 

    busCd: 'bus_cd'
    , 

    busCnt: 'bus_cnt'
    , 

    timeRange: 'time_range'
    , 

    runnCnt: 'runn_cnt'
    , 

    runnSchdDay: 'runn_schd_day'
    , 

    start: 'start'
    , 

    move: 'move'
    , 

    end: 'end'
    , 

    comment: 'comment'
    , 

    endComment: 'end_comment'
    , 

    createdAt: 'created_at'
    , 

    sectionCd: 'section_cd'
    , 

    startTime: 'start_time'
    , 

    endTime: 'end_time'
    , 

    age: 'age'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('biz_order_dtl');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_dtl_view biz_order_dtl');
    
      ar.select("biz_order_dtl.biz_order_dtl_id");

    
    
      ar.select("biz_order_dtl.biz_order_id");

    
    
      ar.select("biz_order_dtl.bus_cd");

    
    
      ar.select("biz_order_dtl.bus_cnt");

    
    
      ar.select("biz_order_dtl.time_range");

    
    
      ar.select("biz_order_dtl.runn_cnt");

    
    
      ar.select("biz_order_dtl.runn_schd_day");

    
    
      ar.select("biz_order_dtl.start");

    
    
      ar.select("biz_order_dtl.move");

    
    
      ar.select("biz_order_dtl.end");

    
    
      ar.select("biz_order_dtl.comment");

    
    
      ar.select("biz_order_dtl.end_comment");

    
    
      ar.select("biz_order_dtl.created_at");

    
    
      ar.select("biz_order_dtl.section_cd");

    
    
      ar.select("biz_order_dtl.start_time");

    
    
      ar.select("biz_order_dtl.end_time");

    
    
      ar.select("biz_order_dtl.age");

    
    
      ar.select("biz_order_dtl.updated_at");

    
    
    return ar;
  }

  

  
}
export const BizOrderDtlSql =  new BizOrderDtlQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IPaidDc } from "../interface";


  class PaidDcQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 paid_dc객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' paid_dc_view paid_dc');

    
      if (alias === true) {
        ar.select("paid_dc.paid_dc_id as 'paid_dc.paid_dc_id'" );

      } else{
        ar.select("paid_dc.paid_dc_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.pay_dc_id as 'paid_dc.pay_dc_id'" );

      } else{
        ar.select("paid_dc.pay_dc_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.paid_id as 'paid_dc.paid_id'" );

      } else{
        ar.select("paid_dc.paid_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.pay_id as 'paid_dc.pay_id'" );

      } else{
        ar.select("paid_dc.pay_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.board_id as 'paid_dc.board_id'" );

      } else{
        ar.select("paid_dc.board_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.rt_id as 'paid_dc.rt_id'" );

      } else{
        ar.select("paid_dc.rt_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.user_id as 'paid_dc.user_id'" );

      } else{
        ar.select("paid_dc.user_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.org_user_auth_id as 'paid_dc.org_user_auth_id'" );

      } else{
        ar.select("paid_dc.org_user_auth_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.email_auth_id as 'paid_dc.email_auth_id'" );

      } else{
        ar.select("paid_dc.email_auth_id");

      }
      
      if (alias === true) {
        ar.select("paid_dc.order_no as 'paid_dc.order_no'" );

      } else{
        ar.select("paid_dc.order_no");

      }
      
      if (alias === true) {
        ar.select("paid_dc.amount as 'paid_dc.amount'" );

      } else{
        ar.select("paid_dc.amount");

      }
      
      if (alias === true) {
        ar.select("paid_dc.created_at as 'paid_dc.created_at'" );

      } else{
        ar.select("paid_dc.created_at");

      }
      
      if (alias === true) {
        ar.select("paid_dc.updated_at as 'paid_dc.updated_at'" );

      } else{
        ar.select("paid_dc.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_paid_dc_id = 'paid_dc.paid_dc_id';
        if (alias !== undefined) {

          col_paid_dc_id = `${alias}.paid_dc_id`;

        }

        ar.select(`${col_paid_dc_id} as '${col_paid_dc_id}' `);

         
        let col_pay_dc_id = 'paid_dc.pay_dc_id';
        if (alias !== undefined) {

          col_pay_dc_id = `${alias}.pay_dc_id`;

        }

        ar.select(`${col_pay_dc_id} as '${col_pay_dc_id}' `);

         
        let col_paid_id = 'paid_dc.paid_id';
        if (alias !== undefined) {

          col_paid_id = `${alias}.paid_id`;

        }

        ar.select(`${col_paid_id} as '${col_paid_id}' `);

         
        let col_pay_id = 'paid_dc.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_board_id = 'paid_dc.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'paid_dc.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'paid_dc.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_org_user_auth_id = 'paid_dc.org_user_auth_id';
        if (alias !== undefined) {

          col_org_user_auth_id = `${alias}.org_user_auth_id`;

        }

        ar.select(`${col_org_user_auth_id} as '${col_org_user_auth_id}' `);

         
        let col_email_auth_id = 'paid_dc.email_auth_id';
        if (alias !== undefined) {

          col_email_auth_id = `${alias}.email_auth_id`;

        }

        ar.select(`${col_email_auth_id} as '${col_email_auth_id}' `);

         
        let col_order_no = 'paid_dc.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_amount = 'paid_dc.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'paid_dc.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'paid_dc.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid_dc');
    
    if (nullCheck(form.paidDcId) === true) {
      ar.set("paid_dc_id", form.paidDcId);
    } 
    

    if (nullCheck(form.payDcId) === true) {
      ar.set("pay_dc_id", form.payDcId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.emailAuthId) === true) {
      ar.set("email_auth_id", form.emailAuthId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid_dc');
    
    if (nullCheck(form.paidDcId) === true) {
      ar.set("paid_dc_id", form.paidDcId);
    } 
    

    if (nullCheck(form.payDcId) === true) {
      ar.set("pay_dc_id", form.payDcId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.emailAuthId) === true) {
      ar.set("email_auth_id", form.emailAuthId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_dc_view paid_dc');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    paidDcId: 'paid_dc_id'
    , 

    payDcId: 'pay_dc_id'
    , 

    paidId: 'paid_id'
    , 

    payId: 'pay_id'
    , 

    boardId: 'board_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    orgUserAuthId: 'org_user_auth_id'
    , 

    emailAuthId: 'email_auth_id'
    , 

    orderNo: 'order_no'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('paid_dc');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_dc_view paid_dc');
    
      ar.select("paid_dc.paid_dc_id");

    
    
      ar.select("paid_dc.pay_dc_id");

    
    
      ar.select("paid_dc.paid_id");

    
    
      ar.select("paid_dc.pay_id");

    
    
      ar.select("paid_dc.board_id");

    
    
      ar.select("paid_dc.rt_id");

    
    
      ar.select("paid_dc.user_id");

    
    
      ar.select("paid_dc.org_user_auth_id");

    
    
      ar.select("paid_dc.email_auth_id");

    
    
      ar.select("paid_dc.order_no");

    
    
      ar.select("paid_dc.amount");

    
    
      ar.select("paid_dc.created_at");

    
    
      ar.select("paid_dc.updated_at");

    
    
    return ar;
  }

  

  
}
export const PaidDcSql =  new PaidDcQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IMsg } from "../interface";


  class MsgQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 msg객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' msg_view msg');

    
      if (alias === true) {
        ar.select("msg.msg_id as 'msg.msg_id'" );

      } else{
        ar.select("msg.msg_id");

      }
      
      if (alias === true) {
        ar.select("msg.rt_id as 'msg.rt_id'" );

      } else{
        ar.select("msg.rt_id");

      }
      
      if (alias === true) {
        ar.select("msg.prods_id as 'msg.prods_id'" );

      } else{
        ar.select("msg.prods_id");

      }
      
      if (alias === true) {
        ar.select("msg.user_id as 'msg.user_id'" );

      } else{
        ar.select("msg.user_id");

      }
      
      if (alias === true) {
        ar.select("msg.phone as 'msg.phone'" );

      } else{
        ar.select("msg.phone");

      }
      
      if (alias === true) {
        ar.select("msg.nm as 'msg.nm'" );

      } else{
        ar.select("msg.nm");

      }
      
      if (alias === true) {
        ar.select("msg.msg_cd as 'msg.msg_cd'" );

      } else{
        ar.select("msg.msg_cd");

      }
      
      if (alias === true) {
        ar.select("msg.rt_cd as 'msg.rt_cd'" );

      } else{
        ar.select("msg.rt_cd");

      }
      
      if (alias === true) {
        ar.select("msg.subject as 'msg.subject'" );

      } else{
        ar.select("msg.subject");

      }
      
      if (alias === true) {
        ar.select("msg.comment as 'msg.comment'" );

      } else{
        ar.select("msg.comment");

      }
      
      if (alias === true) {
        ar.select("msg.created_at as 'msg.created_at'" );

      } else{
        ar.select("msg.created_at");

      }
      
      if (alias === true) {
        ar.select("msg.reserv_at as 'msg.reserv_at'" );

      } else{
        ar.select("msg.reserv_at");

      }
      
      if (alias === true) {
        ar.select("msg.result as 'msg.result'" );

      } else{
        ar.select("msg.result");

      }
      
      if (alias === true) {
        ar.select("msg.send_id as 'msg.send_id'" );

      } else{
        ar.select("msg.send_id");

      }
      
      if (alias === true) {
        ar.select("msg.admin_id as 'msg.admin_id'" );

      } else{
        ar.select("msg.admin_id");

      }
      
      if (alias === true) {
        ar.select("msg.msg_compn_cd as 'msg.msg_compn_cd'" );

      } else{
        ar.select("msg.msg_compn_cd");

      }
      
      if (alias === true) {
        ar.select("msg.msg_tpl_id as 'msg.msg_tpl_id'" );

      } else{
        ar.select("msg.msg_tpl_id");

      }
      
      if (alias === true) {
        ar.select("msg.mbr_cd as 'msg.mbr_cd'" );

      } else{
        ar.select("msg.mbr_cd");

      }
      
      if (alias === true) {
        ar.select("msg.status_cd as 'msg.status_cd'" );

      } else{
        ar.select("msg.status_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_msg_id = 'msg.msg_id';
        if (alias !== undefined) {

          col_msg_id = `${alias}.msg_id`;

        }

        ar.select(`${col_msg_id} as '${col_msg_id}' `);

         
        let col_rt_id = 'msg.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_prods_id = 'msg.prods_id';
        if (alias !== undefined) {

          col_prods_id = `${alias}.prods_id`;

        }

        ar.select(`${col_prods_id} as '${col_prods_id}' `);

         
        let col_user_id = 'msg.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_phone = 'msg.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_nm = 'msg.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_msg_cd = 'msg.msg_cd';
        if (alias !== undefined) {

          col_msg_cd = `${alias}.msg_cd`;

        }

        ar.select(`${col_msg_cd} as '${col_msg_cd}' `);

         
        let col_rt_cd = 'msg.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_subject = 'msg.subject';
        if (alias !== undefined) {

          col_subject = `${alias}.subject`;

        }

        ar.select(`${col_subject} as '${col_subject}' `);

         
        let col_comment = 'msg.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'msg.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_reserv_at = 'msg.reserv_at';
        if (alias !== undefined) {

          col_reserv_at = `${alias}.reserv_at`;

        }

        ar.select(`${col_reserv_at} as '${col_reserv_at}' `);

         
        let col_result = 'msg.result';
        if (alias !== undefined) {

          col_result = `${alias}.result`;

        }

        ar.select(`${col_result} as '${col_result}' `);

         
        let col_send_id = 'msg.send_id';
        if (alias !== undefined) {

          col_send_id = `${alias}.send_id`;

        }

        ar.select(`${col_send_id} as '${col_send_id}' `);

         
        let col_admin_id = 'msg.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_msg_compn_cd = 'msg.msg_compn_cd';
        if (alias !== undefined) {

          col_msg_compn_cd = `${alias}.msg_compn_cd`;

        }

        ar.select(`${col_msg_compn_cd} as '${col_msg_compn_cd}' `);

         
        let col_msg_tpl_id = 'msg.msg_tpl_id';
        if (alias !== undefined) {

          col_msg_tpl_id = `${alias}.msg_tpl_id`;

        }

        ar.select(`${col_msg_tpl_id} as '${col_msg_tpl_id}' `);

         
        let col_mbr_cd = 'msg.mbr_cd';
        if (alias !== undefined) {

          col_mbr_cd = `${alias}.mbr_cd`;

        }

        ar.select(`${col_mbr_cd} as '${col_mbr_cd}' `);

         
        let col_status_cd = 'msg.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_sms');
    
    if (nullCheck(form.msgId) === true) {
      ar.set("a_no", form.msgId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("a_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("a_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("a_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("a_name", form.nm);
    } 
    

    if (nullCheck(form.msgCd) === true) {
      ar.set("a_type", form.msgCd);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.subject) === true) {
      ar.set("a_subject", form.subject);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("a_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.reservAt) === true) {
      ar.set("a_timestamp_send", form.reservAt);
    } 
    

    if (nullCheck(form.result) === true) {
      ar.set("a_result", form.result);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("a_gid", form.sendId);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    

    if (nullCheck(form.msgCompnCd) === true) {
      ar.set("a_stype", form.msgCompnCd);
    } 
    

    if (nullCheck(form.msgTplId) === true) {
      ar.set("a_temp", form.msgTplId);
    } 
    

    if (nullCheck(form.mbrCd) === true) {
      ar.set("a_ptype", form.mbrCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("a_status", form.statusCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_sms');
    
    if (nullCheck(form.msgId) === true) {
      ar.set("a_no", form.msgId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("a_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("a_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("a_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("a_name", form.nm);
    } 
    

    if (nullCheck(form.msgCd) === true) {
      ar.set("a_type", form.msgCd);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.subject) === true) {
      ar.set("a_subject", form.subject);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("a_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.reservAt) === true) {
      ar.set("a_timestamp_send", form.reservAt);
    } 
    

    if (nullCheck(form.result) === true) {
      ar.set("a_result", form.result);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("a_gid", form.sendId);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    

    if (nullCheck(form.msgCompnCd) === true) {
      ar.set("a_stype", form.msgCompnCd);
    } 
    

    if (nullCheck(form.msgTplId) === true) {
      ar.set("a_temp", form.msgTplId);
    } 
    

    if (nullCheck(form.mbrCd) === true) {
      ar.set("a_ptype", form.mbrCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("a_status", form.statusCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_view msg');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    msgId: 'a_no'
    , 

    rtId: 'a_rid'
    , 

    prodsId: 'a_rpid'
    , 

    userId: 'a_mid'
    , 

    phone: 'a_phone'
    , 

    nm: 'a_name'
    , 

    msgCd: 'a_type'
    , 

    rtCd: 'a_rtype'
    , 

    subject: 'a_subject'
    , 

    comment: 'a_text'
    , 

    createdAt: 'a_timestamp'
    , 

    reservAt: 'a_timestamp_send'
    , 

    result: 'a_result'
    , 

    sendId: 'a_gid'
    , 

    adminId: 'a_admin'
    , 

    msgCompnCd: 'a_stype'
    , 

    msgTplId: 'a_temp'
    , 

    mbrCd: 'a_ptype'
    , 

    statusCd: 'a_status'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_sms');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_view msg');
    
      ar.select("msg.msg_id");

    
    
      ar.select("msg.rt_id");

    
    
      ar.select("msg.prods_id");

    
    
      ar.select("msg.user_id");

    
    
      ar.select("msg.phone");

    
    
      ar.select("msg.nm");

    
    
      ar.select("msg.msg_cd");

    
    
      ar.select("msg.rt_cd");

    
    
      ar.select("msg.subject");

    
    
      ar.select("msg.comment");

    
    
      ar.select("msg.created_at");

    
    
      ar.select("msg.reserv_at");

    
    
      ar.select("msg.result");

    
    
      ar.select("msg.send_id");

    
    
      ar.select("msg.admin_id");

    
    
      ar.select("msg.msg_compn_cd");

    
    
      ar.select("msg.msg_tpl_id");

    
    
      ar.select("msg.mbr_cd");

    
    
      ar.select("msg.status_cd");

    
    
    return ar;
  }

  

  
}
export const MsgSql =  new MsgQuery()

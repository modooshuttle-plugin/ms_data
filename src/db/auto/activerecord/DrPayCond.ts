import { Ar, nullCheck } from "../../../util";
 
  import { IDrPayCond } from "../interface";


  class DrPayCondQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_pay_cond객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_cond_view dr_pay_cond');

    
      if (alias === true) {
        ar.select("dr_pay_cond.dr_pay_cond_id as 'dr_pay_cond.dr_pay_cond_id'" );

      } else{
        ar.select("dr_pay_cond.dr_pay_cond_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.dr_pay_id as 'dr_pay_cond.dr_pay_id'" );

      } else{
        ar.select("dr_pay_cond.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.dispatch_id as 'dr_pay_cond.dispatch_id'" );

      } else{
        ar.select("dr_pay_cond.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.start_setl_month as 'dr_pay_cond.start_setl_month'" );

      } else{
        ar.select("dr_pay_cond.start_setl_month");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.cond_cd as 'dr_pay_cond.cond_cd'" );

      } else{
        ar.select("dr_pay_cond.cond_cd");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.min_cnt as 'dr_pay_cond.min_cnt'" );

      } else{
        ar.select("dr_pay_cond.min_cnt");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.amount as 'dr_pay_cond.amount'" );

      } else{
        ar.select("dr_pay_cond.amount");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.created_at as 'dr_pay_cond.created_at'" );

      } else{
        ar.select("dr_pay_cond.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.updated_at as 'dr_pay_cond.updated_at'" );

      } else{
        ar.select("dr_pay_cond.updated_at");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.admin_id as 'dr_pay_cond.admin_id'" );

      } else{
        ar.select("dr_pay_cond.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_cond.comment as 'dr_pay_cond.comment'" );

      } else{
        ar.select("dr_pay_cond.comment");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_pay_cond_id = 'dr_pay_cond.dr_pay_cond_id';
        if (alias !== undefined) {

          col_dr_pay_cond_id = `${alias}.dr_pay_cond_id`;

        }

        ar.select(`${col_dr_pay_cond_id} as '${col_dr_pay_cond_id}' `);

         
        let col_dr_pay_id = 'dr_pay_cond.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_dispatch_id = 'dr_pay_cond.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_start_setl_month = 'dr_pay_cond.start_setl_month';
        if (alias !== undefined) {

          col_start_setl_month = `${alias}.start_setl_month`;

        }

        ar.select(`${col_start_setl_month} as '${col_start_setl_month}' `);

         
        let col_cond_cd = 'dr_pay_cond.cond_cd';
        if (alias !== undefined) {

          col_cond_cd = `${alias}.cond_cd`;

        }

        ar.select(`${col_cond_cd} as '${col_cond_cd}' `);

         
        let col_min_cnt = 'dr_pay_cond.min_cnt';
        if (alias !== undefined) {

          col_min_cnt = `${alias}.min_cnt`;

        }

        ar.select(`${col_min_cnt} as '${col_min_cnt}' `);

         
        let col_amount = 'dr_pay_cond.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'dr_pay_cond.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_pay_cond.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'dr_pay_cond.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_comment = 'dr_pay_cond.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_cond');
    
    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.startSetlMonth) === true) {
      ar.set("start_setl_month", form.startSetlMonth);
    } 
    

    if (nullCheck(form.condCd) === true) {
      ar.set("cond_cd", form.condCd);
    } 
    

    if (nullCheck(form.minCnt) === true) {
      ar.set("min_cnt", form.minCnt);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_cond');
    
    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.startSetlMonth) === true) {
      ar.set("start_setl_month", form.startSetlMonth);
    } 
    

    if (nullCheck(form.condCd) === true) {
      ar.set("cond_cd", form.condCd);
    } 
    

    if (nullCheck(form.minCnt) === true) {
      ar.set("min_cnt", form.minCnt);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_cond_view dr_pay_cond');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drPayCondId: 'dr_pay_cond_id'
    , 

    drPayId: 'dr_pay_id'
    , 

    dispatchId: 'dispatch_id'
    , 

    startSetlMonth: 'start_setl_month'
    , 

    condCd: 'cond_cd'
    , 

    minCnt: 'min_cnt'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    , 

    comment: 'comment'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_pay_cond');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_cond_view dr_pay_cond');
    
      ar.select("dr_pay_cond.dr_pay_cond_id");

    
    
      ar.select("dr_pay_cond.dr_pay_id");

    
    
      ar.select("dr_pay_cond.dispatch_id");

    
    
      ar.select("dr_pay_cond.start_setl_month");

    
    
      ar.select("dr_pay_cond.cond_cd");

    
    
      ar.select("dr_pay_cond.min_cnt");

    
    
      ar.select("dr_pay_cond.amount");

    
    
      ar.select("dr_pay_cond.created_at");

    
    
      ar.select("dr_pay_cond.updated_at");

    
    
      ar.select("dr_pay_cond.admin_id");

    
    
      ar.select("dr_pay_cond.comment");

    
    
    return ar;
  }

  

  
}
export const DrPayCondSql =  new DrPayCondQuery()

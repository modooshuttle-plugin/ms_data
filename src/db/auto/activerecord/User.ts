import { Ar, nullCheck } from "../../../util";
 
  import { IUser } from "../interface";


  class UserQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 user객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' user_view user');

    
      if (alias === true) {
        ar.select("user.user_id as 'user.user_id'" );

      } else{
        ar.select("user.user_id");

      }
      
      if (alias === true) {
        ar.select("user.phone as 'user.phone'" );

      } else{
        ar.select("user.phone");

      }
      
      if (alias === true) {
        ar.select("user.nm as 'user.nm'" );

      } else{
        ar.select("user.nm");

      }
      
      if (alias === true) {
        ar.select("user.user_cd as 'user.user_cd'" );

      } else{
        ar.select("user.user_cd");

      }
      
      if (alias === true) {
        ar.select("user.nick as 'user.nick'" );

      } else{
        ar.select("user.nick");

      }
      
      if (alias === true) {
        ar.select("user.active_cd as 'user.active_cd'" );

      } else{
        ar.select("user.active_cd");

      }
      
      if (alias === true) {
        ar.select("user.terms_check as 'user.terms_check'" );

      } else{
        ar.select("user.terms_check");

      }
      
      if (alias === true) {
        ar.select("user.created_at as 'user.created_at'" );

      } else{
        ar.select("user.created_at");

      }
      
      if (alias === true) {
        ar.select("user.updated_at as 'user.updated_at'" );

      } else{
        ar.select("user.updated_at");

      }
      
      if (alias === true) {
        ar.select("user.ref_cd as 'user.ref_cd'" );

      } else{
        ar.select("user.ref_cd");

      }
      
      if (alias === true) {
        ar.select("user.org_email as 'user.org_email'" );

      } else{
        ar.select("user.org_email");

      }
      
      if (alias === true) {
        ar.select("user.org_id as 'user.org_id'" );

      } else{
        ar.select("user.org_id");

      }
      
      if (alias === true) {
        ar.select("user.gender as 'user.gender'" );

      } else{
        ar.select("user.gender");

      }
      
      if (alias === true) {
        ar.select("user.birth_year as 'user.birth_year'" );

      } else{
        ar.select("user.birth_year");

      }
      
      if (alias === true) {
        ar.select("user.birth_month as 'user.birth_month'" );

      } else{
        ar.select("user.birth_month");

      }
      
      if (alias === true) {
        ar.select("user.birth_day as 'user.birth_day'" );

      } else{
        ar.select("user.birth_day");

      }
      
      if (alias === true) {
        ar.select("user.country_cd as 'user.country_cd'" );

      } else{
        ar.select("user.country_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_user_id = 'user.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_phone = 'user.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_nm = 'user.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_user_cd = 'user.user_cd';
        if (alias !== undefined) {

          col_user_cd = `${alias}.user_cd`;

        }

        ar.select(`${col_user_cd} as '${col_user_cd}' `);

         
        let col_nick = 'user.nick';
        if (alias !== undefined) {

          col_nick = `${alias}.nick`;

        }

        ar.select(`${col_nick} as '${col_nick}' `);

         
        let col_active_cd = 'user.active_cd';
        if (alias !== undefined) {

          col_active_cd = `${alias}.active_cd`;

        }

        ar.select(`${col_active_cd} as '${col_active_cd}' `);

         
        let col_terms_check = 'user.terms_check';
        if (alias !== undefined) {

          col_terms_check = `${alias}.terms_check`;

        }

        ar.select(`${col_terms_check} as '${col_terms_check}' `);

         
        let col_created_at = 'user.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'user.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_ref_cd = 'user.ref_cd';
        if (alias !== undefined) {

          col_ref_cd = `${alias}.ref_cd`;

        }

        ar.select(`${col_ref_cd} as '${col_ref_cd}' `);

         
        let col_org_email = 'user.org_email';
        if (alias !== undefined) {

          col_org_email = `${alias}.org_email`;

        }

        ar.select(`${col_org_email} as '${col_org_email}' `);

         
        let col_org_id = 'user.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_gender = 'user.gender';
        if (alias !== undefined) {

          col_gender = `${alias}.gender`;

        }

        ar.select(`${col_gender} as '${col_gender}' `);

         
        let col_birth_year = 'user.birth_year';
        if (alias !== undefined) {

          col_birth_year = `${alias}.birth_year`;

        }

        ar.select(`${col_birth_year} as '${col_birth_year}' `);

         
        let col_birth_month = 'user.birth_month';
        if (alias !== undefined) {

          col_birth_month = `${alias}.birth_month`;

        }

        ar.select(`${col_birth_month} as '${col_birth_month}' `);

         
        let col_birth_day = 'user.birth_day';
        if (alias !== undefined) {

          col_birth_day = `${alias}.birth_day`;

        }

        ar.select(`${col_birth_day} as '${col_birth_day}' `);

         
        let col_country_cd = 'user.country_cd';
        if (alias !== undefined) {

          col_country_cd = `${alias}.country_cd`;

        }

        ar.select(`${col_country_cd} as '${col_country_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_auth');
    
    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("m_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("m_name", form.nm);
    } 
    

    if (nullCheck(form.userCd) === true) {
      ar.set("m_type", form.userCd);
    } 
    

    if (nullCheck(form.nick) === true) {
      ar.set("m_nick", form.nick);
    } 
    

    if (nullCheck(form.activeCd) === true) {
      ar.set("m_act", form.activeCd);
    } 
    

    if (nullCheck(form.termsCheck) === true) {
      ar.set("m_check", form.termsCheck);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.refCd) === true) {
      ar.set("m_refer_code", form.refCd);
    } 
    

    if (nullCheck(form.orgEmail) === true) {
      ar.set("m_email", form.orgEmail);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("m_company", form.orgId);
    } 
    

    if (nullCheck(form.gender) === true) {
      ar.set("gender", form.gender);
    } 
    

    if (nullCheck(form.birthYear) === true) {
      ar.set("birth_year", form.birthYear);
    } 
    

    if (nullCheck(form.birthMonth) === true) {
      ar.set("birth_month", form.birthMonth);
    } 
    

    if (nullCheck(form.birthDay) === true) {
      ar.set("birth_day", form.birthDay);
    } 
    

    if (nullCheck(form.countryCd) === true) {
      ar.set("country_cd", form.countryCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_auth');
    
    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("m_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("m_name", form.nm);
    } 
    

    if (nullCheck(form.userCd) === true) {
      ar.set("m_type", form.userCd);
    } 
    

    if (nullCheck(form.nick) === true) {
      ar.set("m_nick", form.nick);
    } 
    

    if (nullCheck(form.activeCd) === true) {
      ar.set("m_act", form.activeCd);
    } 
    

    if (nullCheck(form.termsCheck) === true) {
      ar.set("m_check", form.termsCheck);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.refCd) === true) {
      ar.set("m_refer_code", form.refCd);
    } 
    

    if (nullCheck(form.orgEmail) === true) {
      ar.set("m_email", form.orgEmail);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("m_company", form.orgId);
    } 
    

    if (nullCheck(form.gender) === true) {
      ar.set("gender", form.gender);
    } 
    

    if (nullCheck(form.birthYear) === true) {
      ar.set("birth_year", form.birthYear);
    } 
    

    if (nullCheck(form.birthMonth) === true) {
      ar.set("birth_month", form.birthMonth);
    } 
    

    if (nullCheck(form.birthDay) === true) {
      ar.set("birth_day", form.birthDay);
    } 
    

    if (nullCheck(form.countryCd) === true) {
      ar.set("country_cd", form.countryCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' user_view user');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    userId: 'm_mid'
    , 

    phone: 'm_phone'
    , 

    nm: 'm_name'
    , 

    userCd: 'm_type'
    , 

    nick: 'm_nick'
    , 

    activeCd: 'm_act'
    , 

    termsCheck: 'm_check'
    , 

    createdAt: 'm_timestamp'
    , 

    updatedAt: 'm_timestamp_u'
    , 

    refCd: 'm_refer_code'
    , 

    orgEmail: 'm_email'
    , 

    orgId: 'm_company'
    , 

    gender: 'gender'
    , 

    birthYear: 'birth_year'
    , 

    birthMonth: 'birth_month'
    , 

    birthDay: 'birth_day'
    , 

    countryCd: 'country_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('m_auth');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' user_view user');
    
      ar.select("user.user_id");

    
    
      ar.select("user.phone");

    
    
      ar.select("user.nm");

    
    
      ar.select("user.user_cd");

    
    
      ar.select("user.nick");

    
    
      ar.select("user.active_cd");

    
    
      ar.select("user.terms_check");

    
    
      ar.select("user.created_at");

    
    
      ar.select("user.updated_at");

    
    
      ar.select("user.ref_cd");

    
    
      ar.select("user.org_email");

    
    
      ar.select("user.org_id");

    
    
      ar.select("user.gender");

    
    
      ar.select("user.birth_year");

    
    
      ar.select("user.birth_month");

    
    
      ar.select("user.birth_day");

    
    
      ar.select("user.country_cd");

    
    
    return ar;
  }

  

  
}
export const UserSql =  new UserQuery()

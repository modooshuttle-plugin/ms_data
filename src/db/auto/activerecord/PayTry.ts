import { Ar, nullCheck } from "../../../util";
 
  import { IPayTry } from "../interface";


  class PayTryQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 pay_try객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' pay_try_view pay_try');

    
      if (alias === true) {
        ar.select("pay_try.pay_try_id as 'pay_try.pay_try_id'" );

      } else{
        ar.select("pay_try.pay_try_id");

      }
      
      if (alias === true) {
        ar.select("pay_try.order_no as 'pay_try.order_no'" );

      } else{
        ar.select("pay_try.order_no");

      }
      
      if (alias === true) {
        ar.select("pay_try.pay_id as 'pay_try.pay_id'" );

      } else{
        ar.select("pay_try.pay_id");

      }
      
      if (alias === true) {
        ar.select("pay_try.rt_id as 'pay_try.rt_id'" );

      } else{
        ar.select("pay_try.rt_id");

      }
      
      if (alias === true) {
        ar.select("pay_try.user_id as 'pay_try.user_id'" );

      } else{
        ar.select("pay_try.user_id");

      }
      
      if (alias === true) {
        ar.select("pay_try.method_cd as 'pay_try.method_cd'" );

      } else{
        ar.select("pay_try.method_cd");

      }
      
      if (alias === true) {
        ar.select("pay_try.cash_receipt_yn as 'pay_try.cash_receipt_yn'" );

      } else{
        ar.select("pay_try.cash_receipt_yn");

      }
      
      if (alias === true) {
        ar.select("pay_try.status_cd as 'pay_try.status_cd'" );

      } else{
        ar.select("pay_try.status_cd");

      }
      
      if (alias === true) {
        ar.select("pay_try.channel_cd as 'pay_try.channel_cd'" );

      } else{
        ar.select("pay_try.channel_cd");

      }
      
      if (alias === true) {
        ar.select("pay_try.imp_uid as 'pay_try.imp_uid'" );

      } else{
        ar.select("pay_try.imp_uid");

      }
      
      if (alias === true) {
        ar.select("pay_try.provider_cd as 'pay_try.provider_cd'" );

      } else{
        ar.select("pay_try.provider_cd");

      }
      
      if (alias === true) {
        ar.select("pay_try.amount as 'pay_try.amount'" );

      } else{
        ar.select("pay_try.amount");

      }
      
      if (alias === true) {
        ar.select("pay_try.cash_receipt_at as 'pay_try.cash_receipt_at'" );

      } else{
        ar.select("pay_try.cash_receipt_at");

      }
      
      if (alias === true) {
        ar.select("pay_try.created_at as 'pay_try.created_at'" );

      } else{
        ar.select("pay_try.created_at");

      }
      
      if (alias === true) {
        ar.select("pay_try.updated_at as 'pay_try.updated_at'" );

      } else{
        ar.select("pay_try.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_pay_try_id = 'pay_try.pay_try_id';
        if (alias !== undefined) {

          col_pay_try_id = `${alias}.pay_try_id`;

        }

        ar.select(`${col_pay_try_id} as '${col_pay_try_id}' `);

         
        let col_order_no = 'pay_try.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_pay_id = 'pay_try.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_rt_id = 'pay_try.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'pay_try.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_method_cd = 'pay_try.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_cash_receipt_yn = 'pay_try.cash_receipt_yn';
        if (alias !== undefined) {

          col_cash_receipt_yn = `${alias}.cash_receipt_yn`;

        }

        ar.select(`${col_cash_receipt_yn} as '${col_cash_receipt_yn}' `);

         
        let col_status_cd = 'pay_try.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_channel_cd = 'pay_try.channel_cd';
        if (alias !== undefined) {

          col_channel_cd = `${alias}.channel_cd`;

        }

        ar.select(`${col_channel_cd} as '${col_channel_cd}' `);

         
        let col_imp_uid = 'pay_try.imp_uid';
        if (alias !== undefined) {

          col_imp_uid = `${alias}.imp_uid`;

        }

        ar.select(`${col_imp_uid} as '${col_imp_uid}' `);

         
        let col_provider_cd = 'pay_try.provider_cd';
        if (alias !== undefined) {

          col_provider_cd = `${alias}.provider_cd`;

        }

        ar.select(`${col_provider_cd} as '${col_provider_cd}' `);

         
        let col_amount = 'pay_try.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_cash_receipt_at = 'pay_try.cash_receipt_at';
        if (alias !== undefined) {

          col_cash_receipt_at = `${alias}.cash_receipt_at`;

        }

        ar.select(`${col_cash_receipt_at} as '${col_cash_receipt_at}' `);

         
        let col_created_at = 'pay_try.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'pay_try.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' p_order');
    
    if (nullCheck(form.payTryId) === true) {
      ar.set("p_no", form.payTryId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("p_muid", form.orderNo);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("p_pid", form.payId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("p_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("p_mid", form.userId);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("p_method", form.methodCd);
    } 
    

    if (nullCheck(form.cashReceiptYn) === true) {
      ar.set("p_cash_receipt", form.cashReceiptYn);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("p_type", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("p_channel", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("p_imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("p_provider", form.providerCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("p_value", form.amount);
    } 
    

    if (nullCheck(form.cashReceiptAt) === true) {
      ar.set("p_cash_timestamp", form.cashReceiptAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("p_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("p_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' p_order');
    
    if (nullCheck(form.payTryId) === true) {
      ar.set("p_no", form.payTryId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("p_muid", form.orderNo);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("p_pid", form.payId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("p_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("p_mid", form.userId);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("p_method", form.methodCd);
    } 
    

    if (nullCheck(form.cashReceiptYn) === true) {
      ar.set("p_cash_receipt", form.cashReceiptYn);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("p_type", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("p_channel", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("p_imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("p_provider", form.providerCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("p_value", form.amount);
    } 
    

    if (nullCheck(form.cashReceiptAt) === true) {
      ar.set("p_cash_timestamp", form.cashReceiptAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("p_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("p_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_try_view pay_try');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    payTryId: 'p_no'
    , 

    orderNo: 'p_muid'
    , 

    payId: 'p_pid'
    , 

    rtId: 'p_rid'
    , 

    userId: 'p_mid'
    , 

    methodCd: 'p_method'
    , 

    cashReceiptYn: 'p_cash_receipt'
    , 

    statusCd: 'p_type'
    , 

    channelCd: 'p_channel'
    , 

    impUid: 'p_imp_uid'
    , 

    providerCd: 'p_provider'
    , 

    amount: 'p_value'
    , 

    cashReceiptAt: 'p_cash_timestamp'
    , 

    createdAt: 'p_timestamp'
    , 

    updatedAt: 'p_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('p_order');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_try_view pay_try');
    
      ar.select("pay_try.pay_try_id");

    
    
      ar.select("pay_try.order_no");

    
    
      ar.select("pay_try.pay_id");

    
    
      ar.select("pay_try.rt_id");

    
    
      ar.select("pay_try.user_id");

    
    
      ar.select("pay_try.method_cd");

    
    
      ar.select("pay_try.cash_receipt_yn");

    
    
      ar.select("pay_try.status_cd");

    
    
      ar.select("pay_try.channel_cd");

    
    
      ar.select("pay_try.imp_uid");

    
    
      ar.select("pay_try.provider_cd");

    
    
      ar.select("pay_try.amount");

    
    
      ar.select("pay_try.cash_receipt_at");

    
    
      ar.select("pay_try.created_at");

    
    
      ar.select("pay_try.updated_at");

    
    
    return ar;
  }

  

  
}
export const PayTrySql =  new PayTryQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IRunnDiary } from "../interface";


  class RunnDiaryQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 runn_diary객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' runn_diary_view runn_diary');

    
      if (alias === true) {
        ar.select("runn_diary.runn_diary_id as 'runn_diary.runn_diary_id'" );

      } else{
        ar.select("runn_diary.runn_diary_id");

      }
      
      if (alias === true) {
        ar.select("runn_diary.dr_id as 'runn_diary.dr_id'" );

      } else{
        ar.select("runn_diary.dr_id");

      }
      
      if (alias === true) {
        ar.select("runn_diary.year as 'runn_diary.year'" );

      } else{
        ar.select("runn_diary.year");

      }
      
      if (alias === true) {
        ar.select("runn_diary.month as 'runn_diary.month'" );

      } else{
        ar.select("runn_diary.month");

      }
      
      if (alias === true) {
        ar.select("runn_diary.day as 'runn_diary.day'" );

      } else{
        ar.select("runn_diary.day");

      }
      
      if (alias === true) {
        ar.select("runn_diary.started_at as 'runn_diary.started_at'" );

      } else{
        ar.select("runn_diary.started_at");

      }
      
      if (alias === true) {
        ar.select("runn_diary.ended_at as 'runn_diary.ended_at'" );

      } else{
        ar.select("runn_diary.ended_at");

      }
      
      if (alias === true) {
        ar.select("runn_diary.created_at as 'runn_diary.created_at'" );

      } else{
        ar.select("runn_diary.created_at");

      }
      
      if (alias === true) {
        ar.select("runn_diary.rt_id as 'runn_diary.rt_id'" );

      } else{
        ar.select("runn_diary.rt_id");

      }
      
      if (alias === true) {
        ar.select("runn_diary.runn_cd as 'runn_diary.runn_cd'" );

      } else{
        ar.select("runn_diary.runn_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_runn_diary_id = 'runn_diary.runn_diary_id';
        if (alias !== undefined) {

          col_runn_diary_id = `${alias}.runn_diary_id`;

        }

        ar.select(`${col_runn_diary_id} as '${col_runn_diary_id}' `);

         
        let col_dr_id = 'runn_diary.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_year = 'runn_diary.year';
        if (alias !== undefined) {

          col_year = `${alias}.year`;

        }

        ar.select(`${col_year} as '${col_year}' `);

         
        let col_month = 'runn_diary.month';
        if (alias !== undefined) {

          col_month = `${alias}.month`;

        }

        ar.select(`${col_month} as '${col_month}' `);

         
        let col_day = 'runn_diary.day';
        if (alias !== undefined) {

          col_day = `${alias}.day`;

        }

        ar.select(`${col_day} as '${col_day}' `);

         
        let col_started_at = 'runn_diary.started_at';
        if (alias !== undefined) {

          col_started_at = `${alias}.started_at`;

        }

        ar.select(`${col_started_at} as '${col_started_at}' `);

         
        let col_ended_at = 'runn_diary.ended_at';
        if (alias !== undefined) {

          col_ended_at = `${alias}.ended_at`;

        }

        ar.select(`${col_ended_at} as '${col_ended_at}' `);

         
        let col_created_at = 'runn_diary.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_rt_id = 'runn_diary.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_runn_cd = 'runn_diary.runn_cd';
        if (alias !== undefined) {

          col_runn_cd = `${alias}.runn_cd`;

        }

        ar.select(`${col_runn_cd} as '${col_runn_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_diary');
    
    if (nullCheck(form.runnDiaryId) === true) {
      ar.set("d_no", form.runnDiaryId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_uniq", form.drId);
    } 
    

    if (nullCheck(form.year) === true) {
      ar.set("d_year", form.year);
    } 
    

    if (nullCheck(form.month) === true) {
      ar.set("d_month", form.month);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("d_day", form.day);
    } 
    

    if (nullCheck(form.startedAt) === true) {
      ar.set("d_timestamp_start", form.startedAt);
    } 
    

    if (nullCheck(form.endedAt) === true) {
      ar.set("d_timestamp_end", form.endedAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("d_r_uniq", form.rtId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("d_type", form.runnCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_diary');
    
    if (nullCheck(form.runnDiaryId) === true) {
      ar.set("d_no", form.runnDiaryId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_uniq", form.drId);
    } 
    

    if (nullCheck(form.year) === true) {
      ar.set("d_year", form.year);
    } 
    

    if (nullCheck(form.month) === true) {
      ar.set("d_month", form.month);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("d_day", form.day);
    } 
    

    if (nullCheck(form.startedAt) === true) {
      ar.set("d_timestamp_start", form.startedAt);
    } 
    

    if (nullCheck(form.endedAt) === true) {
      ar.set("d_timestamp_end", form.endedAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("d_r_uniq", form.rtId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("d_type", form.runnCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_diary_view runn_diary');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    runnDiaryId: 'd_no'
    , 

    drId: 'd_uniq'
    , 

    year: 'd_year'
    , 

    month: 'd_month'
    , 

    day: 'd_day'
    , 

    startedAt: 'd_timestamp_start'
    , 

    endedAt: 'd_timestamp_end'
    , 

    createdAt: 'd_timestamp'
    , 

    rtId: 'd_r_uniq'
    , 

    runnCd: 'd_type'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_diary');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_diary_view runn_diary');
    
      ar.select("runn_diary.runn_diary_id");

    
    
      ar.select("runn_diary.dr_id");

    
    
      ar.select("runn_diary.year");

    
    
      ar.select("runn_diary.month");

    
    
      ar.select("runn_diary.day");

    
    
      ar.select("runn_diary.started_at");

    
    
      ar.select("runn_diary.ended_at");

    
    
      ar.select("runn_diary.created_at");

    
    
      ar.select("runn_diary.rt_id");

    
    
      ar.select("runn_diary.runn_cd");

    
    
    return ar;
  }

  

  
}
export const RunnDiarySql =  new RunnDiaryQuery()

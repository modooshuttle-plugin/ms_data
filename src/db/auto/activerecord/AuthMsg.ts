import { Ar, nullCheck } from "../../../util";
 
  import { IAuthMsg } from "../interface";


  class AuthMsgQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 auth_msg객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' auth_msg_view auth_msg');

    
      if (alias === true) {
        ar.select("auth_msg.auth_msg_id as 'auth_msg.auth_msg_id'" );

      } else{
        ar.select("auth_msg.auth_msg_id");

      }
      
      if (alias === true) {
        ar.select("auth_msg.user_id as 'auth_msg.user_id'" );

      } else{
        ar.select("auth_msg.user_id");

      }
      
      if (alias === true) {
        ar.select("auth_msg.phone as 'auth_msg.phone'" );

      } else{
        ar.select("auth_msg.phone");

      }
      
      if (alias === true) {
        ar.select("auth_msg.auth_no as 'auth_msg.auth_no'" );

      } else{
        ar.select("auth_msg.auth_no");

      }
      
      if (alias === true) {
        ar.select("auth_msg.auth_cd as 'auth_msg.auth_cd'" );

      } else{
        ar.select("auth_msg.auth_cd");

      }
      
      if (alias === true) {
        ar.select("auth_msg.created_at as 'auth_msg.created_at'" );

      } else{
        ar.select("auth_msg.created_at");

      }
      
      if (alias === true) {
        ar.select("auth_msg.updated_at as 'auth_msg.updated_at'" );

      } else{
        ar.select("auth_msg.updated_at");

      }
      
      if (alias === true) {
        ar.select("auth_msg.result_cd as 'auth_msg.result_cd'" );

      } else{
        ar.select("auth_msg.result_cd");

      }
      
      if (alias === true) {
        ar.select("auth_msg.send_id as 'auth_msg.send_id'" );

      } else{
        ar.select("auth_msg.send_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_auth_msg_id = 'auth_msg.auth_msg_id';
        if (alias !== undefined) {

          col_auth_msg_id = `${alias}.auth_msg_id`;

        }

        ar.select(`${col_auth_msg_id} as '${col_auth_msg_id}' `);

         
        let col_user_id = 'auth_msg.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_phone = 'auth_msg.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_auth_no = 'auth_msg.auth_no';
        if (alias !== undefined) {

          col_auth_no = `${alias}.auth_no`;

        }

        ar.select(`${col_auth_no} as '${col_auth_no}' `);

         
        let col_auth_cd = 'auth_msg.auth_cd';
        if (alias !== undefined) {

          col_auth_cd = `${alias}.auth_cd`;

        }

        ar.select(`${col_auth_cd} as '${col_auth_cd}' `);

         
        let col_created_at = 'auth_msg.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'auth_msg.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_result_cd = 'auth_msg.result_cd';
        if (alias !== undefined) {

          col_result_cd = `${alias}.result_cd`;

        }

        ar.select(`${col_result_cd} as '${col_result_cd}' `);

         
        let col_send_id = 'auth_msg.send_id';
        if (alias !== undefined) {

          col_send_id = `${alias}.send_id`;

        }

        ar.select(`${col_send_id} as '${col_send_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_msg');
    
    if (nullCheck(form.authMsgId) === true) {
      ar.set("m_no", form.authMsgId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("m_phone", form.phone);
    } 
    

    if (nullCheck(form.authNo) === true) {
      ar.set("m_auth_no", form.authNo);
    } 
    

    if (nullCheck(form.authCd) === true) {
      ar.set("m_auth_check", form.authCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.resultCd) === true) {
      ar.set("m_c_result", form.resultCd);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("m_c_gid", form.sendId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_msg');
    
    if (nullCheck(form.authMsgId) === true) {
      ar.set("m_no", form.authMsgId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("m_phone", form.phone);
    } 
    

    if (nullCheck(form.authNo) === true) {
      ar.set("m_auth_no", form.authNo);
    } 
    

    if (nullCheck(form.authCd) === true) {
      ar.set("m_auth_check", form.authCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.resultCd) === true) {
      ar.set("m_c_result", form.resultCd);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("m_c_gid", form.sendId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' auth_msg_view auth_msg');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    authMsgId: 'm_no'
    , 

    userId: 'm_mid'
    , 

    phone: 'm_phone'
    , 

    authNo: 'm_auth_no'
    , 

    authCd: 'm_auth_check'
    , 

    createdAt: 'm_timestamp'
    , 

    updatedAt: 'm_timestamp_u'
    , 

    resultCd: 'm_c_result'
    , 

    sendId: 'm_c_gid'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('m_msg');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' auth_msg_view auth_msg');
    
      ar.select("auth_msg.auth_msg_id");

    
    
      ar.select("auth_msg.user_id");

    
    
      ar.select("auth_msg.phone");

    
    
      ar.select("auth_msg.auth_no");

    
    
      ar.select("auth_msg.auth_cd");

    
    
      ar.select("auth_msg.created_at");

    
    
      ar.select("auth_msg.updated_at");

    
    
      ar.select("auth_msg.result_cd");

    
    
      ar.select("auth_msg.send_id");

    
    
    return ar;
  }

  

  
}
export const AuthMsgSql =  new AuthMsgQuery()

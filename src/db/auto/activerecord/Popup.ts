import { Ar, nullCheck } from "../../../util";
 
  import { IPopup } from "../interface";


  class PopupQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 popup객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' popup_view popup');

    
      if (alias === true) {
        ar.select("popup.popup_id as 'popup.popup_id'" );

      } else{
        ar.select("popup.popup_id");

      }
      
      if (alias === true) {
        ar.select("popup.title as 'popup.title'" );

      } else{
        ar.select("popup.title");

      }
      
      if (alias === true) {
        ar.select("popup.start_day as 'popup.start_day'" );

      } else{
        ar.select("popup.start_day");

      }
      
      if (alias === true) {
        ar.select("popup.end_day as 'popup.end_day'" );

      } else{
        ar.select("popup.end_day");

      }
      
      if (alias === true) {
        ar.select("popup.img as 'popup.img'" );

      } else{
        ar.select("popup.img");

      }
      
      if (alias === true) {
        ar.select("popup.link as 'popup.link'" );

      } else{
        ar.select("popup.link");

      }
      
      if (alias === true) {
        ar.select("popup.admin_id as 'popup.admin_id'" );

      } else{
        ar.select("popup.admin_id");

      }
      
      if (alias === true) {
        ar.select("popup.deploy_cd as 'popup.deploy_cd'" );

      } else{
        ar.select("popup.deploy_cd");

      }
      
      if (alias === true) {
        ar.select("popup.created_at as 'popup.created_at'" );

      } else{
        ar.select("popup.created_at");

      }
      
      if (alias === true) {
        ar.select("popup.updated_at as 'popup.updated_at'" );

      } else{
        ar.select("popup.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_popup_id = 'popup.popup_id';
        if (alias !== undefined) {

          col_popup_id = `${alias}.popup_id`;

        }

        ar.select(`${col_popup_id} as '${col_popup_id}' `);

         
        let col_title = 'popup.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_start_day = 'popup.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'popup.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_img = 'popup.img';
        if (alias !== undefined) {

          col_img = `${alias}.img`;

        }

        ar.select(`${col_img} as '${col_img}' `);

         
        let col_link = 'popup.link';
        if (alias !== undefined) {

          col_link = `${alias}.link`;

        }

        ar.select(`${col_link} as '${col_link}' `);

         
        let col_admin_id = 'popup.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_deploy_cd = 'popup.deploy_cd';
        if (alias !== undefined) {

          col_deploy_cd = `${alias}.deploy_cd`;

        }

        ar.select(`${col_deploy_cd} as '${col_deploy_cd}' `);

         
        let col_created_at = 'popup.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'popup.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mb_popup');
    
    if (nullCheck(form.popupId) === true) {
      ar.set("mb_no", form.popupId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mb_title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("mb_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("mb_end", form.endDay);
    } 
    

    if (nullCheck(form.img) === true) {
      ar.set("mb_background", form.img);
    } 
    

    if (nullCheck(form.link) === true) {
      ar.set("mb_link", form.link);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("mb_admin", form.adminId);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("mb_deploy", form.deployCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mb_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mb_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mb_popup');
    
    if (nullCheck(form.popupId) === true) {
      ar.set("mb_no", form.popupId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mb_title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("mb_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("mb_end", form.endDay);
    } 
    

    if (nullCheck(form.img) === true) {
      ar.set("mb_background", form.img);
    } 
    

    if (nullCheck(form.link) === true) {
      ar.set("mb_link", form.link);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("mb_admin", form.adminId);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("mb_deploy", form.deployCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mb_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mb_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' popup_view popup');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    popupId: 'mb_no'
    , 

    title: 'mb_title'
    , 

    startDay: 'mb_start'
    , 

    endDay: 'mb_end'
    , 

    img: 'mb_background'
    , 

    link: 'mb_link'
    , 

    adminId: 'mb_admin'
    , 

    deployCd: 'mb_deploy'
    , 

    createdAt: 'mb_timestamp'
    , 

    updatedAt: 'mb_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('mb_popup');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' popup_view popup');
    
      ar.select("popup.popup_id");

    
    
      ar.select("popup.title");

    
    
      ar.select("popup.start_day");

    
    
      ar.select("popup.end_day");

    
    
      ar.select("popup.img");

    
    
      ar.select("popup.link");

    
    
      ar.select("popup.admin_id");

    
    
      ar.select("popup.deploy_cd");

    
    
      ar.select("popup.created_at");

    
    
      ar.select("popup.updated_at");

    
    
    return ar;
  }

  

  
}
export const PopupSql =  new PopupQuery()

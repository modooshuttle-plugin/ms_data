import { Ar, nullCheck } from "../../../util";
 
  import { ICall } from "../interface";


  class CallQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 call객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' call_view call');

    
      if (alias === true) {
        ar.select("call.call_id as 'call.call_id'" );

      } else{
        ar.select("call.call_id");

      }
      
      if (alias === true) {
        ar.select("call.user_id as 'call.user_id'" );

      } else{
        ar.select("call.user_id");

      }
      
      if (alias === true) {
        ar.select("call.before_rt_id as 'call.before_rt_id'" );

      } else{
        ar.select("call.before_rt_id");

      }
      
      if (alias === true) {
        ar.select("call.rt_id as 'call.rt_id'" );

      } else{
        ar.select("call.rt_id");

      }
      
      if (alias === true) {
        ar.select("call.rt_cd as 'call.rt_cd'" );

      } else{
        ar.select("call.rt_cd");

      }
      
      if (alias === true) {
        ar.select("call.call_cd as 'call.call_cd'" );

      } else{
        ar.select("call.call_cd");

      }
      
      if (alias === true) {
        ar.select("call.board_id as 'call.board_id'" );

      } else{
        ar.select("call.board_id");

      }
      
      if (alias === true) {
        ar.select("call.idx as 'call.idx'" );

      } else{
        ar.select("call.idx");

      }
      
      if (alias === true) {
        ar.select("call.created_at as 'call.created_at'" );

      } else{
        ar.select("call.created_at");

      }
      
      if (alias === true) {
        ar.select("call.result_cd as 'call.result_cd'" );

      } else{
        ar.select("call.result_cd");

      }
      
      if (alias === true) {
        ar.select("call.admin_id as 'call.admin_id'" );

      } else{
        ar.select("call.admin_id");

      }
      
      if (alias === true) {
        ar.select("call.comment as 'call.comment'" );

      } else{
        ar.select("call.comment");

      }
      
      if (alias === true) {
        ar.select("call.reason_id as 'call.reason_id'" );

      } else{
        ar.select("call.reason_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_call_id = 'call.call_id';
        if (alias !== undefined) {

          col_call_id = `${alias}.call_id`;

        }

        ar.select(`${col_call_id} as '${col_call_id}' `);

         
        let col_user_id = 'call.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_before_rt_id = 'call.before_rt_id';
        if (alias !== undefined) {

          col_before_rt_id = `${alias}.before_rt_id`;

        }

        ar.select(`${col_before_rt_id} as '${col_before_rt_id}' `);

         
        let col_rt_id = 'call.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_rt_cd = 'call.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_call_cd = 'call.call_cd';
        if (alias !== undefined) {

          col_call_cd = `${alias}.call_cd`;

        }

        ar.select(`${col_call_cd} as '${col_call_cd}' `);

         
        let col_board_id = 'call.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_idx = 'call.idx';
        if (alias !== undefined) {

          col_idx = `${alias}.idx`;

        }

        ar.select(`${col_idx} as '${col_idx}' `);

         
        let col_created_at = 'call.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_result_cd = 'call.result_cd';
        if (alias !== undefined) {

          col_result_cd = `${alias}.result_cd`;

        }

        ar.select(`${col_result_cd} as '${col_result_cd}' `);

         
        let col_admin_id = 'call.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_comment = 'call.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_reason_id = 'call.reason_id';
        if (alias !== undefined) {

          col_reason_id = `${alias}.reason_id`;

        }

        ar.select(`${col_reason_id} as '${col_reason_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_calltable');
    
    if (nullCheck(form.callId) === true) {
      ar.set("o_no", form.callId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.beforeRtId) === true) {
      ar.set("o_rid", form.beforeRtId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rrid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_type", form.rtCd);
    } 
    

    if (nullCheck(form.callCd) === true) {
      ar.set("o_status", form.callCd);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("o_callcnt", form.idx);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_calltime", form.createdAt);
    } 
    

    if (nullCheck(form.resultCd) === true) {
      ar.set("o_callresult", form.resultCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_callmbr", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_callmemo", form.comment);
    } 
    

    if (nullCheck(form.reasonId) === true) {
      ar.set("o_orid", form.reasonId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_calltable');
    
    if (nullCheck(form.callId) === true) {
      ar.set("o_no", form.callId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.beforeRtId) === true) {
      ar.set("o_rid", form.beforeRtId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rrid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_type", form.rtCd);
    } 
    

    if (nullCheck(form.callCd) === true) {
      ar.set("o_status", form.callCd);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("o_callcnt", form.idx);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_calltime", form.createdAt);
    } 
    

    if (nullCheck(form.resultCd) === true) {
      ar.set("o_callresult", form.resultCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_callmbr", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_callmemo", form.comment);
    } 
    

    if (nullCheck(form.reasonId) === true) {
      ar.set("o_orid", form.reasonId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' call_view call');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    callId: 'o_no'
    , 

    userId: 'o_mid'
    , 

    beforeRtId: 'o_rid'
    , 

    rtId: 'o_rrid'
    , 

    rtCd: 'o_type'
    , 

    callCd: 'o_status'
    , 

    boardId: 'o_oid'
    , 

    idx: 'o_callcnt'
    , 

    createdAt: 'o_calltime'
    , 

    resultCd: 'o_callresult'
    , 

    adminId: 'o_callmbr'
    , 

    comment: 'o_callmemo'
    , 

    reasonId: 'o_orid'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_calltable');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' call_view call');
    
      ar.select("call.call_id");

    
    
      ar.select("call.user_id");

    
    
      ar.select("call.before_rt_id");

    
    
      ar.select("call.rt_id");

    
    
      ar.select("call.rt_cd");

    
    
      ar.select("call.call_cd");

    
    
      ar.select("call.board_id");

    
    
      ar.select("call.idx");

    
    
      ar.select("call.created_at");

    
    
      ar.select("call.result_cd");

    
    
      ar.select("call.admin_id");

    
    
      ar.select("call.comment");

    
    
      ar.select("call.reason_id");

    
    
    return ar;
  }

  

  
}
export const CallSql =  new CallQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IEvent } from "../interface";


  class EventQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 event객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' event_view event');

    
      if (alias === true) {
        ar.select("event.event_id as 'event.event_id'" );

      } else{
        ar.select("event.event_id");

      }
      
      if (alias === true) {
        ar.select("event.title as 'event.title'" );

      } else{
        ar.select("event.title");

      }
      
      if (alias === true) {
        ar.select("event.title_sub as 'event.title_sub'" );

      } else{
        ar.select("event.title_sub");

      }
      
      if (alias === true) {
        ar.select("event.image_pc as 'event.image_pc'" );

      } else{
        ar.select("event.image_pc");

      }
      
      if (alias === true) {
        ar.select("event.image_mobile as 'event.image_mobile'" );

      } else{
        ar.select("event.image_mobile");

      }
      
      if (alias === true) {
        ar.select("event.detail_1_use as 'event.detail_1_use'" );

      } else{
        ar.select("event.detail_1_use");

      }
      
      if (alias === true) {
        ar.select("event.detail_1_title as 'event.detail_1_title'" );

      } else{
        ar.select("event.detail_1_title");

      }
      
      if (alias === true) {
        ar.select("event.detail_1_content as 'event.detail_1_content'" );

      } else{
        ar.select("event.detail_1_content");

      }
      
      if (alias === true) {
        ar.select("event.detail_2_use as 'event.detail_2_use'" );

      } else{
        ar.select("event.detail_2_use");

      }
      
      if (alias === true) {
        ar.select("event.detail_2_title as 'event.detail_2_title'" );

      } else{
        ar.select("event.detail_2_title");

      }
      
      if (alias === true) {
        ar.select("event.detail_2_content as 'event.detail_2_content'" );

      } else{
        ar.select("event.detail_2_content");

      }
      
      if (alias === true) {
        ar.select("event.detail_3_use as 'event.detail_3_use'" );

      } else{
        ar.select("event.detail_3_use");

      }
      
      if (alias === true) {
        ar.select("event.detail_3_title as 'event.detail_3_title'" );

      } else{
        ar.select("event.detail_3_title");

      }
      
      if (alias === true) {
        ar.select("event.detail_3_content as 'event.detail_3_content'" );

      } else{
        ar.select("event.detail_3_content");

      }
      
      if (alias === true) {
        ar.select("event.btn_use as 'event.btn_use'" );

      } else{
        ar.select("event.btn_use");

      }
      
      if (alias === true) {
        ar.select("event.btn_text as 'event.btn_text'" );

      } else{
        ar.select("event.btn_text");

      }
      
      if (alias === true) {
        ar.select("event.btn_link as 'event.btn_link'" );

      } else{
        ar.select("event.btn_link");

      }
      
      if (alias === true) {
        ar.select("event.start_day as 'event.start_day'" );

      } else{
        ar.select("event.start_day");

      }
      
      if (alias === true) {
        ar.select("event.end_day as 'event.end_day'" );

      } else{
        ar.select("event.end_day");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_event_id = 'event.event_id';
        if (alias !== undefined) {

          col_event_id = `${alias}.event_id`;

        }

        ar.select(`${col_event_id} as '${col_event_id}' `);

         
        let col_title = 'event.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_title_sub = 'event.title_sub';
        if (alias !== undefined) {

          col_title_sub = `${alias}.title_sub`;

        }

        ar.select(`${col_title_sub} as '${col_title_sub}' `);

         
        let col_image_pc = 'event.image_pc';
        if (alias !== undefined) {

          col_image_pc = `${alias}.image_pc`;

        }

        ar.select(`${col_image_pc} as '${col_image_pc}' `);

         
        let col_image_mobile = 'event.image_mobile';
        if (alias !== undefined) {

          col_image_mobile = `${alias}.image_mobile`;

        }

        ar.select(`${col_image_mobile} as '${col_image_mobile}' `);

         
        let col_detail_1_use = 'event.detail_1_use';
        if (alias !== undefined) {

          col_detail_1_use = `${alias}.detail_1_use`;

        }

        ar.select(`${col_detail_1_use} as '${col_detail_1_use}' `);

         
        let col_detail_1_title = 'event.detail_1_title';
        if (alias !== undefined) {

          col_detail_1_title = `${alias}.detail_1_title`;

        }

        ar.select(`${col_detail_1_title} as '${col_detail_1_title}' `);

         
        let col_detail_1_content = 'event.detail_1_content';
        if (alias !== undefined) {

          col_detail_1_content = `${alias}.detail_1_content`;

        }

        ar.select(`${col_detail_1_content} as '${col_detail_1_content}' `);

         
        let col_detail_2_use = 'event.detail_2_use';
        if (alias !== undefined) {

          col_detail_2_use = `${alias}.detail_2_use`;

        }

        ar.select(`${col_detail_2_use} as '${col_detail_2_use}' `);

         
        let col_detail_2_title = 'event.detail_2_title';
        if (alias !== undefined) {

          col_detail_2_title = `${alias}.detail_2_title`;

        }

        ar.select(`${col_detail_2_title} as '${col_detail_2_title}' `);

         
        let col_detail_2_content = 'event.detail_2_content';
        if (alias !== undefined) {

          col_detail_2_content = `${alias}.detail_2_content`;

        }

        ar.select(`${col_detail_2_content} as '${col_detail_2_content}' `);

         
        let col_detail_3_use = 'event.detail_3_use';
        if (alias !== undefined) {

          col_detail_3_use = `${alias}.detail_3_use`;

        }

        ar.select(`${col_detail_3_use} as '${col_detail_3_use}' `);

         
        let col_detail_3_title = 'event.detail_3_title';
        if (alias !== undefined) {

          col_detail_3_title = `${alias}.detail_3_title`;

        }

        ar.select(`${col_detail_3_title} as '${col_detail_3_title}' `);

         
        let col_detail_3_content = 'event.detail_3_content';
        if (alias !== undefined) {

          col_detail_3_content = `${alias}.detail_3_content`;

        }

        ar.select(`${col_detail_3_content} as '${col_detail_3_content}' `);

         
        let col_btn_use = 'event.btn_use';
        if (alias !== undefined) {

          col_btn_use = `${alias}.btn_use`;

        }

        ar.select(`${col_btn_use} as '${col_btn_use}' `);

         
        let col_btn_text = 'event.btn_text';
        if (alias !== undefined) {

          col_btn_text = `${alias}.btn_text`;

        }

        ar.select(`${col_btn_text} as '${col_btn_text}' `);

         
        let col_btn_link = 'event.btn_link';
        if (alias !== undefined) {

          col_btn_link = `${alias}.btn_link`;

        }

        ar.select(`${col_btn_link} as '${col_btn_link}' `);

         
        let col_start_day = 'event.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'event.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' e_list');
    
    if (nullCheck(form.eventId) === true) {
      ar.set("e_no", form.eventId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("e_title", form.title);
    } 
    

    if (nullCheck(form.titleSub) === true) {
      ar.set("e_title_sub", form.titleSub);
    } 
    

    if (nullCheck(form.imagePc) === true) {
      ar.set("e_image_pc", form.imagePc);
    } 
    

    if (nullCheck(form.imageMobile) === true) {
      ar.set("e_image_mobile", form.imageMobile);
    } 
    

    if (nullCheck(form.detail_1Use) === true) {
      ar.set("e_detail_1_use", form.detail_1Use);
    } 
    

    if (nullCheck(form.detail_1Title) === true) {
      ar.set("e_detail_1_title", form.detail_1Title);
    } 
    

    if (nullCheck(form.detail_1Content) === true) {
      ar.set("e_detail_1_content", form.detail_1Content);
    } 
    

    if (nullCheck(form.detail_2Use) === true) {
      ar.set("e_detail_2_use", form.detail_2Use);
    } 
    

    if (nullCheck(form.detail_2Title) === true) {
      ar.set("e_detail_2_title", form.detail_2Title);
    } 
    

    if (nullCheck(form.detail_2Content) === true) {
      ar.set("e_detail_2_content", form.detail_2Content);
    } 
    

    if (nullCheck(form.detail_3Use) === true) {
      ar.set("e_detail_3_use", form.detail_3Use);
    } 
    

    if (nullCheck(form.detail_3Title) === true) {
      ar.set("e_detail_3_title", form.detail_3Title);
    } 
    

    if (nullCheck(form.detail_3Content) === true) {
      ar.set("e_detail_3_content", form.detail_3Content);
    } 
    

    if (nullCheck(form.btnUse) === true) {
      ar.set("e_btn_use", form.btnUse);
    } 
    

    if (nullCheck(form.btnText) === true) {
      ar.set("e_btn_text", form.btnText);
    } 
    

    if (nullCheck(form.btnLink) === true) {
      ar.set("e_btn_link", form.btnLink);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("e_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("e_end", form.endDay);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' e_list');
    
    if (nullCheck(form.eventId) === true) {
      ar.set("e_no", form.eventId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("e_title", form.title);
    } 
    

    if (nullCheck(form.titleSub) === true) {
      ar.set("e_title_sub", form.titleSub);
    } 
    

    if (nullCheck(form.imagePc) === true) {
      ar.set("e_image_pc", form.imagePc);
    } 
    

    if (nullCheck(form.imageMobile) === true) {
      ar.set("e_image_mobile", form.imageMobile);
    } 
    

    if (nullCheck(form.detail_1Use) === true) {
      ar.set("e_detail_1_use", form.detail_1Use);
    } 
    

    if (nullCheck(form.detail_1Title) === true) {
      ar.set("e_detail_1_title", form.detail_1Title);
    } 
    

    if (nullCheck(form.detail_1Content) === true) {
      ar.set("e_detail_1_content", form.detail_1Content);
    } 
    

    if (nullCheck(form.detail_2Use) === true) {
      ar.set("e_detail_2_use", form.detail_2Use);
    } 
    

    if (nullCheck(form.detail_2Title) === true) {
      ar.set("e_detail_2_title", form.detail_2Title);
    } 
    

    if (nullCheck(form.detail_2Content) === true) {
      ar.set("e_detail_2_content", form.detail_2Content);
    } 
    

    if (nullCheck(form.detail_3Use) === true) {
      ar.set("e_detail_3_use", form.detail_3Use);
    } 
    

    if (nullCheck(form.detail_3Title) === true) {
      ar.set("e_detail_3_title", form.detail_3Title);
    } 
    

    if (nullCheck(form.detail_3Content) === true) {
      ar.set("e_detail_3_content", form.detail_3Content);
    } 
    

    if (nullCheck(form.btnUse) === true) {
      ar.set("e_btn_use", form.btnUse);
    } 
    

    if (nullCheck(form.btnText) === true) {
      ar.set("e_btn_text", form.btnText);
    } 
    

    if (nullCheck(form.btnLink) === true) {
      ar.set("e_btn_link", form.btnLink);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("e_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("e_end", form.endDay);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' event_view event');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    eventId: 'e_no'
    , 

    title: 'e_title'
    , 

    titleSub: 'e_title_sub'
    , 

    imagePc: 'e_image_pc'
    , 

    imageMobile: 'e_image_mobile'
    , 

    detail_1Use: 'e_detail_1_use'
    , 

    detail_1Title: 'e_detail_1_title'
    , 

    detail_1Content: 'e_detail_1_content'
    , 

    detail_2Use: 'e_detail_2_use'
    , 

    detail_2Title: 'e_detail_2_title'
    , 

    detail_2Content: 'e_detail_2_content'
    , 

    detail_3Use: 'e_detail_3_use'
    , 

    detail_3Title: 'e_detail_3_title'
    , 

    detail_3Content: 'e_detail_3_content'
    , 

    btnUse: 'e_btn_use'
    , 

    btnText: 'e_btn_text'
    , 

    btnLink: 'e_btn_link'
    , 

    startDay: 'e_start'
    , 

    endDay: 'e_end'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('e_list');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' event_view event');
    
      ar.select("event.event_id");

    
    
      ar.select("event.title");

    
    
      ar.select("event.title_sub");

    
    
      ar.select("event.image_pc");

    
    
      ar.select("event.image_mobile");

    
    
      ar.select("event.detail_1_use");

    
    
      ar.select("event.detail_1_title");

    
    
      ar.select("event.detail_1_content");

    
    
      ar.select("event.detail_2_use");

    
    
      ar.select("event.detail_2_title");

    
    
      ar.select("event.detail_2_content");

    
    
      ar.select("event.detail_3_use");

    
    
      ar.select("event.detail_3_title");

    
    
      ar.select("event.detail_3_content");

    
    
      ar.select("event.btn_use");

    
    
      ar.select("event.btn_text");

    
    
      ar.select("event.btn_link");

    
    
      ar.select("event.start_day");

    
    
      ar.select("event.end_day");

    
    
    return ar;
  }

  

  
}
export const EventSql =  new EventQuery()

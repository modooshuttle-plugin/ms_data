import { Ar, nullCheck } from "../../../util";
 
  import { ITosAgr } from "../interface";


  class TosAgrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 tos_agr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' tos_agr_view tos_agr');

    
      if (alias === true) {
        ar.select("tos_agr.tos_agr_id as 'tos_agr.tos_agr_id'" );

      } else{
        ar.select("tos_agr.tos_agr_id");

      }
      
      if (alias === true) {
        ar.select("tos_agr.user_id as 'tos_agr.user_id'" );

      } else{
        ar.select("tos_agr.user_id");

      }
      
      if (alias === true) {
        ar.select("tos_agr.login_id as 'tos_agr.login_id'" );

      } else{
        ar.select("tos_agr.login_id");

      }
      
      if (alias === true) {
        ar.select("tos_agr.phone as 'tos_agr.phone'" );

      } else{
        ar.select("tos_agr.phone");

      }
      
      if (alias === true) {
        ar.select("tos_agr.gender as 'tos_agr.gender'" );

      } else{
        ar.select("tos_agr.gender");

      }
      
      if (alias === true) {
        ar.select("tos_agr.age_range as 'tos_agr.age_range'" );

      } else{
        ar.select("tos_agr.age_range");

      }
      
      if (alias === true) {
        ar.select("tos_agr.birth_year as 'tos_agr.birth_year'" );

      } else{
        ar.select("tos_agr.birth_year");

      }
      
      if (alias === true) {
        ar.select("tos_agr.birth_month as 'tos_agr.birth_month'" );

      } else{
        ar.select("tos_agr.birth_month");

      }
      
      if (alias === true) {
        ar.select("tos_agr.birth_day as 'tos_agr.birth_day'" );

      } else{
        ar.select("tos_agr.birth_day");

      }
      
      if (alias === true) {
        ar.select("tos_agr.agr_channel as 'tos_agr.agr_channel'" );

      } else{
        ar.select("tos_agr.agr_channel");

      }
      
      if (alias === true) {
        ar.select("tos_agr.delete_yn as 'tos_agr.delete_yn'" );

      } else{
        ar.select("tos_agr.delete_yn");

      }
      
      if (alias === true) {
        ar.select("tos_agr.created_at as 'tos_agr.created_at'" );

      } else{
        ar.select("tos_agr.created_at");

      }
      
      if (alias === true) {
        ar.select("tos_agr.updated_at as 'tos_agr.updated_at'" );

      } else{
        ar.select("tos_agr.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_tos_agr_id = 'tos_agr.tos_agr_id';
        if (alias !== undefined) {

          col_tos_agr_id = `${alias}.tos_agr_id`;

        }

        ar.select(`${col_tos_agr_id} as '${col_tos_agr_id}' `);

         
        let col_user_id = 'tos_agr.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_login_id = 'tos_agr.login_id';
        if (alias !== undefined) {

          col_login_id = `${alias}.login_id`;

        }

        ar.select(`${col_login_id} as '${col_login_id}' `);

         
        let col_phone = 'tos_agr.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_gender = 'tos_agr.gender';
        if (alias !== undefined) {

          col_gender = `${alias}.gender`;

        }

        ar.select(`${col_gender} as '${col_gender}' `);

         
        let col_age_range = 'tos_agr.age_range';
        if (alias !== undefined) {

          col_age_range = `${alias}.age_range`;

        }

        ar.select(`${col_age_range} as '${col_age_range}' `);

         
        let col_birth_year = 'tos_agr.birth_year';
        if (alias !== undefined) {

          col_birth_year = `${alias}.birth_year`;

        }

        ar.select(`${col_birth_year} as '${col_birth_year}' `);

         
        let col_birth_month = 'tos_agr.birth_month';
        if (alias !== undefined) {

          col_birth_month = `${alias}.birth_month`;

        }

        ar.select(`${col_birth_month} as '${col_birth_month}' `);

         
        let col_birth_day = 'tos_agr.birth_day';
        if (alias !== undefined) {

          col_birth_day = `${alias}.birth_day`;

        }

        ar.select(`${col_birth_day} as '${col_birth_day}' `);

         
        let col_agr_channel = 'tos_agr.agr_channel';
        if (alias !== undefined) {

          col_agr_channel = `${alias}.agr_channel`;

        }

        ar.select(`${col_agr_channel} as '${col_agr_channel}' `);

         
        let col_delete_yn = 'tos_agr.delete_yn';
        if (alias !== undefined) {

          col_delete_yn = `${alias}.delete_yn`;

        }

        ar.select(`${col_delete_yn} as '${col_delete_yn}' `);

         
        let col_created_at = 'tos_agr.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'tos_agr.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' POLICY');
    
    if (nullCheck(form.tosAgrId) === true) {
      ar.set("SEQ", form.tosAgrId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.loginId) === true) {
      ar.set("m_id", form.loginId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("PHONE", form.phone);
    } 
    

    if (nullCheck(form.gender) === true) {
      ar.set("GENDER", form.gender);
    } 
    

    if (nullCheck(form.ageRange) === true) {
      ar.set("AGE_RANGE", form.ageRange);
    } 
    

    if (nullCheck(form.birthYear) === true) {
      ar.set("BIRTH_YYYY", form.birthYear);
    } 
    

    if (nullCheck(form.birthMonth) === true) {
      ar.set("BIRTH_MM", form.birthMonth);
    } 
    

    if (nullCheck(form.birthDay) === true) {
      ar.set("BIRTH_DD", form.birthDay);
    } 
    

    if (nullCheck(form.agrChannel) === true) {
      ar.set("AGR_CHNNL", form.agrChannel);
    } 
    

    if (nullCheck(form.deleteYn) === true) {
      ar.set("DEL_YN", form.deleteYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("REG_DT", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("UDT_DT", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' POLICY');
    
    if (nullCheck(form.tosAgrId) === true) {
      ar.set("SEQ", form.tosAgrId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.loginId) === true) {
      ar.set("m_id", form.loginId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("PHONE", form.phone);
    } 
    

    if (nullCheck(form.gender) === true) {
      ar.set("GENDER", form.gender);
    } 
    

    if (nullCheck(form.ageRange) === true) {
      ar.set("AGE_RANGE", form.ageRange);
    } 
    

    if (nullCheck(form.birthYear) === true) {
      ar.set("BIRTH_YYYY", form.birthYear);
    } 
    

    if (nullCheck(form.birthMonth) === true) {
      ar.set("BIRTH_MM", form.birthMonth);
    } 
    

    if (nullCheck(form.birthDay) === true) {
      ar.set("BIRTH_DD", form.birthDay);
    } 
    

    if (nullCheck(form.agrChannel) === true) {
      ar.set("AGR_CHNNL", form.agrChannel);
    } 
    

    if (nullCheck(form.deleteYn) === true) {
      ar.set("DEL_YN", form.deleteYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("REG_DT", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("UDT_DT", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' tos_agr_view tos_agr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    tosAgrId: 'SEQ'
    , 

    userId: 'm_mid'
    , 

    loginId: 'm_id'
    , 

    phone: 'PHONE'
    , 

    gender: 'GENDER'
    , 

    ageRange: 'AGE_RANGE'
    , 

    birthYear: 'BIRTH_YYYY'
    , 

    birthMonth: 'BIRTH_MM'
    , 

    birthDay: 'BIRTH_DD'
    , 

    agrChannel: 'AGR_CHNNL'
    , 

    deleteYn: 'DEL_YN'
    , 

    createdAt: 'REG_DT'
    , 

    updatedAt: 'UDT_DT'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('POLICY');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' tos_agr_view tos_agr');
    
      ar.select("tos_agr.tos_agr_id");

    
    
      ar.select("tos_agr.user_id");

    
    
      ar.select("tos_agr.login_id");

    
    
      ar.select("tos_agr.phone");

    
    
      ar.select("tos_agr.gender");

    
    
      ar.select("tos_agr.age_range");

    
    
      ar.select("tos_agr.birth_year");

    
    
      ar.select("tos_agr.birth_month");

    
    
      ar.select("tos_agr.birth_day");

    
    
      ar.select("tos_agr.agr_channel");

    
    
      ar.select("tos_agr.delete_yn");

    
    
      ar.select("tos_agr.created_at");

    
    
      ar.select("tos_agr.updated_at");

    
    
    return ar;
  }

  

  
}
export const TosAgrSql =  new TosAgrQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IAdmin } from "../interface";


  class AdminQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 admin객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' admin_view admin');

    
      if (alias === true) {
        ar.select("admin.admin_id as 'admin.admin_id'" );

      } else{
        ar.select("admin.admin_id");

      }
      
      if (alias === true) {
        ar.select("admin.pw as 'admin.pw'" );

      } else{
        ar.select("admin.pw");

      }
      
      if (alias === true) {
        ar.select("admin.nm as 'admin.nm'" );

      } else{
        ar.select("admin.nm");

      }
      
      if (alias === true) {
        ar.select("admin.created_at as 'admin.created_at'" );

      } else{
        ar.select("admin.created_at");

      }
      
      if (alias === true) {
        ar.select("admin.updated_at as 'admin.updated_at'" );

      } else{
        ar.select("admin.updated_at");

      }
      
      if (alias === true) {
        ar.select("admin.email as 'admin.email'" );

      } else{
        ar.select("admin.email");

      }
      
      if (alias === true) {
        ar.select("admin.phone as 'admin.phone'" );

      } else{
        ar.select("admin.phone");

      }
      
      if (alias === true) {
        ar.select("admin.notion_user_id as 'admin.notion_user_id'" );

      } else{
        ar.select("admin.notion_user_id");

      }
      
      if (alias === true) {
        ar.select("admin.admin_cd as 'admin.admin_cd'" );

      } else{
        ar.select("admin.admin_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_admin_id = 'admin.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_pw = 'admin.pw';
        if (alias !== undefined) {

          col_pw = `${alias}.pw`;

        }

        ar.select(`${col_pw} as '${col_pw}' `);

         
        let col_nm = 'admin.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_created_at = 'admin.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'admin.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_email = 'admin.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_phone = 'admin.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_notion_user_id = 'admin.notion_user_id';
        if (alias !== undefined) {

          col_notion_user_id = `${alias}.notion_user_id`;

        }

        ar.select(`${col_notion_user_id} as '${col_notion_user_id}' `);

         
        let col_admin_cd = 'admin.admin_cd';
        if (alias !== undefined) {

          col_admin_cd = `${alias}.admin_cd`;

        }

        ar.select(`${col_admin_cd} as '${col_admin_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' admin');
    
    if (nullCheck(form.adminId) === true) {
      ar.set("ad_no", form.adminId);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("ad_pw", form.pw);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("ad_nick", form.nm);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("ad_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("ad_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("ad_email", form.email);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("ad_phone", form.phone);
    } 
    

    if (nullCheck(form.notionUserId) === true) {
      ar.set("notion_user_id", form.notionUserId);
    } 
    

    if (nullCheck(form.adminCd) === true) {
      ar.set("admin_cd", form.adminCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' admin');
    
    if (nullCheck(form.adminId) === true) {
      ar.set("ad_no", form.adminId);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("ad_pw", form.pw);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("ad_nick", form.nm);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("ad_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("ad_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("ad_email", form.email);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("ad_phone", form.phone);
    } 
    

    if (nullCheck(form.notionUserId) === true) {
      ar.set("notion_user_id", form.notionUserId);
    } 
    

    if (nullCheck(form.adminCd) === true) {
      ar.set("admin_cd", form.adminCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' admin_view admin');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    adminId: 'ad_no'
    , 

    pw: 'ad_pw'
    , 

    nm: 'ad_nick'
    , 

    createdAt: 'ad_timestamp'
    , 

    updatedAt: 'ad_timestamp_u'
    , 

    email: 'ad_email'
    , 

    phone: 'ad_phone'
    , 

    notionUserId: 'notion_user_id'
    , 

    adminCd: 'admin_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('admin');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' admin_view admin');
    
      ar.select("admin.admin_id");

    
    
      ar.select("admin.pw");

    
    
      ar.select("admin.nm");

    
    
      ar.select("admin.created_at");

    
    
      ar.select("admin.updated_at");

    
    
      ar.select("admin.email");

    
    
      ar.select("admin.phone");

    
    
      ar.select("admin.notion_user_id");

    
    
      ar.select("admin.admin_cd");

    
    
    return ar;
  }

  

  
}
export const AdminSql =  new AdminQuery()

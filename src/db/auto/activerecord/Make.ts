import { Ar, nullCheck } from "../../../util";
 
  import { IMake } from "../interface";


  class MakeQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 make객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' make_view make');

    
      if (alias === true) {
        ar.select("make.make_id as 'make.make_id'" );

      } else{
        ar.select("make.make_id");

      }
      
      if (alias === true) {
        ar.select("make.user_id as 'make.user_id'" );

      } else{
        ar.select("make.user_id");

      }
      
      if (alias === true) {
        ar.select("make.start_addr as 'make.start_addr'" );

      } else{
        ar.select("make.start_addr");

      }
      
      if (alias === true) {
        ar.select("make.start_lat as 'make.start_lat'" );

      } else{
        ar.select("make.start_lat");

      }
      
      if (alias === true) {
        ar.select("make.start_lng as 'make.start_lng'" );

      } else{
        ar.select("make.start_lng");

      }
      
      if (alias === true) {
        ar.select("make.end_addr as 'make.end_addr'" );

      } else{
        ar.select("make.end_addr");

      }
      
      if (alias === true) {
        ar.select("make.end_lat as 'make.end_lat'" );

      } else{
        ar.select("make.end_lat");

      }
      
      if (alias === true) {
        ar.select("make.end_lng as 'make.end_lng'" );

      } else{
        ar.select("make.end_lng");

      }
      
      if (alias === true) {
        ar.select("make.time_id as 'make.time_id'" );

      } else{
        ar.select("make.time_id");

      }
      
      if (alias === true) {
        ar.select("make.created_at as 'make.created_at'" );

      } else{
        ar.select("make.created_at");

      }
      
      if (alias === true) {
        ar.select("make.updated_at as 'make.updated_at'" );

      } else{
        ar.select("make.updated_at");

      }
      
      if (alias === true) {
        ar.select("make.start_title as 'make.start_title'" );

      } else{
        ar.select("make.start_title");

      }
      
      if (alias === true) {
        ar.select("make.end_title as 'make.end_title'" );

      } else{
        ar.select("make.end_title");

      }
      
      if (alias === true) {
        ar.select("make.comment as 'make.comment'" );

      } else{
        ar.select("make.comment");

      }
      
      if (alias === true) {
        ar.select("make.email as 'make.email'" );

      } else{
        ar.select("make.email");

      }
      
      if (alias === true) {
        ar.select("make.org_id as 'make.org_id'" );

      } else{
        ar.select("make.org_id");

      }
      
      if (alias === true) {
        ar.select("make.start_cat_cd as 'make.start_cat_cd'" );

      } else{
        ar.select("make.start_cat_cd");

      }
      
      if (alias === true) {
        ar.select("make.end_cat_cd as 'make.end_cat_cd'" );

      } else{
        ar.select("make.end_cat_cd");

      }
      
      if (alias === true) {
        ar.select("make.confirm_yn as 'make.confirm_yn'" );

      } else{
        ar.select("make.confirm_yn");

      }
      
      if (alias === true) {
        ar.select("make.start_emd_cd as 'make.start_emd_cd'" );

      } else{
        ar.select("make.start_emd_cd");

      }
      
      if (alias === true) {
        ar.select("make.end_emd_cd as 'make.end_emd_cd'" );

      } else{
        ar.select("make.end_emd_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_make_id = 'make.make_id';
        if (alias !== undefined) {

          col_make_id = `${alias}.make_id`;

        }

        ar.select(`${col_make_id} as '${col_make_id}' `);

         
        let col_user_id = 'make.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_start_addr = 'make.start_addr';
        if (alias !== undefined) {

          col_start_addr = `${alias}.start_addr`;

        }

        ar.select(`${col_start_addr} as '${col_start_addr}' `);

         
        let col_start_lat = 'make.start_lat';
        if (alias !== undefined) {

          col_start_lat = `${alias}.start_lat`;

        }

        ar.select(`${col_start_lat} as '${col_start_lat}' `);

         
        let col_start_lng = 'make.start_lng';
        if (alias !== undefined) {

          col_start_lng = `${alias}.start_lng`;

        }

        ar.select(`${col_start_lng} as '${col_start_lng}' `);

         
        let col_end_addr = 'make.end_addr';
        if (alias !== undefined) {

          col_end_addr = `${alias}.end_addr`;

        }

        ar.select(`${col_end_addr} as '${col_end_addr}' `);

         
        let col_end_lat = 'make.end_lat';
        if (alias !== undefined) {

          col_end_lat = `${alias}.end_lat`;

        }

        ar.select(`${col_end_lat} as '${col_end_lat}' `);

         
        let col_end_lng = 'make.end_lng';
        if (alias !== undefined) {

          col_end_lng = `${alias}.end_lng`;

        }

        ar.select(`${col_end_lng} as '${col_end_lng}' `);

         
        let col_time_id = 'make.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_created_at = 'make.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'make.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_start_title = 'make.start_title';
        if (alias !== undefined) {

          col_start_title = `${alias}.start_title`;

        }

        ar.select(`${col_start_title} as '${col_start_title}' `);

         
        let col_end_title = 'make.end_title';
        if (alias !== undefined) {

          col_end_title = `${alias}.end_title`;

        }

        ar.select(`${col_end_title} as '${col_end_title}' `);

         
        let col_comment = 'make.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_email = 'make.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_org_id = 'make.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_start_cat_cd = 'make.start_cat_cd';
        if (alias !== undefined) {

          col_start_cat_cd = `${alias}.start_cat_cd`;

        }

        ar.select(`${col_start_cat_cd} as '${col_start_cat_cd}' `);

         
        let col_end_cat_cd = 'make.end_cat_cd';
        if (alias !== undefined) {

          col_end_cat_cd = `${alias}.end_cat_cd`;

        }

        ar.select(`${col_end_cat_cd} as '${col_end_cat_cd}' `);

         
        let col_confirm_yn = 'make.confirm_yn';
        if (alias !== undefined) {

          col_confirm_yn = `${alias}.confirm_yn`;

        }

        ar.select(`${col_confirm_yn} as '${col_confirm_yn}' `);

         
        let col_start_emd_cd = 'make.start_emd_cd';
        if (alias !== undefined) {

          col_start_emd_cd = `${alias}.start_emd_cd`;

        }

        ar.select(`${col_start_emd_cd} as '${col_start_emd_cd}' `);

         
        let col_end_emd_cd = 'make.end_emd_cd';
        if (alias !== undefined) {

          col_end_emd_cd = `${alias}.end_emd_cd`;

        }

        ar.select(`${col_end_emd_cd} as '${col_end_emd_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_make');
    
    if (nullCheck(form.makeId) === true) {
      ar.set("o_no", form.makeId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("o_home", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("o_home_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("o_home_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("o_work", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("o_work_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("o_work_lng", form.endLng);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("o_tid", form.timeId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.startTitle) === true) {
      ar.set("o_home_title", form.startTitle);
    } 
    

    if (nullCheck(form.endTitle) === true) {
      ar.set("o_work_title", form.endTitle);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_request", form.comment);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("o_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("o_company", form.orgId);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("o_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("o_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.confirmYn) === true) {
      ar.set("o_confirm", form.confirmYn);
    } 
    

    if (nullCheck(form.startEmdCd) === true) {
      ar.set("sta_emd_cd", form.startEmdCd);
    } 
    

    if (nullCheck(form.endEmdCd) === true) {
      ar.set("end_emd_cd", form.endEmdCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_make');
    
    if (nullCheck(form.makeId) === true) {
      ar.set("o_no", form.makeId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("o_home", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("o_home_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("o_home_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("o_work", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("o_work_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("o_work_lng", form.endLng);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("o_tid", form.timeId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.startTitle) === true) {
      ar.set("o_home_title", form.startTitle);
    } 
    

    if (nullCheck(form.endTitle) === true) {
      ar.set("o_work_title", form.endTitle);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_request", form.comment);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("o_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("o_company", form.orgId);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("o_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("o_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.confirmYn) === true) {
      ar.set("o_confirm", form.confirmYn);
    } 
    

    if (nullCheck(form.startEmdCd) === true) {
      ar.set("sta_emd_cd", form.startEmdCd);
    } 
    

    if (nullCheck(form.endEmdCd) === true) {
      ar.set("end_emd_cd", form.endEmdCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' make_view make');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    makeId: 'o_no'
    , 

    userId: 'o_mid'
    , 

    startAddr: 'o_home'
    , 

    startLat: 'o_home_lat'
    , 

    startLng: 'o_home_lng'
    , 

    endAddr: 'o_work'
    , 

    endLat: 'o_work_lat'
    , 

    endLng: 'o_work_lng'
    , 

    timeId: 'o_tid'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    startTitle: 'o_home_title'
    , 

    endTitle: 'o_work_title'
    , 

    comment: 'o_request'
    , 

    email: 'o_email'
    , 

    orgId: 'o_company'
    , 

    startCatCd: 'o_home_c'
    , 

    endCatCd: 'o_work_c'
    , 

    confirmYn: 'o_confirm'
    , 

    startEmdCd: 'sta_emd_cd'
    , 

    endEmdCd: 'end_emd_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_make');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' make_view make');
    
      ar.select("make.make_id");

    
    
      ar.select("make.user_id");

    
    
      ar.select("make.start_addr");

    
    
      ar.select("make.start_lat");

    
    
      ar.select("make.start_lng");

    
    
      ar.select("make.end_addr");

    
    
      ar.select("make.end_lat");

    
    
      ar.select("make.end_lng");

    
    
      ar.select("make.time_id");

    
    
      ar.select("make.created_at");

    
    
      ar.select("make.updated_at");

    
    
      ar.select("make.start_title");

    
    
      ar.select("make.end_title");

    
    
      ar.select("make.comment");

    
    
      ar.select("make.email");

    
    
      ar.select("make.org_id");

    
    
      ar.select("make.start_cat_cd");

    
    
      ar.select("make.end_cat_cd");

    
    
      ar.select("make.confirm_yn");

    
    
      ar.select("make.start_emd_cd");

    
    
      ar.select("make.end_emd_cd");

    
    
    return ar;
  }

  

  
}
export const MakeSql =  new MakeQuery()

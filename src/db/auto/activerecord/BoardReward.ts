import { Ar, nullCheck } from "../../../util";
 
  import { IBoardReward } from "../interface";


  class BoardRewardQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 board_reward객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' board_reward_view board_reward');

    
      if (alias === true) {
        ar.select("board_reward.board_reward_id as 'board_reward.board_reward_id'" );

      } else{
        ar.select("board_reward.board_reward_id");

      }
      
      if (alias === true) {
        ar.select("board_reward.user_id as 'board_reward.user_id'" );

      } else{
        ar.select("board_reward.user_id");

      }
      
      if (alias === true) {
        ar.select("board_reward.runn_iss_id as 'board_reward.runn_iss_id'" );

      } else{
        ar.select("board_reward.runn_iss_id");

      }
      
      if (alias === true) {
        ar.select("board_reward.board_iss_id as 'board_reward.board_iss_id'" );

      } else{
        ar.select("board_reward.board_iss_id");

      }
      
      if (alias === true) {
        ar.select("board_reward.board_id as 'board_reward.board_id'" );

      } else{
        ar.select("board_reward.board_id");

      }
      
      if (alias === true) {
        ar.select("board_reward.status_cd as 'board_reward.status_cd'" );

      } else{
        ar.select("board_reward.status_cd");

      }
      
      if (alias === true) {
        ar.select("board_reward.reward_cd as 'board_reward.reward_cd'" );

      } else{
        ar.select("board_reward.reward_cd");

      }
      
      if (alias === true) {
        ar.select("board_reward.method_cd as 'board_reward.method_cd'" );

      } else{
        ar.select("board_reward.method_cd");

      }
      
      if (alias === true) {
        ar.select("board_reward.reward_dtl as 'board_reward.reward_dtl'" );

      } else{
        ar.select("board_reward.reward_dtl");

      }
      
      if (alias === true) {
        ar.select("board_reward.path_folder as 'board_reward.path_folder'" );

      } else{
        ar.select("board_reward.path_folder");

      }
      
      if (alias === true) {
        ar.select("board_reward.created_at as 'board_reward.created_at'" );

      } else{
        ar.select("board_reward.created_at");

      }
      
      if (alias === true) {
        ar.select("board_reward.updated_at as 'board_reward.updated_at'" );

      } else{
        ar.select("board_reward.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_board_reward_id = 'board_reward.board_reward_id';
        if (alias !== undefined) {

          col_board_reward_id = `${alias}.board_reward_id`;

        }

        ar.select(`${col_board_reward_id} as '${col_board_reward_id}' `);

         
        let col_user_id = 'board_reward.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_runn_iss_id = 'board_reward.runn_iss_id';
        if (alias !== undefined) {

          col_runn_iss_id = `${alias}.runn_iss_id`;

        }

        ar.select(`${col_runn_iss_id} as '${col_runn_iss_id}' `);

         
        let col_board_iss_id = 'board_reward.board_iss_id';
        if (alias !== undefined) {

          col_board_iss_id = `${alias}.board_iss_id`;

        }

        ar.select(`${col_board_iss_id} as '${col_board_iss_id}' `);

         
        let col_board_id = 'board_reward.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_status_cd = 'board_reward.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_reward_cd = 'board_reward.reward_cd';
        if (alias !== undefined) {

          col_reward_cd = `${alias}.reward_cd`;

        }

        ar.select(`${col_reward_cd} as '${col_reward_cd}' `);

         
        let col_method_cd = 'board_reward.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_reward_dtl = 'board_reward.reward_dtl';
        if (alias !== undefined) {

          col_reward_dtl = `${alias}.reward_dtl`;

        }

        ar.select(`${col_reward_dtl} as '${col_reward_dtl}' `);

         
        let col_path_folder = 'board_reward.path_folder';
        if (alias !== undefined) {

          col_path_folder = `${alias}.path_folder`;

        }

        ar.select(`${col_path_folder} as '${col_path_folder}' `);

         
        let col_created_at = 'board_reward.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'board_reward.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' board_reward');
    
    if (nullCheck(form.boardRewardId) === true) {
      ar.set("board_reward_id", form.boardRewardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.boardIssId) === true) {
      ar.set("board_iss_id", form.boardIssId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.rewardCd) === true) {
      ar.set("reward_cd", form.rewardCd);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.rewardDtl) === true) {
      ar.set("reward_dtl", form.rewardDtl);
    } 
    

    if (nullCheck(form.pathFolder) === true) {
      ar.set("path_folder", form.pathFolder);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' board_reward');
    
    if (nullCheck(form.boardRewardId) === true) {
      ar.set("board_reward_id", form.boardRewardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.boardIssId) === true) {
      ar.set("board_iss_id", form.boardIssId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.rewardCd) === true) {
      ar.set("reward_cd", form.rewardCd);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.rewardDtl) === true) {
      ar.set("reward_dtl", form.rewardDtl);
    } 
    

    if (nullCheck(form.pathFolder) === true) {
      ar.set("path_folder", form.pathFolder);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' board_reward_view board_reward');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    boardRewardId: 'board_reward_id'
    , 

    userId: 'user_id'
    , 

    runnIssId: 'runn_iss_id'
    , 

    boardIssId: 'board_iss_id'
    , 

    boardId: 'board_id'
    , 

    statusCd: 'status_cd'
    , 

    rewardCd: 'reward_cd'
    , 

    methodCd: 'method_cd'
    , 

    rewardDtl: 'reward_dtl'
    , 

    pathFolder: 'path_folder'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('board_reward');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' board_reward_view board_reward');
    
      ar.select("board_reward.board_reward_id");

    
    
      ar.select("board_reward.user_id");

    
    
      ar.select("board_reward.runn_iss_id");

    
    
      ar.select("board_reward.board_iss_id");

    
    
      ar.select("board_reward.board_id");

    
    
      ar.select("board_reward.status_cd");

    
    
      ar.select("board_reward.reward_cd");

    
    
      ar.select("board_reward.method_cd");

    
    
      ar.select("board_reward.reward_dtl");

    
    
      ar.select("board_reward.path_folder");

    
    
      ar.select("board_reward.created_at");

    
    
      ar.select("board_reward.updated_at");

    
    
    return ar;
  }

  

  
}
export const BoardRewardSql =  new BoardRewardQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { ISt } from "../interface";


  class StQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 st객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' st_view st');

    
      if (alias === true) {
        ar.select("st.st_id as 'st.st_id'" );

      } else{
        ar.select("st.st_id");

      }
      
      if (alias === true) {
        ar.select("st.rt_id as 'st.rt_id'" );

      } else{
        ar.select("st.rt_id");

      }
      
      if (alias === true) {
        ar.select("st.section_cd as 'st.section_cd'" );

      } else{
        ar.select("st.section_cd");

      }
      
      if (alias === true) {
        ar.select("st.idx as 'st.idx'" );

      } else{
        ar.select("st.idx");

      }
      
      if (alias === true) {
        ar.select("st.path_ver as 'st.path_ver'" );

      } else{
        ar.select("st.path_ver");

      }
      
      if (alias === true) {
        ar.select("st.st_cd as 'st.st_cd'" );

      } else{
        ar.select("st.st_cd");

      }
      
      if (alias === true) {
        ar.select("st.addr as 'st.addr'" );

      } else{
        ar.select("st.addr");

      }
      
      if (alias === true) {
        ar.select("st.alias as 'st.alias'" );

      } else{
        ar.select("st.alias");

      }
      
      if (alias === true) {
        ar.select("st.lat as 'st.lat'" );

      } else{
        ar.select("st.lat");

      }
      
      if (alias === true) {
        ar.select("st.lng as 'st.lng'" );

      } else{
        ar.select("st.lng");

      }
      
      if (alias === true) {
        ar.select("st.time as 'st.time'" );

      } else{
        ar.select("st.time");

      }
      
      if (alias === true) {
        ar.select("st.time_mon as 'st.time_mon'" );

      } else{
        ar.select("st.time_mon");

      }
      
      if (alias === true) {
        ar.select("st.created_at as 'st.created_at'" );

      } else{
        ar.select("st.created_at");

      }
      
      if (alias === true) {
        ar.select("st.updated_at as 'st.updated_at'" );

      } else{
        ar.select("st.updated_at");

      }
      
      if (alias === true) {
        ar.select("st.emd_cd as 'st.emd_cd'" );

      } else{
        ar.select("st.emd_cd");

      }
      
      if (alias === true) {
        ar.select("st.pan as 'st.pan'" );

      } else{
        ar.select("st.pan");

      }
      
      if (alias === true) {
        ar.select("st.pan_lat as 'st.pan_lat'" );

      } else{
        ar.select("st.pan_lat");

      }
      
      if (alias === true) {
        ar.select("st.pan_lng as 'st.pan_lng'" );

      } else{
        ar.select("st.pan_lng");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_st_id = 'st.st_id';
        if (alias !== undefined) {

          col_st_id = `${alias}.st_id`;

        }

        ar.select(`${col_st_id} as '${col_st_id}' `);

         
        let col_rt_id = 'st.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_section_cd = 'st.section_cd';
        if (alias !== undefined) {

          col_section_cd = `${alias}.section_cd`;

        }

        ar.select(`${col_section_cd} as '${col_section_cd}' `);

         
        let col_idx = 'st.idx';
        if (alias !== undefined) {

          col_idx = `${alias}.idx`;

        }

        ar.select(`${col_idx} as '${col_idx}' `);

         
        let col_path_ver = 'st.path_ver';
        if (alias !== undefined) {

          col_path_ver = `${alias}.path_ver`;

        }

        ar.select(`${col_path_ver} as '${col_path_ver}' `);

         
        let col_st_cd = 'st.st_cd';
        if (alias !== undefined) {

          col_st_cd = `${alias}.st_cd`;

        }

        ar.select(`${col_st_cd} as '${col_st_cd}' `);

         
        let col_addr = 'st.addr';
        if (alias !== undefined) {

          col_addr = `${alias}.addr`;

        }

        ar.select(`${col_addr} as '${col_addr}' `);

         
        let col_alias = 'st.alias';
        if (alias !== undefined) {

          col_alias = `${alias}.alias`;

        }

        ar.select(`${col_alias} as '${col_alias}' `);

         
        let col_lat = 'st.lat';
        if (alias !== undefined) {

          col_lat = `${alias}.lat`;

        }

        ar.select(`${col_lat} as '${col_lat}' `);

         
        let col_lng = 'st.lng';
        if (alias !== undefined) {

          col_lng = `${alias}.lng`;

        }

        ar.select(`${col_lng} as '${col_lng}' `);

         
        let col_time = 'st.time';
        if (alias !== undefined) {

          col_time = `${alias}.time`;

        }

        ar.select(`${col_time} as '${col_time}' `);

         
        let col_time_mon = 'st.time_mon';
        if (alias !== undefined) {

          col_time_mon = `${alias}.time_mon`;

        }

        ar.select(`${col_time_mon} as '${col_time_mon}' `);

         
        let col_created_at = 'st.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'st.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_emd_cd = 'st.emd_cd';
        if (alias !== undefined) {

          col_emd_cd = `${alias}.emd_cd`;

        }

        ar.select(`${col_emd_cd} as '${col_emd_cd}' `);

         
        let col_pan = 'st.pan';
        if (alias !== undefined) {

          col_pan = `${alias}.pan`;

        }

        ar.select(`${col_pan} as '${col_pan}' `);

         
        let col_pan_lat = 'st.pan_lat';
        if (alias !== undefined) {

          col_pan_lat = `${alias}.pan_lat`;

        }

        ar.select(`${col_pan_lat} as '${col_pan_lat}' `);

         
        let col_pan_lng = 'st.pan_lng';
        if (alias !== undefined) {

          col_pan_lng = `${alias}.pan_lng`;

        }

        ar.select(`${col_pan_lng} as '${col_pan_lng}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_stop');
    
    if (nullCheck(form.stId) === true) {
      ar.set("r_no", form.stId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("r_se", form.sectionCd);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("r_idx", form.idx);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("r_line_ver", form.pathVer);
    } 
    

    if (nullCheck(form.stCd) === true) {
      ar.set("r_rsid", form.stCd);
    } 
    

    if (nullCheck(form.addr) === true) {
      ar.set("r_addr", form.addr);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("r_alias", form.alias);
    } 
    

    if (nullCheck(form.lat) === true) {
      ar.set("r_lat", form.lat);
    } 
    

    if (nullCheck(form.lng) === true) {
      ar.set("r_lng", form.lng);
    } 
    

    if (nullCheck(form.time) === true) {
      ar.set("r_time", form.time);
    } 
    

    if (nullCheck(form.timeMon) === true) {
      ar.set("r_time_mon", form.timeMon);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.emdCd) === true) {
      ar.set("emd_cd", form.emdCd);
    } 
    

    if (nullCheck(form.pan) === true) {
      ar.set("pan", form.pan);
    } 
    

    if (nullCheck(form.panLat) === true) {
      ar.set("pan_lat", form.panLat);
    } 
    

    if (nullCheck(form.panLng) === true) {
      ar.set("pan_lng", form.panLng);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_stop');
    
    if (nullCheck(form.stId) === true) {
      ar.set("r_no", form.stId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("r_se", form.sectionCd);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("r_idx", form.idx);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("r_line_ver", form.pathVer);
    } 
    

    if (nullCheck(form.stCd) === true) {
      ar.set("r_rsid", form.stCd);
    } 
    

    if (nullCheck(form.addr) === true) {
      ar.set("r_addr", form.addr);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("r_alias", form.alias);
    } 
    

    if (nullCheck(form.lat) === true) {
      ar.set("r_lat", form.lat);
    } 
    

    if (nullCheck(form.lng) === true) {
      ar.set("r_lng", form.lng);
    } 
    

    if (nullCheck(form.time) === true) {
      ar.set("r_time", form.time);
    } 
    

    if (nullCheck(form.timeMon) === true) {
      ar.set("r_time_mon", form.timeMon);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.emdCd) === true) {
      ar.set("emd_cd", form.emdCd);
    } 
    

    if (nullCheck(form.pan) === true) {
      ar.set("pan", form.pan);
    } 
    

    if (nullCheck(form.panLat) === true) {
      ar.set("pan_lat", form.panLat);
    } 
    

    if (nullCheck(form.panLng) === true) {
      ar.set("pan_lng", form.panLng);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' st_view st');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    stId: 'r_no'
    , 

    rtId: 'r_rid'
    , 

    sectionCd: 'r_se'
    , 

    idx: 'r_idx'
    , 

    pathVer: 'r_line_ver'
    , 

    stCd: 'r_rsid'
    , 

    addr: 'r_addr'
    , 

    alias: 'r_alias'
    , 

    lat: 'r_lat'
    , 

    lng: 'r_lng'
    , 

    time: 'r_time'
    , 

    timeMon: 'r_time_mon'
    , 

    createdAt: 'r_timestamp'
    , 

    updatedAt: 'r_timestamp_u'
    , 

    emdCd: 'emd_cd'
    , 

    pan: 'pan'
    , 

    panLat: 'pan_lat'
    , 

    panLng: 'pan_lng'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('r_stop');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' st_view st');
    
      ar.select("st.st_id");

    
    
      ar.select("st.rt_id");

    
    
      ar.select("st.section_cd");

    
    
      ar.select("st.idx");

    
    
      ar.select("st.path_ver");

    
    
      ar.select("st.st_cd");

    
    
      ar.select("st.addr");

    
    
      ar.select("st.alias");

    
    
      ar.select("st.lat");

    
    
      ar.select("st.lng");

    
    
      ar.select("st.time");

    
    
      ar.select("st.time_mon");

    
    
      ar.select("st.created_at");

    
    
      ar.select("st.updated_at");

    
    
      ar.select("st.emd_cd");

    
    
      ar.select("st.pan");

    
    
      ar.select("st.pan_lat");

    
    
      ar.select("st.pan_lng");

    
    
    return ar;
  }

  

  
}
export const StSql =  new StQuery()

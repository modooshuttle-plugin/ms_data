import { Ar, nullCheck } from "../../../util";
 
  import { IRtPath } from "../interface";


  class RtPathQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt_path객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_path_view rt_path');

    
      if (alias === true) {
        ar.select("rt_path.rt_path_id as 'rt_path.rt_path_id'" );

      } else{
        ar.select("rt_path.rt_path_id");

      }
      
      if (alias === true) {
        ar.select("rt_path.rt_id as 'rt_path.rt_id'" );

      } else{
        ar.select("rt_path.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_path.path_ver as 'rt_path.path_ver'" );

      } else{
        ar.select("rt_path.path_ver");

      }
      
      if (alias === true) {
        ar.select("rt_path.section_cd as 'rt_path.section_cd'" );

      } else{
        ar.select("rt_path.section_cd");

      }
      
      if (alias === true) {
        ar.select("rt_path.geom as 'rt_path.geom'" );

      } else{
        ar.select("rt_path.geom");

      }
      
      if (alias === true) {
        ar.select("rt_path.created_at as 'rt_path.created_at'" );

      } else{
        ar.select("rt_path.created_at");

      }
      
      if (alias === true) {
        ar.select("rt_path.updated_at as 'rt_path.updated_at'" );

      } else{
        ar.select("rt_path.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_path_id = 'rt_path.rt_path_id';
        if (alias !== undefined) {

          col_rt_path_id = `${alias}.rt_path_id`;

        }

        ar.select(`${col_rt_path_id} as '${col_rt_path_id}' `);

         
        let col_rt_id = 'rt_path.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_path_ver = 'rt_path.path_ver';
        if (alias !== undefined) {

          col_path_ver = `${alias}.path_ver`;

        }

        ar.select(`${col_path_ver} as '${col_path_ver}' `);

         
        let col_section_cd = 'rt_path.section_cd';
        if (alias !== undefined) {

          col_section_cd = `${alias}.section_cd`;

        }

        ar.select(`${col_section_cd} as '${col_section_cd}' `);

         
        let col_geom = 'rt_path.geom';
        if (alias !== undefined) {

          col_geom = `${alias}.geom`;

        }

        ar.select(`${col_geom} as '${col_geom}' `);

         
        let col_created_at = 'rt_path.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt_path.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_path');
    
    if (nullCheck(form.rtPathId) === true) {
      ar.set("rt_path_id", form.rtPathId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rid", form.rtId);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("ver", form.pathVer);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section", form.sectionCd);
    } 
    

    if (nullCheck(form.geom) === true) {
      ar.set("geom", form.geom);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_path');
    
    if (nullCheck(form.rtPathId) === true) {
      ar.set("rt_path_id", form.rtPathId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rid", form.rtId);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("ver", form.pathVer);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("section", form.sectionCd);
    } 
    

    if (nullCheck(form.geom) === true) {
      ar.set("geom", form.geom);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_path_view rt_path');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtPathId: 'rt_path_id'
    , 

    rtId: 'rid'
    , 

    pathVer: 'ver'
    , 

    sectionCd: 'section'
    , 

    geom: 'geom'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_path');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_path_view rt_path');
    
      ar.select("rt_path.rt_path_id");

    
    
      ar.select("rt_path.rt_id");

    
    
      ar.select("rt_path.path_ver");

    
    
      ar.select("rt_path.section_cd");

    
    
      ar.select("rt_path.geom");

    
    
      ar.select("rt_path.created_at");

    
    
      ar.select("rt_path.updated_at");

    
    
    return ar;
  }

  

  
}
export const RtPathSql =  new RtPathQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IDrPay } from "../interface";


  class DrPayQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_pay객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_view dr_pay');

    
      if (alias === true) {
        ar.select("dr_pay.dr_pay_id as 'dr_pay.dr_pay_id'" );

      } else{
        ar.select("dr_pay.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.dispatch_id as 'dr_pay.dispatch_id'" );

      } else{
        ar.select("dr_pay.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.start_setl_month as 'dr_pay.start_setl_month'" );

      } else{
        ar.select("dr_pay.start_setl_month");

      }
      
      if (alias === true) {
        ar.select("dr_pay.setl_start_day as 'dr_pay.setl_start_day'" );

      } else{
        ar.select("dr_pay.setl_start_day");

      }
      
      if (alias === true) {
        ar.select("dr_pay.setl_day_cd as 'dr_pay.setl_day_cd'" );

      } else{
        ar.select("dr_pay.setl_day_cd");

      }
      
      if (alias === true) {
        ar.select("dr_pay.rt_id as 'dr_pay.rt_id'" );

      } else{
        ar.select("dr_pay.rt_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.dr_id as 'dr_pay.dr_id'" );

      } else{
        ar.select("dr_pay.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.amount as 'dr_pay.amount'" );

      } else{
        ar.select("dr_pay.amount");

      }
      
      if (alias === true) {
        ar.select("dr_pay.bus_compn_id as 'dr_pay.bus_compn_id'" );

      } else{
        ar.select("dr_pay.bus_compn_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.comment as 'dr_pay.comment'" );

      } else{
        ar.select("dr_pay.comment");

      }
      
      if (alias === true) {
        ar.select("dr_pay.borrow_setl_cd as 'dr_pay.borrow_setl_cd'" );

      } else{
        ar.select("dr_pay.borrow_setl_cd");

      }
      
      if (alias === true) {
        ar.select("dr_pay.admin_id as 'dr_pay.admin_id'" );

      } else{
        ar.select("dr_pay.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay.created_at as 'dr_pay.created_at'" );

      } else{
        ar.select("dr_pay.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_pay.updated_at as 'dr_pay.updated_at'" );

      } else{
        ar.select("dr_pay.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_pay_id = 'dr_pay.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_dispatch_id = 'dr_pay.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_start_setl_month = 'dr_pay.start_setl_month';
        if (alias !== undefined) {

          col_start_setl_month = `${alias}.start_setl_month`;

        }

        ar.select(`${col_start_setl_month} as '${col_start_setl_month}' `);

         
        let col_setl_start_day = 'dr_pay.setl_start_day';
        if (alias !== undefined) {

          col_setl_start_day = `${alias}.setl_start_day`;

        }

        ar.select(`${col_setl_start_day} as '${col_setl_start_day}' `);

         
        let col_setl_day_cd = 'dr_pay.setl_day_cd';
        if (alias !== undefined) {

          col_setl_day_cd = `${alias}.setl_day_cd`;

        }

        ar.select(`${col_setl_day_cd} as '${col_setl_day_cd}' `);

         
        let col_rt_id = 'dr_pay.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_dr_id = 'dr_pay.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_amount = 'dr_pay.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_bus_compn_id = 'dr_pay.bus_compn_id';
        if (alias !== undefined) {

          col_bus_compn_id = `${alias}.bus_compn_id`;

        }

        ar.select(`${col_bus_compn_id} as '${col_bus_compn_id}' `);

         
        let col_comment = 'dr_pay.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_borrow_setl_cd = 'dr_pay.borrow_setl_cd';
        if (alias !== undefined) {

          col_borrow_setl_cd = `${alias}.borrow_setl_cd`;

        }

        ar.select(`${col_borrow_setl_cd} as '${col_borrow_setl_cd}' `);

         
        let col_admin_id = 'dr_pay.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'dr_pay.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_pay.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay');
    
    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.startSetlMonth) === true) {
      ar.set("start_setl_month", form.startSetlMonth);
    } 
    

    if (nullCheck(form.setlStartDay) === true) {
      ar.set("setl_start_day", form.setlStartDay);
    } 
    

    if (nullCheck(form.setlDayCd) === true) {
      ar.set("setl_day_cd", form.setlDayCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.borrowSetlCd) === true) {
      ar.set("borrow_setl_cd", form.borrowSetlCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay');
    
    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.startSetlMonth) === true) {
      ar.set("start_setl_month", form.startSetlMonth);
    } 
    

    if (nullCheck(form.setlStartDay) === true) {
      ar.set("setl_start_day", form.setlStartDay);
    } 
    

    if (nullCheck(form.setlDayCd) === true) {
      ar.set("setl_day_cd", form.setlDayCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.borrowSetlCd) === true) {
      ar.set("borrow_setl_cd", form.borrowSetlCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_view dr_pay');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drPayId: 'dr_pay_id'
    , 

    dispatchId: 'dispatch_id'
    , 

    startSetlMonth: 'start_setl_month'
    , 

    setlStartDay: 'setl_start_day'
    , 

    setlDayCd: 'setl_day_cd'
    , 

    rtId: 'rt_id'
    , 

    drId: 'dr_id'
    , 

    amount: 'amount'
    , 

    busCompnId: 'bus_compn_id'
    , 

    comment: 'comment'
    , 

    borrowSetlCd: 'borrow_setl_cd'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_pay');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_view dr_pay');
    
      ar.select("dr_pay.dr_pay_id");

    
    
      ar.select("dr_pay.dispatch_id");

    
    
      ar.select("dr_pay.start_setl_month");

    
    
      ar.select("dr_pay.setl_start_day");

    
    
      ar.select("dr_pay.setl_day_cd");

    
    
      ar.select("dr_pay.rt_id");

    
    
      ar.select("dr_pay.dr_id");

    
    
      ar.select("dr_pay.amount");

    
    
      ar.select("dr_pay.bus_compn_id");

    
    
      ar.select("dr_pay.comment");

    
    
      ar.select("dr_pay.borrow_setl_cd");

    
    
      ar.select("dr_pay.admin_id");

    
    
      ar.select("dr_pay.created_at");

    
    
      ar.select("dr_pay.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrPaySql =  new DrPayQuery()

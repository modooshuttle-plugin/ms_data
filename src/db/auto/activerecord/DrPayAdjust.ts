import { Ar, nullCheck } from "../../../util";
 
  import { IDrPayAdjust } from "../interface";


  class DrPayAdjustQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_pay_adjust객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_adjust_view dr_pay_adjust');

    
      if (alias === true) {
        ar.select("dr_pay_adjust.dr_pay_adjust_id as 'dr_pay_adjust.dr_pay_adjust_id'" );

      } else{
        ar.select("dr_pay_adjust.dr_pay_adjust_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.dr_pay_id as 'dr_pay_adjust.dr_pay_id'" );

      } else{
        ar.select("dr_pay_adjust.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.rt_id as 'dr_pay_adjust.rt_id'" );

      } else{
        ar.select("dr_pay_adjust.rt_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.amount as 'dr_pay_adjust.amount'" );

      } else{
        ar.select("dr_pay_adjust.amount");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.setl_start_month as 'dr_pay_adjust.setl_start_month'" );

      } else{
        ar.select("dr_pay_adjust.setl_start_month");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.setl_end_month as 'dr_pay_adjust.setl_end_month'" );

      } else{
        ar.select("dr_pay_adjust.setl_end_month");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.dr_id as 'dr_pay_adjust.dr_id'" );

      } else{
        ar.select("dr_pay_adjust.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.comment as 'dr_pay_adjust.comment'" );

      } else{
        ar.select("dr_pay_adjust.comment");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.admin_id as 'dr_pay_adjust.admin_id'" );

      } else{
        ar.select("dr_pay_adjust.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.created_at as 'dr_pay_adjust.created_at'" );

      } else{
        ar.select("dr_pay_adjust.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.updated_at as 'dr_pay_adjust.updated_at'" );

      } else{
        ar.select("dr_pay_adjust.updated_at");

      }
      
      if (alias === true) {
        ar.select("dr_pay_adjust.dr_pay_cond_id as 'dr_pay_adjust.dr_pay_cond_id'" );

      } else{
        ar.select("dr_pay_adjust.dr_pay_cond_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_pay_adjust_id = 'dr_pay_adjust.dr_pay_adjust_id';
        if (alias !== undefined) {

          col_dr_pay_adjust_id = `${alias}.dr_pay_adjust_id`;

        }

        ar.select(`${col_dr_pay_adjust_id} as '${col_dr_pay_adjust_id}' `);

         
        let col_dr_pay_id = 'dr_pay_adjust.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_rt_id = 'dr_pay_adjust.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_amount = 'dr_pay_adjust.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_setl_start_month = 'dr_pay_adjust.setl_start_month';
        if (alias !== undefined) {

          col_setl_start_month = `${alias}.setl_start_month`;

        }

        ar.select(`${col_setl_start_month} as '${col_setl_start_month}' `);

         
        let col_setl_end_month = 'dr_pay_adjust.setl_end_month';
        if (alias !== undefined) {

          col_setl_end_month = `${alias}.setl_end_month`;

        }

        ar.select(`${col_setl_end_month} as '${col_setl_end_month}' `);

         
        let col_dr_id = 'dr_pay_adjust.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_comment = 'dr_pay_adjust.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'dr_pay_adjust.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'dr_pay_adjust.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_pay_adjust.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_dr_pay_cond_id = 'dr_pay_adjust.dr_pay_cond_id';
        if (alias !== undefined) {

          col_dr_pay_cond_id = `${alias}.dr_pay_cond_id`;

        }

        ar.select(`${col_dr_pay_cond_id} as '${col_dr_pay_cond_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_adjust');
    
    if (nullCheck(form.drPayAdjustId) === true) {
      ar.set("dr_pay_adjust_id", form.drPayAdjustId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.setlStartMonth) === true) {
      ar.set("setl_start_month", form.setlStartMonth);
    } 
    

    if (nullCheck(form.setlEndMonth) === true) {
      ar.set("setl_end_month", form.setlEndMonth);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_adjust');
    
    if (nullCheck(form.drPayAdjustId) === true) {
      ar.set("dr_pay_adjust_id", form.drPayAdjustId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.setlStartMonth) === true) {
      ar.set("setl_start_month", form.setlStartMonth);
    } 
    

    if (nullCheck(form.setlEndMonth) === true) {
      ar.set("setl_end_month", form.setlEndMonth);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.drPayCondId) === true) {
      ar.set("dr_pay_cond_id", form.drPayCondId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_adjust_view dr_pay_adjust');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drPayAdjustId: 'dr_pay_adjust_id'
    , 

    drPayId: 'dr_pay_id'
    , 

    rtId: 'rt_id'
    , 

    amount: 'amount'
    , 

    setlStartMonth: 'setl_start_month'
    , 

    setlEndMonth: 'setl_end_month'
    , 

    drId: 'dr_id'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    drPayCondId: 'dr_pay_cond_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_pay_adjust');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_pay_adjust_view dr_pay_adjust');
    
      ar.select("dr_pay_adjust.dr_pay_adjust_id");

    
    
      ar.select("dr_pay_adjust.dr_pay_id");

    
    
      ar.select("dr_pay_adjust.rt_id");

    
    
      ar.select("dr_pay_adjust.amount");

    
    
      ar.select("dr_pay_adjust.setl_start_month");

    
    
      ar.select("dr_pay_adjust.setl_end_month");

    
    
      ar.select("dr_pay_adjust.dr_id");

    
    
      ar.select("dr_pay_adjust.comment");

    
    
      ar.select("dr_pay_adjust.admin_id");

    
    
      ar.select("dr_pay_adjust.created_at");

    
    
      ar.select("dr_pay_adjust.updated_at");

    
    
      ar.select("dr_pay_adjust.dr_pay_cond_id");

    
    
    return ar;
  }

  

  
}
export const DrPayAdjustSql =  new DrPayAdjustQuery()

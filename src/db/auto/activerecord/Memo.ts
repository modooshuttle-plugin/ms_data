import { Ar, nullCheck } from "../../../util";
 
  import { IMemo } from "../interface";


  class MemoQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 memo객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' memo_view memo');

    
      if (alias === true) {
        ar.select("memo.memo_id as 'memo.memo_id'" );

      } else{
        ar.select("memo.memo_id");

      }
      
      if (alias === true) {
        ar.select("memo.memo_cd as 'memo.memo_cd'" );

      } else{
        ar.select("memo.memo_cd");

      }
      
      if (alias === true) {
        ar.select("memo.rt_id as 'memo.rt_id'" );

      } else{
        ar.select("memo.rt_id");

      }
      
      if (alias === true) {
        ar.select("memo.rt_cd as 'memo.rt_cd'" );

      } else{
        ar.select("memo.rt_cd");

      }
      
      if (alias === true) {
        ar.select("memo.user_id as 'memo.user_id'" );

      } else{
        ar.select("memo.user_id");

      }
      
      if (alias === true) {
        ar.select("memo.comment as 'memo.comment'" );

      } else{
        ar.select("memo.comment");

      }
      
      if (alias === true) {
        ar.select("memo.created_at as 'memo.created_at'" );

      } else{
        ar.select("memo.created_at");

      }
      
      if (alias === true) {
        ar.select("memo.updated_at as 'memo.updated_at'" );

      } else{
        ar.select("memo.updated_at");

      }
      
      if (alias === true) {
        ar.select("memo.admin_id as 'memo.admin_id'" );

      } else{
        ar.select("memo.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_memo_id = 'memo.memo_id';
        if (alias !== undefined) {

          col_memo_id = `${alias}.memo_id`;

        }

        ar.select(`${col_memo_id} as '${col_memo_id}' `);

         
        let col_memo_cd = 'memo.memo_cd';
        if (alias !== undefined) {

          col_memo_cd = `${alias}.memo_cd`;

        }

        ar.select(`${col_memo_cd} as '${col_memo_cd}' `);

         
        let col_rt_id = 'memo.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_rt_cd = 'memo.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_user_id = 'memo.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_comment = 'memo.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'memo.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'memo.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'memo.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_memo');
    
    if (nullCheck(form.memoId) === true) {
      ar.set("a_no", form.memoId);
    } 
    

    if (nullCheck(form.memoCd) === true) {
      ar.set("a_type", form.memoCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("a_rid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("a_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_memo');
    
    if (nullCheck(form.memoId) === true) {
      ar.set("a_no", form.memoId);
    } 
    

    if (nullCheck(form.memoCd) === true) {
      ar.set("a_type", form.memoCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("a_rid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("a_mid", form.userId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("a_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("a_admin", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' memo_view memo');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    memoId: 'a_no'
    , 

    memoCd: 'a_type'
    , 

    rtId: 'a_rid'
    , 

    rtCd: 'a_rtype'
    , 

    userId: 'a_mid'
    , 

    comment: 'a_text'
    , 

    createdAt: 'a_timestamp'
    , 

    updatedAt: 'a_timestamp_u'
    , 

    adminId: 'a_admin'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_memo');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' memo_view memo');
    
      ar.select("memo.memo_id");

    
    
      ar.select("memo.memo_cd");

    
    
      ar.select("memo.rt_id");

    
    
      ar.select("memo.rt_cd");

    
    
      ar.select("memo.user_id");

    
    
      ar.select("memo.comment");

    
    
      ar.select("memo.created_at");

    
    
      ar.select("memo.updated_at");

    
    
      ar.select("memo.admin_id");

    
    
    return ar;
  }

  

  
}
export const MemoSql =  new MemoQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IOfferDrPay } from "../interface";


  class OfferDrPayQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 offer_dr_pay객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_pay_view offer_dr_pay');

    
      if (alias === true) {
        ar.select("offer_dr_pay.offer_dr_pay_id as 'offer_dr_pay.offer_dr_pay_id'" );

      } else{
        ar.select("offer_dr_pay.offer_dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.offer_dr_id as 'offer_dr_pay.offer_dr_id'" );

      } else{
        ar.select("offer_dr_pay.offer_dr_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.dr_pay_id as 'offer_dr_pay.dr_pay_id'" );

      } else{
        ar.select("offer_dr_pay.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.cond_cd as 'offer_dr_pay.cond_cd'" );

      } else{
        ar.select("offer_dr_pay.cond_cd");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.prods_amount as 'offer_dr_pay.prods_amount'" );

      } else{
        ar.select("offer_dr_pay.prods_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.discount_amount as 'offer_dr_pay.discount_amount'" );

      } else{
        ar.select("offer_dr_pay.discount_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.discount_prods_amount as 'offer_dr_pay.discount_prods_amount'" );

      } else{
        ar.select("offer_dr_pay.discount_prods_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.paid_cnt as 'offer_dr_pay.paid_cnt'" );

      } else{
        ar.select("offer_dr_pay.paid_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.dr_pay_amount as 'offer_dr_pay.dr_pay_amount'" );

      } else{
        ar.select("offer_dr_pay.dr_pay_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.diff_amount as 'offer_dr_pay.diff_amount'" );

      } else{
        ar.select("offer_dr_pay.diff_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.incnt_cnt as 'offer_dr_pay.incnt_cnt'" );

      } else{
        ar.select("offer_dr_pay.incnt_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.min_paid_cnt as 'offer_dr_pay.min_paid_cnt'" );

      } else{
        ar.select("offer_dr_pay.min_paid_cnt");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.rt_id as 'offer_dr_pay.rt_id'" );

      } else{
        ar.select("offer_dr_pay.rt_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.created_at as 'offer_dr_pay.created_at'" );

      } else{
        ar.select("offer_dr_pay.created_at");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.updated_at as 'offer_dr_pay.updated_at'" );

      } else{
        ar.select("offer_dr_pay.updated_at");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.admin_id as 'offer_dr_pay.admin_id'" );

      } else{
        ar.select("offer_dr_pay.admin_id");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.comment as 'offer_dr_pay.comment'" );

      } else{
        ar.select("offer_dr_pay.comment");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.incnt_amount as 'offer_dr_pay.incnt_amount'" );

      } else{
        ar.select("offer_dr_pay.incnt_amount");

      }
      
      if (alias === true) {
        ar.select("offer_dr_pay.dr_amount as 'offer_dr_pay.dr_amount'" );

      } else{
        ar.select("offer_dr_pay.dr_amount");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_offer_dr_pay_id = 'offer_dr_pay.offer_dr_pay_id';
        if (alias !== undefined) {

          col_offer_dr_pay_id = `${alias}.offer_dr_pay_id`;

        }

        ar.select(`${col_offer_dr_pay_id} as '${col_offer_dr_pay_id}' `);

         
        let col_offer_dr_id = 'offer_dr_pay.offer_dr_id';
        if (alias !== undefined) {

          col_offer_dr_id = `${alias}.offer_dr_id`;

        }

        ar.select(`${col_offer_dr_id} as '${col_offer_dr_id}' `);

         
        let col_dr_pay_id = 'offer_dr_pay.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_cond_cd = 'offer_dr_pay.cond_cd';
        if (alias !== undefined) {

          col_cond_cd = `${alias}.cond_cd`;

        }

        ar.select(`${col_cond_cd} as '${col_cond_cd}' `);

         
        let col_prods_amount = 'offer_dr_pay.prods_amount';
        if (alias !== undefined) {

          col_prods_amount = `${alias}.prods_amount`;

        }

        ar.select(`${col_prods_amount} as '${col_prods_amount}' `);

         
        let col_discount_amount = 'offer_dr_pay.discount_amount';
        if (alias !== undefined) {

          col_discount_amount = `${alias}.discount_amount`;

        }

        ar.select(`${col_discount_amount} as '${col_discount_amount}' `);

         
        let col_discount_prods_amount = 'offer_dr_pay.discount_prods_amount';
        if (alias !== undefined) {

          col_discount_prods_amount = `${alias}.discount_prods_amount`;

        }

        ar.select(`${col_discount_prods_amount} as '${col_discount_prods_amount}' `);

         
        let col_paid_cnt = 'offer_dr_pay.paid_cnt';
        if (alias !== undefined) {

          col_paid_cnt = `${alias}.paid_cnt`;

        }

        ar.select(`${col_paid_cnt} as '${col_paid_cnt}' `);

         
        let col_dr_pay_amount = 'offer_dr_pay.dr_pay_amount';
        if (alias !== undefined) {

          col_dr_pay_amount = `${alias}.dr_pay_amount`;

        }

        ar.select(`${col_dr_pay_amount} as '${col_dr_pay_amount}' `);

         
        let col_diff_amount = 'offer_dr_pay.diff_amount';
        if (alias !== undefined) {

          col_diff_amount = `${alias}.diff_amount`;

        }

        ar.select(`${col_diff_amount} as '${col_diff_amount}' `);

         
        let col_incnt_cnt = 'offer_dr_pay.incnt_cnt';
        if (alias !== undefined) {

          col_incnt_cnt = `${alias}.incnt_cnt`;

        }

        ar.select(`${col_incnt_cnt} as '${col_incnt_cnt}' `);

         
        let col_min_paid_cnt = 'offer_dr_pay.min_paid_cnt';
        if (alias !== undefined) {

          col_min_paid_cnt = `${alias}.min_paid_cnt`;

        }

        ar.select(`${col_min_paid_cnt} as '${col_min_paid_cnt}' `);

         
        let col_rt_id = 'offer_dr_pay.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_created_at = 'offer_dr_pay.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'offer_dr_pay.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'offer_dr_pay.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_comment = 'offer_dr_pay.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_incnt_amount = 'offer_dr_pay.incnt_amount';
        if (alias !== undefined) {

          col_incnt_amount = `${alias}.incnt_amount`;

        }

        ar.select(`${col_incnt_amount} as '${col_incnt_amount}' `);

         
        let col_dr_amount = 'offer_dr_pay.dr_amount';
        if (alias !== undefined) {

          col_dr_amount = `${alias}.dr_amount`;

        }

        ar.select(`${col_dr_amount} as '${col_dr_amount}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_pay');
    
    if (nullCheck(form.offerDrPayId) === true) {
      ar.set("offer_dr_pay_id", form.offerDrPayId);
    } 
    

    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.condCd) === true) {
      ar.set("cond_cd", form.condCd);
    } 
    

    if (nullCheck(form.prodsAmount) === true) {
      ar.set("prods_amount", form.prodsAmount);
    } 
    

    if (nullCheck(form.discountAmount) === true) {
      ar.set("discount_amount", form.discountAmount);
    } 
    

    if (nullCheck(form.discountProdsAmount) === true) {
      ar.set("discount_prods_amount", form.discountProdsAmount);
    } 
    

    if (nullCheck(form.paidCnt) === true) {
      ar.set("paid_cnt", form.paidCnt);
    } 
    

    if (nullCheck(form.drPayAmount) === true) {
      ar.set("dr_pay_amount", form.drPayAmount);
    } 
    

    if (nullCheck(form.diffAmount) === true) {
      ar.set("diff_amount", form.diffAmount);
    } 
    

    if (nullCheck(form.incntCnt) === true) {
      ar.set("incnt_cnt", form.incntCnt);
    } 
    

    if (nullCheck(form.minPaidCnt) === true) {
      ar.set("min_paid_cnt", form.minPaidCnt);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.incntAmount) === true) {
      ar.set("incnt_amount", form.incntAmount);
    } 
    

    if (nullCheck(form.drAmount) === true) {
      ar.set("dr_amount", form.drAmount);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_pay');
    
    if (nullCheck(form.offerDrPayId) === true) {
      ar.set("offer_dr_pay_id", form.offerDrPayId);
    } 
    

    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.condCd) === true) {
      ar.set("cond_cd", form.condCd);
    } 
    

    if (nullCheck(form.prodsAmount) === true) {
      ar.set("prods_amount", form.prodsAmount);
    } 
    

    if (nullCheck(form.discountAmount) === true) {
      ar.set("discount_amount", form.discountAmount);
    } 
    

    if (nullCheck(form.discountProdsAmount) === true) {
      ar.set("discount_prods_amount", form.discountProdsAmount);
    } 
    

    if (nullCheck(form.paidCnt) === true) {
      ar.set("paid_cnt", form.paidCnt);
    } 
    

    if (nullCheck(form.drPayAmount) === true) {
      ar.set("dr_pay_amount", form.drPayAmount);
    } 
    

    if (nullCheck(form.diffAmount) === true) {
      ar.set("diff_amount", form.diffAmount);
    } 
    

    if (nullCheck(form.incntCnt) === true) {
      ar.set("incnt_cnt", form.incntCnt);
    } 
    

    if (nullCheck(form.minPaidCnt) === true) {
      ar.set("min_paid_cnt", form.minPaidCnt);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.incntAmount) === true) {
      ar.set("incnt_amount", form.incntAmount);
    } 
    

    if (nullCheck(form.drAmount) === true) {
      ar.set("dr_amount", form.drAmount);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_pay_view offer_dr_pay');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    offerDrPayId: 'offer_dr_pay_id'
    , 

    offerDrId: 'offer_dr_id'
    , 

    drPayId: 'dr_pay_id'
    , 

    condCd: 'cond_cd'
    , 

    prodsAmount: 'prods_amount'
    , 

    discountAmount: 'discount_amount'
    , 

    discountProdsAmount: 'discount_prods_amount'
    , 

    paidCnt: 'paid_cnt'
    , 

    drPayAmount: 'dr_pay_amount'
    , 

    diffAmount: 'diff_amount'
    , 

    incntCnt: 'incnt_cnt'
    , 

    minPaidCnt: 'min_paid_cnt'
    , 

    rtId: 'rt_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    , 

    comment: 'comment'
    , 

    incntAmount: 'incnt_amount'
    , 

    drAmount: 'dr_amount'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('offer_dr_pay');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_dr_pay_view offer_dr_pay');
    
      ar.select("offer_dr_pay.offer_dr_pay_id");

    
    
      ar.select("offer_dr_pay.offer_dr_id");

    
    
      ar.select("offer_dr_pay.dr_pay_id");

    
    
      ar.select("offer_dr_pay.cond_cd");

    
    
      ar.select("offer_dr_pay.prods_amount");

    
    
      ar.select("offer_dr_pay.discount_amount");

    
    
      ar.select("offer_dr_pay.discount_prods_amount");

    
    
      ar.select("offer_dr_pay.paid_cnt");

    
    
      ar.select("offer_dr_pay.dr_pay_amount");

    
    
      ar.select("offer_dr_pay.diff_amount");

    
    
      ar.select("offer_dr_pay.incnt_cnt");

    
    
      ar.select("offer_dr_pay.min_paid_cnt");

    
    
      ar.select("offer_dr_pay.rt_id");

    
    
      ar.select("offer_dr_pay.created_at");

    
    
      ar.select("offer_dr_pay.updated_at");

    
    
      ar.select("offer_dr_pay.admin_id");

    
    
      ar.select("offer_dr_pay.comment");

    
    
      ar.select("offer_dr_pay.incnt_amount");

    
    
      ar.select("offer_dr_pay.dr_amount");

    
    
    return ar;
  }

  

  
}
export const OfferDrPaySql =  new OfferDrPayQuery()

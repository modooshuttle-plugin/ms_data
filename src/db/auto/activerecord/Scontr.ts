import { Ar, nullCheck } from "../../../util";
 
  import { IScontr } from "../interface";


  class ScontrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 scontr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' scontr_view scontr');

    
      if (alias === true) {
        ar.select("scontr.scontr_id as 'scontr.scontr_id'" );

      } else{
        ar.select("scontr.scontr_id");

      }
      
      if (alias === true) {
        ar.select("scontr.info as 'scontr.info'" );

      } else{
        ar.select("scontr.info");

      }
      
      if (alias === true) {
        ar.select("scontr.platform as 'scontr.platform'" );

      } else{
        ar.select("scontr.platform");

      }
      
      if (alias === true) {
        ar.select("scontr.ver as 'scontr.ver'" );

      } else{
        ar.select("scontr.ver");

      }
      
      if (alias === true) {
        ar.select("scontr.modal as 'scontr.modal'" );

      } else{
        ar.select("scontr.modal");

      }
      
      if (alias === true) {
        ar.select("scontr.etc as 'scontr.etc'" );

      } else{
        ar.select("scontr.etc");

      }
      
      if (alias === true) {
        ar.select("scontr.start as 'scontr.start'" );

      } else{
        ar.select("scontr.start");

      }
      
      if (alias === true) {
        ar.select("scontr.end as 'scontr.end'" );

      } else{
        ar.select("scontr.end");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_scontr_id = 'scontr.scontr_id';
        if (alias !== undefined) {

          col_scontr_id = `${alias}.scontr_id`;

        }

        ar.select(`${col_scontr_id} as '${col_scontr_id}' `);

         
        let col_info = 'scontr.info';
        if (alias !== undefined) {

          col_info = `${alias}.info`;

        }

        ar.select(`${col_info} as '${col_info}' `);

         
        let col_platform = 'scontr.platform';
        if (alias !== undefined) {

          col_platform = `${alias}.platform`;

        }

        ar.select(`${col_platform} as '${col_platform}' `);

         
        let col_ver = 'scontr.ver';
        if (alias !== undefined) {

          col_ver = `${alias}.ver`;

        }

        ar.select(`${col_ver} as '${col_ver}' `);

         
        let col_modal = 'scontr.modal';
        if (alias !== undefined) {

          col_modal = `${alias}.modal`;

        }

        ar.select(`${col_modal} as '${col_modal}' `);

         
        let col_etc = 'scontr.etc';
        if (alias !== undefined) {

          col_etc = `${alias}.etc`;

        }

        ar.select(`${col_etc} as '${col_etc}' `);

         
        let col_start = 'scontr.start';
        if (alias !== undefined) {

          col_start = `${alias}.start`;

        }

        ar.select(`${col_start} as '${col_start}' `);

         
        let col_end = 'scontr.end';
        if (alias !== undefined) {

          col_end = `${alias}.end`;

        }

        ar.select(`${col_end} as '${col_end}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' scontr');
    
    if (nullCheck(form.scontrId) === true) {
      ar.set("sc_no", form.scontrId);
    } 
    

    if (nullCheck(form.info) === true) {
      ar.set("sc_info", form.info);
    } 
    

    if (nullCheck(form.platform) === true) {
      ar.set("sc_platform", form.platform);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("sc_version", form.ver);
    } 
    

    if (nullCheck(form.modal) === true) {
      ar.set("sc_modal", form.modal);
    } 
    

    if (nullCheck(form.etc) === true) {
      ar.set("sc_etc", form.etc);
    } 
    

    if (nullCheck(form.start) === true) {
      ar.set("sc_start", form.start);
    } 
    

    if (nullCheck(form.end) === true) {
      ar.set("sc_end", form.end);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' scontr');
    
    if (nullCheck(form.scontrId) === true) {
      ar.set("sc_no", form.scontrId);
    } 
    

    if (nullCheck(form.info) === true) {
      ar.set("sc_info", form.info);
    } 
    

    if (nullCheck(form.platform) === true) {
      ar.set("sc_platform", form.platform);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("sc_version", form.ver);
    } 
    

    if (nullCheck(form.modal) === true) {
      ar.set("sc_modal", form.modal);
    } 
    

    if (nullCheck(form.etc) === true) {
      ar.set("sc_etc", form.etc);
    } 
    

    if (nullCheck(form.start) === true) {
      ar.set("sc_start", form.start);
    } 
    

    if (nullCheck(form.end) === true) {
      ar.set("sc_end", form.end);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' scontr_view scontr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    scontrId: 'sc_no'
    , 

    info: 'sc_info'
    , 

    platform: 'sc_platform'
    , 

    ver: 'sc_version'
    , 

    modal: 'sc_modal'
    , 

    etc: 'sc_etc'
    , 

    start: 'sc_start'
    , 

    end: 'sc_end'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('scontr');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' scontr_view scontr');
    
      ar.select("scontr.scontr_id");

    
    
      ar.select("scontr.info");

    
    
      ar.select("scontr.platform");

    
    
      ar.select("scontr.ver");

    
    
      ar.select("scontr.modal");

    
    
      ar.select("scontr.etc");

    
    
      ar.select("scontr.start");

    
    
      ar.select("scontr.end");

    
    
    return ar;
  }

  

  
}
export const ScontrSql =  new ScontrQuery()

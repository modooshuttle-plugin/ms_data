import { Ar, nullCheck } from "../../../util";
 
  import { IReasonMk } from "../interface";


  class ReasonMkQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 reason_mk객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' reason_mk_view reason_mk');

    
      if (alias === true) {
        ar.select("reason_mk.reason_mk_id as 'reason_mk.reason_mk_id'" );

      } else{
        ar.select("reason_mk.reason_mk_id");

      }
      
      if (alias === true) {
        ar.select("reason_mk.user_id as 'reason_mk.user_id'" );

      } else{
        ar.select("reason_mk.user_id");

      }
      
      if (alias === true) {
        ar.select("reason_mk.make_id as 'reason_mk.make_id'" );

      } else{
        ar.select("reason_mk.make_id");

      }
      
      if (alias === true) {
        ar.select("reason_mk.rt_id as 'reason_mk.rt_id'" );

      } else{
        ar.select("reason_mk.rt_id");

      }
      
      if (alias === true) {
        ar.select("reason_mk.rt_cd as 'reason_mk.rt_cd'" );

      } else{
        ar.select("reason_mk.rt_cd");

      }
      
      if (alias === true) {
        ar.select("reason_mk.reason_cd as 'reason_mk.reason_cd'" );

      } else{
        ar.select("reason_mk.reason_cd");

      }
      
      if (alias === true) {
        ar.select("reason_mk.comment as 'reason_mk.comment'" );

      } else{
        ar.select("reason_mk.comment");

      }
      
      if (alias === true) {
        ar.select("reason_mk.created_at as 'reason_mk.created_at'" );

      } else{
        ar.select("reason_mk.created_at");

      }
      
      if (alias === true) {
        ar.select("reason_mk.updated_at as 'reason_mk.updated_at'" );

      } else{
        ar.select("reason_mk.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_reason_mk_id = 'reason_mk.reason_mk_id';
        if (alias !== undefined) {

          col_reason_mk_id = `${alias}.reason_mk_id`;

        }

        ar.select(`${col_reason_mk_id} as '${col_reason_mk_id}' `);

         
        let col_user_id = 'reason_mk.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_make_id = 'reason_mk.make_id';
        if (alias !== undefined) {

          col_make_id = `${alias}.make_id`;

        }

        ar.select(`${col_make_id} as '${col_make_id}' `);

         
        let col_rt_id = 'reason_mk.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_rt_cd = 'reason_mk.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_reason_cd = 'reason_mk.reason_cd';
        if (alias !== undefined) {

          col_reason_cd = `${alias}.reason_cd`;

        }

        ar.select(`${col_reason_cd} as '${col_reason_cd}' `);

         
        let col_comment = 'reason_mk.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'reason_mk.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'reason_mk.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_reason_mk');
    
    if (nullCheck(form.reasonMkId) === true) {
      ar.set("o_no", form.reasonMkId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.makeId) === true) {
      ar.set("o_oid", form.makeId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.reasonCd) === true) {
      ar.set("o_type", form.reasonCd);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_reason_mk');
    
    if (nullCheck(form.reasonMkId) === true) {
      ar.set("o_no", form.reasonMkId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.makeId) === true) {
      ar.set("o_oid", form.makeId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_rtype", form.rtCd);
    } 
    

    if (nullCheck(form.reasonCd) === true) {
      ar.set("o_type", form.reasonCd);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_text", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' reason_mk_view reason_mk');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    reasonMkId: 'o_no'
    , 

    userId: 'o_mid'
    , 

    makeId: 'o_oid'
    , 

    rtId: 'o_rid'
    , 

    rtCd: 'o_rtype'
    , 

    reasonCd: 'o_type'
    , 

    comment: 'o_text'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_reason_mk');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' reason_mk_view reason_mk');
    
      ar.select("reason_mk.reason_mk_id");

    
    
      ar.select("reason_mk.user_id");

    
    
      ar.select("reason_mk.make_id");

    
    
      ar.select("reason_mk.rt_id");

    
    
      ar.select("reason_mk.rt_cd");

    
    
      ar.select("reason_mk.reason_cd");

    
    
      ar.select("reason_mk.comment");

    
    
      ar.select("reason_mk.created_at");

    
    
      ar.select("reason_mk.updated_at");

    
    
    return ar;
  }

  

  
}
export const ReasonMkSql =  new ReasonMkQuery()

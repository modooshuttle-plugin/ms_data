import { Ar, nullCheck } from "../../../util";
 
  import { ILoginToken } from "../interface";


  class LoginTokenQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 login_token객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' login_token_view login_token');

    
      if (alias === true) {
        ar.select("login_token.login_token_id as 'login_token.login_token_id'" );

      } else{
        ar.select("login_token.login_token_id");

      }
      
      if (alias === true) {
        ar.select("login_token.user_id as 'login_token.user_id'" );

      } else{
        ar.select("login_token.user_id");

      }
      
      if (alias === true) {
        ar.select("login_token.provider_cd as 'login_token.provider_cd'" );

      } else{
        ar.select("login_token.provider_cd");

      }
      
      if (alias === true) {
        ar.select("login_token.created_at as 'login_token.created_at'" );

      } else{
        ar.select("login_token.created_at");

      }
      
      if (alias === true) {
        ar.select("login_token.ip_addr as 'login_token.ip_addr'" );

      } else{
        ar.select("login_token.ip_addr");

      }
      
      if (alias === true) {
        ar.select("login_token.url as 'login_token.url'" );

      } else{
        ar.select("login_token.url");

      }
      
      if (alias === true) {
        ar.select("login_token.device as 'login_token.device'" );

      } else{
        ar.select("login_token.device");

      }
      
      if (alias === true) {
        ar.select("login_token.os as 'login_token.os'" );

      } else{
        ar.select("login_token.os");

      }
      
      if (alias === true) {
        ar.select("login_token.browser as 'login_token.browser'" );

      } else{
        ar.select("login_token.browser");

      }
      
      if (alias === true) {
        ar.select("login_token.redirect as 'login_token.redirect'" );

      } else{
        ar.select("login_token.redirect");

      }
      
      if (alias === true) {
        ar.select("login_token.token as 'login_token.token'" );

      } else{
        ar.select("login_token.token");

      }
      
      if (alias === true) {
        ar.select("login_token.uuid as 'login_token.uuid'" );

      } else{
        ar.select("login_token.uuid");

      }
      
      if (alias === true) {
        ar.select("login_token.secret as 'login_token.secret'" );

      } else{
        ar.select("login_token.secret");

      }
      
      if (alias === true) {
        ar.select("login_token.expire as 'login_token.expire'" );

      } else{
        ar.select("login_token.expire");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_login_token_id = 'login_token.login_token_id';
        if (alias !== undefined) {

          col_login_token_id = `${alias}.login_token_id`;

        }

        ar.select(`${col_login_token_id} as '${col_login_token_id}' `);

         
        let col_user_id = 'login_token.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_provider_cd = 'login_token.provider_cd';
        if (alias !== undefined) {

          col_provider_cd = `${alias}.provider_cd`;

        }

        ar.select(`${col_provider_cd} as '${col_provider_cd}' `);

         
        let col_created_at = 'login_token.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_ip_addr = 'login_token.ip_addr';
        if (alias !== undefined) {

          col_ip_addr = `${alias}.ip_addr`;

        }

        ar.select(`${col_ip_addr} as '${col_ip_addr}' `);

         
        let col_url = 'login_token.url';
        if (alias !== undefined) {

          col_url = `${alias}.url`;

        }

        ar.select(`${col_url} as '${col_url}' `);

         
        let col_device = 'login_token.device';
        if (alias !== undefined) {

          col_device = `${alias}.device`;

        }

        ar.select(`${col_device} as '${col_device}' `);

         
        let col_os = 'login_token.os';
        if (alias !== undefined) {

          col_os = `${alias}.os`;

        }

        ar.select(`${col_os} as '${col_os}' `);

         
        let col_browser = 'login_token.browser';
        if (alias !== undefined) {

          col_browser = `${alias}.browser`;

        }

        ar.select(`${col_browser} as '${col_browser}' `);

         
        let col_redirect = 'login_token.redirect';
        if (alias !== undefined) {

          col_redirect = `${alias}.redirect`;

        }

        ar.select(`${col_redirect} as '${col_redirect}' `);

         
        let col_token = 'login_token.token';
        if (alias !== undefined) {

          col_token = `${alias}.token`;

        }

        ar.select(`${col_token} as '${col_token}' `);

         
        let col_uuid = 'login_token.uuid';
        if (alias !== undefined) {

          col_uuid = `${alias}.uuid`;

        }

        ar.select(`${col_uuid} as '${col_uuid}' `);

         
        let col_secret = 'login_token.secret';
        if (alias !== undefined) {

          col_secret = `${alias}.secret`;

        }

        ar.select(`${col_secret} as '${col_secret}' `);

         
        let col_expire = 'login_token.expire';
        if (alias !== undefined) {

          col_expire = `${alias}.expire`;

        }

        ar.select(`${col_expire} as '${col_expire}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_login');
    
    if (nullCheck(form.loginTokenId) === true) {
      ar.set("l_no", form.loginTokenId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("l_type", form.providerCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.ipAddr) === true) {
      ar.set("l_ip", form.ipAddr);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("l_url", form.url);
    } 
    

    if (nullCheck(form.device) === true) {
      ar.set("l_device", form.device);
    } 
    

    if (nullCheck(form.os) === true) {
      ar.set("l_os", form.os);
    } 
    

    if (nullCheck(form.browser) === true) {
      ar.set("l_browser", form.browser);
    } 
    

    if (nullCheck(form.redirect) === true) {
      ar.set("l_redirect", form.redirect);
    } 
    

    if (nullCheck(form.token) === true) {
      ar.set("l_token", form.token);
    } 
    

    if (nullCheck(form.uuid) === true) {
      ar.set("l_uuid", form.uuid);
    } 
    

    if (nullCheck(form.secret) === true) {
      ar.set("l_secret", form.secret);
    } 
    

    if (nullCheck(form.expire) === true) {
      ar.set("l_expire", form.expire);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_login');
    
    if (nullCheck(form.loginTokenId) === true) {
      ar.set("l_no", form.loginTokenId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("l_type", form.providerCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.ipAddr) === true) {
      ar.set("l_ip", form.ipAddr);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("l_url", form.url);
    } 
    

    if (nullCheck(form.device) === true) {
      ar.set("l_device", form.device);
    } 
    

    if (nullCheck(form.os) === true) {
      ar.set("l_os", form.os);
    } 
    

    if (nullCheck(form.browser) === true) {
      ar.set("l_browser", form.browser);
    } 
    

    if (nullCheck(form.redirect) === true) {
      ar.set("l_redirect", form.redirect);
    } 
    

    if (nullCheck(form.token) === true) {
      ar.set("l_token", form.token);
    } 
    

    if (nullCheck(form.uuid) === true) {
      ar.set("l_uuid", form.uuid);
    } 
    

    if (nullCheck(form.secret) === true) {
      ar.set("l_secret", form.secret);
    } 
    

    if (nullCheck(form.expire) === true) {
      ar.set("l_expire", form.expire);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' login_token_view login_token');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    loginTokenId: 'l_no'
    , 

    userId: 'l_mid'
    , 

    providerCd: 'l_type'
    , 

    createdAt: 'l_timestamp'
    , 

    ipAddr: 'l_ip'
    , 

    url: 'l_url'
    , 

    device: 'l_device'
    , 

    os: 'l_os'
    , 

    browser: 'l_browser'
    , 

    redirect: 'l_redirect'
    , 

    token: 'l_token'
    , 

    uuid: 'l_uuid'
    , 

    secret: 'l_secret'
    , 

    expire: 'l_expire'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_login');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' login_token_view login_token');
    
      ar.select("login_token.login_token_id");

    
    
      ar.select("login_token.user_id");

    
    
      ar.select("login_token.provider_cd");

    
    
      ar.select("login_token.created_at");

    
    
      ar.select("login_token.ip_addr");

    
    
      ar.select("login_token.url");

    
    
      ar.select("login_token.device");

    
    
      ar.select("login_token.os");

    
    
      ar.select("login_token.browser");

    
    
      ar.select("login_token.redirect");

    
    
      ar.select("login_token.token");

    
    
      ar.select("login_token.uuid");

    
    
      ar.select("login_token.secret");

    
    
      ar.select("login_token.expire");

    
    
    return ar;
  }

  

  
}
export const LoginTokenSql =  new LoginTokenQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IUserBankAccount } from "../interface";


  class UserBankAccountQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 user_bank_account객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' user_bank_account_view user_bank_account');

    
      if (alias === true) {
        ar.select("user_bank_account.user_bank_account_id as 'user_bank_account.user_bank_account_id'" );

      } else{
        ar.select("user_bank_account.user_bank_account_id");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.user_id as 'user_bank_account.user_id'" );

      } else{
        ar.select("user_bank_account.user_id");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.bank_cd as 'user_bank_account.bank_cd'" );

      } else{
        ar.select("user_bank_account.bank_cd");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.account_no as 'user_bank_account.account_no'" );

      } else{
        ar.select("user_bank_account.account_no");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.nm as 'user_bank_account.nm'" );

      } else{
        ar.select("user_bank_account.nm");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.created_at as 'user_bank_account.created_at'" );

      } else{
        ar.select("user_bank_account.created_at");

      }
      
      if (alias === true) {
        ar.select("user_bank_account.updated_at as 'user_bank_account.updated_at'" );

      } else{
        ar.select("user_bank_account.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_user_bank_account_id = 'user_bank_account.user_bank_account_id';
        if (alias !== undefined) {

          col_user_bank_account_id = `${alias}.user_bank_account_id`;

        }

        ar.select(`${col_user_bank_account_id} as '${col_user_bank_account_id}' `);

         
        let col_user_id = 'user_bank_account.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_bank_cd = 'user_bank_account.bank_cd';
        if (alias !== undefined) {

          col_bank_cd = `${alias}.bank_cd`;

        }

        ar.select(`${col_bank_cd} as '${col_bank_cd}' `);

         
        let col_account_no = 'user_bank_account.account_no';
        if (alias !== undefined) {

          col_account_no = `${alias}.account_no`;

        }

        ar.select(`${col_account_no} as '${col_account_no}' `);

         
        let col_nm = 'user_bank_account.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_created_at = 'user_bank_account.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'user_bank_account.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_acnum');
    
    if (nullCheck(form.userBankAccountId) === true) {
      ar.set("m_no", form.userBankAccountId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.bankCd) === true) {
      ar.set("m_bank", form.bankCd);
    } 
    

    if (nullCheck(form.accountNo) === true) {
      ar.set("m_number", form.accountNo);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("m_name", form.nm);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_acnum');
    
    if (nullCheck(form.userBankAccountId) === true) {
      ar.set("m_no", form.userBankAccountId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.bankCd) === true) {
      ar.set("m_bank", form.bankCd);
    } 
    

    if (nullCheck(form.accountNo) === true) {
      ar.set("m_number", form.accountNo);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("m_name", form.nm);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' user_bank_account_view user_bank_account');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    userBankAccountId: 'm_no'
    , 

    userId: 'm_mid'
    , 

    bankCd: 'm_bank'
    , 

    accountNo: 'm_number'
    , 

    nm: 'm_name'
    , 

    createdAt: 'm_timestamp'
    , 

    updatedAt: 'm_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('m_acnum');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' user_bank_account_view user_bank_account');
    
      ar.select("user_bank_account.user_bank_account_id");

    
    
      ar.select("user_bank_account.user_id");

    
    
      ar.select("user_bank_account.bank_cd");

    
    
      ar.select("user_bank_account.account_no");

    
    
      ar.select("user_bank_account.nm");

    
    
      ar.select("user_bank_account.created_at");

    
    
      ar.select("user_bank_account.updated_at");

    
    
    return ar;
  }

  

  
}
export const UserBankAccountSql =  new UserBankAccountQuery()

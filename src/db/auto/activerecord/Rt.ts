import { Ar, nullCheck } from "../../../util";
 
  import { IRt } from "../interface";


  class RtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_view rt');

    
      if (alias === true) {
        ar.select("rt.rt_id as 'rt.rt_id'" );

      } else{
        ar.select("rt.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt.start_info as 'rt.start_info'" );

      } else{
        ar.select("rt.start_info");

      }
      
      if (alias === true) {
        ar.select("rt.end_info as 'rt.end_info'" );

      } else{
        ar.select("rt.end_info");

      }
      
      if (alias === true) {
        ar.select("rt.start_tag as 'rt.start_tag'" );

      } else{
        ar.select("rt.start_tag");

      }
      
      if (alias === true) {
        ar.select("rt.end_tag as 'rt.end_tag'" );

      } else{
        ar.select("rt.end_tag");

      }
      
      if (alias === true) {
        ar.select("rt.start_cat_cd as 'rt.start_cat_cd'" );

      } else{
        ar.select("rt.start_cat_cd");

      }
      
      if (alias === true) {
        ar.select("rt.end_cat_cd as 'rt.end_cat_cd'" );

      } else{
        ar.select("rt.end_cat_cd");

      }
      
      if (alias === true) {
        ar.select("rt.created_at as 'rt.created_at'" );

      } else{
        ar.select("rt.created_at");

      }
      
      if (alias === true) {
        ar.select("rt.updated_at as 'rt.updated_at'" );

      } else{
        ar.select("rt.updated_at");

      }
      
      if (alias === true) {
        ar.select("rt.time_id as 'rt.time_id'" );

      } else{
        ar.select("rt.time_id");

      }
      
      if (alias === true) {
        ar.select("rt.rt_cd as 'rt.rt_cd'" );

      } else{
        ar.select("rt.rt_cd");

      }
      
      if (alias === true) {
        ar.select("rt.rt_status_cd as 'rt.rt_status_cd'" );

      } else{
        ar.select("rt.rt_status_cd");

      }
      
      if (alias === true) {
        ar.select("rt.biz_cd as 'rt.biz_cd'" );

      } else{
        ar.select("rt.biz_cd");

      }
      
      if (alias === true) {
        ar.select("rt.commute_cd as 'rt.commute_cd'" );

      } else{
        ar.select("rt.commute_cd");

      }
      
      if (alias === true) {
        ar.select("rt.activate_cd as 'rt.activate_cd'" );

      } else{
        ar.select("rt.activate_cd");

      }
      
      if (alias === true) {
        ar.select("rt.seat_cd as 'rt.seat_cd'" );

      } else{
        ar.select("rt.seat_cd");

      }
      
      if (alias === true) {
        ar.select("rt.start_day as 'rt.start_day'" );

      } else{
        ar.select("rt.start_day");

      }
      
      if (alias === true) {
        ar.select("rt.end_day as 'rt.end_day'" );

      } else{
        ar.select("rt.end_day");

      }
      
      if (alias === true) {
        ar.select("rt.min_user_cnt as 'rt.min_user_cnt'" );

      } else{
        ar.select("rt.min_user_cnt");

      }
      
      if (alias === true) {
        ar.select("rt.close_day as 'rt.close_day'" );

      } else{
        ar.select("rt.close_day");

      }
      
      if (alias === true) {
        ar.select("rt.schd_url as 'rt.schd_url'" );

      } else{
        ar.select("rt.schd_url");

      }
      
      if (alias === true) {
        ar.select("rt.runn_url as 'rt.runn_url'" );

      } else{
        ar.select("rt.runn_url");

      }
      
      if (alias === true) {
        ar.select("rt.search_cd as 'rt.search_cd'" );

      } else{
        ar.select("rt.search_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_id = 'rt.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_start_info = 'rt.start_info';
        if (alias !== undefined) {

          col_start_info = `${alias}.start_info`;

        }

        ar.select(`${col_start_info} as '${col_start_info}' `);

         
        let col_end_info = 'rt.end_info';
        if (alias !== undefined) {

          col_end_info = `${alias}.end_info`;

        }

        ar.select(`${col_end_info} as '${col_end_info}' `);

         
        let col_start_tag = 'rt.start_tag';
        if (alias !== undefined) {

          col_start_tag = `${alias}.start_tag`;

        }

        ar.select(`${col_start_tag} as '${col_start_tag}' `);

         
        let col_end_tag = 'rt.end_tag';
        if (alias !== undefined) {

          col_end_tag = `${alias}.end_tag`;

        }

        ar.select(`${col_end_tag} as '${col_end_tag}' `);

         
        let col_start_cat_cd = 'rt.start_cat_cd';
        if (alias !== undefined) {

          col_start_cat_cd = `${alias}.start_cat_cd`;

        }

        ar.select(`${col_start_cat_cd} as '${col_start_cat_cd}' `);

         
        let col_end_cat_cd = 'rt.end_cat_cd';
        if (alias !== undefined) {

          col_end_cat_cd = `${alias}.end_cat_cd`;

        }

        ar.select(`${col_end_cat_cd} as '${col_end_cat_cd}' `);

         
        let col_created_at = 'rt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_time_id = 'rt.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_rt_cd = 'rt.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_rt_status_cd = 'rt.rt_status_cd';
        if (alias !== undefined) {

          col_rt_status_cd = `${alias}.rt_status_cd`;

        }

        ar.select(`${col_rt_status_cd} as '${col_rt_status_cd}' `);

         
        let col_biz_cd = 'rt.biz_cd';
        if (alias !== undefined) {

          col_biz_cd = `${alias}.biz_cd`;

        }

        ar.select(`${col_biz_cd} as '${col_biz_cd}' `);

         
        let col_commute_cd = 'rt.commute_cd';
        if (alias !== undefined) {

          col_commute_cd = `${alias}.commute_cd`;

        }

        ar.select(`${col_commute_cd} as '${col_commute_cd}' `);

         
        let col_activate_cd = 'rt.activate_cd';
        if (alias !== undefined) {

          col_activate_cd = `${alias}.activate_cd`;

        }

        ar.select(`${col_activate_cd} as '${col_activate_cd}' `);

         
        let col_seat_cd = 'rt.seat_cd';
        if (alias !== undefined) {

          col_seat_cd = `${alias}.seat_cd`;

        }

        ar.select(`${col_seat_cd} as '${col_seat_cd}' `);

         
        let col_start_day = 'rt.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'rt.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_min_user_cnt = 'rt.min_user_cnt';
        if (alias !== undefined) {

          col_min_user_cnt = `${alias}.min_user_cnt`;

        }

        ar.select(`${col_min_user_cnt} as '${col_min_user_cnt}' `);

         
        let col_close_day = 'rt.close_day';
        if (alias !== undefined) {

          col_close_day = `${alias}.close_day`;

        }

        ar.select(`${col_close_day} as '${col_close_day}' `);

         
        let col_schd_url = 'rt.schd_url';
        if (alias !== undefined) {

          col_schd_url = `${alias}.schd_url`;

        }

        ar.select(`${col_schd_url} as '${col_schd_url}' `);

         
        let col_runn_url = 'rt.runn_url';
        if (alias !== undefined) {

          col_runn_url = `${alias}.runn_url`;

        }

        ar.select(`${col_runn_url} as '${col_runn_url}' `);

         
        let col_search_cd = 'rt.search_cd';
        if (alias !== undefined) {

          col_search_cd = `${alias}.search_cd`;

        }

        ar.select(`${col_search_cd} as '${col_search_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_info');
    
    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.startInfo) === true) {
      ar.set("r_home", form.startInfo);
    } 
    

    if (nullCheck(form.endInfo) === true) {
      ar.set("r_work", form.endInfo);
    } 
    

    if (nullCheck(form.startTag) === true) {
      ar.set("r_home_detail", form.startTag);
    } 
    

    if (nullCheck(form.endTag) === true) {
      ar.set("r_work_detail", form.endTag);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("r_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("r_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("r_tid", form.timeId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("r_type", form.rtCd);
    } 
    

    if (nullCheck(form.rtStatusCd) === true) {
      ar.set("r_status", form.rtStatusCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("r_business", form.bizCd);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("r_commute", form.commuteCd);
    } 
    

    if (nullCheck(form.activateCd) === true) {
      ar.set("r_activation", form.activateCd);
    } 
    

    if (nullCheck(form.seatCd) === true) {
      ar.set("r_seat", form.seatCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("r_end_day", form.endDay);
    } 
    

    if (nullCheck(form.minUserCnt) === true) {
      ar.set("r_max_person", form.minUserCnt);
    } 
    

    if (nullCheck(form.closeDay) === true) {
      ar.set("r_close_day", form.closeDay);
    } 
    

    if (nullCheck(form.schdUrl) === true) {
      ar.set("r_surl", form.schdUrl);
    } 
    

    if (nullCheck(form.runnUrl) === true) {
      ar.set("r_rurl", form.runnUrl);
    } 
    

    if (nullCheck(form.searchCd) === true) {
      ar.set("r_search", form.searchCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_info');
    
    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.startInfo) === true) {
      ar.set("r_home", form.startInfo);
    } 
    

    if (nullCheck(form.endInfo) === true) {
      ar.set("r_work", form.endInfo);
    } 
    

    if (nullCheck(form.startTag) === true) {
      ar.set("r_home_detail", form.startTag);
    } 
    

    if (nullCheck(form.endTag) === true) {
      ar.set("r_work_detail", form.endTag);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("r_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("r_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("r_tid", form.timeId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("r_type", form.rtCd);
    } 
    

    if (nullCheck(form.rtStatusCd) === true) {
      ar.set("r_status", form.rtStatusCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("r_business", form.bizCd);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("r_commute", form.commuteCd);
    } 
    

    if (nullCheck(form.activateCd) === true) {
      ar.set("r_activation", form.activateCd);
    } 
    

    if (nullCheck(form.seatCd) === true) {
      ar.set("r_seat", form.seatCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("r_end_day", form.endDay);
    } 
    

    if (nullCheck(form.minUserCnt) === true) {
      ar.set("r_max_person", form.minUserCnt);
    } 
    

    if (nullCheck(form.closeDay) === true) {
      ar.set("r_close_day", form.closeDay);
    } 
    

    if (nullCheck(form.schdUrl) === true) {
      ar.set("r_surl", form.schdUrl);
    } 
    

    if (nullCheck(form.runnUrl) === true) {
      ar.set("r_rurl", form.runnUrl);
    } 
    

    if (nullCheck(form.searchCd) === true) {
      ar.set("r_search", form.searchCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_view rt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtId: 'r_rid'
    , 

    startInfo: 'r_home'
    , 

    endInfo: 'r_work'
    , 

    startTag: 'r_home_detail'
    , 

    endTag: 'r_work_detail'
    , 

    startCatCd: 'r_home_c'
    , 

    endCatCd: 'r_work_c'
    , 

    createdAt: 'r_timestamp'
    , 

    updatedAt: 'r_timestamp_u'
    , 

    timeId: 'r_tid'
    , 

    rtCd: 'r_type'
    , 

    rtStatusCd: 'r_status'
    , 

    bizCd: 'r_business'
    , 

    commuteCd: 'r_commute'
    , 

    activateCd: 'r_activation'
    , 

    seatCd: 'r_seat'
    , 

    startDay: 'r_start_day'
    , 

    endDay: 'r_end_day'
    , 

    minUserCnt: 'r_max_person'
    , 

    closeDay: 'r_close_day'
    , 

    schdUrl: 'r_surl'
    , 

    runnUrl: 'r_rurl'
    , 

    searchCd: 'r_search'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('r_info');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_view rt');
    
      ar.select("rt.rt_id");

    
    
      ar.select("rt.start_info");

    
    
      ar.select("rt.end_info");

    
    
      ar.select("rt.start_tag");

    
    
      ar.select("rt.end_tag");

    
    
      ar.select("rt.start_cat_cd");

    
    
      ar.select("rt.end_cat_cd");

    
    
      ar.select("rt.created_at");

    
    
      ar.select("rt.updated_at");

    
    
      ar.select("rt.time_id");

    
    
      ar.select("rt.rt_cd");

    
    
      ar.select("rt.rt_status_cd");

    
    
      ar.select("rt.biz_cd");

    
    
      ar.select("rt.commute_cd");

    
    
      ar.select("rt.activate_cd");

    
    
      ar.select("rt.seat_cd");

    
    
      ar.select("rt.start_day");

    
    
      ar.select("rt.end_day");

    
    
      ar.select("rt.min_user_cnt");

    
    
      ar.select("rt.close_day");

    
    
      ar.select("rt.schd_url");

    
    
      ar.select("rt.runn_url");

    
    
      ar.select("rt.search_cd");

    
    
    return ar;
  }

  

  
}
export const RtSql =  new RtQuery()

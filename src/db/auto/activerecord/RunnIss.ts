import { Ar, nullCheck } from "../../../util";
 
  import { IRunnIss } from "../interface";


  class RunnIssQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 runn_iss객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' runn_iss_view runn_iss');

    
      if (alias === true) {
        ar.select("runn_iss.runn_iss_id as 'runn_iss.runn_iss_id'" );

      } else{
        ar.select("runn_iss.runn_iss_id");

      }
      
      if (alias === true) {
        ar.select("runn_iss.rt_id as 'runn_iss.rt_id'" );

      } else{
        ar.select("runn_iss.rt_id");

      }
      
      if (alias === true) {
        ar.select("runn_iss.comment as 'runn_iss.comment'" );

      } else{
        ar.select("runn_iss.comment");

      }
      
      if (alias === true) {
        ar.select("runn_iss.start_day as 'runn_iss.start_day'" );

      } else{
        ar.select("runn_iss.start_day");

      }
      
      if (alias === true) {
        ar.select("runn_iss.end_day as 'runn_iss.end_day'" );

      } else{
        ar.select("runn_iss.end_day");

      }
      
      if (alias === true) {
        ar.select("runn_iss.setl_month as 'runn_iss.setl_month'" );

      } else{
        ar.select("runn_iss.setl_month");

      }
      
      if (alias === true) {
        ar.select("runn_iss.admin_id as 'runn_iss.admin_id'" );

      } else{
        ar.select("runn_iss.admin_id");

      }
      
      if (alias === true) {
        ar.select("runn_iss.iss_cd as 'runn_iss.iss_cd'" );

      } else{
        ar.select("runn_iss.iss_cd");

      }
      
      if (alias === true) {
        ar.select("runn_iss.created_at as 'runn_iss.created_at'" );

      } else{
        ar.select("runn_iss.created_at");

      }
      
      if (alias === true) {
        ar.select("runn_iss.updated_at as 'runn_iss.updated_at'" );

      } else{
        ar.select("runn_iss.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_runn_iss_id = 'runn_iss.runn_iss_id';
        if (alias !== undefined) {

          col_runn_iss_id = `${alias}.runn_iss_id`;

        }

        ar.select(`${col_runn_iss_id} as '${col_runn_iss_id}' `);

         
        let col_rt_id = 'runn_iss.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_comment = 'runn_iss.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_start_day = 'runn_iss.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'runn_iss.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_setl_month = 'runn_iss.setl_month';
        if (alias !== undefined) {

          col_setl_month = `${alias}.setl_month`;

        }

        ar.select(`${col_setl_month} as '${col_setl_month}' `);

         
        let col_admin_id = 'runn_iss.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_iss_cd = 'runn_iss.iss_cd';
        if (alias !== undefined) {

          col_iss_cd = `${alias}.iss_cd`;

        }

        ar.select(`${col_iss_cd} as '${col_iss_cd}' `);

         
        let col_created_at = 'runn_iss.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'runn_iss.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' runn_iss');
    
    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.issCd) === true) {
      ar.set("iss_cd", form.issCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' runn_iss');
    
    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.issCd) === true) {
      ar.set("iss_cd", form.issCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_iss_view runn_iss');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    runnIssId: 'runn_iss_id'
    , 

    rtId: 'rt_id'
    , 

    comment: 'comment'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    setlMonth: 'setl_month'
    , 

    adminId: 'admin_id'
    , 

    issCd: 'iss_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('runn_iss');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' runn_iss_view runn_iss');
    
      ar.select("runn_iss.runn_iss_id");

    
    
      ar.select("runn_iss.rt_id");

    
    
      ar.select("runn_iss.comment");

    
    
      ar.select("runn_iss.start_day");

    
    
      ar.select("runn_iss.end_day");

    
    
      ar.select("runn_iss.setl_month");

    
    
      ar.select("runn_iss.admin_id");

    
    
      ar.select("runn_iss.iss_cd");

    
    
      ar.select("runn_iss.created_at");

    
    
      ar.select("runn_iss.updated_at");

    
    
    return ar;
  }

  

  
}
export const RunnIssSql =  new RunnIssQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { ICat } from "../interface";


  class CatQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cat객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cat_view cat');

    
      if (alias === true) {
        ar.select("cat.cat_id as 'cat.cat_id'" );

      } else{
        ar.select("cat.cat_id");

      }
      
      if (alias === true) {
        ar.select("cat.cat_cd as 'cat.cat_cd'" );

      } else{
        ar.select("cat.cat_cd");

      }
      
      if (alias === true) {
        ar.select("cat.section_cd as 'cat.section_cd'" );

      } else{
        ar.select("cat.section_cd");

      }
      
      if (alias === true) {
        ar.select("cat.alias as 'cat.alias'" );

      } else{
        ar.select("cat.alias");

      }
      
      if (alias === true) {
        ar.select("cat.keyword as 'cat.keyword'" );

      } else{
        ar.select("cat.keyword");

      }
      
      if (alias === true) {
        ar.select("cat.created_at as 'cat.created_at'" );

      } else{
        ar.select("cat.created_at");

      }
      
      if (alias === true) {
        ar.select("cat.updated_at as 'cat.updated_at'" );

      } else{
        ar.select("cat.updated_at");

      }
      
      if (alias === true) {
        ar.select("cat.commute_cd as 'cat.commute_cd'" );

      } else{
        ar.select("cat.commute_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cat_id = 'cat.cat_id';
        if (alias !== undefined) {

          col_cat_id = `${alias}.cat_id`;

        }

        ar.select(`${col_cat_id} as '${col_cat_id}' `);

         
        let col_cat_cd = 'cat.cat_cd';
        if (alias !== undefined) {

          col_cat_cd = `${alias}.cat_cd`;

        }

        ar.select(`${col_cat_cd} as '${col_cat_cd}' `);

         
        let col_section_cd = 'cat.section_cd';
        if (alias !== undefined) {

          col_section_cd = `${alias}.section_cd`;

        }

        ar.select(`${col_section_cd} as '${col_section_cd}' `);

         
        let col_alias = 'cat.alias';
        if (alias !== undefined) {

          col_alias = `${alias}.alias`;

        }

        ar.select(`${col_alias} as '${col_alias}' `);

         
        let col_keyword = 'cat.keyword';
        if (alias !== undefined) {

          col_keyword = `${alias}.keyword`;

        }

        ar.select(`${col_keyword} as '${col_keyword}' `);

         
        let col_created_at = 'cat.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cat.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_commute_cd = 'cat.commute_cd';
        if (alias !== undefined) {

          col_commute_cd = `${alias}.commute_cd`;

        }

        ar.select(`${col_commute_cd} as '${col_commute_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' c_list');
    
    if (nullCheck(form.catId) === true) {
      ar.set("c_no", form.catId);
    } 
    

    if (nullCheck(form.catCd) === true) {
      ar.set("c_uniq", form.catCd);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("c_se", form.sectionCd);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("c_alias", form.alias);
    } 
    

    if (nullCheck(form.keyword) === true) {
      ar.set("c_keyword", form.keyword);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("c_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("c_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("c_commute_type", form.commuteCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' c_list');
    
    if (nullCheck(form.catId) === true) {
      ar.set("c_no", form.catId);
    } 
    

    if (nullCheck(form.catCd) === true) {
      ar.set("c_uniq", form.catCd);
    } 
    

    if (nullCheck(form.sectionCd) === true) {
      ar.set("c_se", form.sectionCd);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("c_alias", form.alias);
    } 
    

    if (nullCheck(form.keyword) === true) {
      ar.set("c_keyword", form.keyword);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("c_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("c_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("c_commute_type", form.commuteCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cat_view cat');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    catId: 'c_no'
    , 

    catCd: 'c_uniq'
    , 

    sectionCd: 'c_se'
    , 

    alias: 'c_alias'
    , 

    keyword: 'c_keyword'
    , 

    createdAt: 'c_timestamp'
    , 

    updatedAt: 'c_timestamp_u'
    , 

    commuteCd: 'c_commute_type'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('c_list');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cat_view cat');
    
      ar.select("cat.cat_id");

    
    
      ar.select("cat.cat_cd");

    
    
      ar.select("cat.section_cd");

    
    
      ar.select("cat.alias");

    
    
      ar.select("cat.keyword");

    
    
      ar.select("cat.created_at");

    
    
      ar.select("cat.updated_at");

    
    
      ar.select("cat.commute_cd");

    
    
    return ar;
  }

  

  
}
export const CatSql =  new CatQuery()

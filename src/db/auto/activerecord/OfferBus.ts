import { Ar, nullCheck } from "../../../util";
 
  import { IOfferBus } from "../interface";


  class OfferBusQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 offer_bus객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' offer_bus_view offer_bus');

    
      if (alias === true) {
        ar.select("offer_bus.offer_bus_id as 'offer_bus.offer_bus_id'" );

      } else{
        ar.select("offer_bus.offer_bus_id");

      }
      
      if (alias === true) {
        ar.select("offer_bus.offer_dr_id as 'offer_bus.offer_dr_id'" );

      } else{
        ar.select("offer_bus.offer_dr_id");

      }
      
      if (alias === true) {
        ar.select("offer_bus.bep as 'offer_bus.bep'" );

      } else{
        ar.select("offer_bus.bep");

      }
      
      if (alias === true) {
        ar.select("offer_bus.fuel_efcnc as 'offer_bus.fuel_efcnc'" );

      } else{
        ar.select("offer_bus.fuel_efcnc");

      }
      
      if (alias === true) {
        ar.select("offer_bus.add_amount as 'offer_bus.add_amount'" );

      } else{
        ar.select("offer_bus.add_amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.insr_amount as 'offer_bus.insr_amount'" );

      } else{
        ar.select("offer_bus.insr_amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.paper_amount as 'offer_bus.paper_amount'" );

      } else{
        ar.select("offer_bus.paper_amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.control_amount as 'offer_bus.control_amount'" );

      } else{
        ar.select("offer_bus.control_amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.expend_amount as 'offer_bus.expend_amount'" );

      } else{
        ar.select("offer_bus.expend_amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.amount as 'offer_bus.amount'" );

      } else{
        ar.select("offer_bus.amount");

      }
      
      if (alias === true) {
        ar.select("offer_bus.admin_id as 'offer_bus.admin_id'" );

      } else{
        ar.select("offer_bus.admin_id");

      }
      
      if (alias === true) {
        ar.select("offer_bus.created_at as 'offer_bus.created_at'" );

      } else{
        ar.select("offer_bus.created_at");

      }
      
      if (alias === true) {
        ar.select("offer_bus.updated_at as 'offer_bus.updated_at'" );

      } else{
        ar.select("offer_bus.updated_at");

      }
      
      if (alias === true) {
        ar.select("offer_bus.bus_cd as 'offer_bus.bus_cd'" );

      } else{
        ar.select("offer_bus.bus_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_offer_bus_id = 'offer_bus.offer_bus_id';
        if (alias !== undefined) {

          col_offer_bus_id = `${alias}.offer_bus_id`;

        }

        ar.select(`${col_offer_bus_id} as '${col_offer_bus_id}' `);

         
        let col_offer_dr_id = 'offer_bus.offer_dr_id';
        if (alias !== undefined) {

          col_offer_dr_id = `${alias}.offer_dr_id`;

        }

        ar.select(`${col_offer_dr_id} as '${col_offer_dr_id}' `);

         
        let col_bep = 'offer_bus.bep';
        if (alias !== undefined) {

          col_bep = `${alias}.bep`;

        }

        ar.select(`${col_bep} as '${col_bep}' `);

         
        let col_fuel_efcnc = 'offer_bus.fuel_efcnc';
        if (alias !== undefined) {

          col_fuel_efcnc = `${alias}.fuel_efcnc`;

        }

        ar.select(`${col_fuel_efcnc} as '${col_fuel_efcnc}' `);

         
        let col_add_amount = 'offer_bus.add_amount';
        if (alias !== undefined) {

          col_add_amount = `${alias}.add_amount`;

        }

        ar.select(`${col_add_amount} as '${col_add_amount}' `);

         
        let col_insr_amount = 'offer_bus.insr_amount';
        if (alias !== undefined) {

          col_insr_amount = `${alias}.insr_amount`;

        }

        ar.select(`${col_insr_amount} as '${col_insr_amount}' `);

         
        let col_paper_amount = 'offer_bus.paper_amount';
        if (alias !== undefined) {

          col_paper_amount = `${alias}.paper_amount`;

        }

        ar.select(`${col_paper_amount} as '${col_paper_amount}' `);

         
        let col_control_amount = 'offer_bus.control_amount';
        if (alias !== undefined) {

          col_control_amount = `${alias}.control_amount`;

        }

        ar.select(`${col_control_amount} as '${col_control_amount}' `);

         
        let col_expend_amount = 'offer_bus.expend_amount';
        if (alias !== undefined) {

          col_expend_amount = `${alias}.expend_amount`;

        }

        ar.select(`${col_expend_amount} as '${col_expend_amount}' `);

         
        let col_amount = 'offer_bus.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_admin_id = 'offer_bus.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'offer_bus.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'offer_bus.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_bus_cd = 'offer_bus.bus_cd';
        if (alias !== undefined) {

          col_bus_cd = `${alias}.bus_cd`;

        }

        ar.select(`${col_bus_cd} as '${col_bus_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_bus');
    
    if (nullCheck(form.offerBusId) === true) {
      ar.set("offer_bus_id", form.offerBusId);
    } 
    

    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.bep) === true) {
      ar.set("bep", form.bep);
    } 
    

    if (nullCheck(form.fuelEfcnc) === true) {
      ar.set("fuel_efcnc", form.fuelEfcnc);
    } 
    

    if (nullCheck(form.addAmount) === true) {
      ar.set("add_amount", form.addAmount);
    } 
    

    if (nullCheck(form.insrAmount) === true) {
      ar.set("insr_amount", form.insrAmount);
    } 
    

    if (nullCheck(form.paperAmount) === true) {
      ar.set("paper_amount", form.paperAmount);
    } 
    

    if (nullCheck(form.controlAmount) === true) {
      ar.set("control_amount", form.controlAmount);
    } 
    

    if (nullCheck(form.expendAmount) === true) {
      ar.set("expend_amount", form.expendAmount);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.busCd) === true) {
      ar.set("bus_cd", form.busCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' offer_bus');
    
    if (nullCheck(form.offerBusId) === true) {
      ar.set("offer_bus_id", form.offerBusId);
    } 
    

    if (nullCheck(form.offerDrId) === true) {
      ar.set("offer_dr_id", form.offerDrId);
    } 
    

    if (nullCheck(form.bep) === true) {
      ar.set("bep", form.bep);
    } 
    

    if (nullCheck(form.fuelEfcnc) === true) {
      ar.set("fuel_efcnc", form.fuelEfcnc);
    } 
    

    if (nullCheck(form.addAmount) === true) {
      ar.set("add_amount", form.addAmount);
    } 
    

    if (nullCheck(form.insrAmount) === true) {
      ar.set("insr_amount", form.insrAmount);
    } 
    

    if (nullCheck(form.paperAmount) === true) {
      ar.set("paper_amount", form.paperAmount);
    } 
    

    if (nullCheck(form.controlAmount) === true) {
      ar.set("control_amount", form.controlAmount);
    } 
    

    if (nullCheck(form.expendAmount) === true) {
      ar.set("expend_amount", form.expendAmount);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.busCd) === true) {
      ar.set("bus_cd", form.busCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_bus_view offer_bus');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    offerBusId: 'offer_bus_id'
    , 

    offerDrId: 'offer_dr_id'
    , 

    bep: 'bep'
    , 

    fuelEfcnc: 'fuel_efcnc'
    , 

    addAmount: 'add_amount'
    , 

    insrAmount: 'insr_amount'
    , 

    paperAmount: 'paper_amount'
    , 

    controlAmount: 'control_amount'
    , 

    expendAmount: 'expend_amount'
    , 

    amount: 'amount'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    busCd: 'bus_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('offer_bus');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' offer_bus_view offer_bus');
    
      ar.select("offer_bus.offer_bus_id");

    
    
      ar.select("offer_bus.offer_dr_id");

    
    
      ar.select("offer_bus.bep");

    
    
      ar.select("offer_bus.fuel_efcnc");

    
    
      ar.select("offer_bus.add_amount");

    
    
      ar.select("offer_bus.insr_amount");

    
    
      ar.select("offer_bus.paper_amount");

    
    
      ar.select("offer_bus.control_amount");

    
    
      ar.select("offer_bus.expend_amount");

    
    
      ar.select("offer_bus.amount");

    
    
      ar.select("offer_bus.admin_id");

    
    
      ar.select("offer_bus.created_at");

    
    
      ar.select("offer_bus.updated_at");

    
    
      ar.select("offer_bus.bus_cd");

    
    
    return ar;
  }

  

  
}
export const OfferBusSql =  new OfferBusQuery()

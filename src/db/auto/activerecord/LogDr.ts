import { Ar, nullCheck } from "../../../util";
 
  import { ILogDr } from "../interface";


  class LogDrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_dr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_dr_view log_dr');

    
      if (alias === true) {
        ar.select("log_dr.log_dr_id as 'log_dr.log_dr_id'" );

      } else{
        ar.select("log_dr.log_dr_id");

      }
      
      if (alias === true) {
        ar.select("log_dr.evt_id as 'log_dr.evt_id'" );

      } else{
        ar.select("log_dr.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_dr.dr_id as 'log_dr.dr_id'" );

      } else{
        ar.select("log_dr.dr_id");

      }
      
      if (alias === true) {
        ar.select("log_dr.nm as 'log_dr.nm'" );

      } else{
        ar.select("log_dr.nm");

      }
      
      if (alias === true) {
        ar.select("log_dr.phone as 'log_dr.phone'" );

      } else{
        ar.select("log_dr.phone");

      }
      
      if (alias === true) {
        ar.select("log_dr.phone_access as 'log_dr.phone_access'" );

      } else{
        ar.select("log_dr.phone_access");

      }
      
      if (alias === true) {
        ar.select("log_dr.status_cd as 'log_dr.status_cd'" );

      } else{
        ar.select("log_dr.status_cd");

      }
      
      if (alias === true) {
        ar.select("log_dr.dr_cd as 'log_dr.dr_cd'" );

      } else{
        ar.select("log_dr.dr_cd");

      }
      
      if (alias === true) {
        ar.select("log_dr.ctrt_yn as 'log_dr.ctrt_yn'" );

      } else{
        ar.select("log_dr.ctrt_yn");

      }
      
      if (alias === true) {
        ar.select("log_dr.created_at as 'log_dr.created_at'" );

      } else{
        ar.select("log_dr.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_dr_id = 'log_dr.log_dr_id';
        if (alias !== undefined) {

          col_log_dr_id = `${alias}.log_dr_id`;

        }

        ar.select(`${col_log_dr_id} as '${col_log_dr_id}' `);

         
        let col_evt_id = 'log_dr.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_dr_id = 'log_dr.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_nm = 'log_dr.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_phone = 'log_dr.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_phone_access = 'log_dr.phone_access';
        if (alias !== undefined) {

          col_phone_access = `${alias}.phone_access`;

        }

        ar.select(`${col_phone_access} as '${col_phone_access}' `);

         
        let col_status_cd = 'log_dr.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_dr_cd = 'log_dr.dr_cd';
        if (alias !== undefined) {

          col_dr_cd = `${alias}.dr_cd`;

        }

        ar.select(`${col_dr_cd} as '${col_dr_cd}' `);

         
        let col_ctrt_yn = 'log_dr.ctrt_yn';
        if (alias !== undefined) {

          col_ctrt_yn = `${alias}.ctrt_yn`;

        }

        ar.select(`${col_ctrt_yn} as '${col_ctrt_yn}' `);

         
        let col_created_at = 'log_dr.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_dinfo');
    
    if (nullCheck(form.logDrId) === true) {
      ar.set("l_no", form.logDrId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("l_did", form.drId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("l_name", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("l_phone", form.phone);
    } 
    

    if (nullCheck(form.phoneAccess) === true) {
      ar.set("l_phone_access", form.phoneAccess);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("l_type", form.statusCd);
    } 
    

    if (nullCheck(form.drCd) === true) {
      ar.set("l_auth", form.drCd);
    } 
    

    if (nullCheck(form.ctrtYn) === true) {
      ar.set("l_contract", form.ctrtYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_dinfo');
    
    if (nullCheck(form.logDrId) === true) {
      ar.set("l_no", form.logDrId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("l_did", form.drId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("l_name", form.nm);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("l_phone", form.phone);
    } 
    

    if (nullCheck(form.phoneAccess) === true) {
      ar.set("l_phone_access", form.phoneAccess);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("l_type", form.statusCd);
    } 
    

    if (nullCheck(form.drCd) === true) {
      ar.set("l_auth", form.drCd);
    } 
    

    if (nullCheck(form.ctrtYn) === true) {
      ar.set("l_contract", form.ctrtYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_dr_view log_dr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logDrId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    drId: 'l_did'
    , 

    nm: 'l_name'
    , 

    phone: 'l_phone'
    , 

    phoneAccess: 'l_phone_access'
    , 

    statusCd: 'l_type'
    , 

    drCd: 'l_auth'
    , 

    ctrtYn: 'l_contract'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_dinfo');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_dr_view log_dr');
    
      ar.select("log_dr.log_dr_id");

    
    
      ar.select("log_dr.evt_id");

    
    
      ar.select("log_dr.dr_id");

    
    
      ar.select("log_dr.nm");

    
    
      ar.select("log_dr.phone");

    
    
      ar.select("log_dr.phone_access");

    
    
      ar.select("log_dr.status_cd");

    
    
      ar.select("log_dr.dr_cd");

    
    
      ar.select("log_dr.ctrt_yn");

    
    
      ar.select("log_dr.created_at");

    
    
    return ar;
  }

  

  
}
export const LogDrSql =  new LogDrQuery()

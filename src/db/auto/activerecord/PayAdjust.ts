import { Ar, nullCheck } from "../../../util";
 
  import { IPayAdjust } from "../interface";


  class PayAdjustQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 pay_adjust객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' pay_adjust_view pay_adjust');

    
      if (alias === true) {
        ar.select("pay_adjust.pay_adjust_id as 'pay_adjust.pay_adjust_id'" );

      } else{
        ar.select("pay_adjust.pay_adjust_id");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.pay_id as 'pay_adjust.pay_id'" );

      } else{
        ar.select("pay_adjust.pay_id");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.board_id as 'pay_adjust.board_id'" );

      } else{
        ar.select("pay_adjust.board_id");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.rt_id as 'pay_adjust.rt_id'" );

      } else{
        ar.select("pay_adjust.rt_id");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.extend_cd as 'pay_adjust.extend_cd'" );

      } else{
        ar.select("pay_adjust.extend_cd");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.user_id as 'pay_adjust.user_id'" );

      } else{
        ar.select("pay_adjust.user_id");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.order_no as 'pay_adjust.order_no'" );

      } else{
        ar.select("pay_adjust.order_no");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.nm as 'pay_adjust.nm'" );

      } else{
        ar.select("pay_adjust.nm");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.amount as 'pay_adjust.amount'" );

      } else{
        ar.select("pay_adjust.amount");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.created_at as 'pay_adjust.created_at'" );

      } else{
        ar.select("pay_adjust.created_at");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.updated_at as 'pay_adjust.updated_at'" );

      } else{
        ar.select("pay_adjust.updated_at");

      }
      
      if (alias === true) {
        ar.select("pay_adjust.admin_id as 'pay_adjust.admin_id'" );

      } else{
        ar.select("pay_adjust.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_pay_adjust_id = 'pay_adjust.pay_adjust_id';
        if (alias !== undefined) {

          col_pay_adjust_id = `${alias}.pay_adjust_id`;

        }

        ar.select(`${col_pay_adjust_id} as '${col_pay_adjust_id}' `);

         
        let col_pay_id = 'pay_adjust.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_board_id = 'pay_adjust.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'pay_adjust.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_extend_cd = 'pay_adjust.extend_cd';
        if (alias !== undefined) {

          col_extend_cd = `${alias}.extend_cd`;

        }

        ar.select(`${col_extend_cd} as '${col_extend_cd}' `);

         
        let col_user_id = 'pay_adjust.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_order_no = 'pay_adjust.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_nm = 'pay_adjust.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_amount = 'pay_adjust.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'pay_adjust.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'pay_adjust.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'pay_adjust.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' pay_adjust');
    
    if (nullCheck(form.payAdjustId) === true) {
      ar.set("pay_adjust_id", form.payAdjustId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.extendCd) === true) {
      ar.set("extend_cd", form.extendCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' pay_adjust');
    
    if (nullCheck(form.payAdjustId) === true) {
      ar.set("pay_adjust_id", form.payAdjustId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.extendCd) === true) {
      ar.set("extend_cd", form.extendCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_adjust_view pay_adjust');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    payAdjustId: 'pay_adjust_id'
    , 

    payId: 'pay_id'
    , 

    boardId: 'board_id'
    , 

    rtId: 'rt_id'
    , 

    extendCd: 'extend_cd'
    , 

    userId: 'user_id'
    , 

    orderNo: 'order_no'
    , 

    nm: 'nm'
    , 

    amount: 'amount'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('pay_adjust');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_adjust_view pay_adjust');
    
      ar.select("pay_adjust.pay_adjust_id");

    
    
      ar.select("pay_adjust.pay_id");

    
    
      ar.select("pay_adjust.board_id");

    
    
      ar.select("pay_adjust.rt_id");

    
    
      ar.select("pay_adjust.extend_cd");

    
    
      ar.select("pay_adjust.user_id");

    
    
      ar.select("pay_adjust.order_no");

    
    
      ar.select("pay_adjust.nm");

    
    
      ar.select("pay_adjust.amount");

    
    
      ar.select("pay_adjust.created_at");

    
    
      ar.select("pay_adjust.updated_at");

    
    
      ar.select("pay_adjust.admin_id");

    
    
    return ar;
  }

  

  
}
export const PayAdjustSql =  new PayAdjustQuery()

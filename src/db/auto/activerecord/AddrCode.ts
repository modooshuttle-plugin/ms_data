import { Ar, nullCheck } from "../../../util";
 
  import { IAddrCode } from "../interface";


  class AddrCodeQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 addr_code객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' addr_code_view addr_code');

    
      if (alias === true) {
        ar.select("addr_code.addr_code_id as 'addr_code.addr_code_id'" );

      } else{
        ar.select("addr_code.addr_code_id");

      }
      
      if (alias === true) {
        ar.select("addr_code.ctp_kor_nm as 'addr_code.ctp_kor_nm'" );

      } else{
        ar.select("addr_code.ctp_kor_nm");

      }
      
      if (alias === true) {
        ar.select("addr_code.ctprvn_cd as 'addr_code.ctprvn_cd'" );

      } else{
        ar.select("addr_code.ctprvn_cd");

      }
      
      if (alias === true) {
        ar.select("addr_code.si_kor_nm as 'addr_code.si_kor_nm'" );

      } else{
        ar.select("addr_code.si_kor_nm");

      }
      
      if (alias === true) {
        ar.select("addr_code.si_cd as 'addr_code.si_cd'" );

      } else{
        ar.select("addr_code.si_cd");

      }
      
      if (alias === true) {
        ar.select("addr_code.sig_kor_nm as 'addr_code.sig_kor_nm'" );

      } else{
        ar.select("addr_code.sig_kor_nm");

      }
      
      if (alias === true) {
        ar.select("addr_code.sig_cd as 'addr_code.sig_cd'" );

      } else{
        ar.select("addr_code.sig_cd");

      }
      
      if (alias === true) {
        ar.select("addr_code.emd_kor_nm as 'addr_code.emd_kor_nm'" );

      } else{
        ar.select("addr_code.emd_kor_nm");

      }
      
      if (alias === true) {
        ar.select("addr_code.emd_cd as 'addr_code.emd_cd'" );

      } else{
        ar.select("addr_code.emd_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_addr_code_id = 'addr_code.addr_code_id';
        if (alias !== undefined) {

          col_addr_code_id = `${alias}.addr_code_id`;

        }

        ar.select(`${col_addr_code_id} as '${col_addr_code_id}' `);

         
        let col_ctp_kor_nm = 'addr_code.ctp_kor_nm';
        if (alias !== undefined) {

          col_ctp_kor_nm = `${alias}.ctp_kor_nm`;

        }

        ar.select(`${col_ctp_kor_nm} as '${col_ctp_kor_nm}' `);

         
        let col_ctprvn_cd = 'addr_code.ctprvn_cd';
        if (alias !== undefined) {

          col_ctprvn_cd = `${alias}.ctprvn_cd`;

        }

        ar.select(`${col_ctprvn_cd} as '${col_ctprvn_cd}' `);

         
        let col_si_kor_nm = 'addr_code.si_kor_nm';
        if (alias !== undefined) {

          col_si_kor_nm = `${alias}.si_kor_nm`;

        }

        ar.select(`${col_si_kor_nm} as '${col_si_kor_nm}' `);

         
        let col_si_cd = 'addr_code.si_cd';
        if (alias !== undefined) {

          col_si_cd = `${alias}.si_cd`;

        }

        ar.select(`${col_si_cd} as '${col_si_cd}' `);

         
        let col_sig_kor_nm = 'addr_code.sig_kor_nm';
        if (alias !== undefined) {

          col_sig_kor_nm = `${alias}.sig_kor_nm`;

        }

        ar.select(`${col_sig_kor_nm} as '${col_sig_kor_nm}' `);

         
        let col_sig_cd = 'addr_code.sig_cd';
        if (alias !== undefined) {

          col_sig_cd = `${alias}.sig_cd`;

        }

        ar.select(`${col_sig_cd} as '${col_sig_cd}' `);

         
        let col_emd_kor_nm = 'addr_code.emd_kor_nm';
        if (alias !== undefined) {

          col_emd_kor_nm = `${alias}.emd_kor_nm`;

        }

        ar.select(`${col_emd_kor_nm} as '${col_emd_kor_nm}' `);

         
        let col_emd_cd = 'addr_code.emd_cd';
        if (alias !== undefined) {

          col_emd_cd = `${alias}.emd_cd`;

        }

        ar.select(`${col_emd_cd} as '${col_emd_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' addr_code');
    
    if (nullCheck(form.addrCodeId) === true) {
      ar.set("addr_seq", form.addrCodeId);
    } 
    

    if (nullCheck(form.ctpKorNm) === true) {
      ar.set("ctp_kor_nm", form.ctpKorNm);
    } 
    

    if (nullCheck(form.ctprvnCd) === true) {
      ar.set("ctprvn_cd", form.ctprvnCd);
    } 
    

    if (nullCheck(form.siKorNm) === true) {
      ar.set("si_kor_nm", form.siKorNm);
    } 
    

    if (nullCheck(form.siCd) === true) {
      ar.set("si_cd", form.siCd);
    } 
    

    if (nullCheck(form.sigKorNm) === true) {
      ar.set("sig_kor_nm", form.sigKorNm);
    } 
    

    if (nullCheck(form.sigCd) === true) {
      ar.set("sig_cd", form.sigCd);
    } 
    

    if (nullCheck(form.emdKorNm) === true) {
      ar.set("emd_kor_nm", form.emdKorNm);
    } 
    

    if (nullCheck(form.emdCd) === true) {
      ar.set("emd_cd", form.emdCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' addr_code');
    
    if (nullCheck(form.addrCodeId) === true) {
      ar.set("addr_seq", form.addrCodeId);
    } 
    

    if (nullCheck(form.ctpKorNm) === true) {
      ar.set("ctp_kor_nm", form.ctpKorNm);
    } 
    

    if (nullCheck(form.ctprvnCd) === true) {
      ar.set("ctprvn_cd", form.ctprvnCd);
    } 
    

    if (nullCheck(form.siKorNm) === true) {
      ar.set("si_kor_nm", form.siKorNm);
    } 
    

    if (nullCheck(form.siCd) === true) {
      ar.set("si_cd", form.siCd);
    } 
    

    if (nullCheck(form.sigKorNm) === true) {
      ar.set("sig_kor_nm", form.sigKorNm);
    } 
    

    if (nullCheck(form.sigCd) === true) {
      ar.set("sig_cd", form.sigCd);
    } 
    

    if (nullCheck(form.emdKorNm) === true) {
      ar.set("emd_kor_nm", form.emdKorNm);
    } 
    

    if (nullCheck(form.emdCd) === true) {
      ar.set("emd_cd", form.emdCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' addr_code_view addr_code');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    addrCodeId: 'addr_seq'
    , 

    ctpKorNm: 'ctp_kor_nm'
    , 

    ctprvnCd: 'ctprvn_cd'
    , 

    siKorNm: 'si_kor_nm'
    , 

    siCd: 'si_cd'
    , 

    sigKorNm: 'sig_kor_nm'
    , 

    sigCd: 'sig_cd'
    , 

    emdKorNm: 'emd_kor_nm'
    , 

    emdCd: 'emd_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('addr_code');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' addr_code_view addr_code');
    
      ar.select("addr_code.addr_code_id");

    
    
      ar.select("addr_code.ctp_kor_nm");

    
    
      ar.select("addr_code.ctprvn_cd");

    
    
      ar.select("addr_code.si_kor_nm");

    
    
      ar.select("addr_code.si_cd");

    
    
      ar.select("addr_code.sig_kor_nm");

    
    
      ar.select("addr_code.sig_cd");

    
    
      ar.select("addr_code.emd_kor_nm");

    
    
      ar.select("addr_code.emd_cd");

    
    
    return ar;
  }

  

  
}
export const AddrCodeSql =  new AddrCodeQuery()

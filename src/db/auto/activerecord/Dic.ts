import { Ar, nullCheck } from "../../../util";
 
  import { IDic } from "../interface";


  class DicQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dic객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dic_view dic');

    
      if (alias === true) {
        ar.select("dic.dic_id as 'dic.dic_id'" );

      } else{
        ar.select("dic.dic_id");

      }
      
      if (alias === true) {
        ar.select("dic.logical as 'dic.logical'" );

      } else{
        ar.select("dic.logical");

      }
      
      if (alias === true) {
        ar.select("dic.physical as 'dic.physical'" );

      } else{
        ar.select("dic.physical");

      }
      
      if (alias === true) {
        ar.select("dic.standard_en as 'dic.standard_en'" );

      } else{
        ar.select("dic.standard_en");

      }
      
      if (alias === true) {
        ar.select("dic.logical_equal_ko as 'dic.logical_equal_ko'" );

      } else{
        ar.select("dic.logical_equal_ko");

      }
      
      if (alias === true) {
        ar.select("dic.physical_equal_en as 'dic.physical_equal_en'" );

      } else{
        ar.select("dic.physical_equal_en");

      }
      
      if (alias === true) {
        ar.select("dic.dic_cd as 'dic.dic_cd'" );

      } else{
        ar.select("dic.dic_cd");

      }
      
      if (alias === true) {
        ar.select("dic.deploy_yn as 'dic.deploy_yn'" );

      } else{
        ar.select("dic.deploy_yn");

      }
      
      if (alias === true) {
        ar.select("dic.created_at as 'dic.created_at'" );

      } else{
        ar.select("dic.created_at");

      }
      
      if (alias === true) {
        ar.select("dic.updated_at as 'dic.updated_at'" );

      } else{
        ar.select("dic.updated_at");

      }
      
      if (alias === true) {
        ar.select("dic.admin_id as 'dic.admin_id'" );

      } else{
        ar.select("dic.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dic_id = 'dic.dic_id';
        if (alias !== undefined) {

          col_dic_id = `${alias}.dic_id`;

        }

        ar.select(`${col_dic_id} as '${col_dic_id}' `);

         
        let col_logical = 'dic.logical';
        if (alias !== undefined) {

          col_logical = `${alias}.logical`;

        }

        ar.select(`${col_logical} as '${col_logical}' `);

         
        let col_physical = 'dic.physical';
        if (alias !== undefined) {

          col_physical = `${alias}.physical`;

        }

        ar.select(`${col_physical} as '${col_physical}' `);

         
        let col_standard_en = 'dic.standard_en';
        if (alias !== undefined) {

          col_standard_en = `${alias}.standard_en`;

        }

        ar.select(`${col_standard_en} as '${col_standard_en}' `);

         
        let col_logical_equal_ko = 'dic.logical_equal_ko';
        if (alias !== undefined) {

          col_logical_equal_ko = `${alias}.logical_equal_ko`;

        }

        ar.select(`${col_logical_equal_ko} as '${col_logical_equal_ko}' `);

         
        let col_physical_equal_en = 'dic.physical_equal_en';
        if (alias !== undefined) {

          col_physical_equal_en = `${alias}.physical_equal_en`;

        }

        ar.select(`${col_physical_equal_en} as '${col_physical_equal_en}' `);

         
        let col_dic_cd = 'dic.dic_cd';
        if (alias !== undefined) {

          col_dic_cd = `${alias}.dic_cd`;

        }

        ar.select(`${col_dic_cd} as '${col_dic_cd}' `);

         
        let col_deploy_yn = 'dic.deploy_yn';
        if (alias !== undefined) {

          col_deploy_yn = `${alias}.deploy_yn`;

        }

        ar.select(`${col_deploy_yn} as '${col_deploy_yn}' `);

         
        let col_created_at = 'dic.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dic.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'dic.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dic');
    
    if (nullCheck(form.dicId) === true) {
      ar.set("dic_id", form.dicId);
    } 
    

    if (nullCheck(form.logical) === true) {
      ar.set("logical", form.logical);
    } 
    

    if (nullCheck(form.physical) === true) {
      ar.set("physical", form.physical);
    } 
    

    if (nullCheck(form.standardEn) === true) {
      ar.set("standard_en", form.standardEn);
    } 
    

    if (nullCheck(form.logicalEqualKo) === true) {
      ar.set("logical_equal_ko", form.logicalEqualKo);
    } 
    

    if (nullCheck(form.physicalEqualEn) === true) {
      ar.set("physical_equal_en", form.physicalEqualEn);
    } 
    

    if (nullCheck(form.dicCd) === true) {
      ar.set("dic_cd", form.dicCd);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("deploy_yn", form.deployYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dic');
    
    if (nullCheck(form.dicId) === true) {
      ar.set("dic_id", form.dicId);
    } 
    

    if (nullCheck(form.logical) === true) {
      ar.set("logical", form.logical);
    } 
    

    if (nullCheck(form.physical) === true) {
      ar.set("physical", form.physical);
    } 
    

    if (nullCheck(form.standardEn) === true) {
      ar.set("standard_en", form.standardEn);
    } 
    

    if (nullCheck(form.logicalEqualKo) === true) {
      ar.set("logical_equal_ko", form.logicalEqualKo);
    } 
    

    if (nullCheck(form.physicalEqualEn) === true) {
      ar.set("physical_equal_en", form.physicalEqualEn);
    } 
    

    if (nullCheck(form.dicCd) === true) {
      ar.set("dic_cd", form.dicCd);
    } 
    

    if (nullCheck(form.deployYn) === true) {
      ar.set("deploy_yn", form.deployYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dic_view dic');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    dicId: 'dic_id'
    , 

    logical: 'logical'
    , 

    physical: 'physical'
    , 

    standardEn: 'standard_en'
    , 

    logicalEqualKo: 'logical_equal_ko'
    , 

    physicalEqualEn: 'physical_equal_en'
    , 

    dicCd: 'dic_cd'
    , 

    deployYn: 'deploy_yn'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dic');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dic_view dic');
    
      ar.select("dic.dic_id");

    
    
      ar.select("dic.logical");

    
    
      ar.select("dic.physical");

    
    
      ar.select("dic.standard_en");

    
    
      ar.select("dic.logical_equal_ko");

    
    
      ar.select("dic.physical_equal_en");

    
    
      ar.select("dic.dic_cd");

    
    
      ar.select("dic.deploy_yn");

    
    
      ar.select("dic.created_at");

    
    
      ar.select("dic.updated_at");

    
    
      ar.select("dic.admin_id");

    
    
    return ar;
  }

  

  
}
export const DicSql =  new DicQuery()

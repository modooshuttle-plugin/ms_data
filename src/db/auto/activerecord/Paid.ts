import { Ar, nullCheck } from "../../../util";
 
  import { IPaid } from "../interface";


  class PaidQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 paid객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' paid_view paid');

    
      if (alias === true) {
        ar.select("paid.paid_id as 'paid.paid_id'" );

      } else{
        ar.select("paid.paid_id");

      }
      
      if (alias === true) {
        ar.select("paid.order_no as 'paid.order_no'" );

      } else{
        ar.select("paid.order_no");

      }
      
      if (alias === true) {
        ar.select("paid.board_id as 'paid.board_id'" );

      } else{
        ar.select("paid.board_id");

      }
      
      if (alias === true) {
        ar.select("paid.pay_id as 'paid.pay_id'" );

      } else{
        ar.select("paid.pay_id");

      }
      
      if (alias === true) {
        ar.select("paid.pay_amount as 'paid.pay_amount'" );

      } else{
        ar.select("paid.pay_amount");

      }
      
      if (alias === true) {
        ar.select("paid.pay_try_id as 'paid.pay_try_id'" );

      } else{
        ar.select("paid.pay_try_id");

      }
      
      if (alias === true) {
        ar.select("paid.rt_id as 'paid.rt_id'" );

      } else{
        ar.select("paid.rt_id");

      }
      
      if (alias === true) {
        ar.select("paid.user_id as 'paid.user_id'" );

      } else{
        ar.select("paid.user_id");

      }
      
      if (alias === true) {
        ar.select("paid.method_cd as 'paid.method_cd'" );

      } else{
        ar.select("paid.method_cd");

      }
      
      if (alias === true) {
        ar.select("paid.cash_receipt_yn as 'paid.cash_receipt_yn'" );

      } else{
        ar.select("paid.cash_receipt_yn");

      }
      
      if (alias === true) {
        ar.select("paid.status_cd as 'paid.status_cd'" );

      } else{
        ar.select("paid.status_cd");

      }
      
      if (alias === true) {
        ar.select("paid.channel_cd as 'paid.channel_cd'" );

      } else{
        ar.select("paid.channel_cd");

      }
      
      if (alias === true) {
        ar.select("paid.imp_uid as 'paid.imp_uid'" );

      } else{
        ar.select("paid.imp_uid");

      }
      
      if (alias === true) {
        ar.select("paid.provider_cd as 'paid.provider_cd'" );

      } else{
        ar.select("paid.provider_cd");

      }
      
      if (alias === true) {
        ar.select("paid.amount as 'paid.amount'" );

      } else{
        ar.select("paid.amount");

      }
      
      if (alias === true) {
        ar.select("paid.cash_receipt_at as 'paid.cash_receipt_at'" );

      } else{
        ar.select("paid.cash_receipt_at");

      }
      
      if (alias === true) {
        ar.select("paid.created_at as 'paid.created_at'" );

      } else{
        ar.select("paid.created_at");

      }
      
      if (alias === true) {
        ar.select("paid.updated_at as 'paid.updated_at'" );

      } else{
        ar.select("paid.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_paid_id = 'paid.paid_id';
        if (alias !== undefined) {

          col_paid_id = `${alias}.paid_id`;

        }

        ar.select(`${col_paid_id} as '${col_paid_id}' `);

         
        let col_order_no = 'paid.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_board_id = 'paid.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_pay_id = 'paid.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_pay_amount = 'paid.pay_amount';
        if (alias !== undefined) {

          col_pay_amount = `${alias}.pay_amount`;

        }

        ar.select(`${col_pay_amount} as '${col_pay_amount}' `);

         
        let col_pay_try_id = 'paid.pay_try_id';
        if (alias !== undefined) {

          col_pay_try_id = `${alias}.pay_try_id`;

        }

        ar.select(`${col_pay_try_id} as '${col_pay_try_id}' `);

         
        let col_rt_id = 'paid.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'paid.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_method_cd = 'paid.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_cash_receipt_yn = 'paid.cash_receipt_yn';
        if (alias !== undefined) {

          col_cash_receipt_yn = `${alias}.cash_receipt_yn`;

        }

        ar.select(`${col_cash_receipt_yn} as '${col_cash_receipt_yn}' `);

         
        let col_status_cd = 'paid.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_channel_cd = 'paid.channel_cd';
        if (alias !== undefined) {

          col_channel_cd = `${alias}.channel_cd`;

        }

        ar.select(`${col_channel_cd} as '${col_channel_cd}' `);

         
        let col_imp_uid = 'paid.imp_uid';
        if (alias !== undefined) {

          col_imp_uid = `${alias}.imp_uid`;

        }

        ar.select(`${col_imp_uid} as '${col_imp_uid}' `);

         
        let col_provider_cd = 'paid.provider_cd';
        if (alias !== undefined) {

          col_provider_cd = `${alias}.provider_cd`;

        }

        ar.select(`${col_provider_cd} as '${col_provider_cd}' `);

         
        let col_amount = 'paid.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_cash_receipt_at = 'paid.cash_receipt_at';
        if (alias !== undefined) {

          col_cash_receipt_at = `${alias}.cash_receipt_at`;

        }

        ar.select(`${col_cash_receipt_at} as '${col_cash_receipt_at}' `);

         
        let col_created_at = 'paid.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'paid.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid');
    
    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.payAmount) === true) {
      ar.set("pay_amount", form.payAmount);
    } 
    

    if (nullCheck(form.payTryId) === true) {
      ar.set("pay_try_id", form.payTryId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.cashReceiptYn) === true) {
      ar.set("cash_receipt_yn", form.cashReceiptYn);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("channel_cd", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("provider_cd", form.providerCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.cashReceiptAt) === true) {
      ar.set("cash_receipt_at", form.cashReceiptAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' paid');
    
    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.payAmount) === true) {
      ar.set("pay_amount", form.payAmount);
    } 
    

    if (nullCheck(form.payTryId) === true) {
      ar.set("pay_try_id", form.payTryId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.cashReceiptYn) === true) {
      ar.set("cash_receipt_yn", form.cashReceiptYn);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("channel_cd", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("provider_cd", form.providerCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.cashReceiptAt) === true) {
      ar.set("cash_receipt_at", form.cashReceiptAt);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_view paid');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    paidId: 'paid_id'
    , 

    orderNo: 'order_no'
    , 

    boardId: 'board_id'
    , 

    payId: 'pay_id'
    , 

    payAmount: 'pay_amount'
    , 

    payTryId: 'pay_try_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    methodCd: 'method_cd'
    , 

    cashReceiptYn: 'cash_receipt_yn'
    , 

    statusCd: 'status_cd'
    , 

    channelCd: 'channel_cd'
    , 

    impUid: 'imp_uid'
    , 

    providerCd: 'provider_cd'
    , 

    amount: 'amount'
    , 

    cashReceiptAt: 'cash_receipt_at'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('paid');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' paid_view paid');
    
      ar.select("paid.paid_id");

    
    
      ar.select("paid.order_no");

    
    
      ar.select("paid.board_id");

    
    
      ar.select("paid.pay_id");

    
    
      ar.select("paid.pay_amount");

    
    
      ar.select("paid.pay_try_id");

    
    
      ar.select("paid.rt_id");

    
    
      ar.select("paid.user_id");

    
    
      ar.select("paid.method_cd");

    
    
      ar.select("paid.cash_receipt_yn");

    
    
      ar.select("paid.status_cd");

    
    
      ar.select("paid.channel_cd");

    
    
      ar.select("paid.imp_uid");

    
    
      ar.select("paid.provider_cd");

    
    
      ar.select("paid.amount");

    
    
      ar.select("paid.cash_receipt_at");

    
    
      ar.select("paid.created_at");

    
    
      ar.select("paid.updated_at");

    
    
    return ar;
  }

  

  
}
export const PaidSql =  new PaidQuery()

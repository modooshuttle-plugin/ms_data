import { Ar, nullCheck } from "../../../util";
 
  import { IRtPause } from "../interface";


  class RtPauseQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt_pause객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_pause_view rt_pause');

    
      if (alias === true) {
        ar.select("rt_pause.rt_pause_id as 'rt_pause.rt_pause_id'" );

      } else{
        ar.select("rt_pause.rt_pause_id");

      }
      
      if (alias === true) {
        ar.select("rt_pause.rt_id as 'rt_pause.rt_id'" );

      } else{
        ar.select("rt_pause.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_pause.pause_cd as 'rt_pause.pause_cd'" );

      } else{
        ar.select("rt_pause.pause_cd");

      }
      
      if (alias === true) {
        ar.select("rt_pause.start_day as 'rt_pause.start_day'" );

      } else{
        ar.select("rt_pause.start_day");

      }
      
      if (alias === true) {
        ar.select("rt_pause.end_day as 'rt_pause.end_day'" );

      } else{
        ar.select("rt_pause.end_day");

      }
      
      if (alias === true) {
        ar.select("rt_pause.comment as 'rt_pause.comment'" );

      } else{
        ar.select("rt_pause.comment");

      }
      
      if (alias === true) {
        ar.select("rt_pause.created_at as 'rt_pause.created_at'" );

      } else{
        ar.select("rt_pause.created_at");

      }
      
      if (alias === true) {
        ar.select("rt_pause.updated_at as 'rt_pause.updated_at'" );

      } else{
        ar.select("rt_pause.updated_at");

      }
      
      if (alias === true) {
        ar.select("rt_pause.admin_id as 'rt_pause.admin_id'" );

      } else{
        ar.select("rt_pause.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_pause_id = 'rt_pause.rt_pause_id';
        if (alias !== undefined) {

          col_rt_pause_id = `${alias}.rt_pause_id`;

        }

        ar.select(`${col_rt_pause_id} as '${col_rt_pause_id}' `);

         
        let col_rt_id = 'rt_pause.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_pause_cd = 'rt_pause.pause_cd';
        if (alias !== undefined) {

          col_pause_cd = `${alias}.pause_cd`;

        }

        ar.select(`${col_pause_cd} as '${col_pause_cd}' `);

         
        let col_start_day = 'rt_pause.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'rt_pause.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_comment = 'rt_pause.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'rt_pause.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt_pause.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'rt_pause.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_pause');
    
    if (nullCheck(form.rtPauseId) === true) {
      ar.set("rt_pause_id", form.rtPauseId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.pauseCd) === true) {
      ar.set("pause_cd", form.pauseCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_pause');
    
    if (nullCheck(form.rtPauseId) === true) {
      ar.set("rt_pause_id", form.rtPauseId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.pauseCd) === true) {
      ar.set("pause_cd", form.pauseCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_pause_view rt_pause');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtPauseId: 'rt_pause_id'
    , 

    rtId: 'rt_id'
    , 

    pauseCd: 'pause_cd'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_pause');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_pause_view rt_pause');
    
      ar.select("rt_pause.rt_pause_id");

    
    
      ar.select("rt_pause.rt_id");

    
    
      ar.select("rt_pause.pause_cd");

    
    
      ar.select("rt_pause.start_day");

    
    
      ar.select("rt_pause.end_day");

    
    
      ar.select("rt_pause.comment");

    
    
      ar.select("rt_pause.created_at");

    
    
      ar.select("rt_pause.updated_at");

    
    
      ar.select("rt_pause.admin_id");

    
    
    return ar;
  }

  

  
}
export const RtPauseSql =  new RtPauseQuery()

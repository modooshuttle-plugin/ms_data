import { Ar, nullCheck } from "../../../util";
 
  import { IBoardIss } from "../interface";


  class BoardIssQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 board_iss객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' board_iss_view board_iss');

    
      if (alias === true) {
        ar.select("board_iss.board_iss_id as 'board_iss.board_iss_id'" );

      } else{
        ar.select("board_iss.board_iss_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.rt_id as 'board_iss.rt_id'" );

      } else{
        ar.select("board_iss.rt_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.board_id as 'board_iss.board_id'" );

      } else{
        ar.select("board_iss.board_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.user_id as 'board_iss.user_id'" );

      } else{
        ar.select("board_iss.user_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.runn_iss_id as 'board_iss.runn_iss_id'" );

      } else{
        ar.select("board_iss.runn_iss_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.comment as 'board_iss.comment'" );

      } else{
        ar.select("board_iss.comment");

      }
      
      if (alias === true) {
        ar.select("board_iss.admin_id as 'board_iss.admin_id'" );

      } else{
        ar.select("board_iss.admin_id");

      }
      
      if (alias === true) {
        ar.select("board_iss.created_at as 'board_iss.created_at'" );

      } else{
        ar.select("board_iss.created_at");

      }
      
      if (alias === true) {
        ar.select("board_iss.updated_at as 'board_iss.updated_at'" );

      } else{
        ar.select("board_iss.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_board_iss_id = 'board_iss.board_iss_id';
        if (alias !== undefined) {

          col_board_iss_id = `${alias}.board_iss_id`;

        }

        ar.select(`${col_board_iss_id} as '${col_board_iss_id}' `);

         
        let col_rt_id = 'board_iss.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_board_id = 'board_iss.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_user_id = 'board_iss.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_runn_iss_id = 'board_iss.runn_iss_id';
        if (alias !== undefined) {

          col_runn_iss_id = `${alias}.runn_iss_id`;

        }

        ar.select(`${col_runn_iss_id} as '${col_runn_iss_id}' `);

         
        let col_comment = 'board_iss.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'board_iss.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'board_iss.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'board_iss.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' board_iss');
    
    if (nullCheck(form.boardIssId) === true) {
      ar.set("board_iss_id", form.boardIssId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' board_iss');
    
    if (nullCheck(form.boardIssId) === true) {
      ar.set("board_iss_id", form.boardIssId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' board_iss_view board_iss');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    boardIssId: 'board_iss_id'
    , 

    rtId: 'rt_id'
    , 

    boardId: 'board_id'
    , 

    userId: 'user_id'
    , 

    runnIssId: 'runn_iss_id'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('board_iss');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' board_iss_view board_iss');
    
      ar.select("board_iss.board_iss_id");

    
    
      ar.select("board_iss.rt_id");

    
    
      ar.select("board_iss.board_id");

    
    
      ar.select("board_iss.user_id");

    
    
      ar.select("board_iss.runn_iss_id");

    
    
      ar.select("board_iss.comment");

    
    
      ar.select("board_iss.admin_id");

    
    
      ar.select("board_iss.created_at");

    
    
      ar.select("board_iss.updated_at");

    
    
    return ar;
  }

  

  
}
export const BoardIssSql =  new BoardIssQuery()

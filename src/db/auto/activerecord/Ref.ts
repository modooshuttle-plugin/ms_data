import { Ar, nullCheck } from "../../../util";
 
  import { IRef } from "../interface";


  class RefQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 ref객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' ref_view ref');

    
      if (alias === true) {
        ar.select("ref.ref_id as 'ref.ref_id'" );

      } else{
        ar.select("ref.ref_id");

      }
      
      if (alias === true) {
        ar.select("ref.board_id as 'ref.board_id'" );

      } else{
        ar.select("ref.board_id");

      }
      
      if (alias === true) {
        ar.select("ref.user_id as 'ref.user_id'" );

      } else{
        ar.select("ref.user_id");

      }
      
      if (alias === true) {
        ar.select("ref.other_user_id as 'ref.other_user_id'" );

      } else{
        ar.select("ref.other_user_id");

      }
      
      if (alias === true) {
        ar.select("ref.cpn_id as 'ref.cpn_id'" );

      } else{
        ar.select("ref.cpn_id");

      }
      
      if (alias === true) {
        ar.select("ref.other_cpn_id as 'ref.other_cpn_id'" );

      } else{
        ar.select("ref.other_cpn_id");

      }
      
      if (alias === true) {
        ar.select("ref.created_at as 'ref.created_at'" );

      } else{
        ar.select("ref.created_at");

      }
      
      if (alias === true) {
        ar.select("ref.updated_at as 'ref.updated_at'" );

      } else{
        ar.select("ref.updated_at");

      }
      
      if (alias === true) {
        ar.select("ref.deploy_cd as 'ref.deploy_cd'" );

      } else{
        ar.select("ref.deploy_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_ref_id = 'ref.ref_id';
        if (alias !== undefined) {

          col_ref_id = `${alias}.ref_id`;

        }

        ar.select(`${col_ref_id} as '${col_ref_id}' `);

         
        let col_board_id = 'ref.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_user_id = 'ref.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_other_user_id = 'ref.other_user_id';
        if (alias !== undefined) {

          col_other_user_id = `${alias}.other_user_id`;

        }

        ar.select(`${col_other_user_id} as '${col_other_user_id}' `);

         
        let col_cpn_id = 'ref.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_other_cpn_id = 'ref.other_cpn_id';
        if (alias !== undefined) {

          col_other_cpn_id = `${alias}.other_cpn_id`;

        }

        ar.select(`${col_other_cpn_id} as '${col_other_cpn_id}' `);

         
        let col_created_at = 'ref.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'ref.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_deploy_cd = 'ref.deploy_cd';
        if (alias !== undefined) {

          col_deploy_cd = `${alias}.deploy_cd`;

        }

        ar.select(`${col_deploy_cd} as '${col_deploy_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_referrer');
    
    if (nullCheck(form.refId) === true) {
      ar.set("o_no", form.refId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.otherUserId) === true) {
      ar.set("o_omid", form.otherUserId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_coupon", form.cpnId);
    } 
    

    if (nullCheck(form.otherCpnId) === true) {
      ar.set("o_ocoupon", form.otherCpnId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("o_deploy", form.deployCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_referrer');
    
    if (nullCheck(form.refId) === true) {
      ar.set("o_no", form.refId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.otherUserId) === true) {
      ar.set("o_omid", form.otherUserId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_coupon", form.cpnId);
    } 
    

    if (nullCheck(form.otherCpnId) === true) {
      ar.set("o_ocoupon", form.otherCpnId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("o_deploy", form.deployCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' ref_view ref');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    refId: 'o_no'
    , 

    boardId: 'o_oid'
    , 

    userId: 'o_mid'
    , 

    otherUserId: 'o_omid'
    , 

    cpnId: 'o_coupon'
    , 

    otherCpnId: 'o_ocoupon'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    deployCd: 'o_deploy'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_referrer');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' ref_view ref');
    
      ar.select("ref.ref_id");

    
    
      ar.select("ref.board_id");

    
    
      ar.select("ref.user_id");

    
    
      ar.select("ref.other_user_id");

    
    
      ar.select("ref.cpn_id");

    
    
      ar.select("ref.other_cpn_id");

    
    
      ar.select("ref.created_at");

    
    
      ar.select("ref.updated_at");

    
    
      ar.select("ref.deploy_cd");

    
    
    return ar;
  }

  

  
}
export const RefSql =  new RefQuery()

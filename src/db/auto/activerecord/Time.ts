import { Ar, nullCheck } from "../../../util";
 
  import { ITime } from "../interface";


  class TimeQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 time객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' time_view time');

    
      if (alias === true) {
        ar.select("time.time_id as 'time.time_id'" );

      } else{
        ar.select("time.time_id");

      }
      
      if (alias === true) {
        ar.select("time.idx as 'time.idx'" );

      } else{
        ar.select("time.idx");

      }
      
      if (alias === true) {
        ar.select("time.time as 'time.time'" );

      } else{
        ar.select("time.time");

      }
      
      if (alias === true) {
        ar.select("time.time_apply as 'time.time_apply'" );

      } else{
        ar.select("time.time_apply");

      }
      
      if (alias === true) {
        ar.select("time.time_search as 'time.time_search'" );

      } else{
        ar.select("time.time_search");

      }
      
      if (alias === true) {
        ar.select("time.time_cd as 'time.time_cd'" );

      } else{
        ar.select("time.time_cd");

      }
      
      if (alias === true) {
        ar.select("time.created_at as 'time.created_at'" );

      } else{
        ar.select("time.created_at");

      }
      
      if (alias === true) {
        ar.select("time.updated_at as 'time.updated_at'" );

      } else{
        ar.select("time.updated_at");

      }
      
      if (alias === true) {
        ar.select("time.commute_cd as 'time.commute_cd'" );

      } else{
        ar.select("time.commute_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_time_id = 'time.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_idx = 'time.idx';
        if (alias !== undefined) {

          col_idx = `${alias}.idx`;

        }

        ar.select(`${col_idx} as '${col_idx}' `);

         
        let col_time = 'time.time';
        if (alias !== undefined) {

          col_time = `${alias}.time`;

        }

        ar.select(`${col_time} as '${col_time}' `);

         
        let col_time_apply = 'time.time_apply';
        if (alias !== undefined) {

          col_time_apply = `${alias}.time_apply`;

        }

        ar.select(`${col_time_apply} as '${col_time_apply}' `);

         
        let col_time_search = 'time.time_search';
        if (alias !== undefined) {

          col_time_search = `${alias}.time_search`;

        }

        ar.select(`${col_time_search} as '${col_time_search}' `);

         
        let col_time_cd = 'time.time_cd';
        if (alias !== undefined) {

          col_time_cd = `${alias}.time_cd`;

        }

        ar.select(`${col_time_cd} as '${col_time_cd}' `);

         
        let col_created_at = 'time.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'time.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_commute_cd = 'time.commute_cd';
        if (alias !== undefined) {

          col_commute_cd = `${alias}.commute_cd`;

        }

        ar.select(`${col_commute_cd} as '${col_commute_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_time');
    
    if (nullCheck(form.timeId) === true) {
      ar.set("t_tid", form.timeId);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("t_idx", form.idx);
    } 
    

    if (nullCheck(form.time) === true) {
      ar.set("t_time", form.time);
    } 
    

    if (nullCheck(form.timeApply) === true) {
      ar.set("t_time_apply", form.timeApply);
    } 
    

    if (nullCheck(form.timeSearch) === true) {
      ar.set("t_time_search", form.timeSearch);
    } 
    

    if (nullCheck(form.timeCd) === true) {
      ar.set("t_type", form.timeCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("t_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("t_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("t_commute", form.commuteCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_time');
    
    if (nullCheck(form.timeId) === true) {
      ar.set("t_tid", form.timeId);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("t_idx", form.idx);
    } 
    

    if (nullCheck(form.time) === true) {
      ar.set("t_time", form.time);
    } 
    

    if (nullCheck(form.timeApply) === true) {
      ar.set("t_time_apply", form.timeApply);
    } 
    

    if (nullCheck(form.timeSearch) === true) {
      ar.set("t_time_search", form.timeSearch);
    } 
    

    if (nullCheck(form.timeCd) === true) {
      ar.set("t_type", form.timeCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("t_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("t_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.commuteCd) === true) {
      ar.set("t_commute", form.commuteCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' time_view time');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    timeId: 't_tid'
    , 

    idx: 't_idx'
    , 

    time: 't_time'
    , 

    timeApply: 't_time_apply'
    , 

    timeSearch: 't_time_search'
    , 

    timeCd: 't_type'
    , 

    createdAt: 't_timestamp'
    , 

    updatedAt: 't_timestamp_u'
    , 

    commuteCd: 't_commute'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('t_time');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' time_view time');
    
      ar.select("time.time_id");

    
    
      ar.select("time.idx");

    
    
      ar.select("time.time");

    
    
      ar.select("time.time_apply");

    
    
      ar.select("time.time_search");

    
    
      ar.select("time.time_cd");

    
    
      ar.select("time.created_at");

    
    
      ar.select("time.updated_at");

    
    
      ar.select("time.commute_cd");

    
    
    return ar;
  }

  

  
}
export const TimeSql =  new TimeQuery()

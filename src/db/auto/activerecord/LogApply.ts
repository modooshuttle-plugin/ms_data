import { Ar, nullCheck } from "../../../util";
 
  import { ILogApply } from "../interface";


  class LogApplyQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_apply객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_apply_view log_apply');

    
      if (alias === true) {
        ar.select("log_apply.log_apply_id as 'log_apply.log_apply_id'" );

      } else{
        ar.select("log_apply.log_apply_id");

      }
      
      if (alias === true) {
        ar.select("log_apply.evt_id as 'log_apply.evt_id'" );

      } else{
        ar.select("log_apply.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_apply.apply_id as 'log_apply.apply_id'" );

      } else{
        ar.select("log_apply.apply_id");

      }
      
      if (alias === true) {
        ar.select("log_apply.user_id as 'log_apply.user_id'" );

      } else{
        ar.select("log_apply.user_id");

      }
      
      if (alias === true) {
        ar.select("log_apply.subscribe_id as 'log_apply.subscribe_id'" );

      } else{
        ar.select("log_apply.subscribe_id");

      }
      
      if (alias === true) {
        ar.select("log_apply.prods_cd as 'log_apply.prods_cd'" );

      } else{
        ar.select("log_apply.prods_cd");

      }
      
      if (alias === true) {
        ar.select("log_apply.start_day as 'log_apply.start_day'" );

      } else{
        ar.select("log_apply.start_day");

      }
      
      if (alias === true) {
        ar.select("log_apply.end_day as 'log_apply.end_day'" );

      } else{
        ar.select("log_apply.end_day");

      }
      
      if (alias === true) {
        ar.select("log_apply.created_at as 'log_apply.created_at'" );

      } else{
        ar.select("log_apply.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_apply_id = 'log_apply.log_apply_id';
        if (alias !== undefined) {

          col_log_apply_id = `${alias}.log_apply_id`;

        }

        ar.select(`${col_log_apply_id} as '${col_log_apply_id}' `);

         
        let col_evt_id = 'log_apply.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_apply_id = 'log_apply.apply_id';
        if (alias !== undefined) {

          col_apply_id = `${alias}.apply_id`;

        }

        ar.select(`${col_apply_id} as '${col_apply_id}' `);

         
        let col_user_id = 'log_apply.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_subscribe_id = 'log_apply.subscribe_id';
        if (alias !== undefined) {

          col_subscribe_id = `${alias}.subscribe_id`;

        }

        ar.select(`${col_subscribe_id} as '${col_subscribe_id}' `);

         
        let col_prods_cd = 'log_apply.prods_cd';
        if (alias !== undefined) {

          col_prods_cd = `${alias}.prods_cd`;

        }

        ar.select(`${col_prods_cd} as '${col_prods_cd}' `);

         
        let col_start_day = 'log_apply.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'log_apply.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_created_at = 'log_apply.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_apply');
    
    if (nullCheck(form.logApplyId) === true) {
      ar.set("l_no", form.logApplyId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("l_aid", form.applyId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("l_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("l_rptype", form.prodsCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_apply');
    
    if (nullCheck(form.logApplyId) === true) {
      ar.set("l_no", form.logApplyId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("l_aid", form.applyId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("l_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("l_rptype", form.prodsCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_apply_view log_apply');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logApplyId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    applyId: 'l_aid'
    , 

    userId: 'l_mid'
    , 

    subscribeId: 'l_sid'
    , 

    prodsCd: 'l_rptype'
    , 

    startDay: 'l_start_day'
    , 

    endDay: 'l_end_day'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_apply');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_apply_view log_apply');
    
      ar.select("log_apply.log_apply_id");

    
    
      ar.select("log_apply.evt_id");

    
    
      ar.select("log_apply.apply_id");

    
    
      ar.select("log_apply.user_id");

    
    
      ar.select("log_apply.subscribe_id");

    
    
      ar.select("log_apply.prods_cd");

    
    
      ar.select("log_apply.start_day");

    
    
      ar.select("log_apply.end_day");

    
    
      ar.select("log_apply.created_at");

    
    
    return ar;
  }

  

  
}
export const LogApplySql =  new LogApplyQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { ICpnLimit } from "../interface";


  class CpnLimitQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cpn_limit객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cpn_limit_view cpn_limit');

    
      if (alias === true) {
        ar.select("cpn_limit.cpn_limit_id as 'cpn_limit.cpn_limit_id'" );

      } else{
        ar.select("cpn_limit.cpn_limit_id");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.cpn_id as 'cpn_limit.cpn_id'" );

      } else{
        ar.select("cpn_limit.cpn_id");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.user_id as 'cpn_limit.user_id'" );

      } else{
        ar.select("cpn_limit.user_id");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.limit_cd as 'cpn_limit.limit_cd'" );

      } else{
        ar.select("cpn_limit.limit_cd");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.target_id as 'cpn_limit.target_id'" );

      } else{
        ar.select("cpn_limit.target_id");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.created_at as 'cpn_limit.created_at'" );

      } else{
        ar.select("cpn_limit.created_at");

      }
      
      if (alias === true) {
        ar.select("cpn_limit.updated_at as 'cpn_limit.updated_at'" );

      } else{
        ar.select("cpn_limit.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cpn_limit_id = 'cpn_limit.cpn_limit_id';
        if (alias !== undefined) {

          col_cpn_limit_id = `${alias}.cpn_limit_id`;

        }

        ar.select(`${col_cpn_limit_id} as '${col_cpn_limit_id}' `);

         
        let col_cpn_id = 'cpn_limit.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_user_id = 'cpn_limit.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_limit_cd = 'cpn_limit.limit_cd';
        if (alias !== undefined) {

          col_limit_cd = `${alias}.limit_cd`;

        }

        ar.select(`${col_limit_cd} as '${col_limit_cd}' `);

         
        let col_target_id = 'cpn_limit.target_id';
        if (alias !== undefined) {

          col_target_id = `${alias}.target_id`;

        }

        ar.select(`${col_target_id} as '${col_target_id}' `);

         
        let col_created_at = 'cpn_limit.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cpn_limit.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' cpn_limit');
    
    if (nullCheck(form.cpnLimitId) === true) {
      ar.set("cpn_limit_id", form.cpnLimitId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("cpn_id", form.cpnId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.limitCd) === true) {
      ar.set("limit_cd", form.limitCd);
    } 
    

    if (nullCheck(form.targetId) === true) {
      ar.set("target_id", form.targetId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' cpn_limit');
    
    if (nullCheck(form.cpnLimitId) === true) {
      ar.set("cpn_limit_id", form.cpnLimitId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("cpn_id", form.cpnId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.limitCd) === true) {
      ar.set("limit_cd", form.limitCd);
    } 
    

    if (nullCheck(form.targetId) === true) {
      ar.set("target_id", form.targetId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_limit_view cpn_limit');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    cpnLimitId: 'cpn_limit_id'
    , 

    cpnId: 'cpn_id'
    , 

    userId: 'user_id'
    , 

    limitCd: 'limit_cd'
    , 

    targetId: 'target_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('cpn_limit');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_limit_view cpn_limit');
    
      ar.select("cpn_limit.cpn_limit_id");

    
    
      ar.select("cpn_limit.cpn_id");

    
    
      ar.select("cpn_limit.user_id");

    
    
      ar.select("cpn_limit.limit_cd");

    
    
      ar.select("cpn_limit.target_id");

    
    
      ar.select("cpn_limit.created_at");

    
    
      ar.select("cpn_limit.updated_at");

    
    
    return ar;
  }

  

  
}
export const CpnLimitSql =  new CpnLimitQuery()

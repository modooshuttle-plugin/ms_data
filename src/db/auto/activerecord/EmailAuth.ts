import { Ar, nullCheck } from "../../../util";
 
  import { IEmailAuth } from "../interface";


  class EmailAuthQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 email_auth객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' email_auth_view email_auth');

    
      if (alias === true) {
        ar.select("email_auth.email_auth_id as 'email_auth.email_auth_id'" );

      } else{
        ar.select("email_auth.email_auth_id");

      }
      
      if (alias === true) {
        ar.select("email_auth.user_id as 'email_auth.user_id'" );

      } else{
        ar.select("email_auth.user_id");

      }
      
      if (alias === true) {
        ar.select("email_auth.auth_id as 'email_auth.auth_id'" );

      } else{
        ar.select("email_auth.auth_id");

      }
      
      if (alias === true) {
        ar.select("email_auth.email_cd as 'email_auth.email_cd'" );

      } else{
        ar.select("email_auth.email_cd");

      }
      
      if (alias === true) {
        ar.select("email_auth.use_yn as 'email_auth.use_yn'" );

      } else{
        ar.select("email_auth.use_yn");

      }
      
      if (alias === true) {
        ar.select("email_auth.cpn_id as 'email_auth.cpn_id'" );

      } else{
        ar.select("email_auth.cpn_id");

      }
      
      if (alias === true) {
        ar.select("email_auth.created_at as 'email_auth.created_at'" );

      } else{
        ar.select("email_auth.created_at");

      }
      
      if (alias === true) {
        ar.select("email_auth.updated_at as 'email_auth.updated_at'" );

      } else{
        ar.select("email_auth.updated_at");

      }
      
      if (alias === true) {
        ar.select("email_auth.org_nm as 'email_auth.org_nm'" );

      } else{
        ar.select("email_auth.org_nm");

      }
      
      if (alias === true) {
        ar.select("email_auth.domain as 'email_auth.domain'" );

      } else{
        ar.select("email_auth.domain");

      }
      
      if (alias === true) {
        ar.select("email_auth.rt_cd as 'email_auth.rt_cd'" );

      } else{
        ar.select("email_auth.rt_cd");

      }
      
      if (alias === true) {
        ar.select("email_auth.email_addr as 'email_auth.email_addr'" );

      } else{
        ar.select("email_auth.email_addr");

      }
      
      if (alias === true) {
        ar.select("email_auth.org_user_auth_id as 'email_auth.org_user_auth_id'" );

      } else{
        ar.select("email_auth.org_user_auth_id");

      }
      
      if (alias === true) {
        ar.select("email_auth.pay_id as 'email_auth.pay_id'" );

      } else{
        ar.select("email_auth.pay_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_email_auth_id = 'email_auth.email_auth_id';
        if (alias !== undefined) {

          col_email_auth_id = `${alias}.email_auth_id`;

        }

        ar.select(`${col_email_auth_id} as '${col_email_auth_id}' `);

         
        let col_user_id = 'email_auth.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_auth_id = 'email_auth.auth_id';
        if (alias !== undefined) {

          col_auth_id = `${alias}.auth_id`;

        }

        ar.select(`${col_auth_id} as '${col_auth_id}' `);

         
        let col_email_cd = 'email_auth.email_cd';
        if (alias !== undefined) {

          col_email_cd = `${alias}.email_cd`;

        }

        ar.select(`${col_email_cd} as '${col_email_cd}' `);

         
        let col_use_yn = 'email_auth.use_yn';
        if (alias !== undefined) {

          col_use_yn = `${alias}.use_yn`;

        }

        ar.select(`${col_use_yn} as '${col_use_yn}' `);

         
        let col_cpn_id = 'email_auth.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_created_at = 'email_auth.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'email_auth.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_org_nm = 'email_auth.org_nm';
        if (alias !== undefined) {

          col_org_nm = `${alias}.org_nm`;

        }

        ar.select(`${col_org_nm} as '${col_org_nm}' `);

         
        let col_domain = 'email_auth.domain';
        if (alias !== undefined) {

          col_domain = `${alias}.domain`;

        }

        ar.select(`${col_domain} as '${col_domain}' `);

         
        let col_rt_cd = 'email_auth.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_email_addr = 'email_auth.email_addr';
        if (alias !== undefined) {

          col_email_addr = `${alias}.email_addr`;

        }

        ar.select(`${col_email_addr} as '${col_email_addr}' `);

         
        let col_org_user_auth_id = 'email_auth.org_user_auth_id';
        if (alias !== undefined) {

          col_org_user_auth_id = `${alias}.org_user_auth_id`;

        }

        ar.select(`${col_org_user_auth_id} as '${col_org_user_auth_id}' `);

         
        let col_pay_id = 'email_auth.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_email');
    
    if (nullCheck(form.emailAuthId) === true) {
      ar.set("o_no", form.emailAuthId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.authId) === true) {
      ar.set("o_ano", form.authId);
    } 
    

    if (nullCheck(form.emailCd) === true) {
      ar.set("o_code", form.emailCd);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("o_use", form.useYn);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.orgNm) === true) {
      ar.set("o_ko", form.orgNm);
    } 
    

    if (nullCheck(form.domain) === true) {
      ar.set("o_en", form.domain);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_type", form.rtCd);
    } 
    

    if (nullCheck(form.emailAddr) === true) {
      ar.set("o_email", form.emailAddr);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_email');
    
    if (nullCheck(form.emailAuthId) === true) {
      ar.set("o_no", form.emailAuthId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.authId) === true) {
      ar.set("o_ano", form.authId);
    } 
    

    if (nullCheck(form.emailCd) === true) {
      ar.set("o_code", form.emailCd);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("o_use", form.useYn);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.orgNm) === true) {
      ar.set("o_ko", form.orgNm);
    } 
    

    if (nullCheck(form.domain) === true) {
      ar.set("o_en", form.domain);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("o_type", form.rtCd);
    } 
    

    if (nullCheck(form.emailAddr) === true) {
      ar.set("o_email", form.emailAddr);
    } 
    

    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' email_auth_view email_auth');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    emailAuthId: 'o_no'
    , 

    userId: 'o_mid'
    , 

    authId: 'o_ano'
    , 

    emailCd: 'o_code'
    , 

    useYn: 'o_use'
    , 

    cpnId: 'o_cid'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    orgNm: 'o_ko'
    , 

    domain: 'o_en'
    , 

    rtCd: 'o_type'
    , 

    emailAddr: 'o_email'
    , 

    orgUserAuthId: 'org_user_auth_id'
    , 

    payId: 'pay_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_email');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' email_auth_view email_auth');
    
      ar.select("email_auth.email_auth_id");

    
    
      ar.select("email_auth.user_id");

    
    
      ar.select("email_auth.auth_id");

    
    
      ar.select("email_auth.email_cd");

    
    
      ar.select("email_auth.use_yn");

    
    
      ar.select("email_auth.cpn_id");

    
    
      ar.select("email_auth.created_at");

    
    
      ar.select("email_auth.updated_at");

    
    
      ar.select("email_auth.org_nm");

    
    
      ar.select("email_auth.domain");

    
    
      ar.select("email_auth.rt_cd");

    
    
      ar.select("email_auth.email_addr");

    
    
      ar.select("email_auth.org_user_auth_id");

    
    
      ar.select("email_auth.pay_id");

    
    
    return ar;
  }

  

  
}
export const EmailAuthSql =  new EmailAuthQuery()

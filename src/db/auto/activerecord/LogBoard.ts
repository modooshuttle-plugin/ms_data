import { Ar, nullCheck } from "../../../util";
 
  import { ILogBoard } from "../interface";


  class LogBoardQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_board객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_board_view log_board');

    
      if (alias === true) {
        ar.select("log_board.log_board_id as 'log_board.log_board_id'" );

      } else{
        ar.select("log_board.log_board_id");

      }
      
      if (alias === true) {
        ar.select("log_board.evt_id as 'log_board.evt_id'" );

      } else{
        ar.select("log_board.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_board.board_id as 'log_board.board_id'" );

      } else{
        ar.select("log_board.board_id");

      }
      
      if (alias === true) {
        ar.select("log_board.apply_id as 'log_board.apply_id'" );

      } else{
        ar.select("log_board.apply_id");

      }
      
      if (alias === true) {
        ar.select("log_board.user_id as 'log_board.user_id'" );

      } else{
        ar.select("log_board.user_id");

      }
      
      if (alias === true) {
        ar.select("log_board.rt_id as 'log_board.rt_id'" );

      } else{
        ar.select("log_board.rt_id");

      }
      
      if (alias === true) {
        ar.select("log_board.prods_id as 'log_board.prods_id'" );

      } else{
        ar.select("log_board.prods_id");

      }
      
      if (alias === true) {
        ar.select("log_board.start_st_cd as 'log_board.start_st_cd'" );

      } else{
        ar.select("log_board.start_st_cd");

      }
      
      if (alias === true) {
        ar.select("log_board.start_st_ver as 'log_board.start_st_ver'" );

      } else{
        ar.select("log_board.start_st_ver");

      }
      
      if (alias === true) {
        ar.select("log_board.end_st_cd as 'log_board.end_st_cd'" );

      } else{
        ar.select("log_board.end_st_cd");

      }
      
      if (alias === true) {
        ar.select("log_board.end_st_ver as 'log_board.end_st_ver'" );

      } else{
        ar.select("log_board.end_st_ver");

      }
      
      if (alias === true) {
        ar.select("log_board.board_cd as 'log_board.board_cd'" );

      } else{
        ar.select("log_board.board_cd");

      }
      
      if (alias === true) {
        ar.select("log_board.board_shape_cd as 'log_board.board_shape_cd'" );

      } else{
        ar.select("log_board.board_shape_cd");

      }
      
      if (alias === true) {
        ar.select("log_board.seat_id as 'log_board.seat_id'" );

      } else{
        ar.select("log_board.seat_id");

      }
      
      if (alias === true) {
        ar.select("log_board.start_day as 'log_board.start_day'" );

      } else{
        ar.select("log_board.start_day");

      }
      
      if (alias === true) {
        ar.select("log_board.end_day as 'log_board.end_day'" );

      } else{
        ar.select("log_board.end_day");

      }
      
      if (alias === true) {
        ar.select("log_board.created_at as 'log_board.created_at'" );

      } else{
        ar.select("log_board.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_board_id = 'log_board.log_board_id';
        if (alias !== undefined) {

          col_log_board_id = `${alias}.log_board_id`;

        }

        ar.select(`${col_log_board_id} as '${col_log_board_id}' `);

         
        let col_evt_id = 'log_board.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_board_id = 'log_board.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_apply_id = 'log_board.apply_id';
        if (alias !== undefined) {

          col_apply_id = `${alias}.apply_id`;

        }

        ar.select(`${col_apply_id} as '${col_apply_id}' `);

         
        let col_user_id = 'log_board.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_rt_id = 'log_board.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_prods_id = 'log_board.prods_id';
        if (alias !== undefined) {

          col_prods_id = `${alias}.prods_id`;

        }

        ar.select(`${col_prods_id} as '${col_prods_id}' `);

         
        let col_start_st_cd = 'log_board.start_st_cd';
        if (alias !== undefined) {

          col_start_st_cd = `${alias}.start_st_cd`;

        }

        ar.select(`${col_start_st_cd} as '${col_start_st_cd}' `);

         
        let col_start_st_ver = 'log_board.start_st_ver';
        if (alias !== undefined) {

          col_start_st_ver = `${alias}.start_st_ver`;

        }

        ar.select(`${col_start_st_ver} as '${col_start_st_ver}' `);

         
        let col_end_st_cd = 'log_board.end_st_cd';
        if (alias !== undefined) {

          col_end_st_cd = `${alias}.end_st_cd`;

        }

        ar.select(`${col_end_st_cd} as '${col_end_st_cd}' `);

         
        let col_end_st_ver = 'log_board.end_st_ver';
        if (alias !== undefined) {

          col_end_st_ver = `${alias}.end_st_ver`;

        }

        ar.select(`${col_end_st_ver} as '${col_end_st_ver}' `);

         
        let col_board_cd = 'log_board.board_cd';
        if (alias !== undefined) {

          col_board_cd = `${alias}.board_cd`;

        }

        ar.select(`${col_board_cd} as '${col_board_cd}' `);

         
        let col_board_shape_cd = 'log_board.board_shape_cd';
        if (alias !== undefined) {

          col_board_shape_cd = `${alias}.board_shape_cd`;

        }

        ar.select(`${col_board_shape_cd} as '${col_board_shape_cd}' `);

         
        let col_seat_id = 'log_board.seat_id';
        if (alias !== undefined) {

          col_seat_id = `${alias}.seat_id`;

        }

        ar.select(`${col_seat_id} as '${col_seat_id}' `);

         
        let col_start_day = 'log_board.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'log_board.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_created_at = 'log_board.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_board');
    
    if (nullCheck(form.logBoardId) === true) {
      ar.set("l_no", form.logBoardId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("l_oid", form.boardId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("l_aid", form.applyId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("l_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("l_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.startStCd) === true) {
      ar.set("l_start_stop", form.startStCd);
    } 
    

    if (nullCheck(form.startStVer) === true) {
      ar.set("l_start_ver", form.startStVer);
    } 
    

    if (nullCheck(form.endStCd) === true) {
      ar.set("l_end_stop", form.endStCd);
    } 
    

    if (nullCheck(form.endStVer) === true) {
      ar.set("l_end_ver", form.endStVer);
    } 
    

    if (nullCheck(form.boardCd) === true) {
      ar.set("l_type", form.boardCd);
    } 
    

    if (nullCheck(form.boardShapeCd) === true) {
      ar.set("l_shape", form.boardShapeCd);
    } 
    

    if (nullCheck(form.seatId) === true) {
      ar.set("l_seat", form.seatId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_board');
    
    if (nullCheck(form.logBoardId) === true) {
      ar.set("l_no", form.logBoardId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("l_oid", form.boardId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("l_aid", form.applyId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("l_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("l_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.startStCd) === true) {
      ar.set("l_start_stop", form.startStCd);
    } 
    

    if (nullCheck(form.startStVer) === true) {
      ar.set("l_start_ver", form.startStVer);
    } 
    

    if (nullCheck(form.endStCd) === true) {
      ar.set("l_end_stop", form.endStCd);
    } 
    

    if (nullCheck(form.endStVer) === true) {
      ar.set("l_end_ver", form.endStVer);
    } 
    

    if (nullCheck(form.boardCd) === true) {
      ar.set("l_type", form.boardCd);
    } 
    

    if (nullCheck(form.boardShapeCd) === true) {
      ar.set("l_shape", form.boardShapeCd);
    } 
    

    if (nullCheck(form.seatId) === true) {
      ar.set("l_seat", form.seatId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("l_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("l_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_board_view log_board');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logBoardId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    boardId: 'l_oid'
    , 

    applyId: 'l_aid'
    , 

    userId: 'l_mid'
    , 

    rtId: 'l_rid'
    , 

    prodsId: 'l_rpid'
    , 

    startStCd: 'l_start_stop'
    , 

    startStVer: 'l_start_ver'
    , 

    endStCd: 'l_end_stop'
    , 

    endStVer: 'l_end_ver'
    , 

    boardCd: 'l_type'
    , 

    boardShapeCd: 'l_shape'
    , 

    seatId: 'l_seat'
    , 

    startDay: 'l_start_day'
    , 

    endDay: 'l_end_day'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_board');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_board_view log_board');
    
      ar.select("log_board.log_board_id");

    
    
      ar.select("log_board.evt_id");

    
    
      ar.select("log_board.board_id");

    
    
      ar.select("log_board.apply_id");

    
    
      ar.select("log_board.user_id");

    
    
      ar.select("log_board.rt_id");

    
    
      ar.select("log_board.prods_id");

    
    
      ar.select("log_board.start_st_cd");

    
    
      ar.select("log_board.start_st_ver");

    
    
      ar.select("log_board.end_st_cd");

    
    
      ar.select("log_board.end_st_ver");

    
    
      ar.select("log_board.board_cd");

    
    
      ar.select("log_board.board_shape_cd");

    
    
      ar.select("log_board.seat_id");

    
    
      ar.select("log_board.start_day");

    
    
      ar.select("log_board.end_day");

    
    
      ar.select("log_board.created_at");

    
    
    return ar;
  }

  

  
}
export const LogBoardSql =  new LogBoardQuery()

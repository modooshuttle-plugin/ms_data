import { Ar, nullCheck } from "../../../util";
 
  import { ILogMake } from "../interface";


  class LogMakeQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_make객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_make_view log_make');

    
      if (alias === true) {
        ar.select("log_make.log_make_id as 'log_make.log_make_id'" );

      } else{
        ar.select("log_make.log_make_id");

      }
      
      if (alias === true) {
        ar.select("log_make.evt_id as 'log_make.evt_id'" );

      } else{
        ar.select("log_make.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_make.make_id as 'log_make.make_id'" );

      } else{
        ar.select("log_make.make_id");

      }
      
      if (alias === true) {
        ar.select("log_make.user_id as 'log_make.user_id'" );

      } else{
        ar.select("log_make.user_id");

      }
      
      if (alias === true) {
        ar.select("log_make.start_addr as 'log_make.start_addr'" );

      } else{
        ar.select("log_make.start_addr");

      }
      
      if (alias === true) {
        ar.select("log_make.start_lat as 'log_make.start_lat'" );

      } else{
        ar.select("log_make.start_lat");

      }
      
      if (alias === true) {
        ar.select("log_make.start_lng as 'log_make.start_lng'" );

      } else{
        ar.select("log_make.start_lng");

      }
      
      if (alias === true) {
        ar.select("log_make.end_addr as 'log_make.end_addr'" );

      } else{
        ar.select("log_make.end_addr");

      }
      
      if (alias === true) {
        ar.select("log_make.end_lat as 'log_make.end_lat'" );

      } else{
        ar.select("log_make.end_lat");

      }
      
      if (alias === true) {
        ar.select("log_make.end_lng as 'log_make.end_lng'" );

      } else{
        ar.select("log_make.end_lng");

      }
      
      if (alias === true) {
        ar.select("log_make.time_id as 'log_make.time_id'" );

      } else{
        ar.select("log_make.time_id");

      }
      
      if (alias === true) {
        ar.select("log_make.start_title as 'log_make.start_title'" );

      } else{
        ar.select("log_make.start_title");

      }
      
      if (alias === true) {
        ar.select("log_make.end_title as 'log_make.end_title'" );

      } else{
        ar.select("log_make.end_title");

      }
      
      if (alias === true) {
        ar.select("log_make.comment as 'log_make.comment'" );

      } else{
        ar.select("log_make.comment");

      }
      
      if (alias === true) {
        ar.select("log_make.email as 'log_make.email'" );

      } else{
        ar.select("log_make.email");

      }
      
      if (alias === true) {
        ar.select("log_make.org_id as 'log_make.org_id'" );

      } else{
        ar.select("log_make.org_id");

      }
      
      if (alias === true) {
        ar.select("log_make.start_cat_cd as 'log_make.start_cat_cd'" );

      } else{
        ar.select("log_make.start_cat_cd");

      }
      
      if (alias === true) {
        ar.select("log_make.end_cat_cd as 'log_make.end_cat_cd'" );

      } else{
        ar.select("log_make.end_cat_cd");

      }
      
      if (alias === true) {
        ar.select("log_make.created_at as 'log_make.created_at'" );

      } else{
        ar.select("log_make.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_make_id = 'log_make.log_make_id';
        if (alias !== undefined) {

          col_log_make_id = `${alias}.log_make_id`;

        }

        ar.select(`${col_log_make_id} as '${col_log_make_id}' `);

         
        let col_evt_id = 'log_make.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_make_id = 'log_make.make_id';
        if (alias !== undefined) {

          col_make_id = `${alias}.make_id`;

        }

        ar.select(`${col_make_id} as '${col_make_id}' `);

         
        let col_user_id = 'log_make.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_start_addr = 'log_make.start_addr';
        if (alias !== undefined) {

          col_start_addr = `${alias}.start_addr`;

        }

        ar.select(`${col_start_addr} as '${col_start_addr}' `);

         
        let col_start_lat = 'log_make.start_lat';
        if (alias !== undefined) {

          col_start_lat = `${alias}.start_lat`;

        }

        ar.select(`${col_start_lat} as '${col_start_lat}' `);

         
        let col_start_lng = 'log_make.start_lng';
        if (alias !== undefined) {

          col_start_lng = `${alias}.start_lng`;

        }

        ar.select(`${col_start_lng} as '${col_start_lng}' `);

         
        let col_end_addr = 'log_make.end_addr';
        if (alias !== undefined) {

          col_end_addr = `${alias}.end_addr`;

        }

        ar.select(`${col_end_addr} as '${col_end_addr}' `);

         
        let col_end_lat = 'log_make.end_lat';
        if (alias !== undefined) {

          col_end_lat = `${alias}.end_lat`;

        }

        ar.select(`${col_end_lat} as '${col_end_lat}' `);

         
        let col_end_lng = 'log_make.end_lng';
        if (alias !== undefined) {

          col_end_lng = `${alias}.end_lng`;

        }

        ar.select(`${col_end_lng} as '${col_end_lng}' `);

         
        let col_time_id = 'log_make.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_start_title = 'log_make.start_title';
        if (alias !== undefined) {

          col_start_title = `${alias}.start_title`;

        }

        ar.select(`${col_start_title} as '${col_start_title}' `);

         
        let col_end_title = 'log_make.end_title';
        if (alias !== undefined) {

          col_end_title = `${alias}.end_title`;

        }

        ar.select(`${col_end_title} as '${col_end_title}' `);

         
        let col_comment = 'log_make.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_email = 'log_make.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_org_id = 'log_make.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_start_cat_cd = 'log_make.start_cat_cd';
        if (alias !== undefined) {

          col_start_cat_cd = `${alias}.start_cat_cd`;

        }

        ar.select(`${col_start_cat_cd} as '${col_start_cat_cd}' `);

         
        let col_end_cat_cd = 'log_make.end_cat_cd';
        if (alias !== undefined) {

          col_end_cat_cd = `${alias}.end_cat_cd`;

        }

        ar.select(`${col_end_cat_cd} as '${col_end_cat_cd}' `);

         
        let col_created_at = 'log_make.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_make');
    
    if (nullCheck(form.logMakeId) === true) {
      ar.set("l_no", form.logMakeId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.makeId) === true) {
      ar.set("l_mkid", form.makeId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("l_home", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("l_home_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("l_home_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("l_work", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("l_work_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("l_work_lng", form.endLng);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("l_tid", form.timeId);
    } 
    

    if (nullCheck(form.startTitle) === true) {
      ar.set("l_home_title", form.startTitle);
    } 
    

    if (nullCheck(form.endTitle) === true) {
      ar.set("l_work_title", form.endTitle);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("l_request", form.comment);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("l_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("l_company", form.orgId);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("l_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("l_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_make');
    
    if (nullCheck(form.logMakeId) === true) {
      ar.set("l_no", form.logMakeId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.makeId) === true) {
      ar.set("l_mkid", form.makeId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("l_home", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("l_home_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("l_home_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("l_work", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("l_work_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("l_work_lng", form.endLng);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("l_tid", form.timeId);
    } 
    

    if (nullCheck(form.startTitle) === true) {
      ar.set("l_home_title", form.startTitle);
    } 
    

    if (nullCheck(form.endTitle) === true) {
      ar.set("l_work_title", form.endTitle);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("l_request", form.comment);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("l_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("l_company", form.orgId);
    } 
    

    if (nullCheck(form.startCatCd) === true) {
      ar.set("l_home_c", form.startCatCd);
    } 
    

    if (nullCheck(form.endCatCd) === true) {
      ar.set("l_work_c", form.endCatCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_make_view log_make');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logMakeId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    makeId: 'l_mkid'
    , 

    userId: 'l_mid'
    , 

    startAddr: 'l_home'
    , 

    startLat: 'l_home_lat'
    , 

    startLng: 'l_home_lng'
    , 

    endAddr: 'l_work'
    , 

    endLat: 'l_work_lat'
    , 

    endLng: 'l_work_lng'
    , 

    timeId: 'l_tid'
    , 

    startTitle: 'l_home_title'
    , 

    endTitle: 'l_work_title'
    , 

    comment: 'l_request'
    , 

    email: 'l_email'
    , 

    orgId: 'l_company'
    , 

    startCatCd: 'l_home_c'
    , 

    endCatCd: 'l_work_c'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_make');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_make_view log_make');
    
      ar.select("log_make.log_make_id");

    
    
      ar.select("log_make.evt_id");

    
    
      ar.select("log_make.make_id");

    
    
      ar.select("log_make.user_id");

    
    
      ar.select("log_make.start_addr");

    
    
      ar.select("log_make.start_lat");

    
    
      ar.select("log_make.start_lng");

    
    
      ar.select("log_make.end_addr");

    
    
      ar.select("log_make.end_lat");

    
    
      ar.select("log_make.end_lng");

    
    
      ar.select("log_make.time_id");

    
    
      ar.select("log_make.start_title");

    
    
      ar.select("log_make.end_title");

    
    
      ar.select("log_make.comment");

    
    
      ar.select("log_make.email");

    
    
      ar.select("log_make.org_id");

    
    
      ar.select("log_make.start_cat_cd");

    
    
      ar.select("log_make.end_cat_cd");

    
    
      ar.select("log_make.created_at");

    
    
    return ar;
  }

  

  
}
export const LogMakeSql =  new LogMakeQuery()

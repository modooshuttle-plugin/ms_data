import { Ar, nullCheck } from "../../../util";
 
  import { IMsgEvt } from "../interface";


  class MsgEvtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 msg_evt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_view msg_evt');

    
      if (alias === true) {
        ar.select("msg_evt.msg_evt_id as 'msg_evt.msg_evt_id'" );

      } else{
        ar.select("msg_evt.msg_evt_id");

      }
      
      if (alias === true) {
        ar.select("msg_evt.title as 'msg_evt.title'" );

      } else{
        ar.select("msg_evt.title");

      }
      
      if (alias === true) {
        ar.select("msg_evt.comment as 'msg_evt.comment'" );

      } else{
        ar.select("msg_evt.comment");

      }
      
      if (alias === true) {
        ar.select("msg_evt.msg_evt_cd as 'msg_evt.msg_evt_cd'" );

      } else{
        ar.select("msg_evt.msg_evt_cd");

      }
      
      if (alias === true) {
        ar.select("msg_evt.deploy_cd as 'msg_evt.deploy_cd'" );

      } else{
        ar.select("msg_evt.deploy_cd");

      }
      
      if (alias === true) {
        ar.select("msg_evt.target as 'msg_evt.target'" );

      } else{
        ar.select("msg_evt.target");

      }
      
      if (alias === true) {
        ar.select("msg_evt.created_at as 'msg_evt.created_at'" );

      } else{
        ar.select("msg_evt.created_at");

      }
      
      if (alias === true) {
        ar.select("msg_evt.updated_at as 'msg_evt.updated_at'" );

      } else{
        ar.select("msg_evt.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_msg_evt_id = 'msg_evt.msg_evt_id';
        if (alias !== undefined) {

          col_msg_evt_id = `${alias}.msg_evt_id`;

        }

        ar.select(`${col_msg_evt_id} as '${col_msg_evt_id}' `);

         
        let col_title = 'msg_evt.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_comment = 'msg_evt.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_msg_evt_cd = 'msg_evt.msg_evt_cd';
        if (alias !== undefined) {

          col_msg_evt_cd = `${alias}.msg_evt_cd`;

        }

        ar.select(`${col_msg_evt_cd} as '${col_msg_evt_cd}' `);

         
        let col_deploy_cd = 'msg_evt.deploy_cd';
        if (alias !== undefined) {

          col_deploy_cd = `${alias}.deploy_cd`;

        }

        ar.select(`${col_deploy_cd} as '${col_deploy_cd}' `);

         
        let col_target = 'msg_evt.target';
        if (alias !== undefined) {

          col_target = `${alias}.target`;

        }

        ar.select(`${col_target} as '${col_target}' `);

         
        let col_created_at = 'msg_evt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'msg_evt.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt');
    
    if (nullCheck(form.msgEvtId) === true) {
      ar.set("msg_evt_id", form.msgEvtId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.msgEvtCd) === true) {
      ar.set("msg_evt_cd", form.msgEvtCd);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("deploy_cd", form.deployCd);
    } 
    

    if (nullCheck(form.target) === true) {
      ar.set("target", form.target);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt');
    
    if (nullCheck(form.msgEvtId) === true) {
      ar.set("msg_evt_id", form.msgEvtId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.msgEvtCd) === true) {
      ar.set("msg_evt_cd", form.msgEvtCd);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("deploy_cd", form.deployCd);
    } 
    

    if (nullCheck(form.target) === true) {
      ar.set("target", form.target);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_view msg_evt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    msgEvtId: 'msg_evt_id'
    , 

    title: 'title'
    , 

    comment: 'comment'
    , 

    msgEvtCd: 'msg_evt_cd'
    , 

    deployCd: 'deploy_cd'
    , 

    target: 'target'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('msg_evt');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_evt_view msg_evt');
    
      ar.select("msg_evt.msg_evt_id");

    
    
      ar.select("msg_evt.title");

    
    
      ar.select("msg_evt.comment");

    
    
      ar.select("msg_evt.msg_evt_cd");

    
    
      ar.select("msg_evt.deploy_cd");

    
    
      ar.select("msg_evt.target");

    
    
      ar.select("msg_evt.created_at");

    
    
      ar.select("msg_evt.updated_at");

    
    
    return ar;
  }

  

  
}
export const MsgEvtSql =  new MsgEvtQuery()

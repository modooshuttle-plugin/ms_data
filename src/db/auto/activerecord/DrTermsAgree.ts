import { Ar, nullCheck } from "../../../util";
 
  import { IDrTermsAgree } from "../interface";


  class DrTermsAgreeQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_terms_agree객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_terms_agree_view dr_terms_agree');

    
      if (alias === true) {
        ar.select("dr_terms_agree.dr_terms_agree_id as 'dr_terms_agree.dr_terms_agree_id'" );

      } else{
        ar.select("dr_terms_agree.dr_terms_agree_id");

      }
      
      if (alias === true) {
        ar.select("dr_terms_agree.dr_id as 'dr_terms_agree.dr_id'" );

      } else{
        ar.select("dr_terms_agree.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_terms_agree.agree_yn as 'dr_terms_agree.agree_yn'" );

      } else{
        ar.select("dr_terms_agree.agree_yn");

      }
      
      if (alias === true) {
        ar.select("dr_terms_agree.created_at as 'dr_terms_agree.created_at'" );

      } else{
        ar.select("dr_terms_agree.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_terms_agree.updated_at as 'dr_terms_agree.updated_at'" );

      } else{
        ar.select("dr_terms_agree.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_terms_agree_id = 'dr_terms_agree.dr_terms_agree_id';
        if (alias !== undefined) {

          col_dr_terms_agree_id = `${alias}.dr_terms_agree_id`;

        }

        ar.select(`${col_dr_terms_agree_id} as '${col_dr_terms_agree_id}' `);

         
        let col_dr_id = 'dr_terms_agree.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_agree_yn = 'dr_terms_agree.agree_yn';
        if (alias !== undefined) {

          col_agree_yn = `${alias}.agree_yn`;

        }

        ar.select(`${col_agree_yn} as '${col_agree_yn}' `);

         
        let col_created_at = 'dr_terms_agree.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_terms_agree.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_terms_agree');
    
    if (nullCheck(form.drTermsAgreeId) === true) {
      ar.set("dr_terms_agree_id", form.drTermsAgreeId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.agreeYn) === true) {
      ar.set("agree_yn", form.agreeYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_terms_agree');
    
    if (nullCheck(form.drTermsAgreeId) === true) {
      ar.set("dr_terms_agree_id", form.drTermsAgreeId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.agreeYn) === true) {
      ar.set("agree_yn", form.agreeYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_terms_agree_view dr_terms_agree');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drTermsAgreeId: 'dr_terms_agree_id'
    , 

    drId: 'dr_id'
    , 

    agreeYn: 'agree_yn'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_terms_agree');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_terms_agree_view dr_terms_agree');
    
      ar.select("dr_terms_agree.dr_terms_agree_id");

    
    
      ar.select("dr_terms_agree.dr_id");

    
    
      ar.select("dr_terms_agree.agree_yn");

    
    
      ar.select("dr_terms_agree.created_at");

    
    
      ar.select("dr_terms_agree.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrTermsAgreeSql =  new DrTermsAgreeQuery()

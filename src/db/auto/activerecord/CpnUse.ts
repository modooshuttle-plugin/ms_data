import { Ar, nullCheck } from "../../../util";
 
  import { ICpnUse } from "../interface";


  class CpnUseQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cpn_use객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cpn_use_view cpn_use');

    
      if (alias === true) {
        ar.select("cpn_use.cpn_use_id as 'cpn_use.cpn_use_id'" );

      } else{
        ar.select("cpn_use.cpn_use_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.rt_id as 'cpn_use.rt_id'" );

      } else{
        ar.select("cpn_use.rt_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.user_id as 'cpn_use.user_id'" );

      } else{
        ar.select("cpn_use.user_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.board_id as 'cpn_use.board_id'" );

      } else{
        ar.select("cpn_use.board_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.pay_id as 'cpn_use.pay_id'" );

      } else{
        ar.select("cpn_use.pay_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.cpn_id as 'cpn_use.cpn_id'" );

      } else{
        ar.select("cpn_use.cpn_id");

      }
      
      if (alias === true) {
        ar.select("cpn_use.order_no as 'cpn_use.order_no'" );

      } else{
        ar.select("cpn_use.order_no");

      }
      
      if (alias === true) {
        ar.select("cpn_use.use_yn as 'cpn_use.use_yn'" );

      } else{
        ar.select("cpn_use.use_yn");

      }
      
      if (alias === true) {
        ar.select("cpn_use.amount as 'cpn_use.amount'" );

      } else{
        ar.select("cpn_use.amount");

      }
      
      if (alias === true) {
        ar.select("cpn_use.created_at as 'cpn_use.created_at'" );

      } else{
        ar.select("cpn_use.created_at");

      }
      
      if (alias === true) {
        ar.select("cpn_use.updated_at as 'cpn_use.updated_at'" );

      } else{
        ar.select("cpn_use.updated_at");

      }
      
      if (alias === true) {
        ar.select("cpn_use.admin_id as 'cpn_use.admin_id'" );

      } else{
        ar.select("cpn_use.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cpn_use_id = 'cpn_use.cpn_use_id';
        if (alias !== undefined) {

          col_cpn_use_id = `${alias}.cpn_use_id`;

        }

        ar.select(`${col_cpn_use_id} as '${col_cpn_use_id}' `);

         
        let col_rt_id = 'cpn_use.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'cpn_use.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_board_id = 'cpn_use.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_pay_id = 'cpn_use.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_cpn_id = 'cpn_use.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_order_no = 'cpn_use.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_use_yn = 'cpn_use.use_yn';
        if (alias !== undefined) {

          col_use_yn = `${alias}.use_yn`;

        }

        ar.select(`${col_use_yn} as '${col_use_yn}' `);

         
        let col_amount = 'cpn_use.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'cpn_use.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cpn_use.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'cpn_use.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_coupon');
    
    if (nullCheck(form.cpnUseId) === true) {
      ar.set("o_no", form.cpnUseId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("o_pid", form.payId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("o_muid", form.orderNo);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("o_cuse", form.useYn);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("o_price", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_admin", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_coupon');
    
    if (nullCheck(form.cpnUseId) === true) {
      ar.set("o_no", form.cpnUseId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("o_pid", form.payId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("o_muid", form.orderNo);
    } 
    

    if (nullCheck(form.useYn) === true) {
      ar.set("o_cuse", form.useYn);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("o_price", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_admin", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_use_view cpn_use');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    cpnUseId: 'o_no'
    , 

    rtId: 'o_rid'
    , 

    userId: 'o_mid'
    , 

    boardId: 'o_oid'
    , 

    payId: 'o_pid'
    , 

    cpnId: 'o_cid'
    , 

    orderNo: 'o_muid'
    , 

    useYn: 'o_cuse'
    , 

    amount: 'o_price'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    adminId: 'o_admin'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_coupon');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_use_view cpn_use');
    
      ar.select("cpn_use.cpn_use_id");

    
    
      ar.select("cpn_use.rt_id");

    
    
      ar.select("cpn_use.user_id");

    
    
      ar.select("cpn_use.board_id");

    
    
      ar.select("cpn_use.pay_id");

    
    
      ar.select("cpn_use.cpn_id");

    
    
      ar.select("cpn_use.order_no");

    
    
      ar.select("cpn_use.use_yn");

    
    
      ar.select("cpn_use.amount");

    
    
      ar.select("cpn_use.created_at");

    
    
      ar.select("cpn_use.updated_at");

    
    
      ar.select("cpn_use.admin_id");

    
    
    return ar;
  }

  

  
}
export const CpnUseSql =  new CpnUseQuery()

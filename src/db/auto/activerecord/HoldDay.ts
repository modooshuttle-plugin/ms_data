import { Ar, nullCheck } from "../../../util";
 
  import { IHoldDay } from "../interface";


  class HoldDayQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 hold_day객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' hold_day_view hold_day');

    
      if (alias === true) {
        ar.select("hold_day.hold_day_id as 'hold_day.hold_day_id'" );

      } else{
        ar.select("hold_day.hold_day_id");

      }
      
      if (alias === true) {
        ar.select("hold_day.hold_id as 'hold_day.hold_id'" );

      } else{
        ar.select("hold_day.hold_id");

      }
      
      if (alias === true) {
        ar.select("hold_day.day as 'hold_day.day'" );

      } else{
        ar.select("hold_day.day");

      }
      
      if (alias === true) {
        ar.select("hold_day.board_id as 'hold_day.board_id'" );

      } else{
        ar.select("hold_day.board_id");

      }
      
      if (alias === true) {
        ar.select("hold_day.user_id as 'hold_day.user_id'" );

      } else{
        ar.select("hold_day.user_id");

      }
      
      if (alias === true) {
        ar.select("hold_day.status_cd as 'hold_day.status_cd'" );

      } else{
        ar.select("hold_day.status_cd");

      }
      
      if (alias === true) {
        ar.select("hold_day.created_at as 'hold_day.created_at'" );

      } else{
        ar.select("hold_day.created_at");

      }
      
      if (alias === true) {
        ar.select("hold_day.updated_at as 'hold_day.updated_at'" );

      } else{
        ar.select("hold_day.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_hold_day_id = 'hold_day.hold_day_id';
        if (alias !== undefined) {

          col_hold_day_id = `${alias}.hold_day_id`;

        }

        ar.select(`${col_hold_day_id} as '${col_hold_day_id}' `);

         
        let col_hold_id = 'hold_day.hold_id';
        if (alias !== undefined) {

          col_hold_id = `${alias}.hold_id`;

        }

        ar.select(`${col_hold_id} as '${col_hold_id}' `);

         
        let col_day = 'hold_day.day';
        if (alias !== undefined) {

          col_day = `${alias}.day`;

        }

        ar.select(`${col_day} as '${col_day}' `);

         
        let col_board_id = 'hold_day.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_user_id = 'hold_day.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_status_cd = 'hold_day.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_created_at = 'hold_day.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'hold_day.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' hold_day');
    
    if (nullCheck(form.holdDayId) === true) {
      ar.set("hold_day_id", form.holdDayId);
    } 
    

    if (nullCheck(form.holdId) === true) {
      ar.set("hold_id", form.holdId);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("day", form.day);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' hold_day');
    
    if (nullCheck(form.holdDayId) === true) {
      ar.set("hold_day_id", form.holdDayId);
    } 
    

    if (nullCheck(form.holdId) === true) {
      ar.set("hold_id", form.holdId);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("day", form.day);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' hold_day_view hold_day');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    holdDayId: 'hold_day_id'
    , 

    holdId: 'hold_id'
    , 

    day: 'day'
    , 

    boardId: 'board_id'
    , 

    userId: 'user_id'
    , 

    statusCd: 'status_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('hold_day');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' hold_day_view hold_day');
    
      ar.select("hold_day.hold_day_id");

    
    
      ar.select("hold_day.hold_id");

    
    
      ar.select("hold_day.day");

    
    
      ar.select("hold_day.board_id");

    
    
      ar.select("hold_day.user_id");

    
    
      ar.select("hold_day.status_cd");

    
    
      ar.select("hold_day.created_at");

    
    
      ar.select("hold_day.updated_at");

    
    
    return ar;
  }

  

  
}
export const HoldDaySql =  new HoldDayQuery()

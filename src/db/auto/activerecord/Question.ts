import { Ar, nullCheck } from "../../../util";
 
  import { IQuestion } from "../interface";


  class QuestionQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 question객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' question_view question');

    
      if (alias === true) {
        ar.select("question.question_id as 'question.question_id'" );

      } else{
        ar.select("question.question_id");

      }
      
      if (alias === true) {
        ar.select("question.board_id as 'question.board_id'" );

      } else{
        ar.select("question.board_id");

      }
      
      if (alias === true) {
        ar.select("question.rt_id as 'question.rt_id'" );

      } else{
        ar.select("question.rt_id");

      }
      
      if (alias === true) {
        ar.select("question.user_id as 'question.user_id'" );

      } else{
        ar.select("question.user_id");

      }
      
      if (alias === true) {
        ar.select("question.comment as 'question.comment'" );

      } else{
        ar.select("question.comment");

      }
      
      if (alias === true) {
        ar.select("question.confirm_cd as 'question.confirm_cd'" );

      } else{
        ar.select("question.confirm_cd");

      }
      
      if (alias === true) {
        ar.select("question.created_at as 'question.created_at'" );

      } else{
        ar.select("question.created_at");

      }
      
      if (alias === true) {
        ar.select("question.updated_at as 'question.updated_at'" );

      } else{
        ar.select("question.updated_at");

      }
      
      if (alias === true) {
        ar.select("question.admin_id as 'question.admin_id'" );

      } else{
        ar.select("question.admin_id");

      }
      
      if (alias === true) {
        ar.select("question.question_cd as 'question.question_cd'" );

      } else{
        ar.select("question.question_cd");

      }
      
      if (alias === true) {
        ar.select("question.dr_confirm_cd as 'question.dr_confirm_cd'" );

      } else{
        ar.select("question.dr_confirm_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_question_id = 'question.question_id';
        if (alias !== undefined) {

          col_question_id = `${alias}.question_id`;

        }

        ar.select(`${col_question_id} as '${col_question_id}' `);

         
        let col_board_id = 'question.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'question.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'question.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_comment = 'question.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_confirm_cd = 'question.confirm_cd';
        if (alias !== undefined) {

          col_confirm_cd = `${alias}.confirm_cd`;

        }

        ar.select(`${col_confirm_cd} as '${col_confirm_cd}' `);

         
        let col_created_at = 'question.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'question.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'question.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_question_cd = 'question.question_cd';
        if (alias !== undefined) {

          col_question_cd = `${alias}.question_cd`;

        }

        ar.select(`${col_question_cd} as '${col_question_cd}' `);

         
        let col_dr_confirm_cd = 'question.dr_confirm_cd';
        if (alias !== undefined) {

          col_dr_confirm_cd = `${alias}.dr_confirm_cd`;

        }

        ar.select(`${col_dr_confirm_cd} as '${col_dr_confirm_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_question');
    
    if (nullCheck(form.questionId) === true) {
      ar.set("o_no", form.questionId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_comment", form.comment);
    } 
    

    if (nullCheck(form.confirmCd) === true) {
      ar.set("o_confirm", form.confirmCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_admin", form.adminId);
    } 
    

    if (nullCheck(form.questionCd) === true) {
      ar.set("o_type", form.questionCd);
    } 
    

    if (nullCheck(form.drConfirmCd) === true) {
      ar.set("o_drv_confirm", form.drConfirmCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_question');
    
    if (nullCheck(form.questionId) === true) {
      ar.set("o_no", form.questionId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("o_comment", form.comment);
    } 
    

    if (nullCheck(form.confirmCd) === true) {
      ar.set("o_confirm", form.confirmCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("o_admin", form.adminId);
    } 
    

    if (nullCheck(form.questionCd) === true) {
      ar.set("o_type", form.questionCd);
    } 
    

    if (nullCheck(form.drConfirmCd) === true) {
      ar.set("o_drv_confirm", form.drConfirmCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' question_view question');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    questionId: 'o_no'
    , 

    boardId: 'o_oid'
    , 

    rtId: 'o_rid'
    , 

    userId: 'o_mid'
    , 

    comment: 'o_comment'
    , 

    confirmCd: 'o_confirm'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    adminId: 'o_admin'
    , 

    questionCd: 'o_type'
    , 

    drConfirmCd: 'o_drv_confirm'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_question');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' question_view question');
    
      ar.select("question.question_id");

    
    
      ar.select("question.board_id");

    
    
      ar.select("question.rt_id");

    
    
      ar.select("question.user_id");

    
    
      ar.select("question.comment");

    
    
      ar.select("question.confirm_cd");

    
    
      ar.select("question.created_at");

    
    
      ar.select("question.updated_at");

    
    
      ar.select("question.admin_id");

    
    
      ar.select("question.question_cd");

    
    
      ar.select("question.dr_confirm_cd");

    
    
    return ar;
  }

  

  
}
export const QuestionSql =  new QuestionQuery()

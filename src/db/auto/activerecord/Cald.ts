import { Ar, nullCheck } from "../../../util";
 
  import { ICald } from "../interface";


  class CaldQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cald객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cald_view cald');

    
      if (alias === true) {
        ar.select("cald.cald_id as 'cald.cald_id'" );

      } else{
        ar.select("cald.cald_id");

      }
      
      if (alias === true) {
        ar.select("cald.runn_cd as 'cald.runn_cd'" );

      } else{
        ar.select("cald.runn_cd");

      }
      
      if (alias === true) {
        ar.select("cald.created_at as 'cald.created_at'" );

      } else{
        ar.select("cald.created_at");

      }
      
      if (alias === true) {
        ar.select("cald.updated_at as 'cald.updated_at'" );

      } else{
        ar.select("cald.updated_at");

      }
      
      if (alias === true) {
        ar.select("cald.day as 'cald.day'" );

      } else{
        ar.select("cald.day");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cald_id = 'cald.cald_id';
        if (alias !== undefined) {

          col_cald_id = `${alias}.cald_id`;

        }

        ar.select(`${col_cald_id} as '${col_cald_id}' `);

         
        let col_runn_cd = 'cald.runn_cd';
        if (alias !== undefined) {

          col_runn_cd = `${alias}.runn_cd`;

        }

        ar.select(`${col_runn_cd} as '${col_runn_cd}' `);

         
        let col_created_at = 'cald.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cald.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_day = 'cald.day';
        if (alias !== undefined) {

          col_day = `${alias}.day`;

        }

        ar.select(`${col_day} as '${col_day}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_calendar');
    
    if (nullCheck(form.caldId) === true) {
      ar.set("a_no", form.caldId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("a_oper", form.runnCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("a_date", form.day);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_calendar');
    
    if (nullCheck(form.caldId) === true) {
      ar.set("a_no", form.caldId);
    } 
    

    if (nullCheck(form.runnCd) === true) {
      ar.set("a_oper", form.runnCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.day) === true) {
      ar.set("a_date", form.day);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cald_view cald');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    caldId: 'a_no'
    , 

    runnCd: 'a_oper'
    , 

    createdAt: 'a_timestamp'
    , 

    updatedAt: 'a_timestamp_u'
    , 

    day: 'a_date'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_calendar');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cald_view cald');
    
      ar.select("cald.cald_id");

    
    
      ar.select("cald.runn_cd");

    
    
      ar.select("cald.created_at");

    
    
      ar.select("cald.updated_at");

    
    
      ar.select("cald.day");

    
    
    return ar;
  }

  

  
}
export const CaldSql =  new CaldQuery()

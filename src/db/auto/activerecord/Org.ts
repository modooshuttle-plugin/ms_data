import { Ar, nullCheck } from "../../../util";
 
  import { IOrg } from "../interface";


  class OrgQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_view org');

    
      if (alias === true) {
        ar.select("org.org_id as 'org.org_id'" );

      } else{
        ar.select("org.org_id");

      }
      
      if (alias === true) {
        ar.select("org.nm as 'org.nm'" );

      } else{
        ar.select("org.nm");

      }
      
      if (alias === true) {
        ar.select("org.biz_nm as 'org.biz_nm'" );

      } else{
        ar.select("org.biz_nm");

      }
      
      if (alias === true) {
        ar.select("org.biz_reg_no as 'org.biz_reg_no'" );

      } else{
        ar.select("org.biz_reg_no");

      }
      
      if (alias === true) {
        ar.select("org.biz_addr as 'org.biz_addr'" );

      } else{
        ar.select("org.biz_addr");

      }
      
      if (alias === true) {
        ar.select("org.org_cd as 'org.org_cd'" );

      } else{
        ar.select("org.org_cd");

      }
      
      if (alias === true) {
        ar.select("org.created_at as 'org.created_at'" );

      } else{
        ar.select("org.created_at");

      }
      
      if (alias === true) {
        ar.select("org.updated_at as 'org.updated_at'" );

      } else{
        ar.select("org.updated_at");

      }
      
      if (alias === true) {
        ar.select("org.admin_id as 'org.admin_id'" );

      } else{
        ar.select("org.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_id = 'org.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_nm = 'org.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_biz_nm = 'org.biz_nm';
        if (alias !== undefined) {

          col_biz_nm = `${alias}.biz_nm`;

        }

        ar.select(`${col_biz_nm} as '${col_biz_nm}' `);

         
        let col_biz_reg_no = 'org.biz_reg_no';
        if (alias !== undefined) {

          col_biz_reg_no = `${alias}.biz_reg_no`;

        }

        ar.select(`${col_biz_reg_no} as '${col_biz_reg_no}' `);

         
        let col_biz_addr = 'org.biz_addr';
        if (alias !== undefined) {

          col_biz_addr = `${alias}.biz_addr`;

        }

        ar.select(`${col_biz_addr} as '${col_biz_addr}' `);

         
        let col_org_cd = 'org.org_cd';
        if (alias !== undefined) {

          col_org_cd = `${alias}.org_cd`;

        }

        ar.select(`${col_org_cd} as '${col_org_cd}' `);

         
        let col_created_at = 'org.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org');
    
    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.bizNm) === true) {
      ar.set("biz_nm", form.bizNm);
    } 
    

    if (nullCheck(form.bizRegNo) === true) {
      ar.set("biz_reg_no", form.bizRegNo);
    } 
    

    if (nullCheck(form.bizAddr) === true) {
      ar.set("biz_addr", form.bizAddr);
    } 
    

    if (nullCheck(form.orgCd) === true) {
      ar.set("org_cd", form.orgCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org');
    
    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.bizNm) === true) {
      ar.set("biz_nm", form.bizNm);
    } 
    

    if (nullCheck(form.bizRegNo) === true) {
      ar.set("biz_reg_no", form.bizRegNo);
    } 
    

    if (nullCheck(form.bizAddr) === true) {
      ar.set("biz_addr", form.bizAddr);
    } 
    

    if (nullCheck(form.orgCd) === true) {
      ar.set("org_cd", form.orgCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_view org');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgId: 'org_id'
    , 

    nm: 'nm'
    , 

    bizNm: 'biz_nm'
    , 

    bizRegNo: 'biz_reg_no'
    , 

    bizAddr: 'biz_addr'
    , 

    orgCd: 'org_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_view org');
    
      ar.select("org.org_id");

    
    
      ar.select("org.nm");

    
    
      ar.select("org.biz_nm");

    
    
      ar.select("org.biz_reg_no");

    
    
      ar.select("org.biz_addr");

    
    
      ar.select("org.org_cd");

    
    
      ar.select("org.created_at");

    
    
      ar.select("org.updated_at");

    
    
      ar.select("org.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgSql =  new OrgQuery()

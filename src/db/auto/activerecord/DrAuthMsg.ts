import { Ar, nullCheck } from "../../../util";
 
  import { IDrAuthMsg } from "../interface";


  class DrAuthMsgQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_auth_msg객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_auth_msg_view dr_auth_msg');

    
      if (alias === true) {
        ar.select("dr_auth_msg.dr_auth_msg_id as 'dr_auth_msg.dr_auth_msg_id'" );

      } else{
        ar.select("dr_auth_msg.dr_auth_msg_id");

      }
      
      if (alias === true) {
        ar.select("dr_auth_msg.phone as 'dr_auth_msg.phone'" );

      } else{
        ar.select("dr_auth_msg.phone");

      }
      
      if (alias === true) {
        ar.select("dr_auth_msg.auth_no as 'dr_auth_msg.auth_no'" );

      } else{
        ar.select("dr_auth_msg.auth_no");

      }
      
      if (alias === true) {
        ar.select("dr_auth_msg.send_id as 'dr_auth_msg.send_id'" );

      } else{
        ar.select("dr_auth_msg.send_id");

      }
      
      if (alias === true) {
        ar.select("dr_auth_msg.created_at as 'dr_auth_msg.created_at'" );

      } else{
        ar.select("dr_auth_msg.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_auth_msg.updated_at as 'dr_auth_msg.updated_at'" );

      } else{
        ar.select("dr_auth_msg.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_auth_msg_id = 'dr_auth_msg.dr_auth_msg_id';
        if (alias !== undefined) {

          col_dr_auth_msg_id = `${alias}.dr_auth_msg_id`;

        }

        ar.select(`${col_dr_auth_msg_id} as '${col_dr_auth_msg_id}' `);

         
        let col_phone = 'dr_auth_msg.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_auth_no = 'dr_auth_msg.auth_no';
        if (alias !== undefined) {

          col_auth_no = `${alias}.auth_no`;

        }

        ar.select(`${col_auth_no} as '${col_auth_no}' `);

         
        let col_send_id = 'dr_auth_msg.send_id';
        if (alias !== undefined) {

          col_send_id = `${alias}.send_id`;

        }

        ar.select(`${col_send_id} as '${col_send_id}' `);

         
        let col_created_at = 'dr_auth_msg.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_auth_msg.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_msg');
    
    if (nullCheck(form.drAuthMsgId) === true) {
      ar.set("d_no", form.drAuthMsgId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("d_phone", form.phone);
    } 
    

    if (nullCheck(form.authNo) === true) {
      ar.set("d_auth_no", form.authNo);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("d_c_gid", form.sendId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_msg');
    
    if (nullCheck(form.drAuthMsgId) === true) {
      ar.set("d_no", form.drAuthMsgId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("d_phone", form.phone);
    } 
    

    if (nullCheck(form.authNo) === true) {
      ar.set("d_auth_no", form.authNo);
    } 
    

    if (nullCheck(form.sendId) === true) {
      ar.set("d_c_gid", form.sendId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_auth_msg_view dr_auth_msg');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drAuthMsgId: 'd_no'
    , 

    phone: 'd_phone'
    , 

    authNo: 'd_auth_no'
    , 

    sendId: 'd_c_gid'
    , 

    createdAt: 'd_timestamp'
    , 

    updatedAt: 'd_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_msg');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_auth_msg_view dr_auth_msg');
    
      ar.select("dr_auth_msg.dr_auth_msg_id");

    
    
      ar.select("dr_auth_msg.phone");

    
    
      ar.select("dr_auth_msg.auth_no");

    
    
      ar.select("dr_auth_msg.send_id");

    
    
      ar.select("dr_auth_msg.created_at");

    
    
      ar.select("dr_auth_msg.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrAuthMsgSql =  new DrAuthMsgQuery()

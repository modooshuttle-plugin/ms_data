import { Ar, nullCheck } from "../../../util";
 
  import { IDrSetl } from "../interface";


  class DrSetlQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_setl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_setl_view dr_setl');

    
      if (alias === true) {
        ar.select("dr_setl.dr_setl_id as 'dr_setl.dr_setl_id'" );

      } else{
        ar.select("dr_setl.dr_setl_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.dr_pay_id as 'dr_setl.dr_pay_id'" );

      } else{
        ar.select("dr_setl.dr_pay_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.biz_cd as 'dr_setl.biz_cd'" );

      } else{
        ar.select("dr_setl.biz_cd");

      }
      
      if (alias === true) {
        ar.select("dr_setl.runn_day_cnt as 'dr_setl.runn_day_cnt'" );

      } else{
        ar.select("dr_setl.runn_day_cnt");

      }
      
      if (alias === true) {
        ar.select("dr_setl.not_runn_day_cnt as 'dr_setl.not_runn_day_cnt'" );

      } else{
        ar.select("dr_setl.not_runn_day_cnt");

      }
      
      if (alias === true) {
        ar.select("dr_setl.except_day_cnt as 'dr_setl.except_day_cnt'" );

      } else{
        ar.select("dr_setl.except_day_cnt");

      }
      
      if (alias === true) {
        ar.select("dr_setl.default_amount as 'dr_setl.default_amount'" );

      } else{
        ar.select("dr_setl.default_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.pay_amount as 'dr_setl.pay_amount'" );

      } else{
        ar.select("dr_setl.pay_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.not_runn_amount as 'dr_setl.not_runn_amount'" );

      } else{
        ar.select("dr_setl.not_runn_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.add_amount as 'dr_setl.add_amount'" );

      } else{
        ar.select("dr_setl.add_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.iss_add_amount as 'dr_setl.iss_add_amount'" );

      } else{
        ar.select("dr_setl.iss_add_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.iss_remove_amount as 'dr_setl.iss_remove_amount'" );

      } else{
        ar.select("dr_setl.iss_remove_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.supply_amount as 'dr_setl.supply_amount'" );

      } else{
        ar.select("dr_setl.supply_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.vat_amount as 'dr_setl.vat_amount'" );

      } else{
        ar.select("dr_setl.vat_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.etc_amount as 'dr_setl.etc_amount'" );

      } else{
        ar.select("dr_setl.etc_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.ref_amount as 'dr_setl.ref_amount'" );

      } else{
        ar.select("dr_setl.ref_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.total_runn_amount as 'dr_setl.total_runn_amount'" );

      } else{
        ar.select("dr_setl.total_runn_amount");

      }
      
      if (alias === true) {
        ar.select("dr_setl.vat_cd as 'dr_setl.vat_cd'" );

      } else{
        ar.select("dr_setl.vat_cd");

      }
      
      if (alias === true) {
        ar.select("dr_setl.setl_cd as 'dr_setl.setl_cd'" );

      } else{
        ar.select("dr_setl.setl_cd");

      }
      
      if (alias === true) {
        ar.select("dr_setl.rt_id as 'dr_setl.rt_id'" );

      } else{
        ar.select("dr_setl.rt_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.dr_id as 'dr_setl.dr_id'" );

      } else{
        ar.select("dr_setl.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.bus_compn_id as 'dr_setl.bus_compn_id'" );

      } else{
        ar.select("dr_setl.bus_compn_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.dispatch_id as 'dr_setl.dispatch_id'" );

      } else{
        ar.select("dr_setl.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.setl_month as 'dr_setl.setl_month'" );

      } else{
        ar.select("dr_setl.setl_month");

      }
      
      if (alias === true) {
        ar.select("dr_setl.start_day as 'dr_setl.start_day'" );

      } else{
        ar.select("dr_setl.start_day");

      }
      
      if (alias === true) {
        ar.select("dr_setl.end_day as 'dr_setl.end_day'" );

      } else{
        ar.select("dr_setl.end_day");

      }
      
      if (alias === true) {
        ar.select("dr_setl.transfer_yn as 'dr_setl.transfer_yn'" );

      } else{
        ar.select("dr_setl.transfer_yn");

      }
      
      if (alias === true) {
        ar.select("dr_setl.created_at as 'dr_setl.created_at'" );

      } else{
        ar.select("dr_setl.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_setl.updated_at as 'dr_setl.updated_at'" );

      } else{
        ar.select("dr_setl.updated_at");

      }
      
      if (alias === true) {
        ar.select("dr_setl.admin_id as 'dr_setl.admin_id'" );

      } else{
        ar.select("dr_setl.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.dr_ref_id as 'dr_setl.dr_ref_id'" );

      } else{
        ar.select("dr_setl.dr_ref_id");

      }
      
      if (alias === true) {
        ar.select("dr_setl.comment as 'dr_setl.comment'" );

      } else{
        ar.select("dr_setl.comment");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_setl_id = 'dr_setl.dr_setl_id';
        if (alias !== undefined) {

          col_dr_setl_id = `${alias}.dr_setl_id`;

        }

        ar.select(`${col_dr_setl_id} as '${col_dr_setl_id}' `);

         
        let col_dr_pay_id = 'dr_setl.dr_pay_id';
        if (alias !== undefined) {

          col_dr_pay_id = `${alias}.dr_pay_id`;

        }

        ar.select(`${col_dr_pay_id} as '${col_dr_pay_id}' `);

         
        let col_biz_cd = 'dr_setl.biz_cd';
        if (alias !== undefined) {

          col_biz_cd = `${alias}.biz_cd`;

        }

        ar.select(`${col_biz_cd} as '${col_biz_cd}' `);

         
        let col_runn_day_cnt = 'dr_setl.runn_day_cnt';
        if (alias !== undefined) {

          col_runn_day_cnt = `${alias}.runn_day_cnt`;

        }

        ar.select(`${col_runn_day_cnt} as '${col_runn_day_cnt}' `);

         
        let col_not_runn_day_cnt = 'dr_setl.not_runn_day_cnt';
        if (alias !== undefined) {

          col_not_runn_day_cnt = `${alias}.not_runn_day_cnt`;

        }

        ar.select(`${col_not_runn_day_cnt} as '${col_not_runn_day_cnt}' `);

         
        let col_except_day_cnt = 'dr_setl.except_day_cnt';
        if (alias !== undefined) {

          col_except_day_cnt = `${alias}.except_day_cnt`;

        }

        ar.select(`${col_except_day_cnt} as '${col_except_day_cnt}' `);

         
        let col_default_amount = 'dr_setl.default_amount';
        if (alias !== undefined) {

          col_default_amount = `${alias}.default_amount`;

        }

        ar.select(`${col_default_amount} as '${col_default_amount}' `);

         
        let col_pay_amount = 'dr_setl.pay_amount';
        if (alias !== undefined) {

          col_pay_amount = `${alias}.pay_amount`;

        }

        ar.select(`${col_pay_amount} as '${col_pay_amount}' `);

         
        let col_not_runn_amount = 'dr_setl.not_runn_amount';
        if (alias !== undefined) {

          col_not_runn_amount = `${alias}.not_runn_amount`;

        }

        ar.select(`${col_not_runn_amount} as '${col_not_runn_amount}' `);

         
        let col_add_amount = 'dr_setl.add_amount';
        if (alias !== undefined) {

          col_add_amount = `${alias}.add_amount`;

        }

        ar.select(`${col_add_amount} as '${col_add_amount}' `);

         
        let col_iss_add_amount = 'dr_setl.iss_add_amount';
        if (alias !== undefined) {

          col_iss_add_amount = `${alias}.iss_add_amount`;

        }

        ar.select(`${col_iss_add_amount} as '${col_iss_add_amount}' `);

         
        let col_iss_remove_amount = 'dr_setl.iss_remove_amount';
        if (alias !== undefined) {

          col_iss_remove_amount = `${alias}.iss_remove_amount`;

        }

        ar.select(`${col_iss_remove_amount} as '${col_iss_remove_amount}' `);

         
        let col_supply_amount = 'dr_setl.supply_amount';
        if (alias !== undefined) {

          col_supply_amount = `${alias}.supply_amount`;

        }

        ar.select(`${col_supply_amount} as '${col_supply_amount}' `);

         
        let col_vat_amount = 'dr_setl.vat_amount';
        if (alias !== undefined) {

          col_vat_amount = `${alias}.vat_amount`;

        }

        ar.select(`${col_vat_amount} as '${col_vat_amount}' `);

         
        let col_etc_amount = 'dr_setl.etc_amount';
        if (alias !== undefined) {

          col_etc_amount = `${alias}.etc_amount`;

        }

        ar.select(`${col_etc_amount} as '${col_etc_amount}' `);

         
        let col_ref_amount = 'dr_setl.ref_amount';
        if (alias !== undefined) {

          col_ref_amount = `${alias}.ref_amount`;

        }

        ar.select(`${col_ref_amount} as '${col_ref_amount}' `);

         
        let col_total_runn_amount = 'dr_setl.total_runn_amount';
        if (alias !== undefined) {

          col_total_runn_amount = `${alias}.total_runn_amount`;

        }

        ar.select(`${col_total_runn_amount} as '${col_total_runn_amount}' `);

         
        let col_vat_cd = 'dr_setl.vat_cd';
        if (alias !== undefined) {

          col_vat_cd = `${alias}.vat_cd`;

        }

        ar.select(`${col_vat_cd} as '${col_vat_cd}' `);

         
        let col_setl_cd = 'dr_setl.setl_cd';
        if (alias !== undefined) {

          col_setl_cd = `${alias}.setl_cd`;

        }

        ar.select(`${col_setl_cd} as '${col_setl_cd}' `);

         
        let col_rt_id = 'dr_setl.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_dr_id = 'dr_setl.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_bus_compn_id = 'dr_setl.bus_compn_id';
        if (alias !== undefined) {

          col_bus_compn_id = `${alias}.bus_compn_id`;

        }

        ar.select(`${col_bus_compn_id} as '${col_bus_compn_id}' `);

         
        let col_dispatch_id = 'dr_setl.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_setl_month = 'dr_setl.setl_month';
        if (alias !== undefined) {

          col_setl_month = `${alias}.setl_month`;

        }

        ar.select(`${col_setl_month} as '${col_setl_month}' `);

         
        let col_start_day = 'dr_setl.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'dr_setl.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_transfer_yn = 'dr_setl.transfer_yn';
        if (alias !== undefined) {

          col_transfer_yn = `${alias}.transfer_yn`;

        }

        ar.select(`${col_transfer_yn} as '${col_transfer_yn}' `);

         
        let col_created_at = 'dr_setl.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_setl.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'dr_setl.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_dr_ref_id = 'dr_setl.dr_ref_id';
        if (alias !== undefined) {

          col_dr_ref_id = `${alias}.dr_ref_id`;

        }

        ar.select(`${col_dr_ref_id} as '${col_dr_ref_id}' `);

         
        let col_comment = 'dr_setl.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_setl');
    
    if (nullCheck(form.drSetlId) === true) {
      ar.set("dr_setl_id", form.drSetlId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("biz_cd", form.bizCd);
    } 
    

    if (nullCheck(form.runnDayCnt) === true) {
      ar.set("runn_day_cnt", form.runnDayCnt);
    } 
    

    if (nullCheck(form.notRunnDayCnt) === true) {
      ar.set("not_runn_day_cnt", form.notRunnDayCnt);
    } 
    

    if (nullCheck(form.exceptDayCnt) === true) {
      ar.set("except_day_cnt", form.exceptDayCnt);
    } 
    

    if (nullCheck(form.defaultAmount) === true) {
      ar.set("default_amount", form.defaultAmount);
    } 
    

    if (nullCheck(form.payAmount) === true) {
      ar.set("pay_amount", form.payAmount);
    } 
    

    if (nullCheck(form.notRunnAmount) === true) {
      ar.set("not_runn_amount", form.notRunnAmount);
    } 
    

    if (nullCheck(form.addAmount) === true) {
      ar.set("add_amount", form.addAmount);
    } 
    

    if (nullCheck(form.issAddAmount) === true) {
      ar.set("iss_add_amount", form.issAddAmount);
    } 
    

    if (nullCheck(form.issRemoveAmount) === true) {
      ar.set("iss_remove_amount", form.issRemoveAmount);
    } 
    

    if (nullCheck(form.supplyAmount) === true) {
      ar.set("supply_amount", form.supplyAmount);
    } 
    

    if (nullCheck(form.vatAmount) === true) {
      ar.set("vat_amount", form.vatAmount);
    } 
    

    if (nullCheck(form.etcAmount) === true) {
      ar.set("etc_amount", form.etcAmount);
    } 
    

    if (nullCheck(form.refAmount) === true) {
      ar.set("ref_amount", form.refAmount);
    } 
    

    if (nullCheck(form.totalRunnAmount) === true) {
      ar.set("total_runn_amount", form.totalRunnAmount);
    } 
    

    if (nullCheck(form.vatCd) === true) {
      ar.set("vat_cd", form.vatCd);
    } 
    

    if (nullCheck(form.setlCd) === true) {
      ar.set("setl_cd", form.setlCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.transferYn) === true) {
      ar.set("transfer_yn", form.transferYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.drRefId) === true) {
      ar.set("dr_ref_id", form.drRefId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_setl');
    
    if (nullCheck(form.drSetlId) === true) {
      ar.set("dr_setl_id", form.drSetlId);
    } 
    

    if (nullCheck(form.drPayId) === true) {
      ar.set("dr_pay_id", form.drPayId);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("biz_cd", form.bizCd);
    } 
    

    if (nullCheck(form.runnDayCnt) === true) {
      ar.set("runn_day_cnt", form.runnDayCnt);
    } 
    

    if (nullCheck(form.notRunnDayCnt) === true) {
      ar.set("not_runn_day_cnt", form.notRunnDayCnt);
    } 
    

    if (nullCheck(form.exceptDayCnt) === true) {
      ar.set("except_day_cnt", form.exceptDayCnt);
    } 
    

    if (nullCheck(form.defaultAmount) === true) {
      ar.set("default_amount", form.defaultAmount);
    } 
    

    if (nullCheck(form.payAmount) === true) {
      ar.set("pay_amount", form.payAmount);
    } 
    

    if (nullCheck(form.notRunnAmount) === true) {
      ar.set("not_runn_amount", form.notRunnAmount);
    } 
    

    if (nullCheck(form.addAmount) === true) {
      ar.set("add_amount", form.addAmount);
    } 
    

    if (nullCheck(form.issAddAmount) === true) {
      ar.set("iss_add_amount", form.issAddAmount);
    } 
    

    if (nullCheck(form.issRemoveAmount) === true) {
      ar.set("iss_remove_amount", form.issRemoveAmount);
    } 
    

    if (nullCheck(form.supplyAmount) === true) {
      ar.set("supply_amount", form.supplyAmount);
    } 
    

    if (nullCheck(form.vatAmount) === true) {
      ar.set("vat_amount", form.vatAmount);
    } 
    

    if (nullCheck(form.etcAmount) === true) {
      ar.set("etc_amount", form.etcAmount);
    } 
    

    if (nullCheck(form.refAmount) === true) {
      ar.set("ref_amount", form.refAmount);
    } 
    

    if (nullCheck(form.totalRunnAmount) === true) {
      ar.set("total_runn_amount", form.totalRunnAmount);
    } 
    

    if (nullCheck(form.vatCd) === true) {
      ar.set("vat_cd", form.vatCd);
    } 
    

    if (nullCheck(form.setlCd) === true) {
      ar.set("setl_cd", form.setlCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.transferYn) === true) {
      ar.set("transfer_yn", form.transferYn);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.drRefId) === true) {
      ar.set("dr_ref_id", form.drRefId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_setl_view dr_setl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drSetlId: 'dr_setl_id'
    , 

    drPayId: 'dr_pay_id'
    , 

    bizCd: 'biz_cd'
    , 

    runnDayCnt: 'runn_day_cnt'
    , 

    notRunnDayCnt: 'not_runn_day_cnt'
    , 

    exceptDayCnt: 'except_day_cnt'
    , 

    defaultAmount: 'default_amount'
    , 

    payAmount: 'pay_amount'
    , 

    notRunnAmount: 'not_runn_amount'
    , 

    addAmount: 'add_amount'
    , 

    issAddAmount: 'iss_add_amount'
    , 

    issRemoveAmount: 'iss_remove_amount'
    , 

    supplyAmount: 'supply_amount'
    , 

    vatAmount: 'vat_amount'
    , 

    etcAmount: 'etc_amount'
    , 

    refAmount: 'ref_amount'
    , 

    totalRunnAmount: 'total_runn_amount'
    , 

    vatCd: 'vat_cd'
    , 

    setlCd: 'setl_cd'
    , 

    rtId: 'rt_id'
    , 

    drId: 'dr_id'
    , 

    busCompnId: 'bus_compn_id'
    , 

    dispatchId: 'dispatch_id'
    , 

    setlMonth: 'setl_month'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    transferYn: 'transfer_yn'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    , 

    drRefId: 'dr_ref_id'
    , 

    comment: 'comment'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_setl');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_setl_view dr_setl');
    
      ar.select("dr_setl.dr_setl_id");

    
    
      ar.select("dr_setl.dr_pay_id");

    
    
      ar.select("dr_setl.biz_cd");

    
    
      ar.select("dr_setl.runn_day_cnt");

    
    
      ar.select("dr_setl.not_runn_day_cnt");

    
    
      ar.select("dr_setl.except_day_cnt");

    
    
      ar.select("dr_setl.default_amount");

    
    
      ar.select("dr_setl.pay_amount");

    
    
      ar.select("dr_setl.not_runn_amount");

    
    
      ar.select("dr_setl.add_amount");

    
    
      ar.select("dr_setl.iss_add_amount");

    
    
      ar.select("dr_setl.iss_remove_amount");

    
    
      ar.select("dr_setl.supply_amount");

    
    
      ar.select("dr_setl.vat_amount");

    
    
      ar.select("dr_setl.etc_amount");

    
    
      ar.select("dr_setl.ref_amount");

    
    
      ar.select("dr_setl.total_runn_amount");

    
    
      ar.select("dr_setl.vat_cd");

    
    
      ar.select("dr_setl.setl_cd");

    
    
      ar.select("dr_setl.rt_id");

    
    
      ar.select("dr_setl.dr_id");

    
    
      ar.select("dr_setl.bus_compn_id");

    
    
      ar.select("dr_setl.dispatch_id");

    
    
      ar.select("dr_setl.setl_month");

    
    
      ar.select("dr_setl.start_day");

    
    
      ar.select("dr_setl.end_day");

    
    
      ar.select("dr_setl.transfer_yn");

    
    
      ar.select("dr_setl.created_at");

    
    
      ar.select("dr_setl.updated_at");

    
    
      ar.select("dr_setl.admin_id");

    
    
      ar.select("dr_setl.dr_ref_id");

    
    
      ar.select("dr_setl.comment");

    
    
    return ar;
  }

  

  
}
export const DrSetlSql =  new DrSetlQuery()

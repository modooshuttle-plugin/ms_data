import { Ar, nullCheck } from "../../../util";
 
  import { IMsgView } from "../interface";


  class MsgViewQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 msg_view객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' msg_view_view msg_view');

    
      if (alias === true) {
        ar.select("msg_view.msg_view_id as 'msg_view.msg_view_id'" );

      } else{
        ar.select("msg_view.msg_view_id");

      }
      
      if (alias === true) {
        ar.select("msg_view.rt_cd as 'msg_view.rt_cd'" );

      } else{
        ar.select("msg_view.rt_cd");

      }
      
      if (alias === true) {
        ar.select("msg_view.title as 'msg_view.title'" );

      } else{
        ar.select("msg_view.title");

      }
      
      if (alias === true) {
        ar.select("msg_view.tpl_list as 'msg_view.tpl_list'" );

      } else{
        ar.select("msg_view.tpl_list");

      }
      
      if (alias === true) {
        ar.select("msg_view.created_at as 'msg_view.created_at'" );

      } else{
        ar.select("msg_view.created_at");

      }
      
      if (alias === true) {
        ar.select("msg_view.updated_at as 'msg_view.updated_at'" );

      } else{
        ar.select("msg_view.updated_at");

      }
      
      if (alias === true) {
        ar.select("msg_view.idx as 'msg_view.idx'" );

      } else{
        ar.select("msg_view.idx");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_msg_view_id = 'msg_view.msg_view_id';
        if (alias !== undefined) {

          col_msg_view_id = `${alias}.msg_view_id`;

        }

        ar.select(`${col_msg_view_id} as '${col_msg_view_id}' `);

         
        let col_rt_cd = 'msg_view.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_title = 'msg_view.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_tpl_list = 'msg_view.tpl_list';
        if (alias !== undefined) {

          col_tpl_list = `${alias}.tpl_list`;

        }

        ar.select(`${col_tpl_list} as '${col_tpl_list}' `);

         
        let col_created_at = 'msg_view.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'msg_view.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_idx = 'msg_view.idx';
        if (alias !== undefined) {

          col_idx = `${alias}.idx`;

        }

        ar.select(`${col_idx} as '${col_idx}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_step');
    
    if (nullCheck(form.msgViewId) === true) {
      ar.set("a_no", form.msgViewId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_r_type", form.rtCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("a_title", form.title);
    } 
    

    if (nullCheck(form.tplList) === true) {
      ar.set("a_items", form.tplList);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("a_idx_v", form.idx);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' a_step');
    
    if (nullCheck(form.msgViewId) === true) {
      ar.set("a_no", form.msgViewId);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("a_r_type", form.rtCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("a_title", form.title);
    } 
    

    if (nullCheck(form.tplList) === true) {
      ar.set("a_items", form.tplList);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("a_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("a_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.idx) === true) {
      ar.set("a_idx_v", form.idx);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_view_view msg_view');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    msgViewId: 'a_no'
    , 

    rtCd: 'a_r_type'
    , 

    title: 'a_title'
    , 

    tplList: 'a_items'
    , 

    createdAt: 'a_timestamp'
    , 

    updatedAt: 'a_timestamp_u'
    , 

    idx: 'a_idx_v'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('a_step');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' msg_view_view msg_view');
    
      ar.select("msg_view.msg_view_id");

    
    
      ar.select("msg_view.rt_cd");

    
    
      ar.select("msg_view.title");

    
    
      ar.select("msg_view.tpl_list");

    
    
      ar.select("msg_view.created_at");

    
    
      ar.select("msg_view.updated_at");

    
    
      ar.select("msg_view.idx");

    
    
    return ar;
  }

  

  
}
export const MsgViewSql =  new MsgViewQuery()

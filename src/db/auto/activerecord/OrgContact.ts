import { Ar, nullCheck } from "../../../util";
 
  import { IOrgContact } from "../interface";


  class OrgContactQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_contact객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_contact_view org_contact');

    
      if (alias === true) {
        ar.select("org_contact.org_contact_id as 'org_contact.org_contact_id'" );

      } else{
        ar.select("org_contact.org_contact_id");

      }
      
      if (alias === true) {
        ar.select("org_contact.org_id as 'org_contact.org_id'" );

      } else{
        ar.select("org_contact.org_id");

      }
      
      if (alias === true) {
        ar.select("org_contact.org_ctrt_id as 'org_contact.org_ctrt_id'" );

      } else{
        ar.select("org_contact.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_contact.contact_nm as 'org_contact.contact_nm'" );

      } else{
        ar.select("org_contact.contact_nm");

      }
      
      if (alias === true) {
        ar.select("org_contact.contact_manager as 'org_contact.contact_manager'" );

      } else{
        ar.select("org_contact.contact_manager");

      }
      
      if (alias === true) {
        ar.select("org_contact.contact_phone as 'org_contact.contact_phone'" );

      } else{
        ar.select("org_contact.contact_phone");

      }
      
      if (alias === true) {
        ar.select("org_contact.contact_email as 'org_contact.contact_email'" );

      } else{
        ar.select("org_contact.contact_email");

      }
      
      if (alias === true) {
        ar.select("org_contact.start_day as 'org_contact.start_day'" );

      } else{
        ar.select("org_contact.start_day");

      }
      
      if (alias === true) {
        ar.select("org_contact.created_at as 'org_contact.created_at'" );

      } else{
        ar.select("org_contact.created_at");

      }
      
      if (alias === true) {
        ar.select("org_contact.updated_at as 'org_contact.updated_at'" );

      } else{
        ar.select("org_contact.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_contact.on_cd as 'org_contact.on_cd'" );

      } else{
        ar.select("org_contact.on_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_contact_id = 'org_contact.org_contact_id';
        if (alias !== undefined) {

          col_org_contact_id = `${alias}.org_contact_id`;

        }

        ar.select(`${col_org_contact_id} as '${col_org_contact_id}' `);

         
        let col_org_id = 'org_contact.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_org_ctrt_id = 'org_contact.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_contact_nm = 'org_contact.contact_nm';
        if (alias !== undefined) {

          col_contact_nm = `${alias}.contact_nm`;

        }

        ar.select(`${col_contact_nm} as '${col_contact_nm}' `);

         
        let col_contact_manager = 'org_contact.contact_manager';
        if (alias !== undefined) {

          col_contact_manager = `${alias}.contact_manager`;

        }

        ar.select(`${col_contact_manager} as '${col_contact_manager}' `);

         
        let col_contact_phone = 'org_contact.contact_phone';
        if (alias !== undefined) {

          col_contact_phone = `${alias}.contact_phone`;

        }

        ar.select(`${col_contact_phone} as '${col_contact_phone}' `);

         
        let col_contact_email = 'org_contact.contact_email';
        if (alias !== undefined) {

          col_contact_email = `${alias}.contact_email`;

        }

        ar.select(`${col_contact_email} as '${col_contact_email}' `);

         
        let col_start_day = 'org_contact.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_created_at = 'org_contact.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_contact.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_on_cd = 'org_contact.on_cd';
        if (alias !== undefined) {

          col_on_cd = `${alias}.on_cd`;

        }

        ar.select(`${col_on_cd} as '${col_on_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_contact');
    
    if (nullCheck(form.orgContactId) === true) {
      ar.set("org_contact_id", form.orgContactId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.contactNm) === true) {
      ar.set("contact_nm", form.contactNm);
    } 
    

    if (nullCheck(form.contactManager) === true) {
      ar.set("contact_manager", form.contactManager);
    } 
    

    if (nullCheck(form.contactPhone) === true) {
      ar.set("contact_phone", form.contactPhone);
    } 
    

    if (nullCheck(form.contactEmail) === true) {
      ar.set("contact_email", form.contactEmail);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.onCd) === true) {
      ar.set("on_cd", form.onCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_contact');
    
    if (nullCheck(form.orgContactId) === true) {
      ar.set("org_contact_id", form.orgContactId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.contactNm) === true) {
      ar.set("contact_nm", form.contactNm);
    } 
    

    if (nullCheck(form.contactManager) === true) {
      ar.set("contact_manager", form.contactManager);
    } 
    

    if (nullCheck(form.contactPhone) === true) {
      ar.set("contact_phone", form.contactPhone);
    } 
    

    if (nullCheck(form.contactEmail) === true) {
      ar.set("contact_email", form.contactEmail);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.onCd) === true) {
      ar.set("on_cd", form.onCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_contact_view org_contact');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgContactId: 'org_contact_id'
    , 

    orgId: 'org_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    contactNm: 'contact_nm'
    , 

    contactManager: 'contact_manager'
    , 

    contactPhone: 'contact_phone'
    , 

    contactEmail: 'contact_email'
    , 

    startDay: 'start_day'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    onCd: 'on_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_contact');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_contact_view org_contact');
    
      ar.select("org_contact.org_contact_id");

    
    
      ar.select("org_contact.org_id");

    
    
      ar.select("org_contact.org_ctrt_id");

    
    
      ar.select("org_contact.contact_nm");

    
    
      ar.select("org_contact.contact_manager");

    
    
      ar.select("org_contact.contact_phone");

    
    
      ar.select("org_contact.contact_email");

    
    
      ar.select("org_contact.start_day");

    
    
      ar.select("org_contact.created_at");

    
    
      ar.select("org_contact.updated_at");

    
    
      ar.select("org_contact.on_cd");

    
    
    return ar;
  }

  

  
}
export const OrgContactSql =  new OrgContactQuery()

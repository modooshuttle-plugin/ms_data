import { Ar, nullCheck } from "../../../util";
 
  import { IDrRef } from "../interface";


  class DrRefQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_ref객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_ref_view dr_ref');

    
      if (alias === true) {
        ar.select("dr_ref.dr_ref_id as 'dr_ref.dr_ref_id'" );

      } else{
        ar.select("dr_ref.dr_ref_id");

      }
      
      if (alias === true) {
        ar.select("dr_ref.comment as 'dr_ref.comment'" );

      } else{
        ar.select("dr_ref.comment");

      }
      
      if (alias === true) {
        ar.select("dr_ref.amount as 'dr_ref.amount'" );

      } else{
        ar.select("dr_ref.amount");

      }
      
      if (alias === true) {
        ar.select("dr_ref.setl_month as 'dr_ref.setl_month'" );

      } else{
        ar.select("dr_ref.setl_month");

      }
      
      if (alias === true) {
        ar.select("dr_ref.dr_id as 'dr_ref.dr_id'" );

      } else{
        ar.select("dr_ref.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_ref.ref_dr_id as 'dr_ref.ref_dr_id'" );

      } else{
        ar.select("dr_ref.ref_dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_ref.rt_id as 'dr_ref.rt_id'" );

      } else{
        ar.select("dr_ref.rt_id");

      }
      
      if (alias === true) {
        ar.select("dr_ref.created_at as 'dr_ref.created_at'" );

      } else{
        ar.select("dr_ref.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_ref.updated_at as 'dr_ref.updated_at'" );

      } else{
        ar.select("dr_ref.updated_at");

      }
      
      if (alias === true) {
        ar.select("dr_ref.admin_id as 'dr_ref.admin_id'" );

      } else{
        ar.select("dr_ref.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_ref_id = 'dr_ref.dr_ref_id';
        if (alias !== undefined) {

          col_dr_ref_id = `${alias}.dr_ref_id`;

        }

        ar.select(`${col_dr_ref_id} as '${col_dr_ref_id}' `);

         
        let col_comment = 'dr_ref.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_amount = 'dr_ref.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_setl_month = 'dr_ref.setl_month';
        if (alias !== undefined) {

          col_setl_month = `${alias}.setl_month`;

        }

        ar.select(`${col_setl_month} as '${col_setl_month}' `);

         
        let col_dr_id = 'dr_ref.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_ref_dr_id = 'dr_ref.ref_dr_id';
        if (alias !== undefined) {

          col_ref_dr_id = `${alias}.ref_dr_id`;

        }

        ar.select(`${col_ref_dr_id} as '${col_ref_dr_id}' `);

         
        let col_rt_id = 'dr_ref.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_created_at = 'dr_ref.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_ref.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'dr_ref.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_ref');
    
    if (nullCheck(form.drRefId) === true) {
      ar.set("dr_ref_id", form.drRefId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.refDrId) === true) {
      ar.set("ref_dr_id", form.refDrId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_ref');
    
    if (nullCheck(form.drRefId) === true) {
      ar.set("dr_ref_id", form.drRefId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.refDrId) === true) {
      ar.set("ref_dr_id", form.refDrId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_ref_view dr_ref');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drRefId: 'dr_ref_id'
    , 

    comment: 'comment'
    , 

    amount: 'amount'
    , 

    setlMonth: 'setl_month'
    , 

    drId: 'dr_id'
    , 

    refDrId: 'ref_dr_id'
    , 

    rtId: 'rt_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_ref');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_ref_view dr_ref');
    
      ar.select("dr_ref.dr_ref_id");

    
    
      ar.select("dr_ref.comment");

    
    
      ar.select("dr_ref.amount");

    
    
      ar.select("dr_ref.setl_month");

    
    
      ar.select("dr_ref.dr_id");

    
    
      ar.select("dr_ref.ref_dr_id");

    
    
      ar.select("dr_ref.rt_id");

    
    
      ar.select("dr_ref.created_at");

    
    
      ar.select("dr_ref.updated_at");

    
    
      ar.select("dr_ref.admin_id");

    
    
    return ar;
  }

  

  
}
export const DrRefSql =  new DrRefQuery()

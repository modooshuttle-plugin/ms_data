import { Ar, nullCheck } from "../../../util";
 
  import { IOrgAccount } from "../interface";


  class OrgAccountQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_account객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_account_view org_account');

    
      if (alias === true) {
        ar.select("org_account.org_account_id as 'org_account.org_account_id'" );

      } else{
        ar.select("org_account.org_account_id");

      }
      
      if (alias === true) {
        ar.select("org_account.org_id as 'org_account.org_id'" );

      } else{
        ar.select("org_account.org_id");

      }
      
      if (alias === true) {
        ar.select("org_account.corp as 'org_account.corp'" );

      } else{
        ar.select("org_account.corp");

      }
      
      if (alias === true) {
        ar.select("org_account.id as 'org_account.id'" );

      } else{
        ar.select("org_account.id");

      }
      
      if (alias === true) {
        ar.select("org_account.pw as 'org_account.pw'" );

      } else{
        ar.select("org_account.pw");

      }
      
      if (alias === true) {
        ar.select("org_account.ko as 'org_account.ko'" );

      } else{
        ar.select("org_account.ko");

      }
      
      if (alias === true) {
        ar.select("org_account.created_at as 'org_account.created_at'" );

      } else{
        ar.select("org_account.created_at");

      }
      
      if (alias === true) {
        ar.select("org_account.updated_at as 'org_account.updated_at'" );

      } else{
        ar.select("org_account.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_account_id = 'org_account.org_account_id';
        if (alias !== undefined) {

          col_org_account_id = `${alias}.org_account_id`;

        }

        ar.select(`${col_org_account_id} as '${col_org_account_id}' `);

         
        let col_org_id = 'org_account.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_corp = 'org_account.corp';
        if (alias !== undefined) {

          col_corp = `${alias}.corp`;

        }

        ar.select(`${col_corp} as '${col_corp}' `);

         
        let col_id = 'org_account.id';
        if (alias !== undefined) {

          col_id = `${alias}.id`;

        }

        ar.select(`${col_id} as '${col_id}' `);

         
        let col_pw = 'org_account.pw';
        if (alias !== undefined) {

          col_pw = `${alias}.pw`;

        }

        ar.select(`${col_pw} as '${col_pw}' `);

         
        let col_ko = 'org_account.ko';
        if (alias !== undefined) {

          col_ko = `${alias}.ko`;

        }

        ar.select(`${col_ko} as '${col_ko}' `);

         
        let col_created_at = 'org_account.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_account.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_account');
    
    if (nullCheck(form.orgAccountId) === true) {
      ar.set("org_account_id", form.orgAccountId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.corp) === true) {
      ar.set("corp", form.corp);
    } 
    

    if (nullCheck(form.id) === true) {
      ar.set("id", form.id);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("pw", form.pw);
    } 
    

    if (nullCheck(form.ko) === true) {
      ar.set("ko", form.ko);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_account');
    
    if (nullCheck(form.orgAccountId) === true) {
      ar.set("org_account_id", form.orgAccountId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.corp) === true) {
      ar.set("corp", form.corp);
    } 
    

    if (nullCheck(form.id) === true) {
      ar.set("id", form.id);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("pw", form.pw);
    } 
    

    if (nullCheck(form.ko) === true) {
      ar.set("ko", form.ko);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_account_view org_account');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgAccountId: 'org_account_id'
    , 

    orgId: 'org_id'
    , 

    corp: 'corp'
    , 

    id: 'id'
    , 

    pw: 'pw'
    , 

    ko: 'ko'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_account');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_account_view org_account');
    
      ar.select("org_account.org_account_id");

    
    
      ar.select("org_account.org_id");

    
    
      ar.select("org_account.corp");

    
    
      ar.select("org_account.id");

    
    
      ar.select("org_account.pw");

    
    
      ar.select("org_account.ko");

    
    
      ar.select("org_account.created_at");

    
    
      ar.select("org_account.updated_at");

    
    
    return ar;
  }

  

  
}
export const OrgAccountSql =  new OrgAccountQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IHold } from "../interface";


  class HoldQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 hold객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' hold_view hold');

    
      if (alias === true) {
        ar.select("hold.hold_id as 'hold.hold_id'" );

      } else{
        ar.select("hold.hold_id");

      }
      
      if (alias === true) {
        ar.select("hold.subscribe_id as 'hold.subscribe_id'" );

      } else{
        ar.select("hold.subscribe_id");

      }
      
      if (alias === true) {
        ar.select("hold.cpn_id as 'hold.cpn_id'" );

      } else{
        ar.select("hold.cpn_id");

      }
      
      if (alias === true) {
        ar.select("hold.apply_id as 'hold.apply_id'" );

      } else{
        ar.select("hold.apply_id");

      }
      
      if (alias === true) {
        ar.select("hold.board_id as 'hold.board_id'" );

      } else{
        ar.select("hold.board_id");

      }
      
      if (alias === true) {
        ar.select("hold.user_id as 'hold.user_id'" );

      } else{
        ar.select("hold.user_id");

      }
      
      if (alias === true) {
        ar.select("hold.hold_cd as 'hold.hold_cd'" );

      } else{
        ar.select("hold.hold_cd");

      }
      
      if (alias === true) {
        ar.select("hold.cpn_pblsh_cd as 'hold.cpn_pblsh_cd'" );

      } else{
        ar.select("hold.cpn_pblsh_cd");

      }
      
      if (alias === true) {
        ar.select("hold.status_cd as 'hold.status_cd'" );

      } else{
        ar.select("hold.status_cd");

      }
      
      if (alias === true) {
        ar.select("hold.created_at as 'hold.created_at'" );

      } else{
        ar.select("hold.created_at");

      }
      
      if (alias === true) {
        ar.select("hold.updated_at as 'hold.updated_at'" );

      } else{
        ar.select("hold.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_hold_id = 'hold.hold_id';
        if (alias !== undefined) {

          col_hold_id = `${alias}.hold_id`;

        }

        ar.select(`${col_hold_id} as '${col_hold_id}' `);

         
        let col_subscribe_id = 'hold.subscribe_id';
        if (alias !== undefined) {

          col_subscribe_id = `${alias}.subscribe_id`;

        }

        ar.select(`${col_subscribe_id} as '${col_subscribe_id}' `);

         
        let col_cpn_id = 'hold.cpn_id';
        if (alias !== undefined) {

          col_cpn_id = `${alias}.cpn_id`;

        }

        ar.select(`${col_cpn_id} as '${col_cpn_id}' `);

         
        let col_apply_id = 'hold.apply_id';
        if (alias !== undefined) {

          col_apply_id = `${alias}.apply_id`;

        }

        ar.select(`${col_apply_id} as '${col_apply_id}' `);

         
        let col_board_id = 'hold.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_user_id = 'hold.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_hold_cd = 'hold.hold_cd';
        if (alias !== undefined) {

          col_hold_cd = `${alias}.hold_cd`;

        }

        ar.select(`${col_hold_cd} as '${col_hold_cd}' `);

         
        let col_cpn_pblsh_cd = 'hold.cpn_pblsh_cd';
        if (alias !== undefined) {

          col_cpn_pblsh_cd = `${alias}.cpn_pblsh_cd`;

        }

        ar.select(`${col_cpn_pblsh_cd} as '${col_cpn_pblsh_cd}' `);

         
        let col_status_cd = 'hold.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_created_at = 'hold.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'hold.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_hold');
    
    if (nullCheck(form.holdId) === true) {
      ar.set("o_no", form.holdId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("o_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("o_aid", form.applyId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.holdCd) === true) {
      ar.set("hold_cd", form.holdCd);
    } 
    

    if (nullCheck(form.cpnPblshCd) === true) {
      ar.set("o_type", form.cpnPblshCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("o_status", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_hold');
    
    if (nullCheck(form.holdId) === true) {
      ar.set("o_no", form.holdId);
    } 
    

    if (nullCheck(form.subscribeId) === true) {
      ar.set("o_sid", form.subscribeId);
    } 
    

    if (nullCheck(form.cpnId) === true) {
      ar.set("o_cid", form.cpnId);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("o_aid", form.applyId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.holdCd) === true) {
      ar.set("hold_cd", form.holdCd);
    } 
    

    if (nullCheck(form.cpnPblshCd) === true) {
      ar.set("o_type", form.cpnPblshCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("o_status", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' hold_view hold');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    holdId: 'o_no'
    , 

    subscribeId: 'o_sid'
    , 

    cpnId: 'o_cid'
    , 

    applyId: 'o_aid'
    , 

    boardId: 'o_oid'
    , 

    userId: 'o_mid'
    , 

    holdCd: 'hold_cd'
    , 

    cpnPblshCd: 'o_type'
    , 

    statusCd: 'o_status'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_hold');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' hold_view hold');
    
      ar.select("hold.hold_id");

    
    
      ar.select("hold.subscribe_id");

    
    
      ar.select("hold.cpn_id");

    
    
      ar.select("hold.apply_id");

    
    
      ar.select("hold.board_id");

    
    
      ar.select("hold.user_id");

    
    
      ar.select("hold.hold_cd");

    
    
      ar.select("hold.cpn_pblsh_cd");

    
    
      ar.select("hold.status_cd");

    
    
      ar.select("hold.created_at");

    
    
      ar.select("hold.updated_at");

    
    
    return ar;
  }

  

  
}
export const HoldSql =  new HoldQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IDrDeploy } from "../interface";


  class DrDeployQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_deploy객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_deploy_view dr_deploy');

    
      if (alias === true) {
        ar.select("dr_deploy.dr_deploy_id as 'dr_deploy.dr_deploy_id'" );

      } else{
        ar.select("dr_deploy.dr_deploy_id");

      }
      
      if (alias === true) {
        ar.select("dr_deploy.dr_id as 'dr_deploy.dr_id'" );

      } else{
        ar.select("dr_deploy.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_deploy.deploy_cd as 'dr_deploy.deploy_cd'" );

      } else{
        ar.select("dr_deploy.deploy_cd");

      }
      
      if (alias === true) {
        ar.select("dr_deploy.status_cd as 'dr_deploy.status_cd'" );

      } else{
        ar.select("dr_deploy.status_cd");

      }
      
      if (alias === true) {
        ar.select("dr_deploy.created_at as 'dr_deploy.created_at'" );

      } else{
        ar.select("dr_deploy.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_deploy.updated_at as 'dr_deploy.updated_at'" );

      } else{
        ar.select("dr_deploy.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_deploy_id = 'dr_deploy.dr_deploy_id';
        if (alias !== undefined) {

          col_dr_deploy_id = `${alias}.dr_deploy_id`;

        }

        ar.select(`${col_dr_deploy_id} as '${col_dr_deploy_id}' `);

         
        let col_dr_id = 'dr_deploy.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_deploy_cd = 'dr_deploy.deploy_cd';
        if (alias !== undefined) {

          col_deploy_cd = `${alias}.deploy_cd`;

        }

        ar.select(`${col_deploy_cd} as '${col_deploy_cd}' `);

         
        let col_status_cd = 'dr_deploy.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_created_at = 'dr_deploy.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_deploy.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_deploy');
    
    if (nullCheck(form.drDeployId) === true) {
      ar.set("d_no", form.drDeployId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("d_tid", form.deployCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("d_status", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_deploy');
    
    if (nullCheck(form.drDeployId) === true) {
      ar.set("d_no", form.drDeployId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.deployCd) === true) {
      ar.set("d_tid", form.deployCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("d_status", form.statusCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_deploy_view dr_deploy');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drDeployId: 'd_no'
    , 

    drId: 'd_did'
    , 

    deployCd: 'd_tid'
    , 

    statusCd: 'd_status'
    , 

    createdAt: 'd_timestamp'
    , 

    updatedAt: 'd_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_deploy');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_deploy_view dr_deploy');
    
      ar.select("dr_deploy.dr_deploy_id");

    
    
      ar.select("dr_deploy.dr_id");

    
    
      ar.select("dr_deploy.deploy_cd");

    
    
      ar.select("dr_deploy.status_cd");

    
    
      ar.select("dr_deploy.created_at");

    
    
      ar.select("dr_deploy.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrDeploySql =  new DrDeployQuery()

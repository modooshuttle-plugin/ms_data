import { Ar, nullCheck } from "../../../util";
 
  import { IPath } from "../interface";


  class PathQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 path객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' path_view path');

    
      if (alias === true) {
        ar.select("path.path_id as 'path.path_id'" );

      } else{
        ar.select("path.path_id");

      }
      
      if (alias === true) {
        ar.select("path.rt_id as 'path.rt_id'" );

      } else{
        ar.select("path.rt_id");

      }
      
      if (alias === true) {
        ar.select("path.start_day as 'path.start_day'" );

      } else{
        ar.select("path.start_day");

      }
      
      if (alias === true) {
        ar.select("path.path_ver as 'path.path_ver'" );

      } else{
        ar.select("path.path_ver");

      }
      
      if (alias === true) {
        ar.select("path.start_cd as 'path.start_cd'" );

      } else{
        ar.select("path.start_cd");

      }
      
      if (alias === true) {
        ar.select("path.end_cd as 'path.end_cd'" );

      } else{
        ar.select("path.end_cd");

      }
      
      if (alias === true) {
        ar.select("path.created_at as 'path.created_at'" );

      } else{
        ar.select("path.created_at");

      }
      
      if (alias === true) {
        ar.select("path.updated_at as 'path.updated_at'" );

      } else{
        ar.select("path.updated_at");

      }
      
      if (alias === true) {
        ar.select("path.dist as 'path.dist'" );

      } else{
        ar.select("path.dist");

      }
      
      if (alias === true) {
        ar.select("path.googlePl as 'path.googlePl'" );

      } else{
        ar.select("path.googlePl");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_path_id = 'path.path_id';
        if (alias !== undefined) {

          col_path_id = `${alias}.path_id`;

        }

        ar.select(`${col_path_id} as '${col_path_id}' `);

         
        let col_rt_id = 'path.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_start_day = 'path.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_path_ver = 'path.path_ver';
        if (alias !== undefined) {

          col_path_ver = `${alias}.path_ver`;

        }

        ar.select(`${col_path_ver} as '${col_path_ver}' `);

         
        let col_start_cd = 'path.start_cd';
        if (alias !== undefined) {

          col_start_cd = `${alias}.start_cd`;

        }

        ar.select(`${col_start_cd} as '${col_start_cd}' `);

         
        let col_end_cd = 'path.end_cd';
        if (alias !== undefined) {

          col_end_cd = `${alias}.end_cd`;

        }

        ar.select(`${col_end_cd} as '${col_end_cd}' `);

         
        let col_created_at = 'path.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'path.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_dist = 'path.dist';
        if (alias !== undefined) {

          col_dist = `${alias}.dist`;

        }

        ar.select(`${col_dist} as '${col_dist}' `);

         
        let col_googlePl = 'path.googlePl';
        if (alias !== undefined) {

          col_googlePl = `${alias}.googlePl`;

        }

        ar.select(`${col_googlePl} as '${col_googlePl}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_way');
    
    if (nullCheck(form.pathId) === true) {
      ar.set("r_no", form.pathId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("r_line_ver", form.pathVer);
    } 
    

    if (nullCheck(form.startCd) === true) {
      ar.set("r_start_type", form.startCd);
    } 
    

    if (nullCheck(form.endCd) === true) {
      ar.set("r_end_type", form.endCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.dist) === true) {
      ar.set("r_distance", form.dist);
    } 
    

    if (nullCheck(form.googlePl) === true) {
      ar.set("r_gp", form.googlePl);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' r_way');
    
    if (nullCheck(form.pathId) === true) {
      ar.set("r_no", form.pathId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("r_rid", form.rtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("r_start_day", form.startDay);
    } 
    

    if (nullCheck(form.pathVer) === true) {
      ar.set("r_line_ver", form.pathVer);
    } 
    

    if (nullCheck(form.startCd) === true) {
      ar.set("r_start_type", form.startCd);
    } 
    

    if (nullCheck(form.endCd) === true) {
      ar.set("r_end_type", form.endCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("r_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("r_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.dist) === true) {
      ar.set("r_distance", form.dist);
    } 
    

    if (nullCheck(form.googlePl) === true) {
      ar.set("r_gp", form.googlePl);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' path_view path');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    pathId: 'r_no'
    , 

    rtId: 'r_rid'
    , 

    startDay: 'r_start_day'
    , 

    pathVer: 'r_line_ver'
    , 

    startCd: 'r_start_type'
    , 

    endCd: 'r_end_type'
    , 

    createdAt: 'r_timestamp'
    , 

    updatedAt: 'r_timestamp_u'
    , 

    dist: 'r_distance'
    , 

    googlePl: 'r_gp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('r_way');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' path_view path');
    
      ar.select("path.path_id");

    
    
      ar.select("path.rt_id");

    
    
      ar.select("path.start_day");

    
    
      ar.select("path.path_ver");

    
    
      ar.select("path.start_cd");

    
    
      ar.select("path.end_cd");

    
    
      ar.select("path.created_at");

    
    
      ar.select("path.updated_at");

    
    
      ar.select("path.dist");

    
    
      ar.select("path.googlePl");

    
    
    return ar;
  }

  

  
}
export const PathSql =  new PathQuery()

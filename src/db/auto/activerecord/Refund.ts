import { Ar, nullCheck } from "../../../util";
 
  import { IRefund } from "../interface";


  class RefundQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 refund객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' refund_view refund');

    
      if (alias === true) {
        ar.select("refund.refund_id as 'refund.refund_id'" );

      } else{
        ar.select("refund.refund_id");

      }
      
      if (alias === true) {
        ar.select("refund.paid_id as 'refund.paid_id'" );

      } else{
        ar.select("refund.paid_id");

      }
      
      if (alias === true) {
        ar.select("refund.pay_id as 'refund.pay_id'" );

      } else{
        ar.select("refund.pay_id");

      }
      
      if (alias === true) {
        ar.select("refund.order_no as 'refund.order_no'" );

      } else{
        ar.select("refund.order_no");

      }
      
      if (alias === true) {
        ar.select("refund.board_id as 'refund.board_id'" );

      } else{
        ar.select("refund.board_id");

      }
      
      if (alias === true) {
        ar.select("refund.rt_id as 'refund.rt_id'" );

      } else{
        ar.select("refund.rt_id");

      }
      
      if (alias === true) {
        ar.select("refund.user_id as 'refund.user_id'" );

      } else{
        ar.select("refund.user_id");

      }
      
      if (alias === true) {
        ar.select("refund.compn_cd as 'refund.compn_cd'" );

      } else{
        ar.select("refund.compn_cd");

      }
      
      if (alias === true) {
        ar.select("refund.amount as 'refund.amount'" );

      } else{
        ar.select("refund.amount");

      }
      
      if (alias === true) {
        ar.select("refund.comment as 'refund.comment'" );

      } else{
        ar.select("refund.comment");

      }
      
      if (alias === true) {
        ar.select("refund.created_at as 'refund.created_at'" );

      } else{
        ar.select("refund.created_at");

      }
      
      if (alias === true) {
        ar.select("refund.admin_id as 'refund.admin_id'" );

      } else{
        ar.select("refund.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_refund_id = 'refund.refund_id';
        if (alias !== undefined) {

          col_refund_id = `${alias}.refund_id`;

        }

        ar.select(`${col_refund_id} as '${col_refund_id}' `);

         
        let col_paid_id = 'refund.paid_id';
        if (alias !== undefined) {

          col_paid_id = `${alias}.paid_id`;

        }

        ar.select(`${col_paid_id} as '${col_paid_id}' `);

         
        let col_pay_id = 'refund.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_order_no = 'refund.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_board_id = 'refund.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'refund.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'refund.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_compn_cd = 'refund.compn_cd';
        if (alias !== undefined) {

          col_compn_cd = `${alias}.compn_cd`;

        }

        ar.select(`${col_compn_cd} as '${col_compn_cd}' `);

         
        let col_amount = 'refund.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_comment = 'refund.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'refund.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_admin_id = 'refund.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' refund');
    
    if (nullCheck(form.refundId) === true) {
      ar.set("refund_id", form.refundId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.compnCd) === true) {
      ar.set("compn_cd", form.compnCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' refund');
    
    if (nullCheck(form.refundId) === true) {
      ar.set("refund_id", form.refundId);
    } 
    

    if (nullCheck(form.paidId) === true) {
      ar.set("paid_id", form.paidId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("pay_id", form.payId);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("order_no", form.orderNo);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.compnCd) === true) {
      ar.set("compn_cd", form.compnCd);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' refund_view refund');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    refundId: 'refund_id'
    , 

    paidId: 'paid_id'
    , 

    payId: 'pay_id'
    , 

    orderNo: 'order_no'
    , 

    boardId: 'board_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    compnCd: 'compn_cd'
    , 

    amount: 'amount'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('refund');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' refund_view refund');
    
      ar.select("refund.refund_id");

    
    
      ar.select("refund.paid_id");

    
    
      ar.select("refund.pay_id");

    
    
      ar.select("refund.order_no");

    
    
      ar.select("refund.board_id");

    
    
      ar.select("refund.rt_id");

    
    
      ar.select("refund.user_id");

    
    
      ar.select("refund.compn_cd");

    
    
      ar.select("refund.amount");

    
    
      ar.select("refund.comment");

    
    
      ar.select("refund.created_at");

    
    
      ar.select("refund.admin_id");

    
    
    return ar;
  }

  

  
}
export const RefundSql =  new RefundQuery()

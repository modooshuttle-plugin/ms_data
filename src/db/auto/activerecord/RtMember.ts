import { Ar, nullCheck } from "../../../util";
 
  import { IRtMember } from "../interface";


  class RtMemberQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt_member객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_member_view rt_member');

    
      if (alias === true) {
        ar.select("rt_member.rt_member_id as 'rt_member.rt_member_id'" );

      } else{
        ar.select("rt_member.rt_member_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.rt_task_id as 'rt_member.rt_task_id'" );

      } else{
        ar.select("rt_member.rt_task_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.rt_task_req_id as 'rt_member.rt_task_req_id'" );

      } else{
        ar.select("rt_member.rt_task_req_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.pre_board_id as 'rt_member.pre_board_id'" );

      } else{
        ar.select("rt_member.pre_board_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.pre_rt_id as 'rt_member.pre_rt_id'" );

      } else{
        ar.select("rt_member.pre_rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.board_id as 'rt_member.board_id'" );

      } else{
        ar.select("rt_member.board_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.rt_id as 'rt_member.rt_id'" );

      } else{
        ar.select("rt_member.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.user_id as 'rt_member.user_id'" );

      } else{
        ar.select("rt_member.user_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.pre_rt_cd as 'rt_member.pre_rt_cd'" );

      } else{
        ar.select("rt_member.pre_rt_cd");

      }
      
      if (alias === true) {
        ar.select("rt_member.admin_id as 'rt_member.admin_id'" );

      } else{
        ar.select("rt_member.admin_id");

      }
      
      if (alias === true) {
        ar.select("rt_member.created_at as 'rt_member.created_at'" );

      } else{
        ar.select("rt_member.created_at");

      }
      
      if (alias === true) {
        ar.select("rt_member.updated_at as 'rt_member.updated_at'" );

      } else{
        ar.select("rt_member.updated_at");

      }
      
      if (alias === true) {
        ar.select("rt_member.task_id as 'rt_member.task_id'" );

      } else{
        ar.select("rt_member.task_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_member_id = 'rt_member.rt_member_id';
        if (alias !== undefined) {

          col_rt_member_id = `${alias}.rt_member_id`;

        }

        ar.select(`${col_rt_member_id} as '${col_rt_member_id}' `);

         
        let col_rt_task_id = 'rt_member.rt_task_id';
        if (alias !== undefined) {

          col_rt_task_id = `${alias}.rt_task_id`;

        }

        ar.select(`${col_rt_task_id} as '${col_rt_task_id}' `);

         
        let col_rt_task_req_id = 'rt_member.rt_task_req_id';
        if (alias !== undefined) {

          col_rt_task_req_id = `${alias}.rt_task_req_id`;

        }

        ar.select(`${col_rt_task_req_id} as '${col_rt_task_req_id}' `);

         
        let col_pre_board_id = 'rt_member.pre_board_id';
        if (alias !== undefined) {

          col_pre_board_id = `${alias}.pre_board_id`;

        }

        ar.select(`${col_pre_board_id} as '${col_pre_board_id}' `);

         
        let col_pre_rt_id = 'rt_member.pre_rt_id';
        if (alias !== undefined) {

          col_pre_rt_id = `${alias}.pre_rt_id`;

        }

        ar.select(`${col_pre_rt_id} as '${col_pre_rt_id}' `);

         
        let col_board_id = 'rt_member.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'rt_member.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_user_id = 'rt_member.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_pre_rt_cd = 'rt_member.pre_rt_cd';
        if (alias !== undefined) {

          col_pre_rt_cd = `${alias}.pre_rt_cd`;

        }

        ar.select(`${col_pre_rt_cd} as '${col_pre_rt_cd}' `);

         
        let col_admin_id = 'rt_member.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'rt_member.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt_member.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_task_id = 'rt_member.task_id';
        if (alias !== undefined) {

          col_task_id = `${alias}.task_id`;

        }

        ar.select(`${col_task_id} as '${col_task_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_member');
    
    if (nullCheck(form.rtMemberId) === true) {
      ar.set("rt_member_id", form.rtMemberId);
    } 
    

    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtTaskReqId) === true) {
      ar.set("rt_task_req_id", form.rtTaskReqId);
    } 
    

    if (nullCheck(form.preBoardId) === true) {
      ar.set("pre_board_id", form.preBoardId);
    } 
    

    if (nullCheck(form.preRtId) === true) {
      ar.set("pre_rt_id", form.preRtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.preRtCd) === true) {
      ar.set("pre_rt_cd", form.preRtCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_member');
    
    if (nullCheck(form.rtMemberId) === true) {
      ar.set("rt_member_id", form.rtMemberId);
    } 
    

    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtTaskReqId) === true) {
      ar.set("rt_task_req_id", form.rtTaskReqId);
    } 
    

    if (nullCheck(form.preBoardId) === true) {
      ar.set("pre_board_id", form.preBoardId);
    } 
    

    if (nullCheck(form.preRtId) === true) {
      ar.set("pre_rt_id", form.preRtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("board_id", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("user_id", form.userId);
    } 
    

    if (nullCheck(form.preRtCd) === true) {
      ar.set("pre_rt_cd", form.preRtCd);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_member_view rt_member');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtMemberId: 'rt_member_id'
    , 

    rtTaskId: 'rt_task_id'
    , 

    rtTaskReqId: 'rt_task_req_id'
    , 

    preBoardId: 'pre_board_id'
    , 

    preRtId: 'pre_rt_id'
    , 

    boardId: 'board_id'
    , 

    rtId: 'rt_id'
    , 

    userId: 'user_id'
    , 

    preRtCd: 'pre_rt_cd'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    taskId: 'task_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_member');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_member_view rt_member');
    
      ar.select("rt_member.rt_member_id");

    
    
      ar.select("rt_member.rt_task_id");

    
    
      ar.select("rt_member.rt_task_req_id");

    
    
      ar.select("rt_member.pre_board_id");

    
    
      ar.select("rt_member.pre_rt_id");

    
    
      ar.select("rt_member.board_id");

    
    
      ar.select("rt_member.rt_id");

    
    
      ar.select("rt_member.user_id");

    
    
      ar.select("rt_member.pre_rt_cd");

    
    
      ar.select("rt_member.admin_id");

    
    
      ar.select("rt_member.created_at");

    
    
      ar.select("rt_member.updated_at");

    
    
      ar.select("rt_member.task_id");

    
    
    return ar;
  }

  

  
}
export const RtMemberSql =  new RtMemberQuery()

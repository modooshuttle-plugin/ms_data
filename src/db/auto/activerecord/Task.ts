import { Ar, nullCheck } from "../../../util";
 
  import { ITask } from "../interface";


  class TaskQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 task객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' task_view task');

    
      if (alias === true) {
        ar.select("task.task_id as 'task.task_id'" );

      } else{
        ar.select("task.task_id");

      }
      
      if (alias === true) {
        ar.select("task.rt_id as 'task.rt_id'" );

      } else{
        ar.select("task.rt_id");

      }
      
      if (alias === true) {
        ar.select("task.task_cd as 'task.task_cd'" );

      } else{
        ar.select("task.task_cd");

      }
      
      if (alias === true) {
        ar.select("task.title as 'task.title'" );

      } else{
        ar.select("task.title");

      }
      
      if (alias === true) {
        ar.select("task.admin_id as 'task.admin_id'" );

      } else{
        ar.select("task.admin_id");

      }
      
      if (alias === true) {
        ar.select("task.created_at as 'task.created_at'" );

      } else{
        ar.select("task.created_at");

      }
      
      if (alias === true) {
        ar.select("task.updated_at as 'task.updated_at'" );

      } else{
        ar.select("task.updated_at");

      }
      
      if (alias === true) {
        ar.select("task.status_cd as 'task.status_cd'" );

      } else{
        ar.select("task.status_cd");

      }
      
      if (alias === true) {
        ar.select("task.admin_ids as 'task.admin_ids'" );

      } else{
        ar.select("task.admin_ids");

      }
      
      if (alias === true) {
        ar.select("task.start_day as 'task.start_day'" );

      } else{
        ar.select("task.start_day");

      }
      
      if (alias === true) {
        ar.select("task.end_day as 'task.end_day'" );

      } else{
        ar.select("task.end_day");

      }
      
      if (alias === true) {
        ar.select("task.notion_page_id as 'task.notion_page_id'" );

      } else{
        ar.select("task.notion_page_id");

      }
      
      if (alias === true) {
        ar.select("task.target_day as 'task.target_day'" );

      } else{
        ar.select("task.target_day");

      }
      
      if (alias === true) {
        ar.select("task.updated_id as 'task.updated_id'" );

      } else{
        ar.select("task.updated_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_task_id = 'task.task_id';
        if (alias !== undefined) {

          col_task_id = `${alias}.task_id`;

        }

        ar.select(`${col_task_id} as '${col_task_id}' `);

         
        let col_rt_id = 'task.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_task_cd = 'task.task_cd';
        if (alias !== undefined) {

          col_task_cd = `${alias}.task_cd`;

        }

        ar.select(`${col_task_cd} as '${col_task_cd}' `);

         
        let col_title = 'task.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_admin_id = 'task.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'task.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'task.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_status_cd = 'task.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_admin_ids = 'task.admin_ids';
        if (alias !== undefined) {

          col_admin_ids = `${alias}.admin_ids`;

        }

        ar.select(`${col_admin_ids} as '${col_admin_ids}' `);

         
        let col_start_day = 'task.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'task.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_notion_page_id = 'task.notion_page_id';
        if (alias !== undefined) {

          col_notion_page_id = `${alias}.notion_page_id`;

        }

        ar.select(`${col_notion_page_id} as '${col_notion_page_id}' `);

         
        let col_target_day = 'task.target_day';
        if (alias !== undefined) {

          col_target_day = `${alias}.target_day`;

        }

        ar.select(`${col_target_day} as '${col_target_day}' `);

         
        let col_updated_id = 'task.updated_id';
        if (alias !== undefined) {

          col_updated_id = `${alias}.updated_id`;

        }

        ar.select(`${col_updated_id} as '${col_updated_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' task');
    
    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.taskCd) === true) {
      ar.set("task_cd", form.taskCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.adminIds) === true) {
      ar.set("admin_ids", form.adminIds);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.notionPageId) === true) {
      ar.set("notion_page_id", form.notionPageId);
    } 
    

    if (nullCheck(form.targetDay) === true) {
      ar.set("target_day", form.targetDay);
    } 
    

    if (nullCheck(form.updatedId) === true) {
      ar.set("updated_id", form.updatedId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' task');
    
    if (nullCheck(form.taskId) === true) {
      ar.set("task_id", form.taskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.taskCd) === true) {
      ar.set("task_cd", form.taskCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("status_cd", form.statusCd);
    } 
    

    if (nullCheck(form.adminIds) === true) {
      ar.set("admin_ids", form.adminIds);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.notionPageId) === true) {
      ar.set("notion_page_id", form.notionPageId);
    } 
    

    if (nullCheck(form.targetDay) === true) {
      ar.set("target_day", form.targetDay);
    } 
    

    if (nullCheck(form.updatedId) === true) {
      ar.set("updated_id", form.updatedId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' task_view task');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    taskId: 'task_id'
    , 

    rtId: 'rt_id'
    , 

    taskCd: 'task_cd'
    , 

    title: 'title'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    statusCd: 'status_cd'
    , 

    adminIds: 'admin_ids'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    notionPageId: 'notion_page_id'
    , 

    targetDay: 'target_day'
    , 

    updatedId: 'updated_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('task');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' task_view task');
    
      ar.select("task.task_id");

    
    
      ar.select("task.rt_id");

    
    
      ar.select("task.task_cd");

    
    
      ar.select("task.title");

    
    
      ar.select("task.admin_id");

    
    
      ar.select("task.created_at");

    
    
      ar.select("task.updated_at");

    
    
      ar.select("task.status_cd");

    
    
      ar.select("task.admin_ids");

    
    
      ar.select("task.start_day");

    
    
      ar.select("task.end_day");

    
    
      ar.select("task.notion_page_id");

    
    
      ar.select("task.target_day");

    
    
      ar.select("task.updated_id");

    
    
    return ar;
  }

  

  
}
export const TaskSql =  new TaskQuery()

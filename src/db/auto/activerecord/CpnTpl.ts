import { Ar, nullCheck } from "../../../util";
 
  import { ICpnTpl } from "../interface";


  class CpnTplQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cpn_tpl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cpn_tpl_view cpn_tpl');

    
      if (alias === true) {
        ar.select("cpn_tpl.cpn_tpl_id as 'cpn_tpl.cpn_tpl_id'" );

      } else{
        ar.select("cpn_tpl.cpn_tpl_id");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.nm as 'cpn_tpl.nm'" );

      } else{
        ar.select("cpn_tpl.nm");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.cal_cd as 'cpn_tpl.cal_cd'" );

      } else{
        ar.select("cpn_tpl.cal_cd");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.func as 'cpn_tpl.func'" );

      } else{
        ar.select("cpn_tpl.func");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.amount as 'cpn_tpl.amount'" );

      } else{
        ar.select("cpn_tpl.amount");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.sign_cd as 'cpn_tpl.sign_cd'" );

      } else{
        ar.select("cpn_tpl.sign_cd");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.admin_update_cd as 'cpn_tpl.admin_update_cd'" );

      } else{
        ar.select("cpn_tpl.admin_update_cd");

      }
      
      if (alias === true) {
        ar.select("cpn_tpl.dupe_yn as 'cpn_tpl.dupe_yn'" );

      } else{
        ar.select("cpn_tpl.dupe_yn");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cpn_tpl_id = 'cpn_tpl.cpn_tpl_id';
        if (alias !== undefined) {

          col_cpn_tpl_id = `${alias}.cpn_tpl_id`;

        }

        ar.select(`${col_cpn_tpl_id} as '${col_cpn_tpl_id}' `);

         
        let col_nm = 'cpn_tpl.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_cal_cd = 'cpn_tpl.cal_cd';
        if (alias !== undefined) {

          col_cal_cd = `${alias}.cal_cd`;

        }

        ar.select(`${col_cal_cd} as '${col_cal_cd}' `);

         
        let col_func = 'cpn_tpl.func';
        if (alias !== undefined) {

          col_func = `${alias}.func`;

        }

        ar.select(`${col_func} as '${col_func}' `);

         
        let col_amount = 'cpn_tpl.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_sign_cd = 'cpn_tpl.sign_cd';
        if (alias !== undefined) {

          col_sign_cd = `${alias}.sign_cd`;

        }

        ar.select(`${col_sign_cd} as '${col_sign_cd}' `);

         
        let col_admin_update_cd = 'cpn_tpl.admin_update_cd';
        if (alias !== undefined) {

          col_admin_update_cd = `${alias}.admin_update_cd`;

        }

        ar.select(`${col_admin_update_cd} as '${col_admin_update_cd}' `);

         
        let col_dupe_yn = 'cpn_tpl.dupe_yn';
        if (alias !== undefined) {

          col_dupe_yn = `${alias}.dupe_yn`;

        }

        ar.select(`${col_dupe_yn} as '${col_dupe_yn}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_coupon');
    
    if (nullCheck(form.cpnTplId) === true) {
      ar.set("t_no", form.cpnTplId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("t_name", form.nm);
    } 
    

    if (nullCheck(form.calCd) === true) {
      ar.set("t_type", form.calCd);
    } 
    

    if (nullCheck(form.func) === true) {
      ar.set("t_func", form.func);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("t_value", form.amount);
    } 
    

    if (nullCheck(form.signCd) === true) {
      ar.set("t_sign", form.signCd);
    } 
    

    if (nullCheck(form.adminUpdateCd) === true) {
      ar.set("t_cupdate", form.adminUpdateCd);
    } 
    

    if (nullCheck(form.dupeYn) === true) {
      ar.set("dupe_yn", form.dupeYn);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' t_coupon');
    
    if (nullCheck(form.cpnTplId) === true) {
      ar.set("t_no", form.cpnTplId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("t_name", form.nm);
    } 
    

    if (nullCheck(form.calCd) === true) {
      ar.set("t_type", form.calCd);
    } 
    

    if (nullCheck(form.func) === true) {
      ar.set("t_func", form.func);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("t_value", form.amount);
    } 
    

    if (nullCheck(form.signCd) === true) {
      ar.set("t_sign", form.signCd);
    } 
    

    if (nullCheck(form.adminUpdateCd) === true) {
      ar.set("t_cupdate", form.adminUpdateCd);
    } 
    

    if (nullCheck(form.dupeYn) === true) {
      ar.set("dupe_yn", form.dupeYn);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_tpl_view cpn_tpl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    cpnTplId: 't_no'
    , 

    nm: 't_name'
    , 

    calCd: 't_type'
    , 

    func: 't_func'
    , 

    amount: 't_value'
    , 

    signCd: 't_sign'
    , 

    adminUpdateCd: 't_cupdate'
    , 

    dupeYn: 'dupe_yn'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('t_coupon');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cpn_tpl_view cpn_tpl');
    
      ar.select("cpn_tpl.cpn_tpl_id");

    
    
      ar.select("cpn_tpl.nm");

    
    
      ar.select("cpn_tpl.cal_cd");

    
    
      ar.select("cpn_tpl.func");

    
    
      ar.select("cpn_tpl.amount");

    
    
      ar.select("cpn_tpl.sign_cd");

    
    
      ar.select("cpn_tpl.admin_update_cd");

    
    
      ar.select("cpn_tpl.dupe_yn");

    
    
    return ar;
  }

  

  
}
export const CpnTplSql =  new CpnTplQuery()

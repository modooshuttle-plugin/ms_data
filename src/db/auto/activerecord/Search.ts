import { Ar, nullCheck } from "../../../util";
 
  import { ISearch } from "../interface";


  class SearchQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 search객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' search_view search');

    
      if (alias === true) {
        ar.select("search.search_id as 'search.search_id'" );

      } else{
        ar.select("search.search_id");

      }
      
      if (alias === true) {
        ar.select("search.user_id as 'search.user_id'" );

      } else{
        ar.select("search.user_id");

      }
      
      if (alias === true) {
        ar.select("search.start_addr as 'search.start_addr'" );

      } else{
        ar.select("search.start_addr");

      }
      
      if (alias === true) {
        ar.select("search.start_lat as 'search.start_lat'" );

      } else{
        ar.select("search.start_lat");

      }
      
      if (alias === true) {
        ar.select("search.start_lng as 'search.start_lng'" );

      } else{
        ar.select("search.start_lng");

      }
      
      if (alias === true) {
        ar.select("search.end_addr as 'search.end_addr'" );

      } else{
        ar.select("search.end_addr");

      }
      
      if (alias === true) {
        ar.select("search.end_lat as 'search.end_lat'" );

      } else{
        ar.select("search.end_lat");

      }
      
      if (alias === true) {
        ar.select("search.end_lng as 'search.end_lng'" );

      } else{
        ar.select("search.end_lng");

      }
      
      if (alias === true) {
        ar.select("search.created_at as 'search.created_at'" );

      } else{
        ar.select("search.created_at");

      }
      
      if (alias === true) {
        ar.select("search.updated_at as 'search.updated_at'" );

      } else{
        ar.select("search.updated_at");

      }
      
      if (alias === true) {
        ar.select("search.time_id as 'search.time_id'" );

      } else{
        ar.select("search.time_id");

      }
      
      if (alias === true) {
        ar.select("search.start_emd_cd as 'search.start_emd_cd'" );

      } else{
        ar.select("search.start_emd_cd");

      }
      
      if (alias === true) {
        ar.select("search.end_emd_cd as 'search.end_emd_cd'" );

      } else{
        ar.select("search.end_emd_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_search_id = 'search.search_id';
        if (alias !== undefined) {

          col_search_id = `${alias}.search_id`;

        }

        ar.select(`${col_search_id} as '${col_search_id}' `);

         
        let col_user_id = 'search.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_start_addr = 'search.start_addr';
        if (alias !== undefined) {

          col_start_addr = `${alias}.start_addr`;

        }

        ar.select(`${col_start_addr} as '${col_start_addr}' `);

         
        let col_start_lat = 'search.start_lat';
        if (alias !== undefined) {

          col_start_lat = `${alias}.start_lat`;

        }

        ar.select(`${col_start_lat} as '${col_start_lat}' `);

         
        let col_start_lng = 'search.start_lng';
        if (alias !== undefined) {

          col_start_lng = `${alias}.start_lng`;

        }

        ar.select(`${col_start_lng} as '${col_start_lng}' `);

         
        let col_end_addr = 'search.end_addr';
        if (alias !== undefined) {

          col_end_addr = `${alias}.end_addr`;

        }

        ar.select(`${col_end_addr} as '${col_end_addr}' `);

         
        let col_end_lat = 'search.end_lat';
        if (alias !== undefined) {

          col_end_lat = `${alias}.end_lat`;

        }

        ar.select(`${col_end_lat} as '${col_end_lat}' `);

         
        let col_end_lng = 'search.end_lng';
        if (alias !== undefined) {

          col_end_lng = `${alias}.end_lng`;

        }

        ar.select(`${col_end_lng} as '${col_end_lng}' `);

         
        let col_created_at = 'search.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'search.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_time_id = 'search.time_id';
        if (alias !== undefined) {

          col_time_id = `${alias}.time_id`;

        }

        ar.select(`${col_time_id} as '${col_time_id}' `);

         
        let col_start_emd_cd = 'search.start_emd_cd';
        if (alias !== undefined) {

          col_start_emd_cd = `${alias}.start_emd_cd`;

        }

        ar.select(`${col_start_emd_cd} as '${col_start_emd_cd}' `);

         
        let col_end_emd_cd = 'search.end_emd_cd';
        if (alias !== undefined) {

          col_end_emd_cd = `${alias}.end_emd_cd`;

        }

        ar.select(`${col_end_emd_cd} as '${col_end_emd_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_spot');
    
    if (nullCheck(form.searchId) === true) {
      ar.set("m_no", form.searchId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("m_start", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("m_start_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("m_start_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("m_end", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("m_end_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("m_end_lng", form.endLng);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("m_time", form.timeId);
    } 
    

    if (nullCheck(form.startEmdCd) === true) {
      ar.set("sta_emd_cd", form.startEmdCd);
    } 
    

    if (nullCheck(form.endEmdCd) === true) {
      ar.set("end_emd_cd", form.endEmdCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_spot');
    
    if (nullCheck(form.searchId) === true) {
      ar.set("m_no", form.searchId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.startAddr) === true) {
      ar.set("m_start", form.startAddr);
    } 
    

    if (nullCheck(form.startLat) === true) {
      ar.set("m_start_lat", form.startLat);
    } 
    

    if (nullCheck(form.startLng) === true) {
      ar.set("m_start_lng", form.startLng);
    } 
    

    if (nullCheck(form.endAddr) === true) {
      ar.set("m_end", form.endAddr);
    } 
    

    if (nullCheck(form.endLat) === true) {
      ar.set("m_end_lat", form.endLat);
    } 
    

    if (nullCheck(form.endLng) === true) {
      ar.set("m_end_lng", form.endLng);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.timeId) === true) {
      ar.set("m_time", form.timeId);
    } 
    

    if (nullCheck(form.startEmdCd) === true) {
      ar.set("sta_emd_cd", form.startEmdCd);
    } 
    

    if (nullCheck(form.endEmdCd) === true) {
      ar.set("end_emd_cd", form.endEmdCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' search_view search');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    searchId: 'm_no'
    , 

    userId: 'm_mid'
    , 

    startAddr: 'm_start'
    , 

    startLat: 'm_start_lat'
    , 

    startLng: 'm_start_lng'
    , 

    endAddr: 'm_end'
    , 

    endLat: 'm_end_lat'
    , 

    endLng: 'm_end_lng'
    , 

    createdAt: 'm_timestamp'
    , 

    updatedAt: 'm_timestamp_u'
    , 

    timeId: 'm_time'
    , 

    startEmdCd: 'sta_emd_cd'
    , 

    endEmdCd: 'end_emd_cd'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('m_spot');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' search_view search');
    
      ar.select("search.search_id");

    
    
      ar.select("search.user_id");

    
    
      ar.select("search.start_addr");

    
    
      ar.select("search.start_lat");

    
    
      ar.select("search.start_lng");

    
    
      ar.select("search.end_addr");

    
    
      ar.select("search.end_lat");

    
    
      ar.select("search.end_lng");

    
    
      ar.select("search.created_at");

    
    
      ar.select("search.updated_at");

    
    
      ar.select("search.time_id");

    
    
      ar.select("search.start_emd_cd");

    
    
      ar.select("search.end_emd_cd");

    
    
    return ar;
  }

  

  
}
export const SearchSql =  new SearchQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IOrgSetlMship } from "../interface";


  class OrgSetlMshipQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_setl_mship객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_setl_mship_view org_setl_mship');

    
      if (alias === true) {
        ar.select("org_setl_mship.org_setl_mship_id as 'org_setl_mship.org_setl_mship_id'" );

      } else{
        ar.select("org_setl_mship.org_setl_mship_id");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.org_ctrt_id as 'org_setl_mship.org_ctrt_id'" );

      } else{
        ar.select("org_setl_mship.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.org_ctrt_dtl_id as 'org_setl_mship.org_ctrt_dtl_id'" );

      } else{
        ar.select("org_setl_mship.org_ctrt_dtl_id");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.org_id as 'org_setl_mship.org_id'" );

      } else{
        ar.select("org_setl_mship.org_id");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.start_day as 'org_setl_mship.start_day'" );

      } else{
        ar.select("org_setl_mship.start_day");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.end_day as 'org_setl_mship.end_day'" );

      } else{
        ar.select("org_setl_mship.end_day");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.setl_month as 'org_setl_mship.setl_month'" );

      } else{
        ar.select("org_setl_mship.setl_month");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.month_amount as 'org_setl_mship.month_amount'" );

      } else{
        ar.select("org_setl_mship.month_amount");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.dc_amount as 'org_setl_mship.dc_amount'" );

      } else{
        ar.select("org_setl_mship.dc_amount");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.user_amount as 'org_setl_mship.user_amount'" );

      } else{
        ar.select("org_setl_mship.user_amount");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.setl_dc_amount as 'org_setl_mship.setl_dc_amount'" );

      } else{
        ar.select("org_setl_mship.setl_dc_amount");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.amount as 'org_setl_mship.amount'" );

      } else{
        ar.select("org_setl_mship.amount");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.tax_iss_day as 'org_setl_mship.tax_iss_day'" );

      } else{
        ar.select("org_setl_mship.tax_iss_day");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.comment as 'org_setl_mship.comment'" );

      } else{
        ar.select("org_setl_mship.comment");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.created_at as 'org_setl_mship.created_at'" );

      } else{
        ar.select("org_setl_mship.created_at");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.updated_at as 'org_setl_mship.updated_at'" );

      } else{
        ar.select("org_setl_mship.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_setl_mship.admin_id as 'org_setl_mship.admin_id'" );

      } else{
        ar.select("org_setl_mship.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_setl_mship_id = 'org_setl_mship.org_setl_mship_id';
        if (alias !== undefined) {

          col_org_setl_mship_id = `${alias}.org_setl_mship_id`;

        }

        ar.select(`${col_org_setl_mship_id} as '${col_org_setl_mship_id}' `);

         
        let col_org_ctrt_id = 'org_setl_mship.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_org_ctrt_dtl_id = 'org_setl_mship.org_ctrt_dtl_id';
        if (alias !== undefined) {

          col_org_ctrt_dtl_id = `${alias}.org_ctrt_dtl_id`;

        }

        ar.select(`${col_org_ctrt_dtl_id} as '${col_org_ctrt_dtl_id}' `);

         
        let col_org_id = 'org_setl_mship.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_start_day = 'org_setl_mship.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'org_setl_mship.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_setl_month = 'org_setl_mship.setl_month';
        if (alias !== undefined) {

          col_setl_month = `${alias}.setl_month`;

        }

        ar.select(`${col_setl_month} as '${col_setl_month}' `);

         
        let col_month_amount = 'org_setl_mship.month_amount';
        if (alias !== undefined) {

          col_month_amount = `${alias}.month_amount`;

        }

        ar.select(`${col_month_amount} as '${col_month_amount}' `);

         
        let col_dc_amount = 'org_setl_mship.dc_amount';
        if (alias !== undefined) {

          col_dc_amount = `${alias}.dc_amount`;

        }

        ar.select(`${col_dc_amount} as '${col_dc_amount}' `);

         
        let col_user_amount = 'org_setl_mship.user_amount';
        if (alias !== undefined) {

          col_user_amount = `${alias}.user_amount`;

        }

        ar.select(`${col_user_amount} as '${col_user_amount}' `);

         
        let col_setl_dc_amount = 'org_setl_mship.setl_dc_amount';
        if (alias !== undefined) {

          col_setl_dc_amount = `${alias}.setl_dc_amount`;

        }

        ar.select(`${col_setl_dc_amount} as '${col_setl_dc_amount}' `);

         
        let col_amount = 'org_setl_mship.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_tax_iss_day = 'org_setl_mship.tax_iss_day';
        if (alias !== undefined) {

          col_tax_iss_day = `${alias}.tax_iss_day`;

        }

        ar.select(`${col_tax_iss_day} as '${col_tax_iss_day}' `);

         
        let col_comment = 'org_setl_mship.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'org_setl_mship.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_setl_mship.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org_setl_mship.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_setl_mship');
    
    if (nullCheck(form.orgSetlMshipId) === true) {
      ar.set("org_setl_mship_id", form.orgSetlMshipId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.monthAmount) === true) {
      ar.set("month_amount", form.monthAmount);
    } 
    

    if (nullCheck(form.dcAmount) === true) {
      ar.set("dc_amount", form.dcAmount);
    } 
    

    if (nullCheck(form.userAmount) === true) {
      ar.set("user_amount", form.userAmount);
    } 
    

    if (nullCheck(form.setlDcAmount) === true) {
      ar.set("setl_dc_amount", form.setlDcAmount);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.taxIssDay) === true) {
      ar.set("tax_iss_day", form.taxIssDay);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_setl_mship');
    
    if (nullCheck(form.orgSetlMshipId) === true) {
      ar.set("org_setl_mship_id", form.orgSetlMshipId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.monthAmount) === true) {
      ar.set("month_amount", form.monthAmount);
    } 
    

    if (nullCheck(form.dcAmount) === true) {
      ar.set("dc_amount", form.dcAmount);
    } 
    

    if (nullCheck(form.userAmount) === true) {
      ar.set("user_amount", form.userAmount);
    } 
    

    if (nullCheck(form.setlDcAmount) === true) {
      ar.set("setl_dc_amount", form.setlDcAmount);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.taxIssDay) === true) {
      ar.set("tax_iss_day", form.taxIssDay);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_setl_mship_view org_setl_mship');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgSetlMshipId: 'org_setl_mship_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    orgCtrtDtlId: 'org_ctrt_dtl_id'
    , 

    orgId: 'org_id'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    setlMonth: 'setl_month'
    , 

    monthAmount: 'month_amount'
    , 

    dcAmount: 'dc_amount'
    , 

    userAmount: 'user_amount'
    , 

    setlDcAmount: 'setl_dc_amount'
    , 

    amount: 'amount'
    , 

    taxIssDay: 'tax_iss_day'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_setl_mship');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_setl_mship_view org_setl_mship');
    
      ar.select("org_setl_mship.org_setl_mship_id");

    
    
      ar.select("org_setl_mship.org_ctrt_id");

    
    
      ar.select("org_setl_mship.org_ctrt_dtl_id");

    
    
      ar.select("org_setl_mship.org_id");

    
    
      ar.select("org_setl_mship.start_day");

    
    
      ar.select("org_setl_mship.end_day");

    
    
      ar.select("org_setl_mship.setl_month");

    
    
      ar.select("org_setl_mship.month_amount");

    
    
      ar.select("org_setl_mship.dc_amount");

    
    
      ar.select("org_setl_mship.user_amount");

    
    
      ar.select("org_setl_mship.setl_dc_amount");

    
    
      ar.select("org_setl_mship.amount");

    
    
      ar.select("org_setl_mship.tax_iss_day");

    
    
      ar.select("org_setl_mship.comment");

    
    
      ar.select("org_setl_mship.created_at");

    
    
      ar.select("org_setl_mship.updated_at");

    
    
      ar.select("org_setl_mship.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgSetlMshipSql =  new OrgSetlMshipQuery()

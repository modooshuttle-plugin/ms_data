import { Ar, nullCheck } from "../../../util";
 
  import { IOrgUserAuth } from "../interface";


  class OrgUserAuthQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_user_auth객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_user_auth_view org_user_auth');

    
      if (alias === true) {
        ar.select("org_user_auth.org_user_auth_id as 'org_user_auth.org_user_auth_id'" );

      } else{
        ar.select("org_user_auth.org_user_auth_id");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.org_id as 'org_user_auth.org_id'" );

      } else{
        ar.select("org_user_auth.org_id");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.org_ctrt_id as 'org_user_auth.org_ctrt_id'" );

      } else{
        ar.select("org_user_auth.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.start_day as 'org_user_auth.start_day'" );

      } else{
        ar.select("org_user_auth.start_day");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.end_day as 'org_user_auth.end_day'" );

      } else{
        ar.select("org_user_auth.end_day");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.method_cd as 'org_user_auth.method_cd'" );

      } else{
        ar.select("org_user_auth.method_cd");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.created_at as 'org_user_auth.created_at'" );

      } else{
        ar.select("org_user_auth.created_at");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.updated_at as 'org_user_auth.updated_at'" );

      } else{
        ar.select("org_user_auth.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_user_auth.admin_id as 'org_user_auth.admin_id'" );

      } else{
        ar.select("org_user_auth.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_user_auth_id = 'org_user_auth.org_user_auth_id';
        if (alias !== undefined) {

          col_org_user_auth_id = `${alias}.org_user_auth_id`;

        }

        ar.select(`${col_org_user_auth_id} as '${col_org_user_auth_id}' `);

         
        let col_org_id = 'org_user_auth.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_org_ctrt_id = 'org_user_auth.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_start_day = 'org_user_auth.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'org_user_auth.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_method_cd = 'org_user_auth.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_created_at = 'org_user_auth.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_user_auth.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org_user_auth.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_user_auth');
    
    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_user_auth');
    
    if (nullCheck(form.orgUserAuthId) === true) {
      ar.set("org_user_auth_id", form.orgUserAuthId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_user_auth_view org_user_auth');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgUserAuthId: 'org_user_auth_id'
    , 

    orgId: 'org_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    methodCd: 'method_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_user_auth');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_user_auth_view org_user_auth');
    
      ar.select("org_user_auth.org_user_auth_id");

    
    
      ar.select("org_user_auth.org_id");

    
    
      ar.select("org_user_auth.org_ctrt_id");

    
    
      ar.select("org_user_auth.start_day");

    
    
      ar.select("org_user_auth.end_day");

    
    
      ar.select("org_user_auth.method_cd");

    
    
      ar.select("org_user_auth.created_at");

    
    
      ar.select("org_user_auth.updated_at");

    
    
      ar.select("org_user_auth.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgUserAuthSql =  new OrgUserAuthQuery()

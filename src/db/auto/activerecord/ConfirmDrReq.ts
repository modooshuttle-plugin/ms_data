import { Ar, nullCheck } from "../../../util";
 
  import { IConfirmDrReq } from "../interface";


  class ConfirmDrReqQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 confirm_dr_req객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' confirm_dr_req_view confirm_dr_req');

    
      if (alias === true) {
        ar.select("confirm_dr_req.confirm_dr_req_id as 'confirm_dr_req.confirm_dr_req_id'" );

      } else{
        ar.select("confirm_dr_req.confirm_dr_req_id");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.rt_id as 'confirm_dr_req.rt_id'" );

      } else{
        ar.select("confirm_dr_req.rt_id");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.from_cd as 'confirm_dr_req.from_cd'" );

      } else{
        ar.select("confirm_dr_req.from_cd");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.to_cd as 'confirm_dr_req.to_cd'" );

      } else{
        ar.select("confirm_dr_req.to_cd");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.from_id as 'confirm_dr_req.from_id'" );

      } else{
        ar.select("confirm_dr_req.from_id");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.to_id as 'confirm_dr_req.to_id'" );

      } else{
        ar.select("confirm_dr_req.to_id");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.req_cd as 'confirm_dr_req.req_cd'" );

      } else{
        ar.select("confirm_dr_req.req_cd");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.created_at as 'confirm_dr_req.created_at'" );

      } else{
        ar.select("confirm_dr_req.created_at");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.updated_at as 'confirm_dr_req.updated_at'" );

      } else{
        ar.select("confirm_dr_req.updated_at");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.comment as 'confirm_dr_req.comment'" );

      } else{
        ar.select("confirm_dr_req.comment");

      }
      
      if (alias === true) {
        ar.select("confirm_dr_req.admin_id as 'confirm_dr_req.admin_id'" );

      } else{
        ar.select("confirm_dr_req.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_confirm_dr_req_id = 'confirm_dr_req.confirm_dr_req_id';
        if (alias !== undefined) {

          col_confirm_dr_req_id = `${alias}.confirm_dr_req_id`;

        }

        ar.select(`${col_confirm_dr_req_id} as '${col_confirm_dr_req_id}' `);

         
        let col_rt_id = 'confirm_dr_req.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_from_cd = 'confirm_dr_req.from_cd';
        if (alias !== undefined) {

          col_from_cd = `${alias}.from_cd`;

        }

        ar.select(`${col_from_cd} as '${col_from_cd}' `);

         
        let col_to_cd = 'confirm_dr_req.to_cd';
        if (alias !== undefined) {

          col_to_cd = `${alias}.to_cd`;

        }

        ar.select(`${col_to_cd} as '${col_to_cd}' `);

         
        let col_from_id = 'confirm_dr_req.from_id';
        if (alias !== undefined) {

          col_from_id = `${alias}.from_id`;

        }

        ar.select(`${col_from_id} as '${col_from_id}' `);

         
        let col_to_id = 'confirm_dr_req.to_id';
        if (alias !== undefined) {

          col_to_id = `${alias}.to_id`;

        }

        ar.select(`${col_to_id} as '${col_to_id}' `);

         
        let col_req_cd = 'confirm_dr_req.req_cd';
        if (alias !== undefined) {

          col_req_cd = `${alias}.req_cd`;

        }

        ar.select(`${col_req_cd} as '${col_req_cd}' `);

         
        let col_created_at = 'confirm_dr_req.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'confirm_dr_req.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_comment = 'confirm_dr_req.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'confirm_dr_req.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' confirm_dr_req');
    
    if (nullCheck(form.confirmDrReqId) === true) {
      ar.set("confirm_dr_req_id", form.confirmDrReqId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.fromCd) === true) {
      ar.set("from_cd", form.fromCd);
    } 
    

    if (nullCheck(form.toCd) === true) {
      ar.set("to_cd", form.toCd);
    } 
    

    if (nullCheck(form.fromId) === true) {
      ar.set("from_id", form.fromId);
    } 
    

    if (nullCheck(form.toId) === true) {
      ar.set("to_id", form.toId);
    } 
    

    if (nullCheck(form.reqCd) === true) {
      ar.set("req_cd", form.reqCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' confirm_dr_req');
    
    if (nullCheck(form.confirmDrReqId) === true) {
      ar.set("confirm_dr_req_id", form.confirmDrReqId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.fromCd) === true) {
      ar.set("from_cd", form.fromCd);
    } 
    

    if (nullCheck(form.toCd) === true) {
      ar.set("to_cd", form.toCd);
    } 
    

    if (nullCheck(form.fromId) === true) {
      ar.set("from_id", form.fromId);
    } 
    

    if (nullCheck(form.toId) === true) {
      ar.set("to_id", form.toId);
    } 
    

    if (nullCheck(form.reqCd) === true) {
      ar.set("req_cd", form.reqCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' confirm_dr_req_view confirm_dr_req');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    confirmDrReqId: 'confirm_dr_req_id'
    , 

    rtId: 'rt_id'
    , 

    fromCd: 'from_cd'
    , 

    toCd: 'to_cd'
    , 

    fromId: 'from_id'
    , 

    toId: 'to_id'
    , 

    reqCd: 'req_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('confirm_dr_req');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' confirm_dr_req_view confirm_dr_req');
    
      ar.select("confirm_dr_req.confirm_dr_req_id");

    
    
      ar.select("confirm_dr_req.rt_id");

    
    
      ar.select("confirm_dr_req.from_cd");

    
    
      ar.select("confirm_dr_req.to_cd");

    
    
      ar.select("confirm_dr_req.from_id");

    
    
      ar.select("confirm_dr_req.to_id");

    
    
      ar.select("confirm_dr_req.req_cd");

    
    
      ar.select("confirm_dr_req.created_at");

    
    
      ar.select("confirm_dr_req.updated_at");

    
    
      ar.select("confirm_dr_req.comment");

    
    
      ar.select("confirm_dr_req.admin_id");

    
    
    return ar;
  }

  

  
}
export const ConfirmDrReqSql =  new ConfirmDrReqQuery()

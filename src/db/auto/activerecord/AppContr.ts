import { Ar, nullCheck } from "../../../util";
 
  import { IAppContr } from "../interface";


  class AppContrQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 app_contr객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' app_contr_view app_contr');

    
      if (alias === true) {
        ar.select("app_contr.app_contr_id as 'app_contr.app_contr_id'" );

      } else{
        ar.select("app_contr.app_contr_id");

      }
      
      if (alias === true) {
        ar.select("app_contr.target_cd as 'app_contr.target_cd'" );

      } else{
        ar.select("app_contr.target_cd");

      }
      
      if (alias === true) {
        ar.select("app_contr.ver as 'app_contr.ver'" );

      } else{
        ar.select("app_contr.ver");

      }
      
      if (alias === true) {
        ar.select("app_contr.native_ver as 'app_contr.native_ver'" );

      } else{
        ar.select("app_contr.native_ver");

      }
      
      if (alias === true) {
        ar.select("app_contr.os_cd as 'app_contr.os_cd'" );

      } else{
        ar.select("app_contr.os_cd");

      }
      
      if (alias === true) {
        ar.select("app_contr.created_at as 'app_contr.created_at'" );

      } else{
        ar.select("app_contr.created_at");

      }
      
      if (alias === true) {
        ar.select("app_contr.updated_at as 'app_contr.updated_at'" );

      } else{
        ar.select("app_contr.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_app_contr_id = 'app_contr.app_contr_id';
        if (alias !== undefined) {

          col_app_contr_id = `${alias}.app_contr_id`;

        }

        ar.select(`${col_app_contr_id} as '${col_app_contr_id}' `);

         
        let col_target_cd = 'app_contr.target_cd';
        if (alias !== undefined) {

          col_target_cd = `${alias}.target_cd`;

        }

        ar.select(`${col_target_cd} as '${col_target_cd}' `);

         
        let col_ver = 'app_contr.ver';
        if (alias !== undefined) {

          col_ver = `${alias}.ver`;

        }

        ar.select(`${col_ver} as '${col_ver}' `);

         
        let col_native_ver = 'app_contr.native_ver';
        if (alias !== undefined) {

          col_native_ver = `${alias}.native_ver`;

        }

        ar.select(`${col_native_ver} as '${col_native_ver}' `);

         
        let col_os_cd = 'app_contr.os_cd';
        if (alias !== undefined) {

          col_os_cd = `${alias}.os_cd`;

        }

        ar.select(`${col_os_cd} as '${col_os_cd}' `);

         
        let col_created_at = 'app_contr.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'app_contr.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' app_contr');
    
    if (nullCheck(form.appContrId) === true) {
      ar.set("app_contr_id", form.appContrId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.nativeVer) === true) {
      ar.set("native_ver", form.nativeVer);
    } 
    

    if (nullCheck(form.osCd) === true) {
      ar.set("os_cd", form.osCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' app_contr');
    
    if (nullCheck(form.appContrId) === true) {
      ar.set("app_contr_id", form.appContrId);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target_cd", form.targetCd);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.nativeVer) === true) {
      ar.set("native_ver", form.nativeVer);
    } 
    

    if (nullCheck(form.osCd) === true) {
      ar.set("os_cd", form.osCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' app_contr_view app_contr');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    appContrId: 'app_contr_id'
    , 

    targetCd: 'target_cd'
    , 

    ver: 'ver'
    , 

    nativeVer: 'native_ver'
    , 

    osCd: 'os_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('app_contr');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' app_contr_view app_contr');
    
      ar.select("app_contr.app_contr_id");

    
    
      ar.select("app_contr.target_cd");

    
    
      ar.select("app_contr.ver");

    
    
      ar.select("app_contr.native_ver");

    
    
      ar.select("app_contr.os_cd");

    
    
      ar.select("app_contr.created_at");

    
    
      ar.select("app_contr.updated_at");

    
    
    return ar;
  }

  

  
}
export const AppContrSql =  new AppContrQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IPromoCd } from "../interface";


  class PromoCdQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 promo_cd객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' promo_cd_view promo_cd');

    
      if (alias === true) {
        ar.select("promo_cd.promo_cd_id as 'promo_cd.promo_cd_id'" );

      } else{
        ar.select("promo_cd.promo_cd_id");

      }
      
      if (alias === true) {
        ar.select("promo_cd.promo_cd as 'promo_cd.promo_cd'" );

      } else{
        ar.select("promo_cd.promo_cd");

      }
      
      if (alias === true) {
        ar.select("promo_cd.org_id as 'promo_cd.org_id'" );

      } else{
        ar.select("promo_cd.org_id");

      }
      
      if (alias === true) {
        ar.select("promo_cd.start_day as 'promo_cd.start_day'" );

      } else{
        ar.select("promo_cd.start_day");

      }
      
      if (alias === true) {
        ar.select("promo_cd.end_day as 'promo_cd.end_day'" );

      } else{
        ar.select("promo_cd.end_day");

      }
      
      if (alias === true) {
        ar.select("promo_cd.title as 'promo_cd.title'" );

      } else{
        ar.select("promo_cd.title");

      }
      
      if (alias === true) {
        ar.select("promo_cd.created_at as 'promo_cd.created_at'" );

      } else{
        ar.select("promo_cd.created_at");

      }
      
      if (alias === true) {
        ar.select("promo_cd.updated_at as 'promo_cd.updated_at'" );

      } else{
        ar.select("promo_cd.updated_at");

      }
      
      if (alias === true) {
        ar.select("promo_cd.use_start_day as 'promo_cd.use_start_day'" );

      } else{
        ar.select("promo_cd.use_start_day");

      }
      
      if (alias === true) {
        ar.select("promo_cd.use_end_day as 'promo_cd.use_end_day'" );

      } else{
        ar.select("promo_cd.use_end_day");

      }
      
      if (alias === true) {
        ar.select("promo_cd.cpn_tpl_id as 'promo_cd.cpn_tpl_id'" );

      } else{
        ar.select("promo_cd.cpn_tpl_id");

      }
      
      if (alias === true) {
        ar.select("promo_cd.promo_status_cd as 'promo_cd.promo_status_cd'" );

      } else{
        ar.select("promo_cd.promo_status_cd");

      }
      
      if (alias === true) {
        ar.select("promo_cd.org_ctrt_id as 'promo_cd.org_ctrt_id'" );

      } else{
        ar.select("promo_cd.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("promo_cd.org_ctrt_dtl_id as 'promo_cd.org_ctrt_dtl_id'" );

      } else{
        ar.select("promo_cd.org_ctrt_dtl_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_promo_cd_id = 'promo_cd.promo_cd_id';
        if (alias !== undefined) {

          col_promo_cd_id = `${alias}.promo_cd_id`;

        }

        ar.select(`${col_promo_cd_id} as '${col_promo_cd_id}' `);

         
        let col_promo_cd = 'promo_cd.promo_cd';
        if (alias !== undefined) {

          col_promo_cd = `${alias}.promo_cd`;

        }

        ar.select(`${col_promo_cd} as '${col_promo_cd}' `);

         
        let col_org_id = 'promo_cd.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_start_day = 'promo_cd.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'promo_cd.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_title = 'promo_cd.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_created_at = 'promo_cd.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'promo_cd.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_use_start_day = 'promo_cd.use_start_day';
        if (alias !== undefined) {

          col_use_start_day = `${alias}.use_start_day`;

        }

        ar.select(`${col_use_start_day} as '${col_use_start_day}' `);

         
        let col_use_end_day = 'promo_cd.use_end_day';
        if (alias !== undefined) {

          col_use_end_day = `${alias}.use_end_day`;

        }

        ar.select(`${col_use_end_day} as '${col_use_end_day}' `);

         
        let col_cpn_tpl_id = 'promo_cd.cpn_tpl_id';
        if (alias !== undefined) {

          col_cpn_tpl_id = `${alias}.cpn_tpl_id`;

        }

        ar.select(`${col_cpn_tpl_id} as '${col_cpn_tpl_id}' `);

         
        let col_promo_status_cd = 'promo_cd.promo_status_cd';
        if (alias !== undefined) {

          col_promo_status_cd = `${alias}.promo_status_cd`;

        }

        ar.select(`${col_promo_status_cd} as '${col_promo_status_cd}' `);

         
        let col_org_ctrt_id = 'promo_cd.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_org_ctrt_dtl_id = 'promo_cd.org_ctrt_dtl_id';
        if (alias !== undefined) {

          col_org_ctrt_dtl_id = `${alias}.org_ctrt_dtl_id`;

        }

        ar.select(`${col_org_ctrt_dtl_id} as '${col_org_ctrt_dtl_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mp_code');
    
    if (nullCheck(form.promoCdId) === true) {
      ar.set("mp_no", form.promoCdId);
    } 
    

    if (nullCheck(form.promoCd) === true) {
      ar.set("mp_code", form.promoCd);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("mp_mc_uniq", form.orgId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("mp_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("mp_end", form.endDay);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mp_title", form.title);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mp_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mp_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.useStartDay) === true) {
      ar.set("mp_start_day", form.useStartDay);
    } 
    

    if (nullCheck(form.useEndDay) === true) {
      ar.set("mp_end_day", form.useEndDay);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("mp_temp", form.cpnTplId);
    } 
    

    if (nullCheck(form.promoStatusCd) === true) {
      ar.set("mp_status", form.promoStatusCd);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mp_code');
    
    if (nullCheck(form.promoCdId) === true) {
      ar.set("mp_no", form.promoCdId);
    } 
    

    if (nullCheck(form.promoCd) === true) {
      ar.set("mp_code", form.promoCd);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("mp_mc_uniq", form.orgId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("mp_start", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("mp_end", form.endDay);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mp_title", form.title);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mp_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mp_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.useStartDay) === true) {
      ar.set("mp_start_day", form.useStartDay);
    } 
    

    if (nullCheck(form.useEndDay) === true) {
      ar.set("mp_end_day", form.useEndDay);
    } 
    

    if (nullCheck(form.cpnTplId) === true) {
      ar.set("mp_temp", form.cpnTplId);
    } 
    

    if (nullCheck(form.promoStatusCd) === true) {
      ar.set("mp_status", form.promoStatusCd);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' promo_cd_view promo_cd');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    promoCdId: 'mp_no'
    , 

    promoCd: 'mp_code'
    , 

    orgId: 'mp_mc_uniq'
    , 

    startDay: 'mp_start'
    , 

    endDay: 'mp_end'
    , 

    title: 'mp_title'
    , 

    createdAt: 'mp_timestamp'
    , 

    updatedAt: 'mp_timestamp_u'
    , 

    useStartDay: 'mp_start_day'
    , 

    useEndDay: 'mp_end_day'
    , 

    cpnTplId: 'mp_temp'
    , 

    promoStatusCd: 'mp_status'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    orgCtrtDtlId: 'org_ctrt_dtl_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('mp_code');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' promo_cd_view promo_cd');
    
      ar.select("promo_cd.promo_cd_id");

    
    
      ar.select("promo_cd.promo_cd");

    
    
      ar.select("promo_cd.org_id");

    
    
      ar.select("promo_cd.start_day");

    
    
      ar.select("promo_cd.end_day");

    
    
      ar.select("promo_cd.title");

    
    
      ar.select("promo_cd.created_at");

    
    
      ar.select("promo_cd.updated_at");

    
    
      ar.select("promo_cd.use_start_day");

    
    
      ar.select("promo_cd.use_end_day");

    
    
      ar.select("promo_cd.cpn_tpl_id");

    
    
      ar.select("promo_cd.promo_status_cd");

    
    
      ar.select("promo_cd.org_ctrt_id");

    
    
      ar.select("promo_cd.org_ctrt_dtl_id");

    
    
    return ar;
  }

  

  
}
export const PromoCdSql =  new PromoCdQuery()

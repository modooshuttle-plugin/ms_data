import { Ar, nullCheck } from "../../../util";
 
  import { IPay } from "../interface";


  class PayQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 pay객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' pay_view pay');

    
      if (alias === true) {
        ar.select("pay.pay_id as 'pay.pay_id'" );

      } else{
        ar.select("pay.pay_id");

      }
      
      if (alias === true) {
        ar.select("pay.user_id as 'pay.user_id'" );

      } else{
        ar.select("pay.user_id");

      }
      
      if (alias === true) {
        ar.select("pay.amount as 'pay.amount'" );

      } else{
        ar.select("pay.amount");

      }
      
      if (alias === true) {
        ar.select("pay.rt_id as 'pay.rt_id'" );

      } else{
        ar.select("pay.rt_id");

      }
      
      if (alias === true) {
        ar.select("pay.board_id as 'pay.board_id'" );

      } else{
        ar.select("pay.board_id");

      }
      
      if (alias === true) {
        ar.select("pay.prods_id as 'pay.prods_id'" );

      } else{
        ar.select("pay.prods_id");

      }
      
      if (alias === true) {
        ar.select("pay.pay_try_id as 'pay.pay_try_id'" );

      } else{
        ar.select("pay.pay_try_id");

      }
      
      if (alias === true) {
        ar.select("pay.created_at as 'pay.created_at'" );

      } else{
        ar.select("pay.created_at");

      }
      
      if (alias === true) {
        ar.select("pay.updated_at as 'pay.updated_at'" );

      } else{
        ar.select("pay.updated_at");

      }
      
      if (alias === true) {
        ar.select("pay.status_cd as 'pay.status_cd'" );

      } else{
        ar.select("pay.status_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_pay_id = 'pay.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_user_id = 'pay.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_amount = 'pay.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_rt_id = 'pay.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_board_id = 'pay.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_prods_id = 'pay.prods_id';
        if (alias !== undefined) {

          col_prods_id = `${alias}.prods_id`;

        }

        ar.select(`${col_prods_id} as '${col_prods_id}' `);

         
        let col_pay_try_id = 'pay.pay_try_id';
        if (alias !== undefined) {

          col_pay_try_id = `${alias}.pay_try_id`;

        }

        ar.select(`${col_pay_try_id} as '${col_pay_try_id}' `);

         
        let col_created_at = 'pay.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'pay.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_status_cd = 'pay.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' p_info');
    
    if (nullCheck(form.payId) === true) {
      ar.set("p_no", form.payId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("p_mid", form.userId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("p_price", form.amount);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("p_rid", form.rtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("p_oid", form.boardId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("p_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.payTryId) === true) {
      ar.set("p_poid", form.payTryId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("p_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("p_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("p_confirm", form.statusCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' p_info');
    
    if (nullCheck(form.payId) === true) {
      ar.set("p_no", form.payId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("p_mid", form.userId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("p_price", form.amount);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("p_rid", form.rtId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("p_oid", form.boardId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("p_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.payTryId) === true) {
      ar.set("p_poid", form.payTryId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("p_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("p_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("p_confirm", form.statusCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_view pay');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    payId: 'p_no'
    , 

    userId: 'p_mid'
    , 

    amount: 'p_price'
    , 

    rtId: 'p_rid'
    , 

    boardId: 'p_oid'
    , 

    prodsId: 'p_rpid'
    , 

    payTryId: 'p_poid'
    , 

    createdAt: 'p_timestamp'
    , 

    updatedAt: 'p_timestamp_u'
    , 

    statusCd: 'p_confirm'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('p_info');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' pay_view pay');
    
      ar.select("pay.pay_id");

    
    
      ar.select("pay.user_id");

    
    
      ar.select("pay.amount");

    
    
      ar.select("pay.rt_id");

    
    
      ar.select("pay.board_id");

    
    
      ar.select("pay.prods_id");

    
    
      ar.select("pay.pay_try_id");

    
    
      ar.select("pay.created_at");

    
    
      ar.select("pay.updated_at");

    
    
      ar.select("pay.status_cd");

    
    
    return ar;
  }

  

  
}
export const PaySql =  new PayQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IDrBus } from "../interface";


  class DrBusQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_bus객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_bus_view dr_bus');

    
      if (alias === true) {
        ar.select("dr_bus.dr_bus_id as 'dr_bus.dr_bus_id'" );

      } else{
        ar.select("dr_bus.dr_bus_id");

      }
      
      if (alias === true) {
        ar.select("dr_bus.dr_id as 'dr_bus.dr_id'" );

      } else{
        ar.select("dr_bus.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_bus.bus_id as 'dr_bus.bus_id'" );

      } else{
        ar.select("dr_bus.bus_id");

      }
      
      if (alias === true) {
        ar.select("dr_bus.year as 'dr_bus.year'" );

      } else{
        ar.select("dr_bus.year");

      }
      
      if (alias === true) {
        ar.select("dr_bus.info as 'dr_bus.info'" );

      } else{
        ar.select("dr_bus.info");

      }
      
      if (alias === true) {
        ar.select("dr_bus.bus_no as 'dr_bus.bus_no'" );

      } else{
        ar.select("dr_bus.bus_no");

      }
      
      if (alias === true) {
        ar.select("dr_bus.created_at as 'dr_bus.created_at'" );

      } else{
        ar.select("dr_bus.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_bus.updated_at as 'dr_bus.updated_at'" );

      } else{
        ar.select("dr_bus.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_bus_id = 'dr_bus.dr_bus_id';
        if (alias !== undefined) {

          col_dr_bus_id = `${alias}.dr_bus_id`;

        }

        ar.select(`${col_dr_bus_id} as '${col_dr_bus_id}' `);

         
        let col_dr_id = 'dr_bus.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_bus_id = 'dr_bus.bus_id';
        if (alias !== undefined) {

          col_bus_id = `${alias}.bus_id`;

        }

        ar.select(`${col_bus_id} as '${col_bus_id}' `);

         
        let col_year = 'dr_bus.year';
        if (alias !== undefined) {

          col_year = `${alias}.year`;

        }

        ar.select(`${col_year} as '${col_year}' `);

         
        let col_info = 'dr_bus.info';
        if (alias !== undefined) {

          col_info = `${alias}.info`;

        }

        ar.select(`${col_info} as '${col_info}' `);

         
        let col_bus_no = 'dr_bus.bus_no';
        if (alias !== undefined) {

          col_bus_no = `${alias}.bus_no`;

        }

        ar.select(`${col_bus_no} as '${col_bus_no}' `);

         
        let col_created_at = 'dr_bus.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_bus.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_bus');
    
    if (nullCheck(form.drBusId) === true) {
      ar.set("d_no", form.drBusId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.busId) === true) {
      ar.set("d_bid", form.busId);
    } 
    

    if (nullCheck(form.year) === true) {
      ar.set("d_year", form.year);
    } 
    

    if (nullCheck(form.info) === true) {
      ar.set("d_info", form.info);
    } 
    

    if (nullCheck(form.busNo) === true) {
      ar.set("d_cno", form.busNo);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_bus');
    
    if (nullCheck(form.drBusId) === true) {
      ar.set("d_no", form.drBusId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.busId) === true) {
      ar.set("d_bid", form.busId);
    } 
    

    if (nullCheck(form.year) === true) {
      ar.set("d_year", form.year);
    } 
    

    if (nullCheck(form.info) === true) {
      ar.set("d_info", form.info);
    } 
    

    if (nullCheck(form.busNo) === true) {
      ar.set("d_cno", form.busNo);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_bus_view dr_bus');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drBusId: 'd_no'
    , 

    drId: 'd_did'
    , 

    busId: 'd_bid'
    , 

    year: 'd_year'
    , 

    info: 'd_info'
    , 

    busNo: 'd_cno'
    , 

    createdAt: 'd_timestamp'
    , 

    updatedAt: 'd_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_bus');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_bus_view dr_bus');
    
      ar.select("dr_bus.dr_bus_id");

    
    
      ar.select("dr_bus.dr_id");

    
    
      ar.select("dr_bus.bus_id");

    
    
      ar.select("dr_bus.year");

    
    
      ar.select("dr_bus.info");

    
    
      ar.select("dr_bus.bus_no");

    
    
      ar.select("dr_bus.created_at");

    
    
      ar.select("dr_bus.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrBusSql =  new DrBusQuery()

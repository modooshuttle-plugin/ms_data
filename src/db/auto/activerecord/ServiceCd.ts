import { Ar, nullCheck } from "../../../util";
 
  import { IServiceCd } from "../interface";


  class ServiceCdQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 service_cd객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' service_cd_view service_cd');

    
      if (alias === true) {
        ar.select("service_cd.service_cd_id as 'service_cd.service_cd_id'" );

      } else{
        ar.select("service_cd.service_cd_id");

      }
      
      if (alias === true) {
        ar.select("service_cd.tb as 'service_cd.tb'" );

      } else{
        ar.select("service_cd.tb");

      }
      
      if (alias === true) {
        ar.select("service_cd.col as 'service_cd.col'" );

      } else{
        ar.select("service_cd.col");

      }
      
      if (alias === true) {
        ar.select("service_cd.ver as 'service_cd.ver'" );

      } else{
        ar.select("service_cd.ver");

      }
      
      if (alias === true) {
        ar.select("service_cd.value as 'service_cd.value'" );

      } else{
        ar.select("service_cd.value");

      }
      
      if (alias === true) {
        ar.select("service_cd.wh as 'service_cd.wh'" );

      } else{
        ar.select("service_cd.wh");

      }
      
      if (alias === true) {
        ar.select("service_cd.comment as 'service_cd.comment'" );

      } else{
        ar.select("service_cd.comment");

      }
      
      if (alias === true) {
        ar.select("service_cd.created_at as 'service_cd.created_at'" );

      } else{
        ar.select("service_cd.created_at");

      }
      
      if (alias === true) {
        ar.select("service_cd.updated_at as 'service_cd.updated_at'" );

      } else{
        ar.select("service_cd.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_service_cd_id = 'service_cd.service_cd_id';
        if (alias !== undefined) {

          col_service_cd_id = `${alias}.service_cd_id`;

        }

        ar.select(`${col_service_cd_id} as '${col_service_cd_id}' `);

         
        let col_tb = 'service_cd.tb';
        if (alias !== undefined) {

          col_tb = `${alias}.tb`;

        }

        ar.select(`${col_tb} as '${col_tb}' `);

         
        let col_col = 'service_cd.col';
        if (alias !== undefined) {

          col_col = `${alias}.col`;

        }

        ar.select(`${col_col} as '${col_col}' `);

         
        let col_ver = 'service_cd.ver';
        if (alias !== undefined) {

          col_ver = `${alias}.ver`;

        }

        ar.select(`${col_ver} as '${col_ver}' `);

         
        let col_value = 'service_cd.value';
        if (alias !== undefined) {

          col_value = `${alias}.value`;

        }

        ar.select(`${col_value} as '${col_value}' `);

         
        let col_wh = 'service_cd.wh';
        if (alias !== undefined) {

          col_wh = `${alias}.wh`;

        }

        ar.select(`${col_wh} as '${col_wh}' `);

         
        let col_comment = 'service_cd.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_created_at = 'service_cd.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'service_cd.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' service_cd');
    
    if (nullCheck(form.serviceCdId) === true) {
      ar.set("service_cd_id", form.serviceCdId);
    } 
    

    if (nullCheck(form.tb) === true) {
      ar.set("tb", form.tb);
    } 
    

    if (nullCheck(form.col) === true) {
      ar.set("col", form.col);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.value) === true) {
      ar.set("value", form.value);
    } 
    

    if (nullCheck(form.wh) === true) {
      ar.set("wh", form.wh);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' service_cd');
    
    if (nullCheck(form.serviceCdId) === true) {
      ar.set("service_cd_id", form.serviceCdId);
    } 
    

    if (nullCheck(form.tb) === true) {
      ar.set("tb", form.tb);
    } 
    

    if (nullCheck(form.col) === true) {
      ar.set("col", form.col);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.value) === true) {
      ar.set("value", form.value);
    } 
    

    if (nullCheck(form.wh) === true) {
      ar.set("wh", form.wh);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' service_cd_view service_cd');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    serviceCdId: 'service_cd_id'
    , 

    tb: 'tb'
    , 

    col: 'col'
    , 

    ver: 'ver'
    , 

    value: 'value'
    , 

    wh: 'wh'
    , 

    comment: 'comment'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('service_cd');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' service_cd_view service_cd');
    
      ar.select("service_cd.service_cd_id");

    
    
      ar.select("service_cd.tb");

    
    
      ar.select("service_cd.col");

    
    
      ar.select("service_cd.ver");

    
    
      ar.select("service_cd.value");

    
    
      ar.select("service_cd.wh");

    
    
      ar.select("service_cd.comment");

    
    
      ar.select("service_cd.created_at");

    
    
      ar.select("service_cd.updated_at");

    
    
    return ar;
  }

  

  
}
export const ServiceCdSql =  new ServiceCdQuery()

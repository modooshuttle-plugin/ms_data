import { Ar, nullCheck } from "../../../util";
 
  import { IDrIss } from "../interface";


  class DrIssQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_iss객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_view dr_iss');

    
      if (alias === true) {
        ar.select("dr_iss.dr_iss_id as 'dr_iss.dr_iss_id'" );

      } else{
        ar.select("dr_iss.dr_iss_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.runn_iss_id as 'dr_iss.runn_iss_id'" );

      } else{
        ar.select("dr_iss.runn_iss_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.dispatch_id as 'dr_iss.dispatch_id'" );

      } else{
        ar.select("dr_iss.dispatch_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.rt_id as 'dr_iss.rt_id'" );

      } else{
        ar.select("dr_iss.rt_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.dr_id as 'dr_iss.dr_id'" );

      } else{
        ar.select("dr_iss.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.comment as 'dr_iss.comment'" );

      } else{
        ar.select("dr_iss.comment");

      }
      
      if (alias === true) {
        ar.select("dr_iss.admin_check_cd as 'dr_iss.admin_check_cd'" );

      } else{
        ar.select("dr_iss.admin_check_cd");

      }
      
      if (alias === true) {
        ar.select("dr_iss.start_day as 'dr_iss.start_day'" );

      } else{
        ar.select("dr_iss.start_day");

      }
      
      if (alias === true) {
        ar.select("dr_iss.end_day as 'dr_iss.end_day'" );

      } else{
        ar.select("dr_iss.end_day");

      }
      
      if (alias === true) {
        ar.select("dr_iss.setl_month as 'dr_iss.setl_month'" );

      } else{
        ar.select("dr_iss.setl_month");

      }
      
      if (alias === true) {
        ar.select("dr_iss.admin_id as 'dr_iss.admin_id'" );

      } else{
        ar.select("dr_iss.admin_id");

      }
      
      if (alias === true) {
        ar.select("dr_iss.iss_cd as 'dr_iss.iss_cd'" );

      } else{
        ar.select("dr_iss.iss_cd");

      }
      
      if (alias === true) {
        ar.select("dr_iss.created_at as 'dr_iss.created_at'" );

      } else{
        ar.select("dr_iss.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_iss.updated_at as 'dr_iss.updated_at'" );

      } else{
        ar.select("dr_iss.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_iss_id = 'dr_iss.dr_iss_id';
        if (alias !== undefined) {

          col_dr_iss_id = `${alias}.dr_iss_id`;

        }

        ar.select(`${col_dr_iss_id} as '${col_dr_iss_id}' `);

         
        let col_runn_iss_id = 'dr_iss.runn_iss_id';
        if (alias !== undefined) {

          col_runn_iss_id = `${alias}.runn_iss_id`;

        }

        ar.select(`${col_runn_iss_id} as '${col_runn_iss_id}' `);

         
        let col_dispatch_id = 'dr_iss.dispatch_id';
        if (alias !== undefined) {

          col_dispatch_id = `${alias}.dispatch_id`;

        }

        ar.select(`${col_dispatch_id} as '${col_dispatch_id}' `);

         
        let col_rt_id = 'dr_iss.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_dr_id = 'dr_iss.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_comment = 'dr_iss.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_check_cd = 'dr_iss.admin_check_cd';
        if (alias !== undefined) {

          col_admin_check_cd = `${alias}.admin_check_cd`;

        }

        ar.select(`${col_admin_check_cd} as '${col_admin_check_cd}' `);

         
        let col_start_day = 'dr_iss.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'dr_iss.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_setl_month = 'dr_iss.setl_month';
        if (alias !== undefined) {

          col_setl_month = `${alias}.setl_month`;

        }

        ar.select(`${col_setl_month} as '${col_setl_month}' `);

         
        let col_admin_id = 'dr_iss.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_iss_cd = 'dr_iss.iss_cd';
        if (alias !== undefined) {

          col_iss_cd = `${alias}.iss_cd`;

        }

        ar.select(`${col_iss_cd} as '${col_iss_cd}' `);

         
        let col_created_at = 'dr_iss.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_iss.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss');
    
    if (nullCheck(form.drIssId) === true) {
      ar.set("dr_iss_id", form.drIssId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminCheckCd) === true) {
      ar.set("admin_check_cd", form.adminCheckCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.issCd) === true) {
      ar.set("iss_cd", form.issCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss');
    
    if (nullCheck(form.drIssId) === true) {
      ar.set("dr_iss_id", form.drIssId);
    } 
    

    if (nullCheck(form.runnIssId) === true) {
      ar.set("runn_iss_id", form.runnIssId);
    } 
    

    if (nullCheck(form.dispatchId) === true) {
      ar.set("dispatch_id", form.dispatchId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminCheckCd) === true) {
      ar.set("admin_check_cd", form.adminCheckCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.setlMonth) === true) {
      ar.set("setl_month", form.setlMonth);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.issCd) === true) {
      ar.set("iss_cd", form.issCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_view dr_iss');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drIssId: 'dr_iss_id'
    , 

    runnIssId: 'runn_iss_id'
    , 

    dispatchId: 'dispatch_id'
    , 

    rtId: 'rt_id'
    , 

    drId: 'dr_id'
    , 

    comment: 'comment'
    , 

    adminCheckCd: 'admin_check_cd'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    setlMonth: 'setl_month'
    , 

    adminId: 'admin_id'
    , 

    issCd: 'iss_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_iss');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_iss_view dr_iss');
    
      ar.select("dr_iss.dr_iss_id");

    
    
      ar.select("dr_iss.runn_iss_id");

    
    
      ar.select("dr_iss.dispatch_id");

    
    
      ar.select("dr_iss.rt_id");

    
    
      ar.select("dr_iss.dr_id");

    
    
      ar.select("dr_iss.comment");

    
    
      ar.select("dr_iss.admin_check_cd");

    
    
      ar.select("dr_iss.start_day");

    
    
      ar.select("dr_iss.end_day");

    
    
      ar.select("dr_iss.setl_month");

    
    
      ar.select("dr_iss.admin_id");

    
    
      ar.select("dr_iss.iss_cd");

    
    
      ar.select("dr_iss.created_at");

    
    
      ar.select("dr_iss.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrIssSql =  new DrIssQuery()

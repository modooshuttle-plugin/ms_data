import { Ar, nullCheck } from "../../../util";
 
  import { IDrAppPush } from "../interface";


  class DrAppPushQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_app_push객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_app_push_view dr_app_push');

    
      if (alias === true) {
        ar.select("dr_app_push.dr_app_push_id as 'dr_app_push.dr_app_push_id'" );

      } else{
        ar.select("dr_app_push.dr_app_push_id");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.dr_id as 'dr_app_push.dr_id'" );

      } else{
        ar.select("dr_app_push.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.app_push_token as 'dr_app_push.app_push_token'" );

      } else{
        ar.select("dr_app_push.app_push_token");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.uuid as 'dr_app_push.uuid'" );

      } else{
        ar.select("dr_app_push.uuid");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.platform as 'dr_app_push.platform'" );

      } else{
        ar.select("dr_app_push.platform");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.os_ver as 'dr_app_push.os_ver'" );

      } else{
        ar.select("dr_app_push.os_ver");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.model as 'dr_app_push.model'" );

      } else{
        ar.select("dr_app_push.model");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.created_at as 'dr_app_push.created_at'" );

      } else{
        ar.select("dr_app_push.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.updated_at as 'dr_app_push.updated_at'" );

      } else{
        ar.select("dr_app_push.updated_at");

      }
      
      if (alias === true) {
        ar.select("dr_app_push.native_ver as 'dr_app_push.native_ver'" );

      } else{
        ar.select("dr_app_push.native_ver");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_app_push_id = 'dr_app_push.dr_app_push_id';
        if (alias !== undefined) {

          col_dr_app_push_id = `${alias}.dr_app_push_id`;

        }

        ar.select(`${col_dr_app_push_id} as '${col_dr_app_push_id}' `);

         
        let col_dr_id = 'dr_app_push.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_app_push_token = 'dr_app_push.app_push_token';
        if (alias !== undefined) {

          col_app_push_token = `${alias}.app_push_token`;

        }

        ar.select(`${col_app_push_token} as '${col_app_push_token}' `);

         
        let col_uuid = 'dr_app_push.uuid';
        if (alias !== undefined) {

          col_uuid = `${alias}.uuid`;

        }

        ar.select(`${col_uuid} as '${col_uuid}' `);

         
        let col_platform = 'dr_app_push.platform';
        if (alias !== undefined) {

          col_platform = `${alias}.platform`;

        }

        ar.select(`${col_platform} as '${col_platform}' `);

         
        let col_os_ver = 'dr_app_push.os_ver';
        if (alias !== undefined) {

          col_os_ver = `${alias}.os_ver`;

        }

        ar.select(`${col_os_ver} as '${col_os_ver}' `);

         
        let col_model = 'dr_app_push.model';
        if (alias !== undefined) {

          col_model = `${alias}.model`;

        }

        ar.select(`${col_model} as '${col_model}' `);

         
        let col_created_at = 'dr_app_push.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_app_push.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_native_ver = 'dr_app_push.native_ver';
        if (alias !== undefined) {

          col_native_ver = `${alias}.native_ver`;

        }

        ar.select(`${col_native_ver} as '${col_native_ver}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_app_push');
    
    if (nullCheck(form.drAppPushId) === true) {
      ar.set("dr_app_push_id", form.drAppPushId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.appPushToken) === true) {
      ar.set("app_push_token", form.appPushToken);
    } 
    

    if (nullCheck(form.uuid) === true) {
      ar.set("uuid", form.uuid);
    } 
    

    if (nullCheck(form.platform) === true) {
      ar.set("platform", form.platform);
    } 
    

    if (nullCheck(form.osVer) === true) {
      ar.set("os_ver", form.osVer);
    } 
    

    if (nullCheck(form.model) === true) {
      ar.set("model", form.model);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.nativeVer) === true) {
      ar.set("native_ver", form.nativeVer);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' dr_app_push');
    
    if (nullCheck(form.drAppPushId) === true) {
      ar.set("dr_app_push_id", form.drAppPushId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("dr_id", form.drId);
    } 
    

    if (nullCheck(form.appPushToken) === true) {
      ar.set("app_push_token", form.appPushToken);
    } 
    

    if (nullCheck(form.uuid) === true) {
      ar.set("uuid", form.uuid);
    } 
    

    if (nullCheck(form.platform) === true) {
      ar.set("platform", form.platform);
    } 
    

    if (nullCheck(form.osVer) === true) {
      ar.set("os_ver", form.osVer);
    } 
    

    if (nullCheck(form.model) === true) {
      ar.set("model", form.model);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.nativeVer) === true) {
      ar.set("native_ver", form.nativeVer);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_app_push_view dr_app_push');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drAppPushId: 'dr_app_push_id'
    , 

    drId: 'dr_id'
    , 

    appPushToken: 'app_push_token'
    , 

    uuid: 'uuid'
    , 

    platform: 'platform'
    , 

    osVer: 'os_ver'
    , 

    model: 'model'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    nativeVer: 'native_ver'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('dr_app_push');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_app_push_view dr_app_push');
    
      ar.select("dr_app_push.dr_app_push_id");

    
    
      ar.select("dr_app_push.dr_id");

    
    
      ar.select("dr_app_push.app_push_token");

    
    
      ar.select("dr_app_push.uuid");

    
    
      ar.select("dr_app_push.platform");

    
    
      ar.select("dr_app_push.os_ver");

    
    
      ar.select("dr_app_push.model");

    
    
      ar.select("dr_app_push.created_at");

    
    
      ar.select("dr_app_push.updated_at");

    
    
      ar.select("dr_app_push.native_ver");

    
    
    return ar;
  }

  

  
}
export const DrAppPushSql =  new DrAppPushQuery()

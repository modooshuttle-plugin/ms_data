import { Ar, nullCheck } from "../../../util";
 
  import { ITerms } from "../interface";


  class TermsQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 terms객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' terms_view terms');

    
      if (alias === true) {
        ar.select("terms.terms_id as 'terms.terms_id'" );

      } else{
        ar.select("terms.terms_id");

      }
      
      if (alias === true) {
        ar.select("terms.title as 'terms.title'" );

      } else{
        ar.select("terms.title");

      }
      
      if (alias === true) {
        ar.select("terms.created_at as 'terms.created_at'" );

      } else{
        ar.select("terms.created_at");

      }
      
      if (alias === true) {
        ar.select("terms.updated_at as 'terms.updated_at'" );

      } else{
        ar.select("terms.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_terms_id = 'terms.terms_id';
        if (alias !== undefined) {

          col_terms_id = `${alias}.terms_id`;

        }

        ar.select(`${col_terms_id} as '${col_terms_id}' `);

         
        let col_title = 'terms.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_created_at = 'terms.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'terms.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mb_policy');
    
    if (nullCheck(form.termsId) === true) {
      ar.set("mb_no", form.termsId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mb_title", form.title);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mb_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mb_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' mb_policy');
    
    if (nullCheck(form.termsId) === true) {
      ar.set("mb_no", form.termsId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("mb_title", form.title);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("mb_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("mb_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' terms_view terms');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    termsId: 'mb_no'
    , 

    title: 'mb_title'
    , 

    createdAt: 'mb_timestamp'
    , 

    updatedAt: 'mb_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('mb_policy');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' terms_view terms');
    
      ar.select("terms.terms_id");

    
    
      ar.select("terms.title");

    
    
      ar.select("terms.created_at");

    
    
      ar.select("terms.updated_at");

    
    
    return ar;
  }

  

  
}
export const TermsSql =  new TermsQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { IRtTaskReq } from "../interface";


  class RtTaskReqQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt_task_req객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_req_view rt_task_req');

    
      if (alias === true) {
        ar.select("rt_task_req.rt_task_req_id as 'rt_task_req.rt_task_req_id'" );

      } else{
        ar.select("rt_task_req.rt_task_req_id");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.rt_task_id as 'rt_task_req.rt_task_id'" );

      } else{
        ar.select("rt_task_req.rt_task_id");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.rt_id as 'rt_task_req.rt_id'" );

      } else{
        ar.select("rt_task_req.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.req_cd as 'rt_task_req.req_cd'" );

      } else{
        ar.select("rt_task_req.req_cd");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.title as 'rt_task_req.title'" );

      } else{
        ar.select("rt_task_req.title");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.admin_id as 'rt_task_req.admin_id'" );

      } else{
        ar.select("rt_task_req.admin_id");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.created_at as 'rt_task_req.created_at'" );

      } else{
        ar.select("rt_task_req.created_at");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.updated_at as 'rt_task_req.updated_at'" );

      } else{
        ar.select("rt_task_req.updated_at");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.req_status_cd as 'rt_task_req.req_status_cd'" );

      } else{
        ar.select("rt_task_req.req_status_cd");

      }
      
      if (alias === true) {
        ar.select("rt_task_req.admin_ids as 'rt_task_req.admin_ids'" );

      } else{
        ar.select("rt_task_req.admin_ids");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_task_req_id = 'rt_task_req.rt_task_req_id';
        if (alias !== undefined) {

          col_rt_task_req_id = `${alias}.rt_task_req_id`;

        }

        ar.select(`${col_rt_task_req_id} as '${col_rt_task_req_id}' `);

         
        let col_rt_task_id = 'rt_task_req.rt_task_id';
        if (alias !== undefined) {

          col_rt_task_id = `${alias}.rt_task_id`;

        }

        ar.select(`${col_rt_task_id} as '${col_rt_task_id}' `);

         
        let col_rt_id = 'rt_task_req.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_req_cd = 'rt_task_req.req_cd';
        if (alias !== undefined) {

          col_req_cd = `${alias}.req_cd`;

        }

        ar.select(`${col_req_cd} as '${col_req_cd}' `);

         
        let col_title = 'rt_task_req.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_admin_id = 'rt_task_req.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_created_at = 'rt_task_req.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt_task_req.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_req_status_cd = 'rt_task_req.req_status_cd';
        if (alias !== undefined) {

          col_req_status_cd = `${alias}.req_status_cd`;

        }

        ar.select(`${col_req_status_cd} as '${col_req_status_cd}' `);

         
        let col_admin_ids = 'rt_task_req.admin_ids';
        if (alias !== undefined) {

          col_admin_ids = `${alias}.admin_ids`;

        }

        ar.select(`${col_admin_ids} as '${col_admin_ids}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_req');
    
    if (nullCheck(form.rtTaskReqId) === true) {
      ar.set("rt_task_req_id", form.rtTaskReqId);
    } 
    

    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.reqCd) === true) {
      ar.set("req_cd", form.reqCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.reqStatusCd) === true) {
      ar.set("req_status_cd", form.reqStatusCd);
    } 
    

    if (nullCheck(form.adminIds) === true) {
      ar.set("admin_ids", form.adminIds);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_req');
    
    if (nullCheck(form.rtTaskReqId) === true) {
      ar.set("rt_task_req_id", form.rtTaskReqId);
    } 
    

    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.reqCd) === true) {
      ar.set("req_cd", form.reqCd);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.reqStatusCd) === true) {
      ar.set("req_status_cd", form.reqStatusCd);
    } 
    

    if (nullCheck(form.adminIds) === true) {
      ar.set("admin_ids", form.adminIds);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_req_view rt_task_req');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtTaskReqId: 'rt_task_req_id'
    , 

    rtTaskId: 'rt_task_id'
    , 

    rtId: 'rt_id'
    , 

    reqCd: 'req_cd'
    , 

    title: 'title'
    , 

    adminId: 'admin_id'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    reqStatusCd: 'req_status_cd'
    , 

    adminIds: 'admin_ids'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_task_req');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_req_view rt_task_req');
    
      ar.select("rt_task_req.rt_task_req_id");

    
    
      ar.select("rt_task_req.rt_task_id");

    
    
      ar.select("rt_task_req.rt_id");

    
    
      ar.select("rt_task_req.req_cd");

    
    
      ar.select("rt_task_req.title");

    
    
      ar.select("rt_task_req.admin_id");

    
    
      ar.select("rt_task_req.created_at");

    
    
      ar.select("rt_task_req.updated_at");

    
    
      ar.select("rt_task_req.req_status_cd");

    
    
      ar.select("rt_task_req.admin_ids");

    
    
    return ar;
  }

  

  
}
export const RtTaskReqSql =  new RtTaskReqQuery()

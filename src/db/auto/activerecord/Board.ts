import { Ar, nullCheck } from "../../../util";
 
  import { IBoard } from "../interface";


  class BoardQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 board객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' board_view board');

    
      if (alias === true) {
        ar.select("board.board_id as 'board.board_id'" );

      } else{
        ar.select("board.board_id");

      }
      
      if (alias === true) {
        ar.select("board.rt_id as 'board.rt_id'" );

      } else{
        ar.select("board.rt_id");

      }
      
      if (alias === true) {
        ar.select("board.prods_id as 'board.prods_id'" );

      } else{
        ar.select("board.prods_id");

      }
      
      if (alias === true) {
        ar.select("board.start_st_cd as 'board.start_st_cd'" );

      } else{
        ar.select("board.start_st_cd");

      }
      
      if (alias === true) {
        ar.select("board.end_st_cd as 'board.end_st_cd'" );

      } else{
        ar.select("board.end_st_cd");

      }
      
      if (alias === true) {
        ar.select("board.start_st_ver as 'board.start_st_ver'" );

      } else{
        ar.select("board.start_st_ver");

      }
      
      if (alias === true) {
        ar.select("board.end_st_ver as 'board.end_st_ver'" );

      } else{
        ar.select("board.end_st_ver");

      }
      
      if (alias === true) {
        ar.select("board.user_id as 'board.user_id'" );

      } else{
        ar.select("board.user_id");

      }
      
      if (alias === true) {
        ar.select("board.board_cd as 'board.board_cd'" );

      } else{
        ar.select("board.board_cd");

      }
      
      if (alias === true) {
        ar.select("board.board_shape_cd as 'board.board_shape_cd'" );

      } else{
        ar.select("board.board_shape_cd");

      }
      
      if (alias === true) {
        ar.select("board.seat_id as 'board.seat_id'" );

      } else{
        ar.select("board.seat_id");

      }
      
      if (alias === true) {
        ar.select("board.start_day as 'board.start_day'" );

      } else{
        ar.select("board.start_day");

      }
      
      if (alias === true) {
        ar.select("board.end_day as 'board.end_day'" );

      } else{
        ar.select("board.end_day");

      }
      
      if (alias === true) {
        ar.select("board.apply_id as 'board.apply_id'" );

      } else{
        ar.select("board.apply_id");

      }
      
      if (alias === true) {
        ar.select("board.created_at as 'board.created_at'" );

      } else{
        ar.select("board.created_at");

      }
      
      if (alias === true) {
        ar.select("board.updated_at as 'board.updated_at'" );

      } else{
        ar.select("board.updated_at");

      }
      
      if (alias === true) {
        ar.select("board.rt_cd as 'board.rt_cd'" );

      } else{
        ar.select("board.rt_cd");

      }
      
      if (alias === true) {
        ar.select("board.target_cd as 'board.target_cd'" );

      } else{
        ar.select("board.target_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_board_id = 'board.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_rt_id = 'board.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_prods_id = 'board.prods_id';
        if (alias !== undefined) {

          col_prods_id = `${alias}.prods_id`;

        }

        ar.select(`${col_prods_id} as '${col_prods_id}' `);

         
        let col_start_st_cd = 'board.start_st_cd';
        if (alias !== undefined) {

          col_start_st_cd = `${alias}.start_st_cd`;

        }

        ar.select(`${col_start_st_cd} as '${col_start_st_cd}' `);

         
        let col_end_st_cd = 'board.end_st_cd';
        if (alias !== undefined) {

          col_end_st_cd = `${alias}.end_st_cd`;

        }

        ar.select(`${col_end_st_cd} as '${col_end_st_cd}' `);

         
        let col_start_st_ver = 'board.start_st_ver';
        if (alias !== undefined) {

          col_start_st_ver = `${alias}.start_st_ver`;

        }

        ar.select(`${col_start_st_ver} as '${col_start_st_ver}' `);

         
        let col_end_st_ver = 'board.end_st_ver';
        if (alias !== undefined) {

          col_end_st_ver = `${alias}.end_st_ver`;

        }

        ar.select(`${col_end_st_ver} as '${col_end_st_ver}' `);

         
        let col_user_id = 'board.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_board_cd = 'board.board_cd';
        if (alias !== undefined) {

          col_board_cd = `${alias}.board_cd`;

        }

        ar.select(`${col_board_cd} as '${col_board_cd}' `);

         
        let col_board_shape_cd = 'board.board_shape_cd';
        if (alias !== undefined) {

          col_board_shape_cd = `${alias}.board_shape_cd`;

        }

        ar.select(`${col_board_shape_cd} as '${col_board_shape_cd}' `);

         
        let col_seat_id = 'board.seat_id';
        if (alias !== undefined) {

          col_seat_id = `${alias}.seat_id`;

        }

        ar.select(`${col_seat_id} as '${col_seat_id}' `);

         
        let col_start_day = 'board.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'board.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_apply_id = 'board.apply_id';
        if (alias !== undefined) {

          col_apply_id = `${alias}.apply_id`;

        }

        ar.select(`${col_apply_id} as '${col_apply_id}' `);

         
        let col_created_at = 'board.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'board.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_rt_cd = 'board.rt_cd';
        if (alias !== undefined) {

          col_rt_cd = `${alias}.rt_cd`;

        }

        ar.select(`${col_rt_cd} as '${col_rt_cd}' `);

         
        let col_target_cd = 'board.target_cd';
        if (alias !== undefined) {

          col_target_cd = `${alias}.target_cd`;

        }

        ar.select(`${col_target_cd} as '${col_target_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_board');
    
    if (nullCheck(form.boardId) === true) {
      ar.set("o_no", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("o_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.startStCd) === true) {
      ar.set("o_start_stop", form.startStCd);
    } 
    

    if (nullCheck(form.endStCd) === true) {
      ar.set("o_end_stop", form.endStCd);
    } 
    

    if (nullCheck(form.startStVer) === true) {
      ar.set("o_start_ver", form.startStVer);
    } 
    

    if (nullCheck(form.endStVer) === true) {
      ar.set("o_end_ver", form.endStVer);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardCd) === true) {
      ar.set("o_type", form.boardCd);
    } 
    

    if (nullCheck(form.boardShapeCd) === true) {
      ar.set("o_shape", form.boardShapeCd);
    } 
    

    if (nullCheck(form.seatId) === true) {
      ar.set("o_seat", form.seatId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("o_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("o_end_day", form.endDay);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("o_aid", form.applyId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("rt_ty", form.rtCd);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target", form.targetCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_board');
    
    if (nullCheck(form.boardId) === true) {
      ar.set("o_no", form.boardId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.prodsId) === true) {
      ar.set("o_rpid", form.prodsId);
    } 
    

    if (nullCheck(form.startStCd) === true) {
      ar.set("o_start_stop", form.startStCd);
    } 
    

    if (nullCheck(form.endStCd) === true) {
      ar.set("o_end_stop", form.endStCd);
    } 
    

    if (nullCheck(form.startStVer) === true) {
      ar.set("o_start_ver", form.startStVer);
    } 
    

    if (nullCheck(form.endStVer) === true) {
      ar.set("o_end_ver", form.endStVer);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardCd) === true) {
      ar.set("o_type", form.boardCd);
    } 
    

    if (nullCheck(form.boardShapeCd) === true) {
      ar.set("o_shape", form.boardShapeCd);
    } 
    

    if (nullCheck(form.seatId) === true) {
      ar.set("o_seat", form.seatId);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("o_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("o_end_day", form.endDay);
    } 
    

    if (nullCheck(form.applyId) === true) {
      ar.set("o_aid", form.applyId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    

    if (nullCheck(form.rtCd) === true) {
      ar.set("rt_ty", form.rtCd);
    } 
    

    if (nullCheck(form.targetCd) === true) {
      ar.set("target", form.targetCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' board_view board');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    boardId: 'o_no'
    , 

    rtId: 'o_rid'
    , 

    prodsId: 'o_rpid'
    , 

    startStCd: 'o_start_stop'
    , 

    endStCd: 'o_end_stop'
    , 

    startStVer: 'o_start_ver'
    , 

    endStVer: 'o_end_ver'
    , 

    userId: 'o_mid'
    , 

    boardCd: 'o_type'
    , 

    boardShapeCd: 'o_shape'
    , 

    seatId: 'o_seat'
    , 

    startDay: 'o_start_day'
    , 

    endDay: 'o_end_day'
    , 

    applyId: 'o_aid'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    , 

    rtCd: 'rt_ty'
    , 

    targetCd: 'target'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_board');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' board_view board');
    
      ar.select("board.board_id");

    
    
      ar.select("board.rt_id");

    
    
      ar.select("board.prods_id");

    
    
      ar.select("board.start_st_cd");

    
    
      ar.select("board.end_st_cd");

    
    
      ar.select("board.start_st_ver");

    
    
      ar.select("board.end_st_ver");

    
    
      ar.select("board.user_id");

    
    
      ar.select("board.board_cd");

    
    
      ar.select("board.board_shape_cd");

    
    
      ar.select("board.seat_id");

    
    
      ar.select("board.start_day");

    
    
      ar.select("board.end_day");

    
    
      ar.select("board.apply_id");

    
    
      ar.select("board.created_at");

    
    
      ar.select("board.updated_at");

    
    
      ar.select("board.rt_cd");

    
    
      ar.select("board.target_cd");

    
    
    return ar;
  }

  

  
}
export const BoardSql =  new BoardQuery()

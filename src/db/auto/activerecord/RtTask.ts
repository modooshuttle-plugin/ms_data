import { Ar, nullCheck } from "../../../util";
 
  import { IRtTask } from "../interface";


  class RtTaskQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 rt_task객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_view rt_task');

    
      if (alias === true) {
        ar.select("rt_task.rt_task_id as 'rt_task.rt_task_id'" );

      } else{
        ar.select("rt_task.rt_task_id");

      }
      
      if (alias === true) {
        ar.select("rt_task.rt_id as 'rt_task.rt_id'" );

      } else{
        ar.select("rt_task.rt_id");

      }
      
      if (alias === true) {
        ar.select("rt_task.title as 'rt_task.title'" );

      } else{
        ar.select("rt_task.title");

      }
      
      if (alias === true) {
        ar.select("rt_task.start_day as 'rt_task.start_day'" );

      } else{
        ar.select("rt_task.start_day");

      }
      
      if (alias === true) {
        ar.select("rt_task.end_day as 'rt_task.end_day'" );

      } else{
        ar.select("rt_task.end_day");

      }
      
      if (alias === true) {
        ar.select("rt_task.admin_id as 'rt_task.admin_id'" );

      } else{
        ar.select("rt_task.admin_id");

      }
      
      if (alias === true) {
        ar.select("rt_task.task_cd as 'rt_task.task_cd'" );

      } else{
        ar.select("rt_task.task_cd");

      }
      
      if (alias === true) {
        ar.select("rt_task.created_at as 'rt_task.created_at'" );

      } else{
        ar.select("rt_task.created_at");

      }
      
      if (alias === true) {
        ar.select("rt_task.updated_at as 'rt_task.updated_at'" );

      } else{
        ar.select("rt_task.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_rt_task_id = 'rt_task.rt_task_id';
        if (alias !== undefined) {

          col_rt_task_id = `${alias}.rt_task_id`;

        }

        ar.select(`${col_rt_task_id} as '${col_rt_task_id}' `);

         
        let col_rt_id = 'rt_task.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_title = 'rt_task.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_start_day = 'rt_task.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'rt_task.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_admin_id = 'rt_task.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

         
        let col_task_cd = 'rt_task.task_cd';
        if (alias !== undefined) {

          col_task_cd = `${alias}.task_cd`;

        }

        ar.select(`${col_task_cd} as '${col_task_cd}' `);

         
        let col_created_at = 'rt_task.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'rt_task.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_task');
    
    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.taskCd) === true) {
      ar.set("task_cd", form.taskCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' rt_task');
    
    if (nullCheck(form.rtTaskId) === true) {
      ar.set("rt_task_id", form.rtTaskId);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("rt_id", form.rtId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    

    if (nullCheck(form.taskCd) === true) {
      ar.set("task_cd", form.taskCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_view rt_task');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    rtTaskId: 'rt_task_id'
    , 

    rtId: 'rt_id'
    , 

    title: 'title'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    adminId: 'admin_id'
    , 

    taskCd: 'task_cd'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('rt_task');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' rt_task_view rt_task');
    
      ar.select("rt_task.rt_task_id");

    
    
      ar.select("rt_task.rt_id");

    
    
      ar.select("rt_task.title");

    
    
      ar.select("rt_task.start_day");

    
    
      ar.select("rt_task.end_day");

    
    
      ar.select("rt_task.admin_id");

    
    
      ar.select("rt_task.task_cd");

    
    
      ar.select("rt_task.created_at");

    
    
      ar.select("rt_task.updated_at");

    
    
    return ar;
  }

  

  
}
export const RtTaskSql =  new RtTaskQuery()

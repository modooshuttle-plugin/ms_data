import { Ar, nullCheck } from "../../../util";
 
  import { IReaction } from "../interface";


  class ReactionQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 reaction객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' reaction_view reaction');

    
      if (alias === true) {
        ar.select("reaction.reaction_id as 'reaction.reaction_id'" );

      } else{
        ar.select("reaction.reaction_id");

      }
      
      if (alias === true) {
        ar.select("reaction.user_id as 'reaction.user_id'" );

      } else{
        ar.select("reaction.user_id");

      }
      
      if (alias === true) {
        ar.select("reaction.board_id as 'reaction.board_id'" );

      } else{
        ar.select("reaction.board_id");

      }
      
      if (alias === true) {
        ar.select("reaction.reaction_cd as 'reaction.reaction_cd'" );

      } else{
        ar.select("reaction.reaction_cd");

      }
      
      if (alias === true) {
        ar.select("reaction.rt_id as 'reaction.rt_id'" );

      } else{
        ar.select("reaction.rt_id");

      }
      
      if (alias === true) {
        ar.select("reaction.created_at as 'reaction.created_at'" );

      } else{
        ar.select("reaction.created_at");

      }
      
      if (alias === true) {
        ar.select("reaction.updated_at as 'reaction.updated_at'" );

      } else{
        ar.select("reaction.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_reaction_id = 'reaction.reaction_id';
        if (alias !== undefined) {

          col_reaction_id = `${alias}.reaction_id`;

        }

        ar.select(`${col_reaction_id} as '${col_reaction_id}' `);

         
        let col_user_id = 'reaction.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_board_id = 'reaction.board_id';
        if (alias !== undefined) {

          col_board_id = `${alias}.board_id`;

        }

        ar.select(`${col_board_id} as '${col_board_id}' `);

         
        let col_reaction_cd = 'reaction.reaction_cd';
        if (alias !== undefined) {

          col_reaction_cd = `${alias}.reaction_cd`;

        }

        ar.select(`${col_reaction_cd} as '${col_reaction_cd}' `);

         
        let col_rt_id = 'reaction.rt_id';
        if (alias !== undefined) {

          col_rt_id = `${alias}.rt_id`;

        }

        ar.select(`${col_rt_id} as '${col_rt_id}' `);

         
        let col_created_at = 'reaction.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'reaction.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_reaction');
    
    if (nullCheck(form.reactionId) === true) {
      ar.set("o_no", form.reactionId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.reactionCd) === true) {
      ar.set("o_type", form.reactionCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' o_reaction');
    
    if (nullCheck(form.reactionId) === true) {
      ar.set("o_no", form.reactionId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("o_mid", form.userId);
    } 
    

    if (nullCheck(form.boardId) === true) {
      ar.set("o_oid", form.boardId);
    } 
    

    if (nullCheck(form.reactionCd) === true) {
      ar.set("o_type", form.reactionCd);
    } 
    

    if (nullCheck(form.rtId) === true) {
      ar.set("o_rid", form.rtId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("o_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("o_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' reaction_view reaction');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    reactionId: 'o_no'
    , 

    userId: 'o_mid'
    , 

    boardId: 'o_oid'
    , 

    reactionCd: 'o_type'
    , 

    rtId: 'o_rid'
    , 

    createdAt: 'o_timestamp'
    , 

    updatedAt: 'o_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('o_reaction');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' reaction_view reaction');
    
      ar.select("reaction.reaction_id");

    
    
      ar.select("reaction.user_id");

    
    
      ar.select("reaction.board_id");

    
    
      ar.select("reaction.reaction_cd");

    
    
      ar.select("reaction.rt_id");

    
    
      ar.select("reaction.created_at");

    
    
      ar.select("reaction.updated_at");

    
    
    return ar;
  }

  

  
}
export const ReactionSql =  new ReactionQuery()

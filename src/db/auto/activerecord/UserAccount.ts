import { Ar, nullCheck } from "../../../util";
 
  import { IUserAccount } from "../interface";


  class UserAccountQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 user_account객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' user_account_view user_account');

    
      if (alias === true) {
        ar.select("user_account.user_account_id as 'user_account.user_account_id'" );

      } else{
        ar.select("user_account.user_account_id");

      }
      
      if (alias === true) {
        ar.select("user_account.user_id as 'user_account.user_id'" );

      } else{
        ar.select("user_account.user_id");

      }
      
      if (alias === true) {
        ar.select("user_account.id as 'user_account.id'" );

      } else{
        ar.select("user_account.id");

      }
      
      if (alias === true) {
        ar.select("user_account.pw as 'user_account.pw'" );

      } else{
        ar.select("user_account.pw");

      }
      
      if (alias === true) {
        ar.select("user_account.provider_cd as 'user_account.provider_cd'" );

      } else{
        ar.select("user_account.provider_cd");

      }
      
      if (alias === true) {
        ar.select("user_account.created_at as 'user_account.created_at'" );

      } else{
        ar.select("user_account.created_at");

      }
      
      if (alias === true) {
        ar.select("user_account.updated_at as 'user_account.updated_at'" );

      } else{
        ar.select("user_account.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_user_account_id = 'user_account.user_account_id';
        if (alias !== undefined) {

          col_user_account_id = `${alias}.user_account_id`;

        }

        ar.select(`${col_user_account_id} as '${col_user_account_id}' `);

         
        let col_user_id = 'user_account.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_id = 'user_account.id';
        if (alias !== undefined) {

          col_id = `${alias}.id`;

        }

        ar.select(`${col_id} as '${col_id}' `);

         
        let col_pw = 'user_account.pw';
        if (alias !== undefined) {

          col_pw = `${alias}.pw`;

        }

        ar.select(`${col_pw} as '${col_pw}' `);

         
        let col_provider_cd = 'user_account.provider_cd';
        if (alias !== undefined) {

          col_provider_cd = `${alias}.provider_cd`;

        }

        ar.select(`${col_provider_cd} as '${col_provider_cd}' `);

         
        let col_created_at = 'user_account.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'user_account.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_account');
    
    if (nullCheck(form.userAccountId) === true) {
      ar.set("m_no", form.userAccountId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.id) === true) {
      ar.set("m_id", form.id);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("m_pw", form.pw);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("m_type", form.providerCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' m_account');
    
    if (nullCheck(form.userAccountId) === true) {
      ar.set("m_no", form.userAccountId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("m_mid", form.userId);
    } 
    

    if (nullCheck(form.id) === true) {
      ar.set("m_id", form.id);
    } 
    

    if (nullCheck(form.pw) === true) {
      ar.set("m_pw", form.pw);
    } 
    

    if (nullCheck(form.providerCd) === true) {
      ar.set("m_type", form.providerCd);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("m_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("m_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' user_account_view user_account');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    userAccountId: 'm_no'
    , 

    userId: 'm_mid'
    , 

    id: 'm_id'
    , 

    pw: 'm_pw'
    , 

    providerCd: 'm_type'
    , 

    createdAt: 'm_timestamp'
    , 

    updatedAt: 'm_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('m_account');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' user_account_view user_account');
    
      ar.select("user_account.user_account_id");

    
    
      ar.select("user_account.user_id");

    
    
      ar.select("user_account.id");

    
    
      ar.select("user_account.pw");

    
    
      ar.select("user_account.provider_cd");

    
    
      ar.select("user_account.created_at");

    
    
      ar.select("user_account.updated_at");

    
    
    return ar;
  }

  

  
}
export const UserAccountSql =  new UserAccountQuery()

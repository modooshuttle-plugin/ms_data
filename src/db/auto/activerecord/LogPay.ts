import { Ar, nullCheck } from "../../../util";
 
  import { ILogPay } from "../interface";


  class LogPayQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_pay객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_pay_view log_pay');

    
      if (alias === true) {
        ar.select("log_pay.log_pay_id as 'log_pay.log_pay_id'" );

      } else{
        ar.select("log_pay.log_pay_id");

      }
      
      if (alias === true) {
        ar.select("log_pay.evt_id as 'log_pay.evt_id'" );

      } else{
        ar.select("log_pay.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_pay.action_cd as 'log_pay.action_cd'" );

      } else{
        ar.select("log_pay.action_cd");

      }
      
      if (alias === true) {
        ar.select("log_pay.status_cd as 'log_pay.status_cd'" );

      } else{
        ar.select("log_pay.status_cd");

      }
      
      if (alias === true) {
        ar.select("log_pay.channel_cd as 'log_pay.channel_cd'" );

      } else{
        ar.select("log_pay.channel_cd");

      }
      
      if (alias === true) {
        ar.select("log_pay.imp_uid as 'log_pay.imp_uid'" );

      } else{
        ar.select("log_pay.imp_uid");

      }
      
      if (alias === true) {
        ar.select("log_pay.order_no as 'log_pay.order_no'" );

      } else{
        ar.select("log_pay.order_no");

      }
      
      if (alias === true) {
        ar.select("log_pay.method_cd as 'log_pay.method_cd'" );

      } else{
        ar.select("log_pay.method_cd");

      }
      
      if (alias === true) {
        ar.select("log_pay.user_id as 'log_pay.user_id'" );

      } else{
        ar.select("log_pay.user_id");

      }
      
      if (alias === true) {
        ar.select("log_pay.pay_id as 'log_pay.pay_id'" );

      } else{
        ar.select("log_pay.pay_id");

      }
      
      if (alias === true) {
        ar.select("log_pay.amount as 'log_pay.amount'" );

      } else{
        ar.select("log_pay.amount");

      }
      
      if (alias === true) {
        ar.select("log_pay.created_at as 'log_pay.created_at'" );

      } else{
        ar.select("log_pay.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_pay_id = 'log_pay.log_pay_id';
        if (alias !== undefined) {

          col_log_pay_id = `${alias}.log_pay_id`;

        }

        ar.select(`${col_log_pay_id} as '${col_log_pay_id}' `);

         
        let col_evt_id = 'log_pay.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_action_cd = 'log_pay.action_cd';
        if (alias !== undefined) {

          col_action_cd = `${alias}.action_cd`;

        }

        ar.select(`${col_action_cd} as '${col_action_cd}' `);

         
        let col_status_cd = 'log_pay.status_cd';
        if (alias !== undefined) {

          col_status_cd = `${alias}.status_cd`;

        }

        ar.select(`${col_status_cd} as '${col_status_cd}' `);

         
        let col_channel_cd = 'log_pay.channel_cd';
        if (alias !== undefined) {

          col_channel_cd = `${alias}.channel_cd`;

        }

        ar.select(`${col_channel_cd} as '${col_channel_cd}' `);

         
        let col_imp_uid = 'log_pay.imp_uid';
        if (alias !== undefined) {

          col_imp_uid = `${alias}.imp_uid`;

        }

        ar.select(`${col_imp_uid} as '${col_imp_uid}' `);

         
        let col_order_no = 'log_pay.order_no';
        if (alias !== undefined) {

          col_order_no = `${alias}.order_no`;

        }

        ar.select(`${col_order_no} as '${col_order_no}' `);

         
        let col_method_cd = 'log_pay.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_user_id = 'log_pay.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_pay_id = 'log_pay.pay_id';
        if (alias !== undefined) {

          col_pay_id = `${alias}.pay_id`;

        }

        ar.select(`${col_pay_id} as '${col_pay_id}' `);

         
        let col_amount = 'log_pay.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_created_at = 'log_pay.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_pay');
    
    if (nullCheck(form.logPayId) === true) {
      ar.set("l_no", form.logPayId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.actionCd) === true) {
      ar.set("l_action", form.actionCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("l_type", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("l_channel", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("l_imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("l_muid", form.orderNo);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("l_method", form.methodCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("l_pid", form.payId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("l_value", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_pay');
    
    if (nullCheck(form.logPayId) === true) {
      ar.set("l_no", form.logPayId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.actionCd) === true) {
      ar.set("l_action", form.actionCd);
    } 
    

    if (nullCheck(form.statusCd) === true) {
      ar.set("l_type", form.statusCd);
    } 
    

    if (nullCheck(form.channelCd) === true) {
      ar.set("l_channel", form.channelCd);
    } 
    

    if (nullCheck(form.impUid) === true) {
      ar.set("l_imp_uid", form.impUid);
    } 
    

    if (nullCheck(form.orderNo) === true) {
      ar.set("l_muid", form.orderNo);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("l_method", form.methodCd);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.payId) === true) {
      ar.set("l_pid", form.payId);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("l_value", form.amount);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_pay_view log_pay');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logPayId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    actionCd: 'l_action'
    , 

    statusCd: 'l_type'
    , 

    channelCd: 'l_channel'
    , 

    impUid: 'l_imp_uid'
    , 

    orderNo: 'l_muid'
    , 

    methodCd: 'l_method'
    , 

    userId: 'l_mid'
    , 

    payId: 'l_pid'
    , 

    amount: 'l_value'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_pay');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_pay_view log_pay');
    
      ar.select("log_pay.log_pay_id");

    
    
      ar.select("log_pay.evt_id");

    
    
      ar.select("log_pay.action_cd");

    
    
      ar.select("log_pay.status_cd");

    
    
      ar.select("log_pay.channel_cd");

    
    
      ar.select("log_pay.imp_uid");

    
    
      ar.select("log_pay.order_no");

    
    
      ar.select("log_pay.method_cd");

    
    
      ar.select("log_pay.user_id");

    
    
      ar.select("log_pay.pay_id");

    
    
      ar.select("log_pay.amount");

    
    
      ar.select("log_pay.created_at");

    
    
    return ar;
  }

  

  
}
export const LogPaySql =  new LogPayQuery()

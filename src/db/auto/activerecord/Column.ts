import { Ar, nullCheck } from "../../../util";
 
  import { IColumn } from "../interface";


  class ColumnQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 column객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' column_view column');

    
      if (alias === true) {
        ar.select("column.column_id as 'column.column_id'" );

      } else{
        ar.select("column.column_id");

      }
      
      if (alias === true) {
        ar.select("column.table_id as 'column.table_id'" );

      } else{
        ar.select("column.table_id");

      }
      
      if (alias === true) {
        ar.select("column.nm as 'column.nm'" );

      } else{
        ar.select("column.nm");

      }
      
      if (alias === true) {
        ar.select("column.old_nm as 'column.old_nm'" );

      } else{
        ar.select("column.old_nm");

      }
      
      if (alias === true) {
        ar.select("column.type as 'column.type'" );

      } else{
        ar.select("column.type");

      }
      
      if (alias === true) {
        ar.select("column.kor_nm as 'column.kor_nm'" );

      } else{
        ar.select("column.kor_nm");

      }
      
      if (alias === true) {
        ar.select("column.ver as 'column.ver'" );

      } else{
        ar.select("column.ver");

      }
      
      if (alias === true) {
        ar.select("column.comment as 'column.comment'" );

      } else{
        ar.select("column.comment");

      }
      
      if (alias === true) {
        ar.select("column.db as 'column.db'" );

      } else{
        ar.select("column.db");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_column_id = 'column.column_id';
        if (alias !== undefined) {

          col_column_id = `${alias}.column_id`;

        }

        ar.select(`${col_column_id} as '${col_column_id}' `);

         
        let col_table_id = 'column.table_id';
        if (alias !== undefined) {

          col_table_id = `${alias}.table_id`;

        }

        ar.select(`${col_table_id} as '${col_table_id}' `);

         
        let col_nm = 'column.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_old_nm = 'column.old_nm';
        if (alias !== undefined) {

          col_old_nm = `${alias}.old_nm`;

        }

        ar.select(`${col_old_nm} as '${col_old_nm}' `);

         
        let col_type = 'column.type';
        if (alias !== undefined) {

          col_type = `${alias}.type`;

        }

        ar.select(`${col_type} as '${col_type}' `);

         
        let col_kor_nm = 'column.kor_nm';
        if (alias !== undefined) {

          col_kor_nm = `${alias}.kor_nm`;

        }

        ar.select(`${col_kor_nm} as '${col_kor_nm}' `);

         
        let col_ver = 'column.ver';
        if (alias !== undefined) {

          col_ver = `${alias}.ver`;

        }

        ar.select(`${col_ver} as '${col_ver}' `);

         
        let col_comment = 'column.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_db = 'column.db';
        if (alias !== undefined) {

          col_db = `${alias}.db`;

        }

        ar.select(`${col_db} as '${col_db}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' column');
    
    if (nullCheck(form.columnId) === true) {
      ar.set("column_id", form.columnId);
    } 
    

    if (nullCheck(form.tableId) === true) {
      ar.set("table_id", form.tableId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.oldNm) === true) {
      ar.set("old_nm", form.oldNm);
    } 
    

    if (nullCheck(form.type) === true) {
      ar.set("type", form.type);
    } 
    

    if (nullCheck(form.korNm) === true) {
      ar.set("kor_nm", form.korNm);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.db) === true) {
      ar.set("db", form.db);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' column');
    
    if (nullCheck(form.columnId) === true) {
      ar.set("column_id", form.columnId);
    } 
    

    if (nullCheck(form.tableId) === true) {
      ar.set("table_id", form.tableId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.oldNm) === true) {
      ar.set("old_nm", form.oldNm);
    } 
    

    if (nullCheck(form.type) === true) {
      ar.set("type", form.type);
    } 
    

    if (nullCheck(form.korNm) === true) {
      ar.set("kor_nm", form.korNm);
    } 
    

    if (nullCheck(form.ver) === true) {
      ar.set("ver", form.ver);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.db) === true) {
      ar.set("db", form.db);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' column_view column');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    columnId: 'column_id'
    , 

    tableId: 'table_id'
    , 

    nm: 'nm'
    , 

    oldNm: 'old_nm'
    , 

    type: 'type'
    , 

    korNm: 'kor_nm'
    , 

    ver: 'ver'
    , 

    comment: 'comment'
    , 

    db: 'db'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('column');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' column_view column');
    
      ar.select("column.column_id");

    
    
      ar.select("column.table_id");

    
    
      ar.select("column.nm");

    
    
      ar.select("column.old_nm");

    
    
      ar.select("column.type");

    
    
      ar.select("column.kor_nm");

    
    
      ar.select("column.ver");

    
    
      ar.select("column.comment");

    
    
      ar.select("column.db");

    
    
    return ar;
  }

  

  
}
export const ColumnSql =  new ColumnQuery()

import { Ar, nullCheck } from "../../../util";
 
  import { ILogEvt } from "../interface";


  class LogEvtQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_evt객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_evt_view log_evt');

    
      if (alias === true) {
        ar.select("log_evt.log_evt_id as 'log_evt.log_evt_id'" );

      } else{
        ar.select("log_evt.log_evt_id");

      }
      
      if (alias === true) {
        ar.select("log_evt.evt_id as 'log_evt.evt_id'" );

      } else{
        ar.select("log_evt.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_evt.obj as 'log_evt.obj'" );

      } else{
        ar.select("log_evt.obj");

      }
      
      if (alias === true) {
        ar.select("log_evt.obj_dtl as 'log_evt.obj_dtl'" );

      } else{
        ar.select("log_evt.obj_dtl");

      }
      
      if (alias === true) {
        ar.select("log_evt.action as 'log_evt.action'" );

      } else{
        ar.select("log_evt.action");

      }
      
      if (alias === true) {
        ar.select("log_evt.target as 'log_evt.target'" );

      } else{
        ar.select("log_evt.target");

      }
      
      if (alias === true) {
        ar.select("log_evt.target_id as 'log_evt.target_id'" );

      } else{
        ar.select("log_evt.target_id");

      }
      
      if (alias === true) {
        ar.select("log_evt.created_at as 'log_evt.created_at'" );

      } else{
        ar.select("log_evt.created_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_evt_id = 'log_evt.log_evt_id';
        if (alias !== undefined) {

          col_log_evt_id = `${alias}.log_evt_id`;

        }

        ar.select(`${col_log_evt_id} as '${col_log_evt_id}' `);

         
        let col_evt_id = 'log_evt.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_obj = 'log_evt.obj';
        if (alias !== undefined) {

          col_obj = `${alias}.obj`;

        }

        ar.select(`${col_obj} as '${col_obj}' `);

         
        let col_obj_dtl = 'log_evt.obj_dtl';
        if (alias !== undefined) {

          col_obj_dtl = `${alias}.obj_dtl`;

        }

        ar.select(`${col_obj_dtl} as '${col_obj_dtl}' `);

         
        let col_action = 'log_evt.action';
        if (alias !== undefined) {

          col_action = `${alias}.action`;

        }

        ar.select(`${col_action} as '${col_action}' `);

         
        let col_target = 'log_evt.target';
        if (alias !== undefined) {

          col_target = `${alias}.target`;

        }

        ar.select(`${col_target} as '${col_target}' `);

         
        let col_target_id = 'log_evt.target_id';
        if (alias !== undefined) {

          col_target_id = `${alias}.target_id`;

        }

        ar.select(`${col_target_id} as '${col_target_id}' `);

         
        let col_created_at = 'log_evt.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_event');
    
    if (nullCheck(form.logEvtId) === true) {
      ar.set("l_no", form.logEvtId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.obj) === true) {
      ar.set("l_obj", form.obj);
    } 
    

    if (nullCheck(form.objDtl) === true) {
      ar.set("l_obj_detail", form.objDtl);
    } 
    

    if (nullCheck(form.action) === true) {
      ar.set("l_action", form.action);
    } 
    

    if (nullCheck(form.target) === true) {
      ar.set("l_type", form.target);
    } 
    

    if (nullCheck(form.targetId) === true) {
      ar.set("l_id", form.targetId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_event');
    
    if (nullCheck(form.logEvtId) === true) {
      ar.set("l_no", form.logEvtId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.obj) === true) {
      ar.set("l_obj", form.obj);
    } 
    

    if (nullCheck(form.objDtl) === true) {
      ar.set("l_obj_detail", form.objDtl);
    } 
    

    if (nullCheck(form.action) === true) {
      ar.set("l_action", form.action);
    } 
    

    if (nullCheck(form.target) === true) {
      ar.set("l_type", form.target);
    } 
    

    if (nullCheck(form.targetId) === true) {
      ar.set("l_id", form.targetId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_evt_view log_evt');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logEvtId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    obj: 'l_obj'
    , 

    objDtl: 'l_obj_detail'
    , 

    action: 'l_action'
    , 

    target: 'l_type'
    , 

    targetId: 'l_id'
    , 

    createdAt: 'l_timestamp'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_event');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_evt_view log_evt');
    
      ar.select("log_evt.log_evt_id");

    
    
      ar.select("log_evt.evt_id");

    
    
      ar.select("log_evt.obj");

    
    
      ar.select("log_evt.obj_dtl");

    
    
      ar.select("log_evt.action");

    
    
      ar.select("log_evt.target");

    
    
      ar.select("log_evt.target_id");

    
    
      ar.select("log_evt.created_at");

    
    
    return ar;
  }

  

  
}
export const LogEvtSql =  new LogEvtQuery()

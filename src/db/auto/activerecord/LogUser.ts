import { Ar, nullCheck } from "../../../util";
 
  import { ILogUser } from "../interface";


  class LogUserQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 log_user객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' log_user_view log_user');

    
      if (alias === true) {
        ar.select("log_user.log_user_id as 'log_user.log_user_id'" );

      } else{
        ar.select("log_user.log_user_id");

      }
      
      if (alias === true) {
        ar.select("log_user.evt_id as 'log_user.evt_id'" );

      } else{
        ar.select("log_user.evt_id");

      }
      
      if (alias === true) {
        ar.select("log_user.user_id as 'log_user.user_id'" );

      } else{
        ar.select("log_user.user_id");

      }
      
      if (alias === true) {
        ar.select("log_user.phone as 'log_user.phone'" );

      } else{
        ar.select("log_user.phone");

      }
      
      if (alias === true) {
        ar.select("log_user.nm as 'log_user.nm'" );

      } else{
        ar.select("log_user.nm");

      }
      
      if (alias === true) {
        ar.select("log_user.email as 'log_user.email'" );

      } else{
        ar.select("log_user.email");

      }
      
      if (alias === true) {
        ar.select("log_user.org_id as 'log_user.org_id'" );

      } else{
        ar.select("log_user.org_id");

      }
      
      if (alias === true) {
        ar.select("log_user.created_at as 'log_user.created_at'" );

      } else{
        ar.select("log_user.created_at");

      }
      
      if (alias === true) {
        ar.select("log_user.user_cd as 'log_user.user_cd'" );

      } else{
        ar.select("log_user.user_cd");

      }
      
      if (alias === true) {
        ar.select("log_user.active_cd as 'log_user.active_cd'" );

      } else{
        ar.select("log_user.active_cd");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_log_user_id = 'log_user.log_user_id';
        if (alias !== undefined) {

          col_log_user_id = `${alias}.log_user_id`;

        }

        ar.select(`${col_log_user_id} as '${col_log_user_id}' `);

         
        let col_evt_id = 'log_user.evt_id';
        if (alias !== undefined) {

          col_evt_id = `${alias}.evt_id`;

        }

        ar.select(`${col_evt_id} as '${col_evt_id}' `);

         
        let col_user_id = 'log_user.user_id';
        if (alias !== undefined) {

          col_user_id = `${alias}.user_id`;

        }

        ar.select(`${col_user_id} as '${col_user_id}' `);

         
        let col_phone = 'log_user.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_nm = 'log_user.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_email = 'log_user.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_org_id = 'log_user.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_created_at = 'log_user.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_user_cd = 'log_user.user_cd';
        if (alias !== undefined) {

          col_user_cd = `${alias}.user_cd`;

        }

        ar.select(`${col_user_cd} as '${col_user_cd}' `);

         
        let col_active_cd = 'log_user.active_cd';
        if (alias !== undefined) {

          col_active_cd = `${alias}.active_cd`;

        }

        ar.select(`${col_active_cd} as '${col_active_cd}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_auth');
    
    if (nullCheck(form.logUserId) === true) {
      ar.set("l_no", form.logUserId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("l_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("l_name", form.nm);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("l_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("l_company", form.orgId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.userCd) === true) {
      ar.set("l_type", form.userCd);
    } 
    

    if (nullCheck(form.activeCd) === true) {
      ar.set("l_act", form.activeCd);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' l_auth');
    
    if (nullCheck(form.logUserId) === true) {
      ar.set("l_no", form.logUserId);
    } 
    

    if (nullCheck(form.evtId) === true) {
      ar.set("l_eid", form.evtId);
    } 
    

    if (nullCheck(form.userId) === true) {
      ar.set("l_mid", form.userId);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("l_phone", form.phone);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("l_name", form.nm);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("l_email", form.email);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("l_company", form.orgId);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("l_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.userCd) === true) {
      ar.set("l_type", form.userCd);
    } 
    

    if (nullCheck(form.activeCd) === true) {
      ar.set("l_act", form.activeCd);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' log_user_view log_user');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    logUserId: 'l_no'
    , 

    evtId: 'l_eid'
    , 

    userId: 'l_mid'
    , 

    phone: 'l_phone'
    , 

    nm: 'l_name'
    , 

    email: 'l_email'
    , 

    orgId: 'l_company'
    , 

    createdAt: 'l_timestamp'
    , 

    userCd: 'l_type'
    , 

    activeCd: 'l_act'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('l_auth');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' log_user_view log_user');
    
      ar.select("log_user.log_user_id");

    
    
      ar.select("log_user.evt_id");

    
    
      ar.select("log_user.user_id");

    
    
      ar.select("log_user.phone");

    
    
      ar.select("log_user.nm");

    
    
      ar.select("log_user.email");

    
    
      ar.select("log_user.org_id");

    
    
      ar.select("log_user.created_at");

    
    
      ar.select("log_user.user_cd");

    
    
      ar.select("log_user.active_cd");

    
    
    return ar;
  }

  

  
}
export const LogUserSql =  new LogUserQuery()

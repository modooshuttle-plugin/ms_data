import { Ar, nullCheck } from "../../../util";
 
  import { IDrDoc } from "../interface";


  class DrDocQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 dr_doc객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' dr_doc_view dr_doc');

    
      if (alias === true) {
        ar.select("dr_doc.dr_doc_id as 'dr_doc.dr_doc_id'" );

      } else{
        ar.select("dr_doc.dr_doc_id");

      }
      
      if (alias === true) {
        ar.select("dr_doc.dr_id as 'dr_doc.dr_id'" );

      } else{
        ar.select("dr_doc.dr_id");

      }
      
      if (alias === true) {
        ar.select("dr_doc.path_folder as 'dr_doc.path_folder'" );

      } else{
        ar.select("dr_doc.path_folder");

      }
      
      if (alias === true) {
        ar.select("dr_doc.dr_doc_cd as 'dr_doc.dr_doc_cd'" );

      } else{
        ar.select("dr_doc.dr_doc_cd");

      }
      
      if (alias === true) {
        ar.select("dr_doc.start_day as 'dr_doc.start_day'" );

      } else{
        ar.select("dr_doc.start_day");

      }
      
      if (alias === true) {
        ar.select("dr_doc.end_day as 'dr_doc.end_day'" );

      } else{
        ar.select("dr_doc.end_day");

      }
      
      if (alias === true) {
        ar.select("dr_doc.created_at as 'dr_doc.created_at'" );

      } else{
        ar.select("dr_doc.created_at");

      }
      
      if (alias === true) {
        ar.select("dr_doc.updated_at as 'dr_doc.updated_at'" );

      } else{
        ar.select("dr_doc.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_dr_doc_id = 'dr_doc.dr_doc_id';
        if (alias !== undefined) {

          col_dr_doc_id = `${alias}.dr_doc_id`;

        }

        ar.select(`${col_dr_doc_id} as '${col_dr_doc_id}' `);

         
        let col_dr_id = 'dr_doc.dr_id';
        if (alias !== undefined) {

          col_dr_id = `${alias}.dr_id`;

        }

        ar.select(`${col_dr_id} as '${col_dr_id}' `);

         
        let col_path_folder = 'dr_doc.path_folder';
        if (alias !== undefined) {

          col_path_folder = `${alias}.path_folder`;

        }

        ar.select(`${col_path_folder} as '${col_path_folder}' `);

         
        let col_dr_doc_cd = 'dr_doc.dr_doc_cd';
        if (alias !== undefined) {

          col_dr_doc_cd = `${alias}.dr_doc_cd`;

        }

        ar.select(`${col_dr_doc_cd} as '${col_dr_doc_cd}' `);

         
        let col_start_day = 'dr_doc.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'dr_doc.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_created_at = 'dr_doc.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'dr_doc.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_doc');
    
    if (nullCheck(form.drDocId) === true) {
      ar.set("d_no", form.drDocId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.pathFolder) === true) {
      ar.set("d_path", form.pathFolder);
    } 
    

    if (nullCheck(form.drDocCd) === true) {
      ar.set("d_tid", form.drDocCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("d_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("d_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' d_doc');
    
    if (nullCheck(form.drDocId) === true) {
      ar.set("d_no", form.drDocId);
    } 
    

    if (nullCheck(form.drId) === true) {
      ar.set("d_did", form.drId);
    } 
    

    if (nullCheck(form.pathFolder) === true) {
      ar.set("d_path", form.pathFolder);
    } 
    

    if (nullCheck(form.drDocCd) === true) {
      ar.set("d_tid", form.drDocCd);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("d_start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("d_end_day", form.endDay);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("d_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("d_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_doc_view dr_doc');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    drDocId: 'd_no'
    , 

    drId: 'd_did'
    , 

    pathFolder: 'd_path'
    , 

    drDocCd: 'd_tid'
    , 

    startDay: 'd_start_day'
    , 

    endDay: 'd_end_day'
    , 

    createdAt: 'd_timestamp'
    , 

    updatedAt: 'd_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('d_doc');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' dr_doc_view dr_doc');
    
      ar.select("dr_doc.dr_doc_id");

    
    
      ar.select("dr_doc.dr_id");

    
    
      ar.select("dr_doc.path_folder");

    
    
      ar.select("dr_doc.dr_doc_cd");

    
    
      ar.select("dr_doc.start_day");

    
    
      ar.select("dr_doc.end_day");

    
    
      ar.select("dr_doc.created_at");

    
    
      ar.select("dr_doc.updated_at");

    
    
    return ar;
  }

  

  
}
export const DrDocSql =  new DrDocQuery()

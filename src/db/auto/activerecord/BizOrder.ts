import { Ar, nullCheck } from "../../../util";
 
  import { IBizOrder } from "../interface";


  class BizOrderQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 biz_order객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_view biz_order');

    
      if (alias === true) {
        ar.select("biz_order.biz_order_id as 'biz_order.biz_order_id'" );

      } else{
        ar.select("biz_order.biz_order_id");

      }
      
      if (alias === true) {
        ar.select("biz_order.notion as 'biz_order.notion'" );

      } else{
        ar.select("biz_order.notion");

      }
      
      if (alias === true) {
        ar.select("biz_order.created_at as 'biz_order.created_at'" );

      } else{
        ar.select("biz_order.created_at");

      }
      
      if (alias === true) {
        ar.select("biz_order.apply as 'biz_order.apply'" );

      } else{
        ar.select("biz_order.apply");

      }
      
      if (alias === true) {
        ar.select("biz_order.phone as 'biz_order.phone'" );

      } else{
        ar.select("biz_order.phone");

      }
      
      if (alias === true) {
        ar.select("biz_order.email as 'biz_order.email'" );

      } else{
        ar.select("biz_order.email");

      }
      
      if (alias === true) {
        ar.select("biz_order.order_cd as 'biz_order.order_cd'" );

      } else{
        ar.select("biz_order.order_cd");

      }
      
      if (alias === true) {
        ar.select("biz_order.path_cd as 'biz_order.path_cd'" );

      } else{
        ar.select("biz_order.path_cd");

      }
      
      if (alias === true) {
        ar.select("biz_order.path_comment as 'biz_order.path_comment'" );

      } else{
        ar.select("biz_order.path_comment");

      }
      
      if (alias === true) {
        ar.select("biz_order.method_cd as 'biz_order.method_cd'" );

      } else{
        ar.select("biz_order.method_cd");

      }
      
      if (alias === true) {
        ar.select("biz_order.updated_at as 'biz_order.updated_at'" );

      } else{
        ar.select("biz_order.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_biz_order_id = 'biz_order.biz_order_id';
        if (alias !== undefined) {

          col_biz_order_id = `${alias}.biz_order_id`;

        }

        ar.select(`${col_biz_order_id} as '${col_biz_order_id}' `);

         
        let col_notion = 'biz_order.notion';
        if (alias !== undefined) {

          col_notion = `${alias}.notion`;

        }

        ar.select(`${col_notion} as '${col_notion}' `);

         
        let col_created_at = 'biz_order.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_apply = 'biz_order.apply';
        if (alias !== undefined) {

          col_apply = `${alias}.apply`;

        }

        ar.select(`${col_apply} as '${col_apply}' `);

         
        let col_phone = 'biz_order.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_email = 'biz_order.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_order_cd = 'biz_order.order_cd';
        if (alias !== undefined) {

          col_order_cd = `${alias}.order_cd`;

        }

        ar.select(`${col_order_cd} as '${col_order_cd}' `);

         
        let col_path_cd = 'biz_order.path_cd';
        if (alias !== undefined) {

          col_path_cd = `${alias}.path_cd`;

        }

        ar.select(`${col_path_cd} as '${col_path_cd}' `);

         
        let col_path_comment = 'biz_order.path_comment';
        if (alias !== undefined) {

          col_path_comment = `${alias}.path_comment`;

        }

        ar.select(`${col_path_comment} as '${col_path_comment}' `);

         
        let col_method_cd = 'biz_order.method_cd';
        if (alias !== undefined) {

          col_method_cd = `${alias}.method_cd`;

        }

        ar.select(`${col_method_cd} as '${col_method_cd}' `);

         
        let col_updated_at = 'biz_order.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' biz_order');
    
    if (nullCheck(form.bizOrderId) === true) {
      ar.set("biz_order_id", form.bizOrderId);
    } 
    

    if (nullCheck(form.notion) === true) {
      ar.set("notion", form.notion);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.apply) === true) {
      ar.set("apply", form.apply);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("email", form.email);
    } 
    

    if (nullCheck(form.orderCd) === true) {
      ar.set("order_cd", form.orderCd);
    } 
    

    if (nullCheck(form.pathCd) === true) {
      ar.set("path_cd", form.pathCd);
    } 
    

    if (nullCheck(form.pathComment) === true) {
      ar.set("path_comment", form.pathComment);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' biz_order');
    
    if (nullCheck(form.bizOrderId) === true) {
      ar.set("biz_order_id", form.bizOrderId);
    } 
    

    if (nullCheck(form.notion) === true) {
      ar.set("notion", form.notion);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.apply) === true) {
      ar.set("apply", form.apply);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("email", form.email);
    } 
    

    if (nullCheck(form.orderCd) === true) {
      ar.set("order_cd", form.orderCd);
    } 
    

    if (nullCheck(form.pathCd) === true) {
      ar.set("path_cd", form.pathCd);
    } 
    

    if (nullCheck(form.pathComment) === true) {
      ar.set("path_comment", form.pathComment);
    } 
    

    if (nullCheck(form.methodCd) === true) {
      ar.set("method_cd", form.methodCd);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_view biz_order');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    bizOrderId: 'biz_order_id'
    , 

    notion: 'notion'
    , 

    createdAt: 'created_at'
    , 

    apply: 'apply'
    , 

    phone: 'phone'
    , 

    email: 'email'
    , 

    orderCd: 'order_cd'
    , 

    pathCd: 'path_cd'
    , 

    pathComment: 'path_comment'
    , 

    methodCd: 'method_cd'
    , 

    updatedAt: 'updated_at'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('biz_order');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' biz_order_view biz_order');
    
      ar.select("biz_order.biz_order_id");

    
    
      ar.select("biz_order.notion");

    
    
      ar.select("biz_order.created_at");

    
    
      ar.select("biz_order.apply");

    
    
      ar.select("biz_order.phone");

    
    
      ar.select("biz_order.email");

    
    
      ar.select("biz_order.order_cd");

    
    
      ar.select("biz_order.path_cd");

    
    
      ar.select("biz_order.path_comment");

    
    
      ar.select("biz_order.method_cd");

    
    
      ar.select("biz_order.updated_at");

    
    
    return ar;
  }

  

  
}
export const BizOrderSql =  new BizOrderQuery()

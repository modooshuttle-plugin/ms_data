import { Ar, nullCheck } from "../../../util";
 
  import { ICatSig } from "../interface";


  class CatSigQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 cat_sig객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' cat_sig_view cat_sig');

    
      if (alias === true) {
        ar.select("cat_sig.cat_sig_id as 'cat_sig.cat_sig_id'" );

      } else{
        ar.select("cat_sig.cat_sig_id");

      }
      
      if (alias === true) {
        ar.select("cat_sig.cat_id as 'cat_sig.cat_id'" );

      } else{
        ar.select("cat_sig.cat_id");

      }
      
      if (alias === true) {
        ar.select("cat_sig.sig_cd as 'cat_sig.sig_cd'" );

      } else{
        ar.select("cat_sig.sig_cd");

      }
      
      if (alias === true) {
        ar.select("cat_sig.alias as 'cat_sig.alias'" );

      } else{
        ar.select("cat_sig.alias");

      }
      
      if (alias === true) {
        ar.select("cat_sig.created_at as 'cat_sig.created_at'" );

      } else{
        ar.select("cat_sig.created_at");

      }
      
      if (alias === true) {
        ar.select("cat_sig.updated_at as 'cat_sig.updated_at'" );

      } else{
        ar.select("cat_sig.updated_at");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_cat_sig_id = 'cat_sig.cat_sig_id';
        if (alias !== undefined) {

          col_cat_sig_id = `${alias}.cat_sig_id`;

        }

        ar.select(`${col_cat_sig_id} as '${col_cat_sig_id}' `);

         
        let col_cat_id = 'cat_sig.cat_id';
        if (alias !== undefined) {

          col_cat_id = `${alias}.cat_id`;

        }

        ar.select(`${col_cat_id} as '${col_cat_id}' `);

         
        let col_sig_cd = 'cat_sig.sig_cd';
        if (alias !== undefined) {

          col_sig_cd = `${alias}.sig_cd`;

        }

        ar.select(`${col_sig_cd} as '${col_sig_cd}' `);

         
        let col_alias = 'cat_sig.alias';
        if (alias !== undefined) {

          col_alias = `${alias}.alias`;

        }

        ar.select(`${col_alias} as '${col_alias}' `);

         
        let col_created_at = 'cat_sig.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'cat_sig.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' c_sig');
    
    if (nullCheck(form.catSigId) === true) {
      ar.set("c_no", form.catSigId);
    } 
    

    if (nullCheck(form.catId) === true) {
      ar.set("c_uniq", form.catId);
    } 
    

    if (nullCheck(form.sigCd) === true) {
      ar.set("c_sig_cd", form.sigCd);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("c_alias", form.alias);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("c_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("c_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' c_sig');
    
    if (nullCheck(form.catSigId) === true) {
      ar.set("c_no", form.catSigId);
    } 
    

    if (nullCheck(form.catId) === true) {
      ar.set("c_uniq", form.catId);
    } 
    

    if (nullCheck(form.sigCd) === true) {
      ar.set("c_sig_cd", form.sigCd);
    } 
    

    if (nullCheck(form.alias) === true) {
      ar.set("c_alias", form.alias);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("c_timestamp", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("c_timestamp_u", form.updatedAt);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' cat_sig_view cat_sig');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    catSigId: 'c_no'
    , 

    catId: 'c_uniq'
    , 

    sigCd: 'c_sig_cd'
    , 

    alias: 'c_alias'
    , 

    createdAt: 'c_timestamp'
    , 

    updatedAt: 'c_timestamp_u'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('c_sig');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' cat_sig_view cat_sig');
    
      ar.select("cat_sig.cat_sig_id");

    
    
      ar.select("cat_sig.cat_id");

    
    
      ar.select("cat_sig.sig_cd");

    
    
      ar.select("cat_sig.alias");

    
    
      ar.select("cat_sig.created_at");

    
    
      ar.select("cat_sig.updated_at");

    
    
    return ar;
  }

  

  
}
export const CatSigSql =  new CatSigQuery()

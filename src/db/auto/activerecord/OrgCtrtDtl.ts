import { Ar, nullCheck } from "../../../util";
 
  import { IOrgCtrtDtl } from "../interface";


  class OrgCtrtDtlQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 org_ctrt_dtl객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_dtl_view org_ctrt_dtl');

    
      if (alias === true) {
        ar.select("org_ctrt_dtl.org_ctrt_dtl_id as 'org_ctrt_dtl.org_ctrt_dtl_id'" );

      } else{
        ar.select("org_ctrt_dtl.org_ctrt_dtl_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.org_ctrt_id as 'org_ctrt_dtl.org_ctrt_id'" );

      } else{
        ar.select("org_ctrt_dtl.org_ctrt_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.org_id as 'org_ctrt_dtl.org_id'" );

      } else{
        ar.select("org_ctrt_dtl.org_id");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.title as 'org_ctrt_dtl.title'" );

      } else{
        ar.select("org_ctrt_dtl.title");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.start_day as 'org_ctrt_dtl.start_day'" );

      } else{
        ar.select("org_ctrt_dtl.start_day");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.end_day as 'org_ctrt_dtl.end_day'" );

      } else{
        ar.select("org_ctrt_dtl.end_day");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.amount as 'org_ctrt_dtl.amount'" );

      } else{
        ar.select("org_ctrt_dtl.amount");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.comment as 'org_ctrt_dtl.comment'" );

      } else{
        ar.select("org_ctrt_dtl.comment");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.setl_cd as 'org_ctrt_dtl.setl_cd'" );

      } else{
        ar.select("org_ctrt_dtl.setl_cd");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.biz_cd as 'org_ctrt_dtl.biz_cd'" );

      } else{
        ar.select("org_ctrt_dtl.biz_cd");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.prods_cd as 'org_ctrt_dtl.prods_cd'" );

      } else{
        ar.select("org_ctrt_dtl.prods_cd");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.subscribe_cd as 'org_ctrt_dtl.subscribe_cd'" );

      } else{
        ar.select("org_ctrt_dtl.subscribe_cd");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.url as 'org_ctrt_dtl.url'" );

      } else{
        ar.select("org_ctrt_dtl.url");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.created_at as 'org_ctrt_dtl.created_at'" );

      } else{
        ar.select("org_ctrt_dtl.created_at");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.updated_at as 'org_ctrt_dtl.updated_at'" );

      } else{
        ar.select("org_ctrt_dtl.updated_at");

      }
      
      if (alias === true) {
        ar.select("org_ctrt_dtl.admin_id as 'org_ctrt_dtl.admin_id'" );

      } else{
        ar.select("org_ctrt_dtl.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_org_ctrt_dtl_id = 'org_ctrt_dtl.org_ctrt_dtl_id';
        if (alias !== undefined) {

          col_org_ctrt_dtl_id = `${alias}.org_ctrt_dtl_id`;

        }

        ar.select(`${col_org_ctrt_dtl_id} as '${col_org_ctrt_dtl_id}' `);

         
        let col_org_ctrt_id = 'org_ctrt_dtl.org_ctrt_id';
        if (alias !== undefined) {

          col_org_ctrt_id = `${alias}.org_ctrt_id`;

        }

        ar.select(`${col_org_ctrt_id} as '${col_org_ctrt_id}' `);

         
        let col_org_id = 'org_ctrt_dtl.org_id';
        if (alias !== undefined) {

          col_org_id = `${alias}.org_id`;

        }

        ar.select(`${col_org_id} as '${col_org_id}' `);

         
        let col_title = 'org_ctrt_dtl.title';
        if (alias !== undefined) {

          col_title = `${alias}.title`;

        }

        ar.select(`${col_title} as '${col_title}' `);

         
        let col_start_day = 'org_ctrt_dtl.start_day';
        if (alias !== undefined) {

          col_start_day = `${alias}.start_day`;

        }

        ar.select(`${col_start_day} as '${col_start_day}' `);

         
        let col_end_day = 'org_ctrt_dtl.end_day';
        if (alias !== undefined) {

          col_end_day = `${alias}.end_day`;

        }

        ar.select(`${col_end_day} as '${col_end_day}' `);

         
        let col_amount = 'org_ctrt_dtl.amount';
        if (alias !== undefined) {

          col_amount = `${alias}.amount`;

        }

        ar.select(`${col_amount} as '${col_amount}' `);

         
        let col_comment = 'org_ctrt_dtl.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_setl_cd = 'org_ctrt_dtl.setl_cd';
        if (alias !== undefined) {

          col_setl_cd = `${alias}.setl_cd`;

        }

        ar.select(`${col_setl_cd} as '${col_setl_cd}' `);

         
        let col_biz_cd = 'org_ctrt_dtl.biz_cd';
        if (alias !== undefined) {

          col_biz_cd = `${alias}.biz_cd`;

        }

        ar.select(`${col_biz_cd} as '${col_biz_cd}' `);

         
        let col_prods_cd = 'org_ctrt_dtl.prods_cd';
        if (alias !== undefined) {

          col_prods_cd = `${alias}.prods_cd`;

        }

        ar.select(`${col_prods_cd} as '${col_prods_cd}' `);

         
        let col_subscribe_cd = 'org_ctrt_dtl.subscribe_cd';
        if (alias !== undefined) {

          col_subscribe_cd = `${alias}.subscribe_cd`;

        }

        ar.select(`${col_subscribe_cd} as '${col_subscribe_cd}' `);

         
        let col_url = 'org_ctrt_dtl.url';
        if (alias !== undefined) {

          col_url = `${alias}.url`;

        }

        ar.select(`${col_url} as '${col_url}' `);

         
        let col_created_at = 'org_ctrt_dtl.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'org_ctrt_dtl.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_admin_id = 'org_ctrt_dtl.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_dtl');
    
    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.setlCd) === true) {
      ar.set("setl_cd", form.setlCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("biz_cd", form.bizCd);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("prods_cd", form.prodsCd);
    } 
    

    if (nullCheck(form.subscribeCd) === true) {
      ar.set("subscribe_cd", form.subscribeCd);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("url", form.url);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_dtl');
    
    if (nullCheck(form.orgCtrtDtlId) === true) {
      ar.set("org_ctrt_dtl_id", form.orgCtrtDtlId);
    } 
    

    if (nullCheck(form.orgCtrtId) === true) {
      ar.set("org_ctrt_id", form.orgCtrtId);
    } 
    

    if (nullCheck(form.orgId) === true) {
      ar.set("org_id", form.orgId);
    } 
    

    if (nullCheck(form.title) === true) {
      ar.set("title", form.title);
    } 
    

    if (nullCheck(form.startDay) === true) {
      ar.set("start_day", form.startDay);
    } 
    

    if (nullCheck(form.endDay) === true) {
      ar.set("end_day", form.endDay);
    } 
    

    if (nullCheck(form.amount) === true) {
      ar.set("amount", form.amount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.setlCd) === true) {
      ar.set("setl_cd", form.setlCd);
    } 
    

    if (nullCheck(form.bizCd) === true) {
      ar.set("biz_cd", form.bizCd);
    } 
    

    if (nullCheck(form.prodsCd) === true) {
      ar.set("prods_cd", form.prodsCd);
    } 
    

    if (nullCheck(form.subscribeCd) === true) {
      ar.set("subscribe_cd", form.subscribeCd);
    } 
    

    if (nullCheck(form.url) === true) {
      ar.set("url", form.url);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_dtl_view org_ctrt_dtl');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    orgCtrtDtlId: 'org_ctrt_dtl_id'
    , 

    orgCtrtId: 'org_ctrt_id'
    , 

    orgId: 'org_id'
    , 

    title: 'title'
    , 

    startDay: 'start_day'
    , 

    endDay: 'end_day'
    , 

    amount: 'amount'
    , 

    comment: 'comment'
    , 

    setlCd: 'setl_cd'
    , 

    bizCd: 'biz_cd'
    , 

    prodsCd: 'prods_cd'
    , 

    subscribeCd: 'subscribe_cd'
    , 

    url: 'url'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('org_ctrt_dtl');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' org_ctrt_dtl_view org_ctrt_dtl');
    
      ar.select("org_ctrt_dtl.org_ctrt_dtl_id");

    
    
      ar.select("org_ctrt_dtl.org_ctrt_id");

    
    
      ar.select("org_ctrt_dtl.org_id");

    
    
      ar.select("org_ctrt_dtl.title");

    
    
      ar.select("org_ctrt_dtl.start_day");

    
    
      ar.select("org_ctrt_dtl.end_day");

    
    
      ar.select("org_ctrt_dtl.amount");

    
    
      ar.select("org_ctrt_dtl.comment");

    
    
      ar.select("org_ctrt_dtl.setl_cd");

    
    
      ar.select("org_ctrt_dtl.biz_cd");

    
    
      ar.select("org_ctrt_dtl.prods_cd");

    
    
      ar.select("org_ctrt_dtl.subscribe_cd");

    
    
      ar.select("org_ctrt_dtl.url");

    
    
      ar.select("org_ctrt_dtl.created_at");

    
    
      ar.select("org_ctrt_dtl.updated_at");

    
    
      ar.select("org_ctrt_dtl.admin_id");

    
    
    return ar;
  }

  

  
}
export const OrgCtrtDtlSql =  new OrgCtrtDtlQuery()

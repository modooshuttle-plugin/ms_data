import { Ar, nullCheck } from "../../../util";
 
  import { IBusCompn } from "../interface";


  class BusCompnQuery {
    
  /**
   * 선택된 테이블 및 컬럼 정보를 불러온다. 
   * @param alias true일 경우에 bus_compn객체 안에 넣어서 돌려준다. 보통 조인할 테이블이 있을 경우 true로 지정하자. 기본값은 undefined
   * @returns 
   */
  select = (alias?: boolean): Ar => {
    const ar = new Ar();
    ar.from(' bus_compn_view bus_compn');

    
      if (alias === true) {
        ar.select("bus_compn.bus_compn_id as 'bus_compn.bus_compn_id'" );

      } else{
        ar.select("bus_compn.bus_compn_id");

      }
      
      if (alias === true) {
        ar.select("bus_compn.nm as 'bus_compn.nm'" );

      } else{
        ar.select("bus_compn.nm");

      }
      
      if (alias === true) {
        ar.select("bus_compn.biz_nm as 'bus_compn.biz_nm'" );

      } else{
        ar.select("bus_compn.biz_nm");

      }
      
      if (alias === true) {
        ar.select("bus_compn.email as 'bus_compn.email'" );

      } else{
        ar.select("bus_compn.email");

      }
      
      if (alias === true) {
        ar.select("bus_compn.phone as 'bus_compn.phone'" );

      } else{
        ar.select("bus_compn.phone");

      }
      
      if (alias === true) {
        ar.select("bus_compn.created_at as 'bus_compn.created_at'" );

      } else{
        ar.select("bus_compn.created_at");

      }
      
      if (alias === true) {
        ar.select("bus_compn.updated_at as 'bus_compn.updated_at'" );

      } else{
        ar.select("bus_compn.updated_at");

      }
      
      if (alias === true) {
        ar.select("bus_compn.biz_reg_no as 'bus_compn.biz_reg_no'" );

      } else{
        ar.select("bus_compn.biz_reg_no");

      }
      
      if (alias === true) {
        ar.select("bus_compn.biz_addr as 'bus_compn.biz_addr'" );

      } else{
        ar.select("bus_compn.biz_addr");

      }
      
      if (alias === true) {
        ar.select("bus_compn.bank_nm as 'bus_compn.bank_nm'" );

      } else{
        ar.select("bus_compn.bank_nm");

      }
      
      if (alias === true) {
        ar.select("bus_compn.bank_account as 'bus_compn.bank_account'" );

      } else{
        ar.select("bus_compn.bank_account");

      }
      
      if (alias === true) {
        ar.select("bus_compn.comment as 'bus_compn.comment'" );

      } else{
        ar.select("bus_compn.comment");

      }
      
      if (alias === true) {
        ar.select("bus_compn.admin_id as 'bus_compn.admin_id'" );

      } else{
        ar.select("bus_compn.admin_id");

      }
      
    return ar;
  }
  
    
  /**
   * from 절이 아닌 join으로 추가된 테이블에 대해서 컬럼명을 불러올때 사용한다.
   */
  selectAlias = (ar: Ar, alias?: string) => {
    
     
        let col_bus_compn_id = 'bus_compn.bus_compn_id';
        if (alias !== undefined) {

          col_bus_compn_id = `${alias}.bus_compn_id`;

        }

        ar.select(`${col_bus_compn_id} as '${col_bus_compn_id}' `);

         
        let col_nm = 'bus_compn.nm';
        if (alias !== undefined) {

          col_nm = `${alias}.nm`;

        }

        ar.select(`${col_nm} as '${col_nm}' `);

         
        let col_biz_nm = 'bus_compn.biz_nm';
        if (alias !== undefined) {

          col_biz_nm = `${alias}.biz_nm`;

        }

        ar.select(`${col_biz_nm} as '${col_biz_nm}' `);

         
        let col_email = 'bus_compn.email';
        if (alias !== undefined) {

          col_email = `${alias}.email`;

        }

        ar.select(`${col_email} as '${col_email}' `);

         
        let col_phone = 'bus_compn.phone';
        if (alias !== undefined) {

          col_phone = `${alias}.phone`;

        }

        ar.select(`${col_phone} as '${col_phone}' `);

         
        let col_created_at = 'bus_compn.created_at';
        if (alias !== undefined) {

          col_created_at = `${alias}.created_at`;

        }

        ar.select(`${col_created_at} as '${col_created_at}' `);

         
        let col_updated_at = 'bus_compn.updated_at';
        if (alias !== undefined) {

          col_updated_at = `${alias}.updated_at`;

        }

        ar.select(`${col_updated_at} as '${col_updated_at}' `);

         
        let col_biz_reg_no = 'bus_compn.biz_reg_no';
        if (alias !== undefined) {

          col_biz_reg_no = `${alias}.biz_reg_no`;

        }

        ar.select(`${col_biz_reg_no} as '${col_biz_reg_no}' `);

         
        let col_biz_addr = 'bus_compn.biz_addr';
        if (alias !== undefined) {

          col_biz_addr = `${alias}.biz_addr`;

        }

        ar.select(`${col_biz_addr} as '${col_biz_addr}' `);

         
        let col_bank_nm = 'bus_compn.bank_nm';
        if (alias !== undefined) {

          col_bank_nm = `${alias}.bank_nm`;

        }

        ar.select(`${col_bank_nm} as '${col_bank_nm}' `);

         
        let col_bank_account = 'bus_compn.bank_account';
        if (alias !== undefined) {

          col_bank_account = `${alias}.bank_account`;

        }

        ar.select(`${col_bank_account} as '${col_bank_account}' `);

         
        let col_comment = 'bus_compn.comment';
        if (alias !== undefined) {

          col_comment = `${alias}.comment`;

        }

        ar.select(`${col_comment} as '${col_comment}' `);

         
        let col_admin_id = 'bus_compn.admin_id';
        if (alias !== undefined) {

          col_admin_id = `${alias}.admin_id`;

        }

        ar.select(`${col_admin_id} as '${col_admin_id}' `);

        
  }
  
    
  /**
   * set 입력 | 변경할 컬럼을 설정한다.
   * @form 
   */
  set = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' bus_compn');
    
    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.bizNm) === true) {
      ar.set("biz_nm", form.bizNm);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("email", form.email);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.bizRegNo) === true) {
      ar.set("biz_reg_no", form.bizRegNo);
    } 
    

    if (nullCheck(form.bizAddr) === true) {
      ar.set("biz_addr", form.bizAddr);
    } 
    

    if (nullCheck(form.bankNm) === true) {
      ar.set("bank_nm", form.bankNm);
    } 
    

    if (nullCheck(form.bankAccount) === true) {
      ar.set("bank_account", form.bankAccount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  /**
   *  @deprecated set을 사용하자
   */
  insert = (form: any): Ar => {
    const ar = new Ar();
    ar.from(' bus_compn');
    
    if (nullCheck(form.busCompnId) === true) {
      ar.set("bus_compn_id", form.busCompnId);
    } 
    

    if (nullCheck(form.nm) === true) {
      ar.set("nm", form.nm);
    } 
    

    if (nullCheck(form.bizNm) === true) {
      ar.set("biz_nm", form.bizNm);
    } 
    

    if (nullCheck(form.email) === true) {
      ar.set("email", form.email);
    } 
    

    if (nullCheck(form.phone) === true) {
      ar.set("phone", form.phone);
    } 
    

    if (nullCheck(form.createdAt) === true) {
      ar.set("created_at", form.createdAt);
    } 
    

    if (nullCheck(form.updatedAt) === true) {
      ar.set("updated_at", form.updatedAt);
    } 
    

    if (nullCheck(form.bizRegNo) === true) {
      ar.set("biz_reg_no", form.bizRegNo);
    } 
    

    if (nullCheck(form.bizAddr) === true) {
      ar.set("biz_addr", form.bizAddr);
    } 
    

    if (nullCheck(form.bankNm) === true) {
      ar.set("bank_nm", form.bankNm);
    } 
    

    if (nullCheck(form.bankAccount) === true) {
      ar.set("bank_account", form.bankAccount);
    } 
    

    if (nullCheck(form.comment) === true) {
      ar.set("comment", form.comment);
    } 
    

    if (nullCheck(form.adminId) === true) {
      ar.set("admin_id", form.adminId);
    } 
    
    return ar;
  }

  
    
  /**
   * 테이블 명: 신규 테이블 view 값으로 불러온다. getSelect로 해결이 안되는 경우만 사용하자.
   */
  table = (): Ar => {
    const ar = new Ar();
    ar.from(' bus_compn_view bus_compn');

    return ar;
  }
  
    
  /**
   *  신규컬럼을 선택하면 이전 컬럼명으로 변경된다. (insert, update 시 조건 걸때 주로 사용하면 될듯)
   */
  col = {
      
    busCompnId: 'bus_compn_id'
    , 

    nm: 'nm'
    , 

    bizNm: 'biz_nm'
    , 

    email: 'email'
    , 

    phone: 'phone'
    , 

    createdAt: 'created_at'
    , 

    updatedAt: 'updated_at'
    , 

    bizRegNo: 'biz_reg_no'
    , 

    bizAddr: 'biz_addr'
    , 

    bankNm: 'bank_nm'
    , 

    bankAccount: 'bank_account'
    , 

    comment: 'comment'
    , 

    adminId: 'admin_id'
    
  }
  
    
  /**
   * 이전 테이블명 (필요시 사용하자) 표준화 이후 deprecate 예정
   */
  tb = () => {
    const ar = new Ar();
    ar.from('bus_compn');

    return ar;
  }
  
    
  /**
   * 테이블에 컬럼이 정확하게 있는지 체크하려고 만들었음. 쿼리돌렸을때 컬럼이 없다면 생성해두지 않은것
   */
  check = (): Ar => {
    const ar = new Ar();
    ar.from(' bus_compn_view bus_compn');
    
      ar.select("bus_compn.bus_compn_id");

    
    
      ar.select("bus_compn.nm");

    
    
      ar.select("bus_compn.biz_nm");

    
    
      ar.select("bus_compn.email");

    
    
      ar.select("bus_compn.phone");

    
    
      ar.select("bus_compn.created_at");

    
    
      ar.select("bus_compn.updated_at");

    
    
      ar.select("bus_compn.biz_reg_no");

    
    
      ar.select("bus_compn.biz_addr");

    
    
      ar.select("bus_compn.bank_nm");

    
    
      ar.select("bus_compn.bank_account");

    
    
      ar.select("bus_compn.comment");

    
    
      ar.select("bus_compn.admin_id");

    
    
    return ar;
  }

  

  
}
export const BusCompnSql =  new BusCompnQuery()

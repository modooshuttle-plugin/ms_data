
	export class StModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['r_no'] !== undefined) {
			this._obj['stId'] = obj['r_no'];
		}
		if(obj['r_rid'] !== undefined) {
			this._obj['rtId'] = obj['r_rid'];
		}
		if(obj['r_se'] !== undefined) {
			this._obj['sectionCd'] = obj['r_se'];
		}
		if(obj['r_idx'] !== undefined) {
			this._obj['idx'] = obj['r_idx'];
		}
		if(obj['r_line_ver'] !== undefined) {
			this._obj['pathVer'] = obj['r_line_ver'];
		}
		if(obj['r_rsid'] !== undefined) {
			this._obj['stCd'] = obj['r_rsid'];
		}
		if(obj['r_addr'] !== undefined) {
			this._obj['addr'] = obj['r_addr'];
		}
		if(obj['r_alias'] !== undefined) {
			this._obj['alias'] = obj['r_alias'];
		}
		if(obj['r_lat'] !== undefined) {
			this._obj['lat'] = obj['r_lat'];
		}
		if(obj['r_lng'] !== undefined) {
			this._obj['lng'] = obj['r_lng'];
		}
		if(obj['r_time'] !== undefined) {
			this._obj['time'] = obj['r_time'];
		}
		if(obj['r_time_mon'] !== undefined) {
			this._obj['timeMon'] = obj['r_time_mon'];
		}
		if(obj['r_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['r_timestamp'];
		}
		if(obj['r_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['r_timestamp_u'];
		}
		if(obj['emd_cd'] !== undefined) {
			this._obj['emdCd'] = obj['emd_cd'];
		}
		if(obj['pan'] !== undefined) {
			this._obj['pan'] = obj['pan'];
		}
		if(obj['pan_lat'] !== undefined) {
			this._obj['panLat'] = obj['pan_lat'];
		}
		if(obj['pan_lng'] !== undefined) {
			this._obj['panLng'] = obj['pan_lng'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
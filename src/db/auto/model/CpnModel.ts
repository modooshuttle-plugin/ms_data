
	export class CpnModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['cpnId'] = obj['a_no'];
		}
		if(obj['a_temp'] !== undefined) {
			this._obj['cpnTplId'] = obj['a_temp'];
		}
		if(obj['a_name'] !== undefined) {
			this._obj['nm'] = obj['a_name'];
		}
		if(obj['a_type'] !== undefined) {
			this._obj['calCd'] = obj['a_type'];
		}
		if(obj['a_status'] !== undefined) {
			this._obj['statusCd'] = obj['a_status'];
		}
		if(obj['a_func'] !== undefined) {
			this._obj['func'] = obj['a_func'];
		}
		if(obj['a_value'] !== undefined) {
			this._obj['amount'] = obj['a_value'];
		}
		if(obj['a_sign'] !== undefined) {
			this._obj['signCd'] = obj['a_sign'];
		}
		if(obj['a_mid'] !== undefined) {
			this._obj['userId'] = obj['a_mid'];
		}
		if(obj['a_admin'] !== undefined) {
			this._obj['adminId'] = obj['a_admin'];
		}
		if(obj['a_mp_code'] !== undefined) {
			this._obj['promoCd'] = obj['a_mp_code'];
		}
		if(obj['a_start_day'] !== undefined) {
			this._obj['startDay'] = obj['a_start_day'];
		}
		if(obj['a_end_day'] !== undefined) {
			this._obj['endDay'] = obj['a_end_day'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['a_timestamp_u'];
		}
		if(obj['dupe_yn'] !== undefined) {
			this._obj['dupeYn'] = obj['dupe_yn'];
		}
		if(obj['min_default_amount'] !== undefined) {
			this._obj['minDefaultAmount'] = obj['min_default_amount'];
		}
		if(obj['use_cd'] !== undefined) {
			this._obj['useCd'] = obj['use_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
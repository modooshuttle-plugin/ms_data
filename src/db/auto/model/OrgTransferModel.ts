
	export class OrgTransferModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_transfer_id'] !== undefined) {
			this._obj['orgTransferId'] = obj['org_transfer_id'];
		}
		if(obj['prods_cd'] !== undefined) {
			this._obj['prodsCd'] = obj['prods_cd'];
		}
		if(obj['org_setl_id'] !== undefined) {
			this._obj['orgSetlId'] = obj['org_setl_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['transfer_at'] !== undefined) {
			this._obj['transferAt'] = obj['transfer_at'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class ScontrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['sc_no'] !== undefined) {
			this._obj['scontrId'] = obj['sc_no'];
		}
		if(obj['sc_info'] !== undefined) {
			this._obj['info'] = obj['sc_info'];
		}
		if(obj['sc_platform'] !== undefined) {
			this._obj['platform'] = obj['sc_platform'];
		}
		if(obj['sc_version'] !== undefined) {
			this._obj['ver'] = obj['sc_version'];
		}
		if(obj['sc_modal'] !== undefined) {
			this._obj['modal'] = obj['sc_modal'];
		}
		if(obj['sc_etc'] !== undefined) {
			this._obj['etc'] = obj['sc_etc'];
		}
		if(obj['sc_start'] !== undefined) {
			this._obj['start'] = obj['sc_start'];
		}
		if(obj['sc_end'] !== undefined) {
			this._obj['end'] = obj['sc_end'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
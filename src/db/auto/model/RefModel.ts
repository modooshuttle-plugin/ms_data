
	export class RefModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['refId'] = obj['o_no'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_omid'] !== undefined) {
			this._obj['otherUserId'] = obj['o_omid'];
		}
		if(obj['o_coupon'] !== undefined) {
			this._obj['cpnId'] = obj['o_coupon'];
		}
		if(obj['o_ocoupon'] !== undefined) {
			this._obj['otherCpnId'] = obj['o_ocoupon'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['o_deploy'] !== undefined) {
			this._obj['deployCd'] = obj['o_deploy'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class PopupModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['mb_no'] !== undefined) {
			this._obj['popupId'] = obj['mb_no'];
		}
		if(obj['mb_title'] !== undefined) {
			this._obj['title'] = obj['mb_title'];
		}
		if(obj['mb_start'] !== undefined) {
			this._obj['startDay'] = obj['mb_start'];
		}
		if(obj['mb_end'] !== undefined) {
			this._obj['endDay'] = obj['mb_end'];
		}
		if(obj['mb_background'] !== undefined) {
			this._obj['img'] = obj['mb_background'];
		}
		if(obj['mb_link'] !== undefined) {
			this._obj['link'] = obj['mb_link'];
		}
		if(obj['mb_admin'] !== undefined) {
			this._obj['adminId'] = obj['mb_admin'];
		}
		if(obj['mb_deploy'] !== undefined) {
			this._obj['deployCd'] = obj['mb_deploy'];
		}
		if(obj['mb_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['mb_timestamp'];
		}
		if(obj['mb_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['mb_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
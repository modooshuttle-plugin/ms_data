
	export class RdtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['rdtId'] = obj['a_no'];
		}
		if(obj['a_url'] !== undefined) {
			this._obj['url'] = obj['a_url'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_action'] !== undefined) {
			this._obj['actionCd'] = obj['a_action'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
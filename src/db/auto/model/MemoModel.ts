
	export class MemoModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['memoId'] = obj['a_no'];
		}
		if(obj['a_type'] !== undefined) {
			this._obj['memoCd'] = obj['a_type'];
		}
		if(obj['a_rid'] !== undefined) {
			this._obj['rtId'] = obj['a_rid'];
		}
		if(obj['a_rtype'] !== undefined) {
			this._obj['rtCd'] = obj['a_rtype'];
		}
		if(obj['a_mid'] !== undefined) {
			this._obj['userId'] = obj['a_mid'];
		}
		if(obj['a_text'] !== undefined) {
			this._obj['comment'] = obj['a_text'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['a_timestamp_u'];
		}
		if(obj['a_admin'] !== undefined) {
			this._obj['adminId'] = obj['a_admin'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DrIssDayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_iss_day_id'] !== undefined) {
			this._obj['drIssDayId'] = obj['dr_iss_day_id'];
		}
		if(obj['dr_iss_id'] !== undefined) {
			this._obj['drIssId'] = obj['dr_iss_id'];
		}
		if(obj['day'] !== undefined) {
			this._obj['day'] = obj['day'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
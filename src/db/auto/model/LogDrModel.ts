
	export class LogDrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logDrId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_did'] !== undefined) {
			this._obj['drId'] = obj['l_did'];
		}
		if(obj['l_name'] !== undefined) {
			this._obj['nm'] = obj['l_name'];
		}
		if(obj['l_phone'] !== undefined) {
			this._obj['phone'] = obj['l_phone'];
		}
		if(obj['l_phone_access'] !== undefined) {
			this._obj['phoneAccess'] = obj['l_phone_access'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['statusCd'] = obj['l_type'];
		}
		if(obj['l_auth'] !== undefined) {
			this._obj['drCd'] = obj['l_auth'];
		}
		if(obj['l_contract'] !== undefined) {
			this._obj['ctrtYn'] = obj['l_contract'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
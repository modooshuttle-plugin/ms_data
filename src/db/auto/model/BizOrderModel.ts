
	export class BizOrderModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['biz_order_id'] !== undefined) {
			this._obj['bizOrderId'] = obj['biz_order_id'];
		}
		if(obj['notion'] !== undefined) {
			this._obj['notion'] = obj['notion'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['apply'] !== undefined) {
			this._obj['apply'] = obj['apply'];
		}
		if(obj['phone'] !== undefined) {
			this._obj['phone'] = obj['phone'];
		}
		if(obj['email'] !== undefined) {
			this._obj['email'] = obj['email'];
		}
		if(obj['order_cd'] !== undefined) {
			this._obj['orderCd'] = obj['order_cd'];
		}
		if(obj['path_cd'] !== undefined) {
			this._obj['pathCd'] = obj['path_cd'];
		}
		if(obj['path_comment'] !== undefined) {
			this._obj['pathComment'] = obj['path_comment'];
		}
		if(obj['method_cd'] !== undefined) {
			this._obj['methodCd'] = obj['method_cd'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
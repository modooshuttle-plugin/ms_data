
	export class RunnCaldModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['runn_cald_id'] !== undefined) {
			this._obj['runnCaldId'] = obj['runn_cald_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['runn_cd'] !== undefined) {
			this._obj['runnCd'] = obj['runn_cd'];
		}
		if(obj['time_cd'] !== undefined) {
			this._obj['timeCd'] = obj['time_cd'];
		}
		if(obj['day'] !== undefined) {
			this._obj['day'] = obj['day'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
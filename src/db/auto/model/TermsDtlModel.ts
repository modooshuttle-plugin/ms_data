
	export class TermsDtlModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['mb_no'] !== undefined) {
			this._obj['termsDtlId'] = obj['mb_no'];
		}
		if(obj['mb_uniq'] !== undefined) {
			this._obj['termsId'] = obj['mb_uniq'];
		}
		if(obj['mb_deploy'] !== undefined) {
			this._obj['deployYn'] = obj['mb_deploy'];
		}
		if(obj['mb_date'] !== undefined) {
			this._obj['startDay'] = obj['mb_date'];
		}
		if(obj['mb_content'] !== undefined) {
			this._obj['comment'] = obj['mb_content'];
		}
		if(obj['mb_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['mb_timestamp'];
		}
		if(obj['mb_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['mb_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DrDeployModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['d_no'] !== undefined) {
			this._obj['drDeployId'] = obj['d_no'];
		}
		if(obj['d_did'] !== undefined) {
			this._obj['drId'] = obj['d_did'];
		}
		if(obj['d_tid'] !== undefined) {
			this._obj['deployCd'] = obj['d_tid'];
		}
		if(obj['d_status'] !== undefined) {
			this._obj['statusCd'] = obj['d_status'];
		}
		if(obj['d_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['d_timestamp'];
		}
		if(obj['d_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['d_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
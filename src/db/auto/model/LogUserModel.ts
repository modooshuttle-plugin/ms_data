
	export class LogUserModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logUserId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_phone'] !== undefined) {
			this._obj['phone'] = obj['l_phone'];
		}
		if(obj['l_name'] !== undefined) {
			this._obj['nm'] = obj['l_name'];
		}
		if(obj['l_email'] !== undefined) {
			this._obj['email'] = obj['l_email'];
		}
		if(obj['l_company'] !== undefined) {
			this._obj['orgId'] = obj['l_company'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['userCd'] = obj['l_type'];
		}
		if(obj['l_act'] !== undefined) {
			this._obj['activeCd'] = obj['l_act'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
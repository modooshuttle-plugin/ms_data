
	export class DrIssAmountModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_iss_amount_id'] !== undefined) {
			this._obj['drIssAmountId'] = obj['dr_iss_amount_id'];
		}
		if(obj['dr_iss_id'] !== undefined) {
			this._obj['drIssId'] = obj['dr_iss_id'];
		}
		if(obj['sign'] !== undefined) {
			this._obj['sign'] = obj['sign'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['iss_amount_cd'] !== undefined) {
			this._obj['issAmountCd'] = obj['iss_amount_cd'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
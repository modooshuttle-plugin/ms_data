
	export class RtMemberModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['rt_member_id'] !== undefined) {
			this._obj['rtMemberId'] = obj['rt_member_id'];
		}
		if(obj['rt_task_id'] !== undefined) {
			this._obj['rtTaskId'] = obj['rt_task_id'];
		}
		if(obj['rt_task_req_id'] !== undefined) {
			this._obj['rtTaskReqId'] = obj['rt_task_req_id'];
		}
		if(obj['pre_board_id'] !== undefined) {
			this._obj['preBoardId'] = obj['pre_board_id'];
		}
		if(obj['pre_rt_id'] !== undefined) {
			this._obj['preRtId'] = obj['pre_rt_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['pre_rt_cd'] !== undefined) {
			this._obj['preRtCd'] = obj['pre_rt_cd'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['task_id'] !== undefined) {
			this._obj['taskId'] = obj['task_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
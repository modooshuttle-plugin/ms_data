
	export class AppContrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['app_contr_id'] !== undefined) {
			this._obj['appContrId'] = obj['app_contr_id'];
		}
		if(obj['target_cd'] !== undefined) {
			this._obj['targetCd'] = obj['target_cd'];
		}
		if(obj['ver'] !== undefined) {
			this._obj['ver'] = obj['ver'];
		}
		if(obj['native_ver'] !== undefined) {
			this._obj['nativeVer'] = obj['native_ver'];
		}
		if(obj['os_cd'] !== undefined) {
			this._obj['osCd'] = obj['os_cd'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
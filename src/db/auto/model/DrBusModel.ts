
	export class DrBusModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['d_no'] !== undefined) {
			this._obj['drBusId'] = obj['d_no'];
		}
		if(obj['d_did'] !== undefined) {
			this._obj['drId'] = obj['d_did'];
		}
		if(obj['d_bid'] !== undefined) {
			this._obj['busId'] = obj['d_bid'];
		}
		if(obj['d_year'] !== undefined) {
			this._obj['year'] = obj['d_year'];
		}
		if(obj['d_info'] !== undefined) {
			this._obj['info'] = obj['d_info'];
		}
		if(obj['d_cno'] !== undefined) {
			this._obj['busNo'] = obj['d_cno'];
		}
		if(obj['d_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['d_timestamp'];
		}
		if(obj['d_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['d_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class QuestionModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['questionId'] = obj['o_no'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_rid'] !== undefined) {
			this._obj['rtId'] = obj['o_rid'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_comment'] !== undefined) {
			this._obj['comment'] = obj['o_comment'];
		}
		if(obj['o_confirm'] !== undefined) {
			this._obj['confirmCd'] = obj['o_confirm'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['o_admin'] !== undefined) {
			this._obj['adminId'] = obj['o_admin'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['questionCd'] = obj['o_type'];
		}
		if(obj['o_drv_confirm'] !== undefined) {
			this._obj['drConfirmCd'] = obj['o_drv_confirm'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class AdminModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['ad_no'] !== undefined) {
			this._obj['adminId'] = obj['ad_no'];
		}
		if(obj['ad_pw'] !== undefined) {
			this._obj['pw'] = obj['ad_pw'];
		}
		if(obj['ad_nick'] !== undefined) {
			this._obj['nm'] = obj['ad_nick'];
		}
		if(obj['ad_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['ad_timestamp'];
		}
		if(obj['ad_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['ad_timestamp_u'];
		}
		if(obj['ad_email'] !== undefined) {
			this._obj['email'] = obj['ad_email'];
		}
		if(obj['ad_phone'] !== undefined) {
			this._obj['phone'] = obj['ad_phone'];
		}
		if(obj['notion_user_id'] !== undefined) {
			this._obj['notionUserId'] = obj['notion_user_id'];
		}
		if(obj['admin_cd'] !== undefined) {
			this._obj['adminCd'] = obj['admin_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class OfferDrPayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['offer_dr_pay_id'] !== undefined) {
			this._obj['offerDrPayId'] = obj['offer_dr_pay_id'];
		}
		if(obj['offer_dr_id'] !== undefined) {
			this._obj['offerDrId'] = obj['offer_dr_id'];
		}
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['cond_cd'] !== undefined) {
			this._obj['condCd'] = obj['cond_cd'];
		}
		if(obj['prods_amount'] !== undefined) {
			this._obj['prodsAmount'] = obj['prods_amount'];
		}
		if(obj['discount_amount'] !== undefined) {
			this._obj['discountAmount'] = obj['discount_amount'];
		}
		if(obj['discount_prods_amount'] !== undefined) {
			this._obj['discountProdsAmount'] = obj['discount_prods_amount'];
		}
		if(obj['paid_cnt'] !== undefined) {
			this._obj['paidCnt'] = obj['paid_cnt'];
		}
		if(obj['dr_pay_amount'] !== undefined) {
			this._obj['drPayAmount'] = obj['dr_pay_amount'];
		}
		if(obj['diff_amount'] !== undefined) {
			this._obj['diffAmount'] = obj['diff_amount'];
		}
		if(obj['incnt_cnt'] !== undefined) {
			this._obj['incntCnt'] = obj['incnt_cnt'];
		}
		if(obj['min_paid_cnt'] !== undefined) {
			this._obj['minPaidCnt'] = obj['min_paid_cnt'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['incnt_amount'] !== undefined) {
			this._obj['incntAmount'] = obj['incnt_amount'];
		}
		if(obj['dr_amount'] !== undefined) {
			this._obj['drAmount'] = obj['dr_amount'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
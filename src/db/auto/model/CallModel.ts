
	export class CallModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['callId'] = obj['o_no'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_rid'] !== undefined) {
			this._obj['beforeRtId'] = obj['o_rid'];
		}
		if(obj['o_rrid'] !== undefined) {
			this._obj['rtId'] = obj['o_rrid'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['rtCd'] = obj['o_type'];
		}
		if(obj['o_status'] !== undefined) {
			this._obj['callCd'] = obj['o_status'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_callcnt'] !== undefined) {
			this._obj['idx'] = obj['o_callcnt'];
		}
		if(obj['o_calltime'] !== undefined) {
			this._obj['createdAt'] = obj['o_calltime'];
		}
		if(obj['o_callresult'] !== undefined) {
			this._obj['resultCd'] = obj['o_callresult'];
		}
		if(obj['o_callmbr'] !== undefined) {
			this._obj['adminId'] = obj['o_callmbr'];
		}
		if(obj['o_callmemo'] !== undefined) {
			this._obj['comment'] = obj['o_callmemo'];
		}
		if(obj['o_orid'] !== undefined) {
			this._obj['reasonId'] = obj['o_orid'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
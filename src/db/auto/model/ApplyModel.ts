
	export class ApplyModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['applyId'] = obj['o_no'];
		}
		if(obj['o_sid'] !== undefined) {
			this._obj['subscribeId'] = obj['o_sid'];
		}
		if(obj['o_rptype'] !== undefined) {
			this._obj['prodsCd'] = obj['o_rptype'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_start_day_init'] !== undefined) {
			this._obj['initStartDay'] = obj['o_start_day_init'];
		}
		if(obj['o_start_day'] !== undefined) {
			this._obj['startDay'] = obj['o_start_day'];
		}
		if(obj['o_end_day'] !== undefined) {
			this._obj['endDay'] = obj['o_end_day'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['apply_ty'] !== undefined) {
			this._obj['applyCd'] = obj['apply_ty'];
		}
		if(obj['prev_aid'] !== undefined) {
			this._obj['prevApplyId'] = obj['prev_aid'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
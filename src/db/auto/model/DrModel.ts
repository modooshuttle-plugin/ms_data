
	export class DrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['d_no'] !== undefined) {
			this._obj['drInfoId'] = obj['d_no'];
		}
		if(obj['d_did'] !== undefined) {
			this._obj['drId'] = obj['d_did'];
		}
		if(obj['d_name'] !== undefined) {
			this._obj['nm'] = obj['d_name'];
		}
		if(obj['d_phone'] !== undefined) {
			this._obj['phone'] = obj['d_phone'];
		}
		if(obj['d_phone_access'] !== undefined) {
			this._obj['phoneAccess'] = obj['d_phone_access'];
		}
		if(obj['d_type'] !== undefined) {
			this._obj['statusCd'] = obj['d_type'];
		}
		if(obj['d_auth'] !== undefined) {
			this._obj['drCd'] = obj['d_auth'];
		}
		if(obj['d_contract'] !== undefined) {
			this._obj['ctrtYn'] = obj['d_contract'];
		}
		if(obj['d_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['d_timestamp'];
		}
		if(obj['d_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['d_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
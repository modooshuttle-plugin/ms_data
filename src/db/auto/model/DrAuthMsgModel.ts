
	export class DrAuthMsgModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['d_no'] !== undefined) {
			this._obj['drAuthMsgId'] = obj['d_no'];
		}
		if(obj['d_phone'] !== undefined) {
			this._obj['phone'] = obj['d_phone'];
		}
		if(obj['d_auth_no'] !== undefined) {
			this._obj['authNo'] = obj['d_auth_no'];
		}
		if(obj['d_c_gid'] !== undefined) {
			this._obj['sendId'] = obj['d_c_gid'];
		}
		if(obj['d_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['d_timestamp'];
		}
		if(obj['d_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['d_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
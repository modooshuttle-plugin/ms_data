
	export class RtTaskReqModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['rt_task_req_id'] !== undefined) {
			this._obj['rtTaskReqId'] = obj['rt_task_req_id'];
		}
		if(obj['rt_task_id'] !== undefined) {
			this._obj['rtTaskId'] = obj['rt_task_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['req_cd'] !== undefined) {
			this._obj['reqCd'] = obj['req_cd'];
		}
		if(obj['title'] !== undefined) {
			this._obj['title'] = obj['title'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['req_status_cd'] !== undefined) {
			this._obj['reqStatusCd'] = obj['req_status_cd'];
		}
		if(obj['admin_ids'] !== undefined) {
			this._obj['adminIds'] = obj['admin_ids'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
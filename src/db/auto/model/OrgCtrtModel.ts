
	export class OrgCtrtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['ctrt_subj_cd'] !== undefined) {
			this._obj['ctrtSubjCd'] = obj['ctrt_subj_cd'];
		}
		if(obj['title'] !== undefined) {
			this._obj['title'] = obj['title'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
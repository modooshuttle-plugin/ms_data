
	export class UserAccountModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['m_no'] !== undefined) {
			this._obj['userAccountId'] = obj['m_no'];
		}
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_id'] !== undefined) {
			this._obj['id'] = obj['m_id'];
		}
		if(obj['m_pw'] !== undefined) {
			this._obj['pw'] = obj['m_pw'];
		}
		if(obj['m_type'] !== undefined) {
			this._obj['providerCd'] = obj['m_type'];
		}
		if(obj['m_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['m_timestamp'];
		}
		if(obj['m_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['m_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
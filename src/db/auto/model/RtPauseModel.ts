
	export class RtPauseModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['rt_pause_id'] !== undefined) {
			this._obj['rtPauseId'] = obj['rt_pause_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['pause_cd'] !== undefined) {
			this._obj['pauseCd'] = obj['pause_cd'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
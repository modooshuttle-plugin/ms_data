
	export class CpnLimitModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['cpn_limit_id'] !== undefined) {
			this._obj['cpnLimitId'] = obj['cpn_limit_id'];
		}
		if(obj['cpn_id'] !== undefined) {
			this._obj['cpnId'] = obj['cpn_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['limit_cd'] !== undefined) {
			this._obj['limitCd'] = obj['limit_cd'];
		}
		if(obj['target_id'] !== undefined) {
			this._obj['targetId'] = obj['target_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
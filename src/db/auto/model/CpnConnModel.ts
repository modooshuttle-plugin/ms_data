
	export class CpnConnModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['cpn_conn_id'] !== undefined) {
			this._obj['cpnConnId'] = obj['cpn_conn_id'];
		}
		if(obj['cpn_id'] !== undefined) {
			this._obj['cpnId'] = obj['cpn_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
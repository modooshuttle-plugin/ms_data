
	export class MakeModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['makeId'] = obj['o_no'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_home'] !== undefined) {
			this._obj['startAddr'] = obj['o_home'];
		}
		if(obj['o_home_lat'] !== undefined) {
			this._obj['startLat'] = obj['o_home_lat'];
		}
		if(obj['o_home_lng'] !== undefined) {
			this._obj['startLng'] = obj['o_home_lng'];
		}
		if(obj['o_work'] !== undefined) {
			this._obj['endAddr'] = obj['o_work'];
		}
		if(obj['o_work_lat'] !== undefined) {
			this._obj['endLat'] = obj['o_work_lat'];
		}
		if(obj['o_work_lng'] !== undefined) {
			this._obj['endLng'] = obj['o_work_lng'];
		}
		if(obj['o_tid'] !== undefined) {
			this._obj['timeId'] = obj['o_tid'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['o_home_title'] !== undefined) {
			this._obj['startTitle'] = obj['o_home_title'];
		}
		if(obj['o_work_title'] !== undefined) {
			this._obj['endTitle'] = obj['o_work_title'];
		}
		if(obj['o_request'] !== undefined) {
			this._obj['comment'] = obj['o_request'];
		}
		if(obj['o_email'] !== undefined) {
			this._obj['email'] = obj['o_email'];
		}
		if(obj['o_company'] !== undefined) {
			this._obj['orgId'] = obj['o_company'];
		}
		if(obj['o_home_c'] !== undefined) {
			this._obj['startCatCd'] = obj['o_home_c'];
		}
		if(obj['o_work_c'] !== undefined) {
			this._obj['endCatCd'] = obj['o_work_c'];
		}
		if(obj['o_confirm'] !== undefined) {
			this._obj['confirmYn'] = obj['o_confirm'];
		}
		if(obj['sta_emd_cd'] !== undefined) {
			this._obj['startEmdCd'] = obj['sta_emd_cd'];
		}
		if(obj['end_emd_cd'] !== undefined) {
			this._obj['endEmdCd'] = obj['end_emd_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
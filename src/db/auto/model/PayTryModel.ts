
	export class PayTryModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['p_no'] !== undefined) {
			this._obj['payTryId'] = obj['p_no'];
		}
		if(obj['p_muid'] !== undefined) {
			this._obj['orderNo'] = obj['p_muid'];
		}
		if(obj['p_pid'] !== undefined) {
			this._obj['payId'] = obj['p_pid'];
		}
		if(obj['p_rid'] !== undefined) {
			this._obj['rtId'] = obj['p_rid'];
		}
		if(obj['p_mid'] !== undefined) {
			this._obj['userId'] = obj['p_mid'];
		}
		if(obj['p_method'] !== undefined) {
			this._obj['methodCd'] = obj['p_method'];
		}
		if(obj['p_cash_receipt'] !== undefined) {
			this._obj['cashReceiptYn'] = obj['p_cash_receipt'];
		}
		if(obj['p_type'] !== undefined) {
			this._obj['statusCd'] = obj['p_type'];
		}
		if(obj['p_channel'] !== undefined) {
			this._obj['channelCd'] = obj['p_channel'];
		}
		if(obj['p_imp_uid'] !== undefined) {
			this._obj['impUid'] = obj['p_imp_uid'];
		}
		if(obj['p_provider'] !== undefined) {
			this._obj['providerCd'] = obj['p_provider'];
		}
		if(obj['p_value'] !== undefined) {
			this._obj['amount'] = obj['p_value'];
		}
		if(obj['p_cash_timestamp'] !== undefined) {
			this._obj['cashReceiptAt'] = obj['p_cash_timestamp'];
		}
		if(obj['p_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['p_timestamp'];
		}
		if(obj['p_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['p_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
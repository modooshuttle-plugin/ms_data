
	export class OrgEmailModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_email_id'] !== undefined) {
			this._obj['orgEmailId'] = obj['org_email_id'];
		}
		if(obj['org_user_auth_id'] !== undefined) {
			this._obj['orgUserAuthId'] = obj['org_user_auth_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['domain'] !== undefined) {
			this._obj['domain'] = obj['domain'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
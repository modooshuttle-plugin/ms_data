
	export class UserBankAccountModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['m_no'] !== undefined) {
			this._obj['userBankAccountId'] = obj['m_no'];
		}
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_bank'] !== undefined) {
			this._obj['bankCd'] = obj['m_bank'];
		}
		if(obj['m_number'] !== undefined) {
			this._obj['accountNo'] = obj['m_number'];
		}
		if(obj['m_name'] !== undefined) {
			this._obj['nm'] = obj['m_name'];
		}
		if(obj['m_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['m_timestamp'];
		}
		if(obj['m_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['m_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class EmailAuthModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['emailAuthId'] = obj['o_no'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_ano'] !== undefined) {
			this._obj['authId'] = obj['o_ano'];
		}
		if(obj['o_code'] !== undefined) {
			this._obj['emailCd'] = obj['o_code'];
		}
		if(obj['o_use'] !== undefined) {
			this._obj['useYn'] = obj['o_use'];
		}
		if(obj['o_cid'] !== undefined) {
			this._obj['cpnId'] = obj['o_cid'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['o_ko'] !== undefined) {
			this._obj['orgNm'] = obj['o_ko'];
		}
		if(obj['o_en'] !== undefined) {
			this._obj['domain'] = obj['o_en'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['rtCd'] = obj['o_type'];
		}
		if(obj['o_email'] !== undefined) {
			this._obj['emailAddr'] = obj['o_email'];
		}
		if(obj['org_user_auth_id'] !== undefined) {
			this._obj['orgUserAuthId'] = obj['org_user_auth_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class RefundModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['refund_id'] !== undefined) {
			this._obj['refundId'] = obj['refund_id'];
		}
		if(obj['paid_id'] !== undefined) {
			this._obj['paidId'] = obj['paid_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['order_no'] !== undefined) {
			this._obj['orderNo'] = obj['order_no'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['compn_cd'] !== undefined) {
			this._obj['compnCd'] = obj['compn_cd'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
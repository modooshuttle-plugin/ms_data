
	export class DrTermsAgreeModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_terms_agree_id'] !== undefined) {
			this._obj['drTermsAgreeId'] = obj['dr_terms_agree_id'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['agree_yn'] !== undefined) {
			this._obj['agreeYn'] = obj['agree_yn'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DrPayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['dispatch_id'] !== undefined) {
			this._obj['dispatchId'] = obj['dispatch_id'];
		}
		if(obj['start_setl_month'] !== undefined) {
			this._obj['startSetlMonth'] = obj['start_setl_month'];
		}
		if(obj['setl_start_day'] !== undefined) {
			this._obj['setlStartDay'] = obj['setl_start_day'];
		}
		if(obj['setl_day_cd'] !== undefined) {
			this._obj['setlDayCd'] = obj['setl_day_cd'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['bus_compn_id'] !== undefined) {
			this._obj['busCompnId'] = obj['bus_compn_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['borrow_setl_cd'] !== undefined) {
			this._obj['borrowSetlCd'] = obj['borrow_setl_cd'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
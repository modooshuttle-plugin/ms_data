
	export class EventModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['e_no'] !== undefined) {
			this._obj['eventId'] = obj['e_no'];
		}
		if(obj['e_title'] !== undefined) {
			this._obj['title'] = obj['e_title'];
		}
		if(obj['e_title_sub'] !== undefined) {
			this._obj['titleSub'] = obj['e_title_sub'];
		}
		if(obj['e_image_pc'] !== undefined) {
			this._obj['imagePc'] = obj['e_image_pc'];
		}
		if(obj['e_image_mobile'] !== undefined) {
			this._obj['imageMobile'] = obj['e_image_mobile'];
		}
		if(obj['e_detail_1_use'] !== undefined) {
			this._obj['detail_1Use'] = obj['e_detail_1_use'];
		}
		if(obj['e_detail_1_title'] !== undefined) {
			this._obj['detail_1Title'] = obj['e_detail_1_title'];
		}
		if(obj['e_detail_1_content'] !== undefined) {
			this._obj['detail_1Content'] = obj['e_detail_1_content'];
		}
		if(obj['e_detail_2_use'] !== undefined) {
			this._obj['detail_2Use'] = obj['e_detail_2_use'];
		}
		if(obj['e_detail_2_title'] !== undefined) {
			this._obj['detail_2Title'] = obj['e_detail_2_title'];
		}
		if(obj['e_detail_2_content'] !== undefined) {
			this._obj['detail_2Content'] = obj['e_detail_2_content'];
		}
		if(obj['e_detail_3_use'] !== undefined) {
			this._obj['detail_3Use'] = obj['e_detail_3_use'];
		}
		if(obj['e_detail_3_title'] !== undefined) {
			this._obj['detail_3Title'] = obj['e_detail_3_title'];
		}
		if(obj['e_detail_3_content'] !== undefined) {
			this._obj['detail_3Content'] = obj['e_detail_3_content'];
		}
		if(obj['e_btn_use'] !== undefined) {
			this._obj['btnUse'] = obj['e_btn_use'];
		}
		if(obj['e_btn_text'] !== undefined) {
			this._obj['btnText'] = obj['e_btn_text'];
		}
		if(obj['e_btn_link'] !== undefined) {
			this._obj['btnLink'] = obj['e_btn_link'];
		}
		if(obj['e_start'] !== undefined) {
			this._obj['startDay'] = obj['e_start'];
		}
		if(obj['e_end'] !== undefined) {
			this._obj['endDay'] = obj['e_end'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DrPayCondModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_pay_cond_id'] !== undefined) {
			this._obj['drPayCondId'] = obj['dr_pay_cond_id'];
		}
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['dispatch_id'] !== undefined) {
			this._obj['dispatchId'] = obj['dispatch_id'];
		}
		if(obj['start_setl_month'] !== undefined) {
			this._obj['startSetlMonth'] = obj['start_setl_month'];
		}
		if(obj['cond_cd'] !== undefined) {
			this._obj['condCd'] = obj['cond_cd'];
		}
		if(obj['min_cnt'] !== undefined) {
			this._obj['minCnt'] = obj['min_cnt'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
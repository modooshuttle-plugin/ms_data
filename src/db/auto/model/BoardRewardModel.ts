
	export class BoardRewardModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['board_reward_id'] !== undefined) {
			this._obj['boardRewardId'] = obj['board_reward_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['runn_iss_id'] !== undefined) {
			this._obj['runnIssId'] = obj['runn_iss_id'];
		}
		if(obj['board_iss_id'] !== undefined) {
			this._obj['boardIssId'] = obj['board_iss_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['status_cd'] !== undefined) {
			this._obj['statusCd'] = obj['status_cd'];
		}
		if(obj['reward_cd'] !== undefined) {
			this._obj['rewardCd'] = obj['reward_cd'];
		}
		if(obj['method_cd'] !== undefined) {
			this._obj['methodCd'] = obj['method_cd'];
		}
		if(obj['reward_dtl'] !== undefined) {
			this._obj['rewardDtl'] = obj['reward_dtl'];
		}
		if(obj['path_folder'] !== undefined) {
			this._obj['pathFolder'] = obj['path_folder'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
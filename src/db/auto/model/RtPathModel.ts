
	export class RtPathModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['rt_path_id'] !== undefined) {
			this._obj['rtPathId'] = obj['rt_path_id'];
		}
		if(obj['rid'] !== undefined) {
			this._obj['rtId'] = obj['rid'];
		}
		if(obj['ver'] !== undefined) {
			this._obj['pathVer'] = obj['ver'];
		}
		if(obj['section'] !== undefined) {
			this._obj['sectionCd'] = obj['section'];
		}
		if(obj['geom'] !== undefined) {
			this._obj['geom'] = obj['geom'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DrAppPushModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_app_push_id'] !== undefined) {
			this._obj['drAppPushId'] = obj['dr_app_push_id'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['app_push_token'] !== undefined) {
			this._obj['appPushToken'] = obj['app_push_token'];
		}
		if(obj['uuid'] !== undefined) {
			this._obj['uuid'] = obj['uuid'];
		}
		if(obj['platform'] !== undefined) {
			this._obj['platform'] = obj['platform'];
		}
		if(obj['os_ver'] !== undefined) {
			this._obj['osVer'] = obj['os_ver'];
		}
		if(obj['model'] !== undefined) {
			this._obj['model'] = obj['model'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['native_ver'] !== undefined) {
			this._obj['nativeVer'] = obj['native_ver'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
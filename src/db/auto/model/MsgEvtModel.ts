
	export class MsgEvtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['msg_evt_id'] !== undefined) {
			this._obj['msgEvtId'] = obj['msg_evt_id'];
		}
		if(obj['title'] !== undefined) {
			this._obj['title'] = obj['title'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['msg_evt_cd'] !== undefined) {
			this._obj['msgEvtCd'] = obj['msg_evt_cd'];
		}
		if(obj['deploy_cd'] !== undefined) {
			this._obj['deployCd'] = obj['deploy_cd'];
		}
		if(obj['target'] !== undefined) {
			this._obj['target'] = obj['target'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
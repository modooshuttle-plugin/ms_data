
	export class SearchModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['m_no'] !== undefined) {
			this._obj['searchId'] = obj['m_no'];
		}
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_start'] !== undefined) {
			this._obj['startAddr'] = obj['m_start'];
		}
		if(obj['m_start_lat'] !== undefined) {
			this._obj['startLat'] = obj['m_start_lat'];
		}
		if(obj['m_start_lng'] !== undefined) {
			this._obj['startLng'] = obj['m_start_lng'];
		}
		if(obj['m_end'] !== undefined) {
			this._obj['endAddr'] = obj['m_end'];
		}
		if(obj['m_end_lat'] !== undefined) {
			this._obj['endLat'] = obj['m_end_lat'];
		}
		if(obj['m_end_lng'] !== undefined) {
			this._obj['endLng'] = obj['m_end_lng'];
		}
		if(obj['m_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['m_timestamp'];
		}
		if(obj['m_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['m_timestamp_u'];
		}
		if(obj['m_time'] !== undefined) {
			this._obj['timeId'] = obj['m_time'];
		}
		if(obj['sta_emd_cd'] !== undefined) {
			this._obj['startEmdCd'] = obj['sta_emd_cd'];
		}
		if(obj['end_emd_cd'] !== undefined) {
			this._obj['endEmdCd'] = obj['end_emd_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class OfferBusModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['offer_bus_id'] !== undefined) {
			this._obj['offerBusId'] = obj['offer_bus_id'];
		}
		if(obj['offer_dr_id'] !== undefined) {
			this._obj['offerDrId'] = obj['offer_dr_id'];
		}
		if(obj['bep'] !== undefined) {
			this._obj['bep'] = obj['bep'];
		}
		if(obj['fuel_efcnc'] !== undefined) {
			this._obj['fuelEfcnc'] = obj['fuel_efcnc'];
		}
		if(obj['add_amount'] !== undefined) {
			this._obj['addAmount'] = obj['add_amount'];
		}
		if(obj['insr_amount'] !== undefined) {
			this._obj['insrAmount'] = obj['insr_amount'];
		}
		if(obj['paper_amount'] !== undefined) {
			this._obj['paperAmount'] = obj['paper_amount'];
		}
		if(obj['control_amount'] !== undefined) {
			this._obj['controlAmount'] = obj['control_amount'];
		}
		if(obj['expend_amount'] !== undefined) {
			this._obj['expendAmount'] = obj['expend_amount'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['bus_cd'] !== undefined) {
			this._obj['busCd'] = obj['bus_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
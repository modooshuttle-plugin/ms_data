
	export class LogBoardModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logBoardId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_oid'] !== undefined) {
			this._obj['boardId'] = obj['l_oid'];
		}
		if(obj['l_aid'] !== undefined) {
			this._obj['applyId'] = obj['l_aid'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_rid'] !== undefined) {
			this._obj['rtId'] = obj['l_rid'];
		}
		if(obj['l_rpid'] !== undefined) {
			this._obj['prodsId'] = obj['l_rpid'];
		}
		if(obj['l_start_stop'] !== undefined) {
			this._obj['startStCd'] = obj['l_start_stop'];
		}
		if(obj['l_start_ver'] !== undefined) {
			this._obj['startStVer'] = obj['l_start_ver'];
		}
		if(obj['l_end_stop'] !== undefined) {
			this._obj['endStCd'] = obj['l_end_stop'];
		}
		if(obj['l_end_ver'] !== undefined) {
			this._obj['endStVer'] = obj['l_end_ver'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['boardCd'] = obj['l_type'];
		}
		if(obj['l_shape'] !== undefined) {
			this._obj['boardShapeCd'] = obj['l_shape'];
		}
		if(obj['l_seat'] !== undefined) {
			this._obj['seatId'] = obj['l_seat'];
		}
		if(obj['l_start_day'] !== undefined) {
			this._obj['startDay'] = obj['l_start_day'];
		}
		if(obj['l_end_day'] !== undefined) {
			this._obj['endDay'] = obj['l_end_day'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
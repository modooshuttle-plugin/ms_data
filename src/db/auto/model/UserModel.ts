
	export class UserModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_phone'] !== undefined) {
			this._obj['phone'] = obj['m_phone'];
		}
		if(obj['m_name'] !== undefined) {
			this._obj['nm'] = obj['m_name'];
		}
		if(obj['m_type'] !== undefined) {
			this._obj['userCd'] = obj['m_type'];
		}
		if(obj['m_nick'] !== undefined) {
			this._obj['nick'] = obj['m_nick'];
		}
		if(obj['m_act'] !== undefined) {
			this._obj['activeCd'] = obj['m_act'];
		}
		if(obj['m_check'] !== undefined) {
			this._obj['termsCheck'] = obj['m_check'];
		}
		if(obj['m_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['m_timestamp'];
		}
		if(obj['m_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['m_timestamp_u'];
		}
		if(obj['m_refer_code'] !== undefined) {
			this._obj['refCd'] = obj['m_refer_code'];
		}
		if(obj['m_email'] !== undefined) {
			this._obj['orgEmail'] = obj['m_email'];
		}
		if(obj['m_company'] !== undefined) {
			this._obj['orgId'] = obj['m_company'];
		}
		if(obj['gender'] !== undefined) {
			this._obj['gender'] = obj['gender'];
		}
		if(obj['birth_year'] !== undefined) {
			this._obj['birthYear'] = obj['birth_year'];
		}
		if(obj['birth_month'] !== undefined) {
			this._obj['birthMonth'] = obj['birth_month'];
		}
		if(obj['birth_day'] !== undefined) {
			this._obj['birthDay'] = obj['birth_day'];
		}
		if(obj['country_cd'] !== undefined) {
			this._obj['countryCd'] = obj['country_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
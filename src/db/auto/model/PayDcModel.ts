
	export class PayDcModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['pay_dc_id'] !== undefined) {
			this._obj['payDcId'] = obj['pay_dc_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['org_user_auth_id'] !== undefined) {
			this._obj['orgUserAuthId'] = obj['org_user_auth_id'];
		}
		if(obj['email_auth_id'] !== undefined) {
			this._obj['emailAuthId'] = obj['email_auth_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
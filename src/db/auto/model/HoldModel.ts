
	export class HoldModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['holdId'] = obj['o_no'];
		}
		if(obj['o_sid'] !== undefined) {
			this._obj['subscribeId'] = obj['o_sid'];
		}
		if(obj['o_cid'] !== undefined) {
			this._obj['cpnId'] = obj['o_cid'];
		}
		if(obj['o_aid'] !== undefined) {
			this._obj['applyId'] = obj['o_aid'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['hold_cd'] !== undefined) {
			this._obj['holdCd'] = obj['hold_cd'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['cpnPblshCd'] = obj['o_type'];
		}
		if(obj['o_status'] !== undefined) {
			this._obj['statusCd'] = obj['o_status'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class TrkEvtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['trk_evt_id'] !== undefined) {
			this._obj['trkEvtId'] = obj['trk_evt_id'];
		}
		if(obj['target_cd'] !== undefined) {
			this._obj['targetCd'] = obj['target_cd'];
		}
		if(obj['trk_evt_cd'] !== undefined) {
			this._obj['trkEvtCd'] = obj['trk_evt_cd'];
		}
		if(obj['evt_nm'] !== undefined) {
			this._obj['evtNm'] = obj['evt_nm'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['params'] !== undefined) {
			this._obj['params'] = obj['params'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
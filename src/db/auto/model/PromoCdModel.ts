
	export class PromoCdModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['mp_no'] !== undefined) {
			this._obj['promoCdId'] = obj['mp_no'];
		}
		if(obj['mp_code'] !== undefined) {
			this._obj['promoCd'] = obj['mp_code'];
		}
		if(obj['mp_mc_uniq'] !== undefined) {
			this._obj['orgId'] = obj['mp_mc_uniq'];
		}
		if(obj['mp_start'] !== undefined) {
			this._obj['startDay'] = obj['mp_start'];
		}
		if(obj['mp_end'] !== undefined) {
			this._obj['endDay'] = obj['mp_end'];
		}
		if(obj['mp_title'] !== undefined) {
			this._obj['title'] = obj['mp_title'];
		}
		if(obj['mp_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['mp_timestamp'];
		}
		if(obj['mp_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['mp_timestamp_u'];
		}
		if(obj['mp_start_day'] !== undefined) {
			this._obj['useStartDay'] = obj['mp_start_day'];
		}
		if(obj['mp_end_day'] !== undefined) {
			this._obj['useEndDay'] = obj['mp_end_day'];
		}
		if(obj['mp_temp'] !== undefined) {
			this._obj['cpnTplId'] = obj['mp_temp'];
		}
		if(obj['mp_status'] !== undefined) {
			this._obj['promoStatusCd'] = obj['mp_status'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['org_ctrt_dtl_id'] !== undefined) {
			this._obj['orgCtrtDtlId'] = obj['org_ctrt_dtl_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
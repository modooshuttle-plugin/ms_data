
	export class PathModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['r_no'] !== undefined) {
			this._obj['pathId'] = obj['r_no'];
		}
		if(obj['r_rid'] !== undefined) {
			this._obj['rtId'] = obj['r_rid'];
		}
		if(obj['r_start_day'] !== undefined) {
			this._obj['startDay'] = obj['r_start_day'];
		}
		if(obj['r_line_ver'] !== undefined) {
			this._obj['pathVer'] = obj['r_line_ver'];
		}
		if(obj['r_start_type'] !== undefined) {
			this._obj['startCd'] = obj['r_start_type'];
		}
		if(obj['r_end_type'] !== undefined) {
			this._obj['endCd'] = obj['r_end_type'];
		}
		if(obj['r_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['r_timestamp'];
		}
		if(obj['r_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['r_timestamp_u'];
		}
		if(obj['r_distance'] !== undefined) {
			this._obj['dist'] = obj['r_distance'];
		}
		if(obj['r_gp'] !== undefined) {
			this._obj['googlePl'] = obj['r_gp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class RtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['r_rid'] !== undefined) {
			this._obj['rtId'] = obj['r_rid'];
		}
		if(obj['r_home'] !== undefined) {
			this._obj['startInfo'] = obj['r_home'];
		}
		if(obj['r_work'] !== undefined) {
			this._obj['endInfo'] = obj['r_work'];
		}
		if(obj['r_home_detail'] !== undefined) {
			this._obj['startTag'] = obj['r_home_detail'];
		}
		if(obj['r_work_detail'] !== undefined) {
			this._obj['endTag'] = obj['r_work_detail'];
		}
		if(obj['r_home_c'] !== undefined) {
			this._obj['startCatCd'] = obj['r_home_c'];
		}
		if(obj['r_work_c'] !== undefined) {
			this._obj['endCatCd'] = obj['r_work_c'];
		}
		if(obj['r_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['r_timestamp'];
		}
		if(obj['r_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['r_timestamp_u'];
		}
		if(obj['r_tid'] !== undefined) {
			this._obj['timeId'] = obj['r_tid'];
		}
		if(obj['r_type'] !== undefined) {
			this._obj['rtCd'] = obj['r_type'];
		}
		if(obj['r_status'] !== undefined) {
			this._obj['rtStatusCd'] = obj['r_status'];
		}
		if(obj['r_business'] !== undefined) {
			this._obj['bizCd'] = obj['r_business'];
		}
		if(obj['r_commute'] !== undefined) {
			this._obj['commuteCd'] = obj['r_commute'];
		}
		if(obj['r_activation'] !== undefined) {
			this._obj['activateCd'] = obj['r_activation'];
		}
		if(obj['r_seat'] !== undefined) {
			this._obj['seatCd'] = obj['r_seat'];
		}
		if(obj['r_start_day'] !== undefined) {
			this._obj['startDay'] = obj['r_start_day'];
		}
		if(obj['r_end_day'] !== undefined) {
			this._obj['endDay'] = obj['r_end_day'];
		}
		if(obj['r_max_person'] !== undefined) {
			this._obj['minUserCnt'] = obj['r_max_person'];
		}
		if(obj['r_close_day'] !== undefined) {
			this._obj['closeDay'] = obj['r_close_day'];
		}
		if(obj['r_surl'] !== undefined) {
			this._obj['schdUrl'] = obj['r_surl'];
		}
		if(obj['r_rurl'] !== undefined) {
			this._obj['runnUrl'] = obj['r_rurl'];
		}
		if(obj['r_search'] !== undefined) {
			this._obj['searchCd'] = obj['r_search'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class HoldDayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['hold_day_id'] !== undefined) {
			this._obj['holdDayId'] = obj['hold_day_id'];
		}
		if(obj['hold_id'] !== undefined) {
			this._obj['holdId'] = obj['hold_id'];
		}
		if(obj['day'] !== undefined) {
			this._obj['day'] = obj['day'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['status_cd'] !== undefined) {
			this._obj['statusCd'] = obj['status_cd'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
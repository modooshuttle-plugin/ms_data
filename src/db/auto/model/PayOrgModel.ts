
	export class PayOrgModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['pay_org_id'] !== undefined) {
			this._obj['payOrgId'] = obj['pay_org_id'];
		}
		if(obj['pay_org_cd'] !== undefined) {
			this._obj['payOrgCd'] = obj['pay_org_cd'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['org_user_auth_id'] !== undefined) {
			this._obj['orgUserAuthId'] = obj['org_user_auth_id'];
		}
		if(obj['email_auth_id'] !== undefined) {
			this._obj['emailAuthId'] = obj['email_auth_id'];
		}
		if(obj['order_no'] !== undefined) {
			this._obj['orderNo'] = obj['order_no'];
		}
		if(obj['ratio'] !== undefined) {
			this._obj['ratio'] = obj['ratio'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class ProdsModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['r_no'] !== undefined) {
			this._obj['prodsNo'] = obj['r_no'];
		}
		if(obj['r_rid'] !== undefined) {
			this._obj['rtId'] = obj['r_rid'];
		}
		if(obj['r_rpid'] !== undefined) {
			this._obj['prodsId'] = obj['r_rpid'];
		}
		if(obj['r_price'] !== undefined) {
			this._obj['amount'] = obj['r_price'];
		}
		if(obj['r_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['r_timestamp'];
		}
		if(obj['r_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['r_timestamp_u'];
		}
		if(obj['r_start_day'] !== undefined) {
			this._obj['startDay'] = obj['r_start_day'];
		}
		if(obj['r_price_default'] !== undefined) {
			this._obj['defaultAmount'] = obj['r_price_default'];
		}
		if(obj['r_deploy'] !== undefined) {
			this._obj['deployYn'] = obj['r_deploy'];
		}
		if(obj['r_admin'] !== undefined) {
			this._obj['adminId'] = obj['r_admin'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class OrgSetlMshipModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_setl_mship_id'] !== undefined) {
			this._obj['orgSetlMshipId'] = obj['org_setl_mship_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['org_ctrt_dtl_id'] !== undefined) {
			this._obj['orgCtrtDtlId'] = obj['org_ctrt_dtl_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['setl_month'] !== undefined) {
			this._obj['setlMonth'] = obj['setl_month'];
		}
		if(obj['month_amount'] !== undefined) {
			this._obj['monthAmount'] = obj['month_amount'];
		}
		if(obj['dc_amount'] !== undefined) {
			this._obj['dcAmount'] = obj['dc_amount'];
		}
		if(obj['user_amount'] !== undefined) {
			this._obj['userAmount'] = obj['user_amount'];
		}
		if(obj['setl_dc_amount'] !== undefined) {
			this._obj['setlDcAmount'] = obj['setl_dc_amount'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['tax_iss_day'] !== undefined) {
			this._obj['taxIssDay'] = obj['tax_iss_day'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
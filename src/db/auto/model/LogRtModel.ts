
	export class LogRtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logRtId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_rid'] !== undefined) {
			this._obj['rtId'] = obj['l_rid'];
		}
		if(obj['l_home'] !== undefined) {
			this._obj['startInfo'] = obj['l_home'];
		}
		if(obj['l_work'] !== undefined) {
			this._obj['endInfo'] = obj['l_work'];
		}
		if(obj['l_home_detail'] !== undefined) {
			this._obj['startTag'] = obj['l_home_detail'];
		}
		if(obj['l_work_detail'] !== undefined) {
			this._obj['endTag'] = obj['l_work_detail'];
		}
		if(obj['l_home_c'] !== undefined) {
			this._obj['startCatCd'] = obj['l_home_c'];
		}
		if(obj['l_work_c'] !== undefined) {
			this._obj['endCatCd'] = obj['l_work_c'];
		}
		if(obj['l_tid'] !== undefined) {
			this._obj['timeId'] = obj['l_tid'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['rtCd'] = obj['l_type'];
		}
		if(obj['l_status'] !== undefined) {
			this._obj['rtStatusCd'] = obj['l_status'];
		}
		if(obj['l_business'] !== undefined) {
			this._obj['bizCd'] = obj['l_business'];
		}
		if(obj['l_commute'] !== undefined) {
			this._obj['commuteCd'] = obj['l_commute'];
		}
		if(obj['l_activation'] !== undefined) {
			this._obj['activateCd'] = obj['l_activation'];
		}
		if(obj['l_seat'] !== undefined) {
			this._obj['seatCd'] = obj['l_seat'];
		}
		if(obj['l_start_day'] !== undefined) {
			this._obj['startDay'] = obj['l_start_day'];
		}
		if(obj['l_end_day'] !== undefined) {
			this._obj['endDay'] = obj['l_end_day'];
		}
		if(obj['l_close_day'] !== undefined) {
			this._obj['closeDay'] = obj['l_close_day'];
		}
		if(obj['l_max_person'] !== undefined) {
			this._obj['minUserCnt'] = obj['l_max_person'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}
		if(obj['l_search'] !== undefined) {
			this._obj['searchCd'] = obj['l_search'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class PaidAdjustModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['paid_adjust_id'] !== undefined) {
			this._obj['paidAdjustId'] = obj['paid_adjust_id'];
		}
		if(obj['pay_adjust_id'] !== undefined) {
			this._obj['payAdjustId'] = obj['pay_adjust_id'];
		}
		if(obj['paid_id'] !== undefined) {
			this._obj['paidId'] = obj['paid_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['extend_cd'] !== undefined) {
			this._obj['extendCd'] = obj['extend_cd'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['order_no'] !== undefined) {
			this._obj['orderNo'] = obj['order_no'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class TimeModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['t_tid'] !== undefined) {
			this._obj['timeId'] = obj['t_tid'];
		}
		if(obj['t_idx'] !== undefined) {
			this._obj['idx'] = obj['t_idx'];
		}
		if(obj['t_time'] !== undefined) {
			this._obj['time'] = obj['t_time'];
		}
		if(obj['t_time_apply'] !== undefined) {
			this._obj['timeApply'] = obj['t_time_apply'];
		}
		if(obj['t_time_search'] !== undefined) {
			this._obj['timeSearch'] = obj['t_time_search'];
		}
		if(obj['t_type'] !== undefined) {
			this._obj['timeCd'] = obj['t_type'];
		}
		if(obj['t_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['t_timestamp'];
		}
		if(obj['t_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['t_timestamp_u'];
		}
		if(obj['t_commute'] !== undefined) {
			this._obj['commuteCd'] = obj['t_commute'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
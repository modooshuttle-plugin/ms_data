
	export class BizOrderDtlModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['biz_order_dtl_id'] !== undefined) {
			this._obj['bizOrderDtlId'] = obj['biz_order_dtl_id'];
		}
		if(obj['biz_order_id'] !== undefined) {
			this._obj['bizOrderId'] = obj['biz_order_id'];
		}
		if(obj['bus_cd'] !== undefined) {
			this._obj['busCd'] = obj['bus_cd'];
		}
		if(obj['bus_cnt'] !== undefined) {
			this._obj['busCnt'] = obj['bus_cnt'];
		}
		if(obj['time_range'] !== undefined) {
			this._obj['timeRange'] = obj['time_range'];
		}
		if(obj['runn_cnt'] !== undefined) {
			this._obj['runnCnt'] = obj['runn_cnt'];
		}
		if(obj['runn_schd_day'] !== undefined) {
			this._obj['runnSchdDay'] = obj['runn_schd_day'];
		}
		if(obj['start'] !== undefined) {
			this._obj['start'] = obj['start'];
		}
		if(obj['move'] !== undefined) {
			this._obj['move'] = obj['move'];
		}
		if(obj['end'] !== undefined) {
			this._obj['end'] = obj['end'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['end_comment'] !== undefined) {
			this._obj['endComment'] = obj['end_comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['section_cd'] !== undefined) {
			this._obj['sectionCd'] = obj['section_cd'];
		}
		if(obj['start_time'] !== undefined) {
			this._obj['startTime'] = obj['start_time'];
		}
		if(obj['end_time'] !== undefined) {
			this._obj['endTime'] = obj['end_time'];
		}
		if(obj['age'] !== undefined) {
			this._obj['age'] = obj['age'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
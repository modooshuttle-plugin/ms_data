
	export class MsgModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['msgId'] = obj['a_no'];
		}
		if(obj['a_rid'] !== undefined) {
			this._obj['rtId'] = obj['a_rid'];
		}
		if(obj['a_rpid'] !== undefined) {
			this._obj['prodsId'] = obj['a_rpid'];
		}
		if(obj['a_mid'] !== undefined) {
			this._obj['userId'] = obj['a_mid'];
		}
		if(obj['a_phone'] !== undefined) {
			this._obj['phone'] = obj['a_phone'];
		}
		if(obj['a_name'] !== undefined) {
			this._obj['nm'] = obj['a_name'];
		}
		if(obj['a_type'] !== undefined) {
			this._obj['msgCd'] = obj['a_type'];
		}
		if(obj['a_rtype'] !== undefined) {
			this._obj['rtCd'] = obj['a_rtype'];
		}
		if(obj['a_subject'] !== undefined) {
			this._obj['subject'] = obj['a_subject'];
		}
		if(obj['a_text'] !== undefined) {
			this._obj['comment'] = obj['a_text'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_timestamp_send'] !== undefined) {
			this._obj['reservAt'] = obj['a_timestamp_send'];
		}
		if(obj['a_result'] !== undefined) {
			this._obj['result'] = obj['a_result'];
		}
		if(obj['a_gid'] !== undefined) {
			this._obj['sendId'] = obj['a_gid'];
		}
		if(obj['a_admin'] !== undefined) {
			this._obj['adminId'] = obj['a_admin'];
		}
		if(obj['a_stype'] !== undefined) {
			this._obj['msgCompnCd'] = obj['a_stype'];
		}
		if(obj['a_temp'] !== undefined) {
			this._obj['msgTplId'] = obj['a_temp'];
		}
		if(obj['a_ptype'] !== undefined) {
			this._obj['mbrCd'] = obj['a_ptype'];
		}
		if(obj['a_status'] !== undefined) {
			this._obj['statusCd'] = obj['a_status'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class OrgAccountModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_account_id'] !== undefined) {
			this._obj['orgAccountId'] = obj['org_account_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['corp'] !== undefined) {
			this._obj['corp'] = obj['corp'];
		}
		if(obj['id'] !== undefined) {
			this._obj['id'] = obj['id'];
		}
		if(obj['pw'] !== undefined) {
			this._obj['pw'] = obj['pw'];
		}
		if(obj['ko'] !== undefined) {
			this._obj['ko'] = obj['ko'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
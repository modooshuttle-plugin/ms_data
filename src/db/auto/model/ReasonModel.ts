
	export class ReasonModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['reasonId'] = obj['o_no'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_otype'] !== undefined) {
			this._obj['boardCd'] = obj['o_otype'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['reasonCd'] = obj['o_type'];
		}
		if(obj['o_text'] !== undefined) {
			this._obj['comment'] = obj['o_text'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
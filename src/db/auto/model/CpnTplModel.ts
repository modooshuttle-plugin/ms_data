
	export class CpnTplModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['t_no'] !== undefined) {
			this._obj['cpnTplId'] = obj['t_no'];
		}
		if(obj['t_name'] !== undefined) {
			this._obj['nm'] = obj['t_name'];
		}
		if(obj['t_type'] !== undefined) {
			this._obj['calCd'] = obj['t_type'];
		}
		if(obj['t_func'] !== undefined) {
			this._obj['func'] = obj['t_func'];
		}
		if(obj['t_value'] !== undefined) {
			this._obj['amount'] = obj['t_value'];
		}
		if(obj['t_sign'] !== undefined) {
			this._obj['signCd'] = obj['t_sign'];
		}
		if(obj['t_cupdate'] !== undefined) {
			this._obj['adminUpdateCd'] = obj['t_cupdate'];
		}
		if(obj['dupe_yn'] !== undefined) {
			this._obj['dupeYn'] = obj['dupe_yn'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
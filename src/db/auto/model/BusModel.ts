
	export class BusModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['b_no'] !== undefined) {
			this._obj['busId'] = obj['b_no'];
		}
		if(obj['b_seat'] !== undefined) {
			this._obj['seat'] = obj['b_seat'];
		}
		if(obj['b_name'] !== undefined) {
			this._obj['nm'] = obj['b_name'];
		}
		if(obj['b_cnt'] !== undefined) {
			this._obj['seatCnt'] = obj['b_cnt'];
		}
		if(obj['b_cnt_view'] !== undefined) {
			this._obj['viewSeatCnt'] = obj['b_cnt_view'];
		}
		if(obj['b_etc'] !== undefined) {
			this._obj['comment'] = obj['b_etc'];
		}
		if(obj['b_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['b_timestamp'];
		}
		if(obj['b_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['b_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
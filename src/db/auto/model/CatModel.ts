
	export class CatModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['c_no'] !== undefined) {
			this._obj['catId'] = obj['c_no'];
		}
		if(obj['c_uniq'] !== undefined) {
			this._obj['catCd'] = obj['c_uniq'];
		}
		if(obj['c_se'] !== undefined) {
			this._obj['sectionCd'] = obj['c_se'];
		}
		if(obj['c_alias'] !== undefined) {
			this._obj['alias'] = obj['c_alias'];
		}
		if(obj['c_keyword'] !== undefined) {
			this._obj['keyword'] = obj['c_keyword'];
		}
		if(obj['c_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['c_timestamp'];
		}
		if(obj['c_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['c_timestamp_u'];
		}
		if(obj['c_commute_type'] !== undefined) {
			this._obj['commuteCd'] = obj['c_commute_type'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class CatSigModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['c_no'] !== undefined) {
			this._obj['catSigId'] = obj['c_no'];
		}
		if(obj['c_uniq'] !== undefined) {
			this._obj['catId'] = obj['c_uniq'];
		}
		if(obj['c_sig_cd'] !== undefined) {
			this._obj['sigCd'] = obj['c_sig_cd'];
		}
		if(obj['c_alias'] !== undefined) {
			this._obj['alias'] = obj['c_alias'];
		}
		if(obj['c_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['c_timestamp'];
		}
		if(obj['c_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['c_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class DispatchModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['r_no'] !== undefined) {
			this._obj['dispatchId'] = obj['r_no'];
		}
		if(obj['r_rid'] !== undefined) {
			this._obj['rtId'] = obj['r_rid'];
		}
		if(obj['r_did'] !== undefined) {
			this._obj['drId'] = obj['r_did'];
		}
		if(obj['r_start_day'] !== undefined) {
			this._obj['startDay'] = obj['r_start_day'];
		}
		if(obj['r_end_day'] !== undefined) {
			this._obj['endDay'] = obj['r_end_day'];
		}
		if(obj['r_type'] !== undefined) {
			this._obj['dispatchCd'] = obj['r_type'];
		}
		if(obj['r_deploy'] !== undefined) {
			this._obj['deployYn'] = obj['r_deploy'];
		}
		if(obj['r_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['r_timestamp'];
		}
		if(obj['r_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['r_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
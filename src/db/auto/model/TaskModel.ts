
	export class TaskModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['task_id'] !== undefined) {
			this._obj['taskId'] = obj['task_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['task_cd'] !== undefined) {
			this._obj['taskCd'] = obj['task_cd'];
		}
		if(obj['title'] !== undefined) {
			this._obj['title'] = obj['title'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['status_cd'] !== undefined) {
			this._obj['statusCd'] = obj['status_cd'];
		}
		if(obj['admin_ids'] !== undefined) {
			this._obj['adminIds'] = obj['admin_ids'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['notion_page_id'] !== undefined) {
			this._obj['notionPageId'] = obj['notion_page_id'];
		}
		if(obj['target_day'] !== undefined) {
			this._obj['targetDay'] = obj['target_day'];
		}
		if(obj['updated_id'] !== undefined) {
			this._obj['updatedId'] = obj['updated_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
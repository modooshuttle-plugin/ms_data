
	export class RunnDiaryModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['d_no'] !== undefined) {
			this._obj['runnDiaryId'] = obj['d_no'];
		}
		if(obj['d_uniq'] !== undefined) {
			this._obj['drId'] = obj['d_uniq'];
		}
		if(obj['d_year'] !== undefined) {
			this._obj['year'] = obj['d_year'];
		}
		if(obj['d_month'] !== undefined) {
			this._obj['month'] = obj['d_month'];
		}
		if(obj['d_day'] !== undefined) {
			this._obj['day'] = obj['d_day'];
		}
		if(obj['d_timestamp_start'] !== undefined) {
			this._obj['startedAt'] = obj['d_timestamp_start'];
		}
		if(obj['d_timestamp_end'] !== undefined) {
			this._obj['endedAt'] = obj['d_timestamp_end'];
		}
		if(obj['d_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['d_timestamp'];
		}
		if(obj['d_r_uniq'] !== undefined) {
			this._obj['rtId'] = obj['d_r_uniq'];
		}
		if(obj['d_type'] !== undefined) {
			this._obj['runnCd'] = obj['d_type'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
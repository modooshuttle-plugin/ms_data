
	export class PaidCpnModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['paid_cpn_id'] !== undefined) {
			this._obj['paidCpnId'] = obj['paid_cpn_id'];
		}
		if(obj['cpn_tpl_id'] !== undefined) {
			this._obj['cpnTplId'] = obj['cpn_tpl_id'];
		}
		if(obj['paid_id'] !== undefined) {
			this._obj['paidId'] = obj['paid_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['cpn_id'] !== undefined) {
			this._obj['cpnId'] = obj['cpn_id'];
		}
		if(obj['cpn_use_id'] !== undefined) {
			this._obj['cpnUseId'] = obj['cpn_use_id'];
		}
		if(obj['order_no'] !== undefined) {
			this._obj['orderNo'] = obj['order_no'];
		}
		if(obj['use_yn'] !== undefined) {
			this._obj['useYn'] = obj['use_yn'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
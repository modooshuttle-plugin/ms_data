
	export class BusCompnModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['bus_compn_id'] !== undefined) {
			this._obj['busCompnId'] = obj['bus_compn_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['biz_nm'] !== undefined) {
			this._obj['bizNm'] = obj['biz_nm'];
		}
		if(obj['email'] !== undefined) {
			this._obj['email'] = obj['email'];
		}
		if(obj['phone'] !== undefined) {
			this._obj['phone'] = obj['phone'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['biz_reg_no'] !== undefined) {
			this._obj['bizRegNo'] = obj['biz_reg_no'];
		}
		if(obj['biz_addr'] !== undefined) {
			this._obj['bizAddr'] = obj['biz_addr'];
		}
		if(obj['bank_nm'] !== undefined) {
			this._obj['bankNm'] = obj['bank_nm'];
		}
		if(obj['bank_account'] !== undefined) {
			this._obj['bankAccount'] = obj['bank_account'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
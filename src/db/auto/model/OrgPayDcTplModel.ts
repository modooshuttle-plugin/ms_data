
	export class OrgPayDcTplModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_pay_dc_tpl_id'] !== undefined) {
			this._obj['orgPayDcTplId'] = obj['org_pay_dc_tpl_id'];
		}
		if(obj['org_user_auth_id'] !== undefined) {
			this._obj['orgUserAuthId'] = obj['org_user_auth_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
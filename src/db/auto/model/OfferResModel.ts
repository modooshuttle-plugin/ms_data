
	export class OfferResModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['offer_res_id'] !== undefined) {
			this._obj['offerResId'] = obj['offer_res_id'];
		}
		if(obj['offer_req_id'] !== undefined) {
			this._obj['offerReqId'] = obj['offer_req_id'];
		}
		if(obj['task_id'] !== undefined) {
			this._obj['taskId'] = obj['task_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['phone'] !== undefined) {
			this._obj['phone'] = obj['phone'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['dispatch_id'] !== undefined) {
			this._obj['dispatchId'] = obj['dispatch_id'];
		}
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['dr_pay_cond_id'] !== undefined) {
			this._obj['drPayCondId'] = obj['dr_pay_cond_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
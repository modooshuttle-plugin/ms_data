
	export class NotiModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['mb_no'] !== undefined) {
			this._obj['notiId'] = obj['mb_no'];
		}
		if(obj['mb_title'] !== undefined) {
			this._obj['title'] = obj['mb_title'];
		}
		if(obj['mb_content'] !== undefined) {
			this._obj['content'] = obj['mb_content'];
		}
		if(obj['mb_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['mb_timestamp'];
		}
		if(obj['mb_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['mb_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
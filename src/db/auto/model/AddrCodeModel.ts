
	export class AddrCodeModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['addr_seq'] !== undefined) {
			this._obj['addrCodeId'] = obj['addr_seq'];
		}
		if(obj['ctp_kor_nm'] !== undefined) {
			this._obj['ctpKorNm'] = obj['ctp_kor_nm'];
		}
		if(obj['ctprvn_cd'] !== undefined) {
			this._obj['ctprvnCd'] = obj['ctprvn_cd'];
		}
		if(obj['si_kor_nm'] !== undefined) {
			this._obj['siKorNm'] = obj['si_kor_nm'];
		}
		if(obj['si_cd'] !== undefined) {
			this._obj['siCd'] = obj['si_cd'];
		}
		if(obj['sig_kor_nm'] !== undefined) {
			this._obj['sigKorNm'] = obj['sig_kor_nm'];
		}
		if(obj['sig_cd'] !== undefined) {
			this._obj['sigCd'] = obj['sig_cd'];
		}
		if(obj['emd_kor_nm'] !== undefined) {
			this._obj['emdKorNm'] = obj['emd_kor_nm'];
		}
		if(obj['emd_cd'] !== undefined) {
			this._obj['emdCd'] = obj['emd_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
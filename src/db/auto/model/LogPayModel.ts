
	export class LogPayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logPayId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_action'] !== undefined) {
			this._obj['actionCd'] = obj['l_action'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['statusCd'] = obj['l_type'];
		}
		if(obj['l_channel'] !== undefined) {
			this._obj['channelCd'] = obj['l_channel'];
		}
		if(obj['l_imp_uid'] !== undefined) {
			this._obj['impUid'] = obj['l_imp_uid'];
		}
		if(obj['l_muid'] !== undefined) {
			this._obj['orderNo'] = obj['l_muid'];
		}
		if(obj['l_method'] !== undefined) {
			this._obj['methodCd'] = obj['l_method'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_pid'] !== undefined) {
			this._obj['payId'] = obj['l_pid'];
		}
		if(obj['l_value'] !== undefined) {
			this._obj['amount'] = obj['l_value'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
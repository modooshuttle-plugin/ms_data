
	export class ServiceCdModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['service_cd_id'] !== undefined) {
			this._obj['serviceCdId'] = obj['service_cd_id'];
		}
		if(obj['tb'] !== undefined) {
			this._obj['tb'] = obj['tb'];
		}
		if(obj['col'] !== undefined) {
			this._obj['col'] = obj['col'];
		}
		if(obj['ver'] !== undefined) {
			this._obj['ver'] = obj['ver'];
		}
		if(obj['value'] !== undefined) {
			this._obj['value'] = obj['value'];
		}
		if(obj['wh'] !== undefined) {
			this._obj['wh'] = obj['wh'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
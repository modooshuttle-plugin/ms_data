
	export class DrIssModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_iss_id'] !== undefined) {
			this._obj['drIssId'] = obj['dr_iss_id'];
		}
		if(obj['runn_iss_id'] !== undefined) {
			this._obj['runnIssId'] = obj['runn_iss_id'];
		}
		if(obj['dispatch_id'] !== undefined) {
			this._obj['dispatchId'] = obj['dispatch_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_check_cd'] !== undefined) {
			this._obj['adminCheckCd'] = obj['admin_check_cd'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['setl_month'] !== undefined) {
			this._obj['setlMonth'] = obj['setl_month'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['iss_cd'] !== undefined) {
			this._obj['issCd'] = obj['iss_cd'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
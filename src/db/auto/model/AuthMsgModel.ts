
	export class AuthMsgModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['m_no'] !== undefined) {
			this._obj['authMsgId'] = obj['m_no'];
		}
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_phone'] !== undefined) {
			this._obj['phone'] = obj['m_phone'];
		}
		if(obj['m_auth_no'] !== undefined) {
			this._obj['authNo'] = obj['m_auth_no'];
		}
		if(obj['m_auth_check'] !== undefined) {
			this._obj['authCd'] = obj['m_auth_check'];
		}
		if(obj['m_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['m_timestamp'];
		}
		if(obj['m_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['m_timestamp_u'];
		}
		if(obj['m_c_result'] !== undefined) {
			this._obj['resultCd'] = obj['m_c_result'];
		}
		if(obj['m_c_gid'] !== undefined) {
			this._obj['sendId'] = obj['m_c_gid'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
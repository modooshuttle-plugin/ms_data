
	export class OfferDrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['offer_dr_id'] !== undefined) {
			this._obj['offerDrId'] = obj['offer_dr_id'];
		}
		if(obj['task_id'] !== undefined) {
			this._obj['taskId'] = obj['task_id'];
		}
		if(obj['oil_amount'] !== undefined) {
			this._obj['oilAmount'] = obj['oil_amount'];
		}
		if(obj['tollgate_amount'] !== undefined) {
			this._obj['tollgateAmount'] = obj['tollgate_amount'];
		}
		if(obj['runn_day_cnt'] !== undefined) {
			this._obj['runnDayCnt'] = obj['runn_day_cnt'];
		}
		if(obj['weekday_cnt'] !== undefined) {
			this._obj['weekdayCnt'] = obj['weekday_cnt'];
		}
		if(obj['repeat_runn_cnt'] !== undefined) {
			this._obj['repeatRunnCnt'] = obj['repeat_runn_cnt'];
		}
		if(obj['runn_dist'] !== undefined) {
			this._obj['runnDist'] = obj['runn_dist'];
		}
		if(obj['runn_time'] !== undefined) {
			this._obj['runnTime'] = obj['runn_time'];
		}
		if(obj['time_id'] !== undefined) {
			this._obj['timeId'] = obj['time_id'];
		}
		if(obj['interest_ratio'] !== undefined) {
			this._obj['interestRatio'] = obj['interest_ratio'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
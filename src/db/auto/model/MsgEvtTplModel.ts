
	export class MsgEvtTplModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['msg_evt_tpl_id'] !== undefined) {
			this._obj['msgEvtTplId'] = obj['msg_evt_tpl_id'];
		}
		if(obj['msg_evt_id'] !== undefined) {
			this._obj['msgEvtId'] = obj['msg_evt_id'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['msg_tpl_id'] !== undefined) {
			this._obj['msgTplId'] = obj['msg_tpl_id'];
		}
		if(obj['msg_cd'] !== undefined) {
			this._obj['msgCd'] = obj['msg_cd'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
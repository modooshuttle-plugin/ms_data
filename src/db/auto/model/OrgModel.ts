
	export class OrgModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['biz_nm'] !== undefined) {
			this._obj['bizNm'] = obj['biz_nm'];
		}
		if(obj['biz_reg_no'] !== undefined) {
			this._obj['bizRegNo'] = obj['biz_reg_no'];
		}
		if(obj['biz_addr'] !== undefined) {
			this._obj['bizAddr'] = obj['biz_addr'];
		}
		if(obj['org_cd'] !== undefined) {
			this._obj['orgCd'] = obj['org_cd'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
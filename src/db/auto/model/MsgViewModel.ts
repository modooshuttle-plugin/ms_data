
	export class MsgViewModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['msgViewId'] = obj['a_no'];
		}
		if(obj['a_r_type'] !== undefined) {
			this._obj['rtCd'] = obj['a_r_type'];
		}
		if(obj['a_title'] !== undefined) {
			this._obj['title'] = obj['a_title'];
		}
		if(obj['a_items'] !== undefined) {
			this._obj['tplList'] = obj['a_items'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['a_timestamp_u'];
		}
		if(obj['a_idx_v'] !== undefined) {
			this._obj['idx'] = obj['a_idx_v'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
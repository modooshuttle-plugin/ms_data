
	export class CaldModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['a_no'] !== undefined) {
			this._obj['caldId'] = obj['a_no'];
		}
		if(obj['a_oper'] !== undefined) {
			this._obj['runnCd'] = obj['a_oper'];
		}
		if(obj['a_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['a_timestamp'];
		}
		if(obj['a_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['a_timestamp_u'];
		}
		if(obj['a_date'] !== undefined) {
			this._obj['day'] = obj['a_date'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class OfferReqModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['offer_req_id'] !== undefined) {
			this._obj['offerReqId'] = obj['offer_req_id'];
		}
		if(obj['task_id'] !== undefined) {
			this._obj['taskId'] = obj['task_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['from_cd'] !== undefined) {
			this._obj['fromCd'] = obj['from_cd'];
		}
		if(obj['to_cd'] !== undefined) {
			this._obj['toCd'] = obj['to_cd'];
		}
		if(obj['from_id'] !== undefined) {
			this._obj['fromId'] = obj['from_id'];
		}
		if(obj['to_id'] !== undefined) {
			this._obj['toId'] = obj['to_id'];
		}
		if(obj['req_cd'] !== undefined) {
			this._obj['reqCd'] = obj['req_cd'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['offer_dr_id'] !== undefined) {
			this._obj['offerDrId'] = obj['offer_dr_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
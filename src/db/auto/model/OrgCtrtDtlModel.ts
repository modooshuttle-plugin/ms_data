
	export class OrgCtrtDtlModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_ctrt_dtl_id'] !== undefined) {
			this._obj['orgCtrtDtlId'] = obj['org_ctrt_dtl_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['title'] !== undefined) {
			this._obj['title'] = obj['title'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['setl_cd'] !== undefined) {
			this._obj['setlCd'] = obj['setl_cd'];
		}
		if(obj['biz_cd'] !== undefined) {
			this._obj['bizCd'] = obj['biz_cd'];
		}
		if(obj['prods_cd'] !== undefined) {
			this._obj['prodsCd'] = obj['prods_cd'];
		}
		if(obj['subscribe_cd'] !== undefined) {
			this._obj['subscribeCd'] = obj['subscribe_cd'];
		}
		if(obj['url'] !== undefined) {
			this._obj['url'] = obj['url'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
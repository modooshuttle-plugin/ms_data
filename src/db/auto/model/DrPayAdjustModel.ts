
	export class DrPayAdjustModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_pay_adjust_id'] !== undefined) {
			this._obj['drPayAdjustId'] = obj['dr_pay_adjust_id'];
		}
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['setl_start_month'] !== undefined) {
			this._obj['setlStartMonth'] = obj['setl_start_month'];
		}
		if(obj['setl_end_month'] !== undefined) {
			this._obj['setlEndMonth'] = obj['setl_end_month'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['dr_pay_cond_id'] !== undefined) {
			this._obj['drPayCondId'] = obj['dr_pay_cond_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
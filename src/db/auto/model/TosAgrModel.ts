
	export class TosAgrModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['SEQ'] !== undefined) {
			this._obj['tosAgrId'] = obj['SEQ'];
		}
		if(obj['m_mid'] !== undefined) {
			this._obj['userId'] = obj['m_mid'];
		}
		if(obj['m_id'] !== undefined) {
			this._obj['loginId'] = obj['m_id'];
		}
		if(obj['PHONE'] !== undefined) {
			this._obj['phone'] = obj['PHONE'];
		}
		if(obj['GENDER'] !== undefined) {
			this._obj['gender'] = obj['GENDER'];
		}
		if(obj['AGE_RANGE'] !== undefined) {
			this._obj['ageRange'] = obj['AGE_RANGE'];
		}
		if(obj['BIRTH_YYYY'] !== undefined) {
			this._obj['birthYear'] = obj['BIRTH_YYYY'];
		}
		if(obj['BIRTH_MM'] !== undefined) {
			this._obj['birthMonth'] = obj['BIRTH_MM'];
		}
		if(obj['BIRTH_DD'] !== undefined) {
			this._obj['birthDay'] = obj['BIRTH_DD'];
		}
		if(obj['AGR_CHNNL'] !== undefined) {
			this._obj['agrChannel'] = obj['AGR_CHNNL'];
		}
		if(obj['DEL_YN'] !== undefined) {
			this._obj['deleteYn'] = obj['DEL_YN'];
		}
		if(obj['REG_DT'] !== undefined) {
			this._obj['createdAt'] = obj['REG_DT'];
		}
		if(obj['UDT_DT'] !== undefined) {
			this._obj['updatedAt'] = obj['UDT_DT'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
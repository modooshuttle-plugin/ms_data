
	export class TableModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['table_id'] !== undefined) {
			this._obj['tableId'] = obj['table_id'];
		}
		if(obj['target_cd'] !== undefined) {
			this._obj['targetCd'] = obj['target_cd'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['old_nm'] !== undefined) {
			this._obj['oldNm'] = obj['old_nm'];
		}
		if(obj['kor_nm'] !== undefined) {
			this._obj['korNm'] = obj['kor_nm'];
		}
		if(obj['ver'] !== undefined) {
			this._obj['ver'] = obj['ver'];
		}
		if(obj['db'] !== undefined) {
			this._obj['db'] = obj['db'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class LogMakeModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logMakeId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_mkid'] !== undefined) {
			this._obj['makeId'] = obj['l_mkid'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_home'] !== undefined) {
			this._obj['startAddr'] = obj['l_home'];
		}
		if(obj['l_home_lat'] !== undefined) {
			this._obj['startLat'] = obj['l_home_lat'];
		}
		if(obj['l_home_lng'] !== undefined) {
			this._obj['startLng'] = obj['l_home_lng'];
		}
		if(obj['l_work'] !== undefined) {
			this._obj['endAddr'] = obj['l_work'];
		}
		if(obj['l_work_lat'] !== undefined) {
			this._obj['endLat'] = obj['l_work_lat'];
		}
		if(obj['l_work_lng'] !== undefined) {
			this._obj['endLng'] = obj['l_work_lng'];
		}
		if(obj['l_tid'] !== undefined) {
			this._obj['timeId'] = obj['l_tid'];
		}
		if(obj['l_home_title'] !== undefined) {
			this._obj['startTitle'] = obj['l_home_title'];
		}
		if(obj['l_work_title'] !== undefined) {
			this._obj['endTitle'] = obj['l_work_title'];
		}
		if(obj['l_request'] !== undefined) {
			this._obj['comment'] = obj['l_request'];
		}
		if(obj['l_email'] !== undefined) {
			this._obj['email'] = obj['l_email'];
		}
		if(obj['l_company'] !== undefined) {
			this._obj['orgId'] = obj['l_company'];
		}
		if(obj['l_home_c'] !== undefined) {
			this._obj['startCatCd'] = obj['l_home_c'];
		}
		if(obj['l_work_c'] !== undefined) {
			this._obj['endCatCd'] = obj['l_work_c'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
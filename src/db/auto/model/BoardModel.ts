
	export class BoardModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['boardId'] = obj['o_no'];
		}
		if(obj['o_rid'] !== undefined) {
			this._obj['rtId'] = obj['o_rid'];
		}
		if(obj['o_rpid'] !== undefined) {
			this._obj['prodsId'] = obj['o_rpid'];
		}
		if(obj['o_start_stop'] !== undefined) {
			this._obj['startStCd'] = obj['o_start_stop'];
		}
		if(obj['o_end_stop'] !== undefined) {
			this._obj['endStCd'] = obj['o_end_stop'];
		}
		if(obj['o_start_ver'] !== undefined) {
			this._obj['startStVer'] = obj['o_start_ver'];
		}
		if(obj['o_end_ver'] !== undefined) {
			this._obj['endStVer'] = obj['o_end_ver'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_type'] !== undefined) {
			this._obj['boardCd'] = obj['o_type'];
		}
		if(obj['o_shape'] !== undefined) {
			this._obj['boardShapeCd'] = obj['o_shape'];
		}
		if(obj['o_seat'] !== undefined) {
			this._obj['seatId'] = obj['o_seat'];
		}
		if(obj['o_start_day'] !== undefined) {
			this._obj['startDay'] = obj['o_start_day'];
		}
		if(obj['o_end_day'] !== undefined) {
			this._obj['endDay'] = obj['o_end_day'];
		}
		if(obj['o_aid'] !== undefined) {
			this._obj['applyId'] = obj['o_aid'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['rt_ty'] !== undefined) {
			this._obj['rtCd'] = obj['rt_ty'];
		}
		if(obj['target'] !== undefined) {
			this._obj['targetCd'] = obj['target'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
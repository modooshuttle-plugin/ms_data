
	export class PromoAmountModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['mp_no'] !== undefined) {
			this._obj['promoAmountId'] = obj['mp_no'];
		}
		if(obj['mp_cid'] !== undefined) {
			this._obj['promoCdId'] = obj['mp_cid'];
		}
		if(obj['mp_price'] !== undefined) {
			this._obj['amount'] = obj['mp_price'];
		}
		if(obj['mp_idx'] !== undefined) {
			this._obj['idx'] = obj['mp_idx'];
		}
		if(obj['mp_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['mp_timestamp'];
		}
		if(obj['mp_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['mp_timestamp_u'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
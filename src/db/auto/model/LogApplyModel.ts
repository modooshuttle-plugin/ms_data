
	export class LogApplyModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logApplyId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_aid'] !== undefined) {
			this._obj['applyId'] = obj['l_aid'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_sid'] !== undefined) {
			this._obj['subscribeId'] = obj['l_sid'];
		}
		if(obj['l_rptype'] !== undefined) {
			this._obj['prodsCd'] = obj['l_rptype'];
		}
		if(obj['l_start_day'] !== undefined) {
			this._obj['startDay'] = obj['l_start_day'];
		}
		if(obj['l_end_day'] !== undefined) {
			this._obj['endDay'] = obj['l_end_day'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
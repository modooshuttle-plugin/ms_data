
	export class PaidModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['paid_id'] !== undefined) {
			this._obj['paidId'] = obj['paid_id'];
		}
		if(obj['order_no'] !== undefined) {
			this._obj['orderNo'] = obj['order_no'];
		}
		if(obj['board_id'] !== undefined) {
			this._obj['boardId'] = obj['board_id'];
		}
		if(obj['pay_id'] !== undefined) {
			this._obj['payId'] = obj['pay_id'];
		}
		if(obj['pay_amount'] !== undefined) {
			this._obj['payAmount'] = obj['pay_amount'];
		}
		if(obj['pay_try_id'] !== undefined) {
			this._obj['payTryId'] = obj['pay_try_id'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['user_id'] !== undefined) {
			this._obj['userId'] = obj['user_id'];
		}
		if(obj['method_cd'] !== undefined) {
			this._obj['methodCd'] = obj['method_cd'];
		}
		if(obj['cash_receipt_yn'] !== undefined) {
			this._obj['cashReceiptYn'] = obj['cash_receipt_yn'];
		}
		if(obj['status_cd'] !== undefined) {
			this._obj['statusCd'] = obj['status_cd'];
		}
		if(obj['channel_cd'] !== undefined) {
			this._obj['channelCd'] = obj['channel_cd'];
		}
		if(obj['imp_uid'] !== undefined) {
			this._obj['impUid'] = obj['imp_uid'];
		}
		if(obj['provider_cd'] !== undefined) {
			this._obj['providerCd'] = obj['provider_cd'];
		}
		if(obj['amount'] !== undefined) {
			this._obj['amount'] = obj['amount'];
		}
		if(obj['cash_receipt_at'] !== undefined) {
			this._obj['cashReceiptAt'] = obj['cash_receipt_at'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class LogEvtModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['logEvtId'] = obj['l_no'];
		}
		if(obj['l_eid'] !== undefined) {
			this._obj['evtId'] = obj['l_eid'];
		}
		if(obj['l_obj'] !== undefined) {
			this._obj['obj'] = obj['l_obj'];
		}
		if(obj['l_obj_detail'] !== undefined) {
			this._obj['objDtl'] = obj['l_obj_detail'];
		}
		if(obj['l_action'] !== undefined) {
			this._obj['action'] = obj['l_action'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['target'] = obj['l_type'];
		}
		if(obj['l_id'] !== undefined) {
			this._obj['targetId'] = obj['l_id'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
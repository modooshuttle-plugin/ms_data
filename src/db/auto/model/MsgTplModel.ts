
	export class MsgTplModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['t_no'] !== undefined) {
			this._obj['msgTplId'] = obj['t_no'];
		}
		if(obj['t_rtype'] !== undefined) {
			this._obj['rtCd'] = obj['t_rtype'];
		}
		if(obj['t_title'] !== undefined) {
			this._obj['title'] = obj['t_title'];
		}
		if(obj['t_subject'] !== undefined) {
			this._obj['subject'] = obj['t_subject'];
		}
		if(obj['t_content'] !== undefined) {
			this._obj['content'] = obj['t_content'];
		}
		if(obj['t_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['t_timestamp'];
		}
		if(obj['t_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['t_timestamp_u'];
		}
		if(obj['t_ata_id'] !== undefined) {
			this._obj['ataId'] = obj['t_ata_id'];
		}
		if(obj['ata_tpl_id'] !== undefined) {
			this._obj['ataTplId'] = obj['ata_tpl_id'];
		}
		if(obj['utm_source'] !== undefined) {
			this._obj['utmSource'] = obj['utm_source'];
		}
		if(obj['utm_medium'] !== undefined) {
			this._obj['utmMedium'] = obj['utm_medium'];
		}
		if(obj['utm_campaign'] !== undefined) {
			this._obj['utmCampaign'] = obj['utm_campaign'];
		}
		if(obj['use_yn'] !== undefined) {
			this._obj['useYn'] = obj['use_yn'];
		}
		if(obj['kakao_opt'] !== undefined) {
			this._obj['kakaoOpt'] = obj['kakao_opt'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
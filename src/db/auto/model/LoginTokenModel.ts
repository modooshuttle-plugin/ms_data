
	export class LoginTokenModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['l_no'] !== undefined) {
			this._obj['loginTokenId'] = obj['l_no'];
		}
		if(obj['l_mid'] !== undefined) {
			this._obj['userId'] = obj['l_mid'];
		}
		if(obj['l_type'] !== undefined) {
			this._obj['providerCd'] = obj['l_type'];
		}
		if(obj['l_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['l_timestamp'];
		}
		if(obj['l_ip'] !== undefined) {
			this._obj['ipAddr'] = obj['l_ip'];
		}
		if(obj['l_url'] !== undefined) {
			this._obj['url'] = obj['l_url'];
		}
		if(obj['l_device'] !== undefined) {
			this._obj['device'] = obj['l_device'];
		}
		if(obj['l_os'] !== undefined) {
			this._obj['os'] = obj['l_os'];
		}
		if(obj['l_browser'] !== undefined) {
			this._obj['browser'] = obj['l_browser'];
		}
		if(obj['l_redirect'] !== undefined) {
			this._obj['redirect'] = obj['l_redirect'];
		}
		if(obj['l_token'] !== undefined) {
			this._obj['token'] = obj['l_token'];
		}
		if(obj['l_uuid'] !== undefined) {
			this._obj['uuid'] = obj['l_uuid'];
		}
		if(obj['l_secret'] !== undefined) {
			this._obj['secret'] = obj['l_secret'];
		}
		if(obj['l_expire'] !== undefined) {
			this._obj['expire'] = obj['l_expire'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
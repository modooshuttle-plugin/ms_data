
	export class DicModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dic_id'] !== undefined) {
			this._obj['dicId'] = obj['dic_id'];
		}
		if(obj['logical'] !== undefined) {
			this._obj['logical'] = obj['logical'];
		}
		if(obj['physical'] !== undefined) {
			this._obj['physical'] = obj['physical'];
		}
		if(obj['standard_en'] !== undefined) {
			this._obj['standardEn'] = obj['standard_en'];
		}
		if(obj['logical_equal_ko'] !== undefined) {
			this._obj['logicalEqualKo'] = obj['logical_equal_ko'];
		}
		if(obj['physical_equal_en'] !== undefined) {
			this._obj['physicalEqualEn'] = obj['physical_equal_en'];
		}
		if(obj['dic_cd'] !== undefined) {
			this._obj['dicCd'] = obj['dic_cd'];
		}
		if(obj['deploy_yn'] !== undefined) {
			this._obj['deployYn'] = obj['deploy_yn'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}

	export class CpnUseModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['o_no'] !== undefined) {
			this._obj['cpnUseId'] = obj['o_no'];
		}
		if(obj['o_rid'] !== undefined) {
			this._obj['rtId'] = obj['o_rid'];
		}
		if(obj['o_mid'] !== undefined) {
			this._obj['userId'] = obj['o_mid'];
		}
		if(obj['o_oid'] !== undefined) {
			this._obj['boardId'] = obj['o_oid'];
		}
		if(obj['o_pid'] !== undefined) {
			this._obj['payId'] = obj['o_pid'];
		}
		if(obj['o_cid'] !== undefined) {
			this._obj['cpnId'] = obj['o_cid'];
		}
		if(obj['o_muid'] !== undefined) {
			this._obj['orderNo'] = obj['o_muid'];
		}
		if(obj['o_cuse'] !== undefined) {
			this._obj['useYn'] = obj['o_cuse'];
		}
		if(obj['o_price'] !== undefined) {
			this._obj['amount'] = obj['o_price'];
		}
		if(obj['o_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['o_timestamp'];
		}
		if(obj['o_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['o_timestamp_u'];
		}
		if(obj['o_admin'] !== undefined) {
			this._obj['adminId'] = obj['o_admin'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
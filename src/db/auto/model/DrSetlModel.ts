
	export class DrSetlModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['dr_setl_id'] !== undefined) {
			this._obj['drSetlId'] = obj['dr_setl_id'];
		}
		if(obj['dr_pay_id'] !== undefined) {
			this._obj['drPayId'] = obj['dr_pay_id'];
		}
		if(obj['biz_cd'] !== undefined) {
			this._obj['bizCd'] = obj['biz_cd'];
		}
		if(obj['runn_day_cnt'] !== undefined) {
			this._obj['runnDayCnt'] = obj['runn_day_cnt'];
		}
		if(obj['not_runn_day_cnt'] !== undefined) {
			this._obj['notRunnDayCnt'] = obj['not_runn_day_cnt'];
		}
		if(obj['except_day_cnt'] !== undefined) {
			this._obj['exceptDayCnt'] = obj['except_day_cnt'];
		}
		if(obj['default_amount'] !== undefined) {
			this._obj['defaultAmount'] = obj['default_amount'];
		}
		if(obj['pay_amount'] !== undefined) {
			this._obj['payAmount'] = obj['pay_amount'];
		}
		if(obj['not_runn_amount'] !== undefined) {
			this._obj['notRunnAmount'] = obj['not_runn_amount'];
		}
		if(obj['add_amount'] !== undefined) {
			this._obj['addAmount'] = obj['add_amount'];
		}
		if(obj['iss_add_amount'] !== undefined) {
			this._obj['issAddAmount'] = obj['iss_add_amount'];
		}
		if(obj['iss_remove_amount'] !== undefined) {
			this._obj['issRemoveAmount'] = obj['iss_remove_amount'];
		}
		if(obj['supply_amount'] !== undefined) {
			this._obj['supplyAmount'] = obj['supply_amount'];
		}
		if(obj['vat_amount'] !== undefined) {
			this._obj['vatAmount'] = obj['vat_amount'];
		}
		if(obj['etc_amount'] !== undefined) {
			this._obj['etcAmount'] = obj['etc_amount'];
		}
		if(obj['ref_amount'] !== undefined) {
			this._obj['refAmount'] = obj['ref_amount'];
		}
		if(obj['total_runn_amount'] !== undefined) {
			this._obj['totalRunnAmount'] = obj['total_runn_amount'];
		}
		if(obj['vat_cd'] !== undefined) {
			this._obj['vatCd'] = obj['vat_cd'];
		}
		if(obj['setl_cd'] !== undefined) {
			this._obj['setlCd'] = obj['setl_cd'];
		}
		if(obj['rt_id'] !== undefined) {
			this._obj['rtId'] = obj['rt_id'];
		}
		if(obj['dr_id'] !== undefined) {
			this._obj['drId'] = obj['dr_id'];
		}
		if(obj['bus_compn_id'] !== undefined) {
			this._obj['busCompnId'] = obj['bus_compn_id'];
		}
		if(obj['dispatch_id'] !== undefined) {
			this._obj['dispatchId'] = obj['dispatch_id'];
		}
		if(obj['setl_month'] !== undefined) {
			this._obj['setlMonth'] = obj['setl_month'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['end_day'] !== undefined) {
			this._obj['endDay'] = obj['end_day'];
		}
		if(obj['transfer_yn'] !== undefined) {
			this._obj['transferYn'] = obj['transfer_yn'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['admin_id'] !== undefined) {
			this._obj['adminId'] = obj['admin_id'];
		}
		if(obj['dr_ref_id'] !== undefined) {
			this._obj['drRefId'] = obj['dr_ref_id'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
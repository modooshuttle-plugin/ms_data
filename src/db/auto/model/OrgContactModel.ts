
	export class OrgContactModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['org_contact_id'] !== undefined) {
			this._obj['orgContactId'] = obj['org_contact_id'];
		}
		if(obj['org_id'] !== undefined) {
			this._obj['orgId'] = obj['org_id'];
		}
		if(obj['org_ctrt_id'] !== undefined) {
			this._obj['orgCtrtId'] = obj['org_ctrt_id'];
		}
		if(obj['contact_nm'] !== undefined) {
			this._obj['contactNm'] = obj['contact_nm'];
		}
		if(obj['contact_manager'] !== undefined) {
			this._obj['contactManager'] = obj['contact_manager'];
		}
		if(obj['contact_phone'] !== undefined) {
			this._obj['contactPhone'] = obj['contact_phone'];
		}
		if(obj['contact_email'] !== undefined) {
			this._obj['contactEmail'] = obj['contact_email'];
		}
		if(obj['start_day'] !== undefined) {
			this._obj['startDay'] = obj['start_day'];
		}
		if(obj['created_at'] !== undefined) {
			this._obj['createdAt'] = obj['created_at'];
		}
		if(obj['updated_at'] !== undefined) {
			this._obj['updatedAt'] = obj['updated_at'];
		}
		if(obj['on_cd'] !== undefined) {
			this._obj['onCd'] = obj['on_cd'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
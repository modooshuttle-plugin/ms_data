
	export class ColumnModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['column_id'] !== undefined) {
			this._obj['columnId'] = obj['column_id'];
		}
		if(obj['table_id'] !== undefined) {
			this._obj['tableId'] = obj['table_id'];
		}
		if(obj['nm'] !== undefined) {
			this._obj['nm'] = obj['nm'];
		}
		if(obj['old_nm'] !== undefined) {
			this._obj['oldNm'] = obj['old_nm'];
		}
		if(obj['type'] !== undefined) {
			this._obj['type'] = obj['type'];
		}
		if(obj['kor_nm'] !== undefined) {
			this._obj['korNm'] = obj['kor_nm'];
		}
		if(obj['ver'] !== undefined) {
			this._obj['ver'] = obj['ver'];
		}
		if(obj['comment'] !== undefined) {
			this._obj['comment'] = obj['comment'];
		}
		if(obj['db'] !== undefined) {
			this._obj['db'] = obj['db'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
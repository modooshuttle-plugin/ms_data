
	export class PayModel {
		_obj: any = {};
		constructor(obj: any) {
		if(obj['p_no'] !== undefined) {
			this._obj['payId'] = obj['p_no'];
		}
		if(obj['p_mid'] !== undefined) {
			this._obj['userId'] = obj['p_mid'];
		}
		if(obj['p_price'] !== undefined) {
			this._obj['amount'] = obj['p_price'];
		}
		if(obj['p_rid'] !== undefined) {
			this._obj['rtId'] = obj['p_rid'];
		}
		if(obj['p_oid'] !== undefined) {
			this._obj['boardId'] = obj['p_oid'];
		}
		if(obj['p_rpid'] !== undefined) {
			this._obj['prodsId'] = obj['p_rpid'];
		}
		if(obj['p_poid'] !== undefined) {
			this._obj['payTryId'] = obj['p_poid'];
		}
		if(obj['p_timestamp'] !== undefined) {
			this._obj['createdAt'] = obj['p_timestamp'];
		}
		if(obj['p_timestamp_u'] !== undefined) {
			this._obj['updatedAt'] = obj['p_timestamp_u'];
		}
		if(obj['p_confirm'] !== undefined) {
			this._obj['statusCd'] = obj['p_confirm'];
		}

		}

		get obj(){
			return Object.keys(this._obj).length > 0 ? this._obj : null;
		}
}
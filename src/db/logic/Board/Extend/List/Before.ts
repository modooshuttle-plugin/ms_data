import { ApplySql, BoardSql, RtSql, UserSql } from "../../../../";
import { Ar } from "../../../../../util";

class BeforeList {
  /**
   * 탑승자 정보
   * @returns
   */
  public getBoardInfo = (col: boolean): Ar => {
    let ar;
    if (col === true) {
      ar = BoardSql.select(true);
      ApplySql.selectAlias(ar);
      UserSql.selectAlias(ar);
      RtSql.selectAlias(ar);
    } else {
      ar = BoardSql.table();
    }

    ar.join(`apply_view apply on board.apply_id = apply.apply_id`);
    ar.join(`user_view user on board.user_id = user.user_id`);
    ar.join(`rt_view rt on board.rt_id = rt.rt_id`);

    ar.join(
      `apply_view apply_sub 
        on apply.subscribe_id = apply_sub.subscribe_id 
        and apply_sub.start_day > board.end_day
      `,
      `left`
    );

    ar.where(` apply.prods_cd = 1 `);
    ar.where(` board.board_cd = 'b' `);
    ar.where(` board.board_shape_cd in (0, 2) `);
    ar.where(` user.user_cd = 'a' `);
    ar.where(` rt.rt_cd = 'b' `);
    ar.where(` rt.rt_status_cd <> 'a' `);
    ar.where(` apply_sub.apply_id is not null `);

    return ar;
  };
}

export const ExtendBeforeListService = new BeforeList();

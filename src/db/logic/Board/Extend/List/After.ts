import { ApplySql, BoardSql, RtSql, UserSql } from "../../../../";
import { Ar } from "../../../../../util";

/**
 *  연장 복사된 탑승정보 쿼리
 */
class After {
  /**
   * 연장 복사자
   * @param col
   * @returns
   */
  public getCopiedBoard = (col: boolean): Ar => {
    let ar;
    if (col === true) {
      ar = BoardSql.select(true);
      ApplySql.selectAlias(ar);
      UserSql.selectAlias(ar);
      RtSql.selectAlias(ar);
    } else {
      ar = BoardSql.table();
    }

    ar.join(`apply_view apply on board.apply_id = apply.apply_id`);
    ar.join(`user_view user on board.user_id = user.user_id`);
    ar.join(`rt_view rt on board.rt_id = rt.rt_id`);

    ar.where(` apply.apply_cd = 'b' `); // 연장복사된 데이터

    ar.where(` board.board_cd = 'd' `); // 탑승구분

    ar.where(` rt.rt_cd = 'b' `); // 운행중
    ar.where(` rt.activate_cd = 1 `); // 활성화
    ar.where(` rt.commute_cd = 0 `); // 출근
    ar.where(` rt.biz_cd = 0 `); // b2c

    return ar;
  };
}

export const ExtendAfterListService = new After();

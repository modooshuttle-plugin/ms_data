import { ApplySql, BoardSql } from "../../../../";
import { Ar } from "../../../../../util";

/**
 *  정식 복사된 정보
 */
class After {
  public getFreeCopyBoard = (col: boolean): Ar => {
    let ar = BoardSql.table();
    if (col === true) {
      ApplySql.selectAlias(ar);
    }

    ar.select(`apply.apply_id`);
    ar.select(`apply.prev_apply_id`);
    ar.join(`apply_view apply on board.apply_id = apply.apply_id`);
    ar.where(` apply.apply_cd = 'd' `);
    ar.where(` board.board_cd = 'b' `);

    return ar;
  };
}

export const FreeAfterListService = new After();

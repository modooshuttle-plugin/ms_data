import { ApplySql, BoardSql, RtSql, UserSql } from "../../../../";
import { Ar } from "../../../../../util";

// 무료탑승 쿼리
class BeforeList {
  /**
   * 무료탑승자
   * @returns
   */
  public getFreeBoard = (col: boolean): Ar => {
    let ar;
    if (col === true) {
      ar = BoardSql.select(true);
      ApplySql.selectAlias(ar);
      UserSql.selectAlias(ar);
      RtSql.selectAlias(ar);
    } else {
      ar = BoardSql.table();
    }

    ar.join(` apply_view apply on board.apply_id = apply.apply_id `);
    ar.join(` user_view user on board.user_id = user.user_id `);
    ar.join(` rt_view rt on board.rt_id = rt.rt_id `);

    ar.where(` apply.prods_cd = 0 `);
    ar.where(` apply.apply_cd = 'c' `);

    ar.where(` board.board_cd = 'b' `);

    ar.where(` user.user_cd = 'a' `);

    ar.where(` rt.rt_cd = 'b' `);
    ar.where(` rt.rt_status_cd <> 'a' `);

    return ar;
  };
}

export const FreeBeforeListService = new BeforeList();

export interface DrRunn {
  date: string;
  device_id: string;
  runn_id: string;
  rt_id: string;
  started_at?: string;
  ended_at?: string;
}

export interface RunnHist {
  accuracy?: number;
  device_id: string;
  latitude?: number;
  longitude?: number;
  runn_id: string;
  created_at: string;
}

export interface RunnCurr {
  accuracy?: number;
  device_id: string;
  latitude?: number;
  longitude?: number;
  rt_id?: string;
  runn_id: string;
  created_at: string;
  date: string;
}

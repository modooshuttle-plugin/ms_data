export const msgDic = () => {
  return [
    { type: "user", code: "#{username}", ko: "유저이름", editable: false },
    { type: "user", code: "#{phone}", ko: "유저 휴대폰", editable: false },

    { type: "rt", code: "#{no}", ko: "출퇴근길 번호", editable: false },
    { type: "rt", code: "#{title}", ko: "출발 > 도착", editable: false },
    { type: "rt", code: "#{time}", ko: "도착시간", editable: false },
    {
      type: "rt",
      code: "#{s_category}",
      ko: "출발지 카테고리",
      editable: false,
    },
    {
      type: "rt",
      code: "#{e_category}",
      ko: "도착지 카테고리",
      editable: false,
    },
    {
      type: "rt",
      code: "#{link}",
      ko: "운행|운행예정 단축 url",
      editable: false,
    },

    { type: "board", code: "#{period}", ko: "탑승기간", editable: false },
    { type: "board", code: "#{pweek}", ko: "기간 요일 표시", editable: false },
    { type: "board", code: "#{dweek}", ko: "시작 요일", editable: false },
    { type: "board", code: "#{start_day}", ko: "시작일", editable: false },
    { type: "board", code: "#{end_day}", ko: "종료일", editable: false },
    { type: "board", code: "#{stop_day}", ko: "연장 중단일", editable: true },
    { type: "board", code: "#{sdweek}", ko: "연장중단 요일", editable: true },

    { type: "board", code: "#{npday}", ko: "신규결제 기한", editable: false },
    { type: "board", code: "#{epday}", ko: "연장결제 기한", editable: false },
    { type: "board", code: "#{start_alias}", ko: "탑승지", editable: false },
    { type: "board", code: "#{end_alias}", ko: "하차지", editable: false },

    {
      type: "board",
      code: "#{mypagelink}",
      ko: "마이페이지링크",
      editable: false,
    },

    {
      type: "pay",
      code: "#{price}",
      ko: "실 결제액 (월회비 - 쿠폰가)",
      editable: false,
    },
    { type: "pay", code: "#{paylink}", ko: "결제 링크", editable: false },
    { type: "vbank", code: "#{vbank_name}", ko: "은행명", editable: false },
    { type: "vbank", code: "#{vbank_num}", ko: "계좌번호", editable: false },
    { type: "vbank", code: "#{vbank_holder}", ko: "예금주", editable: false },
    { type: "vbank", code: "#{vbank_date}", ko: "결제기한", editable: false },

    { type: "blank", code: "#{blank}", ko: "빈칸", editable: true },

    {
      type: "make",
      code: "#{makelink}",
      ko: "알림받기 신청 페이지 url",
      editable: false,
    },
  ];
};

export const msgDicCheck = (type: string, tpl: string) => {
  const a = msgDic().filter((c) => c.type === type);
  let res = false;
  a.map((c) => {
    if (tpl.indexOf(c.code) > -1) {
      res = true;
    }
  });

  return res;
};

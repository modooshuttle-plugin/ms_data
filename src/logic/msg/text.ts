import {
  IApply,
  IBoard,
  ICat,
  IMake,
  IMsgTpl,
  IPaid,
  IPay,
  IPayTry,
  IRt,
  ITime,
  IUser,
} from "../../db";
import { userCvt, rtCvt, applyCvt, payCvt, makeCvt, vbankCvt } from "./convert";
import { makeCoolsms, makeMsgObj } from "./makeMsg";
import { tplCheck } from "./tpl";

// 유저 웹 페이지
export type TUrl = {
  USER: string;
  AUTH: string;
  PAY: string;
  ROUTE: string;
  MAKE: string;
};

type TVbank = {
  vbankDate: string;
  vbankHolder: string;
  vbankCode: string;
  vbankName: string;
  vbankNum: string;
};

export interface IMsgComp {
  user?: IUser;
  apply?: IApply;
  board?: IBoard;
  make?: IMake;
  pay?: IPay;
  payTry?: IPayTry;
  paid?: IPaid;

  rt?: IRt;
  vbank?: TVbank;
}

export interface IMsgMake extends IMsgComp {
  msg: any;
  msgTpl: IMsgTpl;
  timeList: ITime[];
  catList: ICat[];
  url: TUrl;
}

export class MakeMsgText {
  _obj: IMsgMake;

  constructor(obj: IMsgMake) {
    this._obj = obj;
  }

  get makeText() {
    const { msgTpl } = this._obj;

    const check = tplCheck(msgTpl.content);

    if (check.blank === true) {
    } else {
      let tplTxt: string = msgTpl.content;

      if (check.user === true) {
        // user
        const userR = userCvt(this._obj, tplTxt);
        tplTxt = userR.text;
        // tplTxt
      }

      // rt, catList, timeList
      if (check.rt === true) {
        const rtR = rtCvt(this._obj, tplTxt);
        tplTxt = rtR.text;
      }

      // apply, board, rt
      if (check.board === true) {
        const boardR = applyCvt(this._obj, tplTxt);
        tplTxt = boardR.text;
      }

      // pay
      if (check.pay === true) {
        const payR = payCvt(this._obj, tplTxt);
        tplTxt = payR.text;
      }

      // make - 일단 지금은 사용 안함.
      if (check.make === true) {
        const makeR = makeCvt(this._obj, tplTxt);
        tplTxt = makeR.text;
      }

      //
      if (check.vbank === true) {
        const vbankR = vbankCvt(this._obj, tplTxt);
        tplTxt = vbankR.text;
      }

      return {
        check,
        tplTxt,
        msg: makeMsgObj(this._obj, tplTxt),
        coolsms: makeCoolsms(this._obj, tplTxt),
      };
    }

    return {
      check,
    };
  }
}

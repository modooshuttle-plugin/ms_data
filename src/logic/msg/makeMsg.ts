import { mFormat } from "../../util/mymoment";
import { IMsgMake } from "./text";

/**
 * 문자내용 체크
 * @param text 탬플릿 내용
 * @returns
 */
export const makeMsgObj = (obj: IMsgMake, tplTxt: string) => {
  const { rt, user, msg, msgTpl } = obj;

  const { subject, phone, reservAt, msgCompnCd, adminId, msgTplId } = msg;

  const cur = mFormat(`YYYY-MM-DD HH:mm:ss`);
  const res = {
    subject,
    phone,
    rtId: rt?.rtId || null,
    reservAt,
    mbrCd: msg.msgCd || "user",

    msgCompnCd: msg.msgCompnCd || "coolsms",
    userId: user?.userId || "",
    msgTplId,
    comment: tplTxt,
    createdAt: cur,
  };

  return res;
};

export interface ICoolsms {
  from: string;
  text: string;
  to: string;
  type: string;
  subject?: string;
}

/**
 * coolsms 발송 객체
 * @param obj
 * @param tplTxt
 * @returns
 */
export const makeCoolsms = (obj: IMsgMake, tplTxt: string): ICoolsms => {
  const { user, msg } = obj;

  const mshuttlePhone = {
    driver: "01046740617",
    user: "027751008",
  };

  const res: any = {
    from: mshuttlePhone.user,
    to: user?.phone || "",
    subject: msg.subject,
    text: tplTxt,
    type: msg.msgCd,
  };
  // if (res.type === "ATA") {
  //   res.kakaoOptions = {
  //     pfId: "KA01PF200323182344978rYCZNjngIcJ",
  //     templateId: msg.ataTplId,
  //   };
  // }

  return res;
};

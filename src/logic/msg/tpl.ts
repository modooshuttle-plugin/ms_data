import { msgDicCheck } from "./dic";

// 탬플릿을 체크하여 무엇을 변경해야하는지 판단한다.
export const tplCheck = (text: string) => {
  const obj = {
    blank: false,
    rt: false,
    user: false,
    board: false,
    pay: false,
    vbank: false,
    make: false,
  };

  if (msgDicCheck("blank", text)) {
    obj.blank = true;
    return obj;
  }

  if (msgDicCheck("rt", text)) {
    obj.rt = true;
  }

  if (msgDicCheck("user", text)) {
    obj.user = true;
  }

  if (msgDicCheck("board", text)) {
    obj.board = true;
  }

  if (msgDicCheck("pay", text)) {
    obj.pay = true;
  }

  if (msgDicCheck("make", text)) {
    obj.make = true;
  }

  if (msgDicCheck("vbank", text)) {
    obj.vbank = true;
  }

  return obj;
};

/**
 * 탬플릿에 변수가 전부 치환되었는지 체크함.
 * @param text
 * @returns
 */
export const hasNotValue = (text: string) => {
  const obj: any = tplCheck(text);

  const idx = Object.keys(obj).findIndex((c) => obj[c] === true);

  return idx === -1 ? true : false;
};

import { mComma, nullCheck } from "../../util";
import { mFormat } from "../../util/mymoment";
import { IMsgMake } from "./text";
const moment = require("moment-timezone");

export const rtCvt = (msg: IMsgMake, text: string) => {
  let valid = false;
  let reason = [];

  try {
    const { rt, catList, timeList } = msg;
    // console.log(route);
    if (rt) {
      text = text.replace(/#{no}/g, rt.rtId);
      text = text.replace(/#{title}/g, `${rt.startInfo} > ${rt.endInfo}`);

      const timeObj = timeList.find((c) => c.timeId === rt.timeId);

      text = text.replace(/#{time}/g, `${timeObj?.time}`);

      const start = catList.find((c) => c.catCd === rt.startCatCd);
      const end = catList.find((c) => c.catCd === rt.endCatCd);
      text = text.replace(/#{s_category}/g, `${start?.alias}`);
      text = text.replace(/#{e_category}/g, `${end?.alias}`);

      if (rt.rtCd === "b") {
        text = text.replace(/#{link}/g, `${rt.runnUrl}`);
      } else if (rt.rtCd === "c") {
        text = text.replace(/#{link}/g, `${rt.schdUrl}`);
      }

      valid = true;
    } else {
      valid = false;
      reason.push("경로 정보가 존재하지 않습니다.");
    }
  } catch (error) {
    reason.push("경로 정보를 불러오던 도중 오류가 발생했습니다.");
    valid = false;
  }

  return {
    reason,
    text,
    valid,
  };
};

export const userCvt = (msg: IMsgMake, text: string) => {
  const { user } = msg;
  const reason: any[] = [];

  let valid = true;

  try {
    console.log(user);
    if (user) {
      if (user.nm && user.nm.length > 0) {
        text = text.replace(/#{username}/g, `${user.nm}`);
      } else {
        text = text.replace(/#{username}/g, "회원");
      }

      text = text.replace(/#{phone}/g, `${user.phone}`);

      valid = true;
    } else {
      valid = false;
      reason.push("회원정보가 존재하지 않습니다.");
    }
  } catch (error) {
    reason.push("회원 정보를 불러오던 도중 오류가 발생했습니다.");
  }

  return {
    valid,
    text,
    reason,
  };
};

// 탑승 정보 텍스트 변경 - oid
export const applyCvt = (msg: IMsgMake, text: string) => {
  const { apply, board, rt, url } = msg;

  const reason: any[] = [];

  let valid = true;

  try {
    const dw: any = ["일", "월", "화", "수", "목", "금", "토"];
    if (apply && board) {
      const link = `${url.USER}/mypage/ApplyDetail?n=${board.boardId}`;
      if (
        nullCheck(board.startDay) === true &&
        nullCheck(board.endDay) === true
      ) {
        const start = board.startDay.split(`-`);
        const end = board.endDay.split(`-`);
        const period = `${parseInt(start[1], 10)}/${parseInt(
          start[2],
          10
        )}~${parseInt(end[1], 10)}/${parseInt(end[2], 10)}`;
        text = text.replace(/#{period}/g, period);

        if (text.indexOf("#{dweek}") > -1) {
          const dws = moment(board.startDay).tz("Asia/Seoul").day();
          text = text.replace(/#{dweek}/g, dw[dws]);
        }

        if (text.indexOf("#{pweek}") > -1) {
          const dws = moment(board.startDay).tz("Asia/Seoul").day();
          const dwe = moment(board.startDay).tz("Asia/Seoul").day();
          text = text.replace(/#{pweek}/g, `${dw[dws]}~${dw[dwe]}`);
        }

        if (text.indexOf("#{epday}") > -1) {
          const sd = board.startDay.split(`-`);
          const start_day = `${parseInt(sd[1], 10)}/${parseInt(sd[2], 10)} (${
            dw[moment(board.startDay).tz("Asia/Seoul").day()]
          }) 오후 4시까지`;
          text = text.replace(/#{epday}/g, start_day);
        }

        // `if(${ob}.o_start_day is not null, (select a_date from a_calendar  where a_date < ${ob}.o_start_day and a_oper = 0  order by a_date desc limit 0, 1), null ) as npday`

        // if (text.indexOf("#{npday}") > -1) {
        //   const d = moment(apply.npday).tz("Asia/Seoul").format("YYYY-MM-DD");
        //   const sd = d.split(`-`);
        //   const start_day = `${parseInt(sd[1], 10)}/${parseInt(sd[2], 10)} (${
        //     dw[moment(d).tz("Asia/Seoul").day()]
        //   }) 오후 4시까지`;
        //   text = text.replace(/#{npday}/g, start_day);
        // }

        // cron
        if (text.indexOf("#{npday}") > -1) {
          if (board.startDay !== null && board.startDay !== undefined) {
            if (rt?.rtCd === "b") {
              // const d = moment(obj.start_day).tz('Asia/Seoul').add(-1, 'day').format('YYYY-MM-DD');
              const d = mFormat("YYYY-MM-DD");
              const start = d.split(`-`);
              const start_day = `${parseInt(start[1], 10)}/${parseInt(
                start[2],
                10
              )} (${dw[moment(d).tz("Asia/Seoul").day()]}) 오후 4시까지`;
              text = text.replace(/#{npday}/g, start_day);
            } else if (rt?.rtCd === "c") {
              // const d = moment(obj.timestamp).tz('Asia/Seoul').add(6, 'day').format('YYYY-MM-DD');
              const d = mFormat("YYYY-MM-DD");
              const start = d.split(`-`);
              const start_day = `${parseInt(start[1], 10)}/${parseInt(
                start[2],
                10
              )} (${dw[moment(d).tz("Asia/Seoul").day()]}) 오후 4시까지`;
              text = text.replace(/#{npday}/g, start_day);
            }
          }
        }

        if (text.indexOf("#{start_day}") > -1) {
          const sd = board.startDay.split(`-`);
          const start_day = `${parseInt(sd[1], 10)}/${parseInt(sd[2], 10)}`;
          text = text.replace(/#{start_day}/g, start_day);
        }

        // if (text.indexOf("#{start_alias}") > -1) {
        //   text = text.replace(/#{start_alias}/g, apply.start_alias);
        // }

        // if (text.indexOf("#{end_alias}") > -1) {
        //   text = text.replace(/#{end_alias}/g, apply.end_alias);
        // }
      }

      text = text.replace(/#{mypagelink}/g, `${link}`);
      console.log(text);
      valid = true;
    } else {
      valid = false;
      reason.push("신청 정보가 존재하지 않습니다.");
    }
  } catch (error) {
    console.log(error);
    reason.push("신청 정보를 불러오던 도중 오류가 발생했습니다.");
    valid = false;
  }

  return {
    valid,
    text,
    reason,
    apply,
  };
};

export const vbankCvt = (msg: IMsgMake, text: string) => {
  const { vbank } = msg;
  const reason = [];
  let payOrder: any;

  let valid = true;

  valid = true;

  try {
    if (vbank) {
      const { vbankName, vbankNum, vbankHolder, vbankDate } = vbank;
      text = text.replace(/#{vbank_name}/g, vbankName);
      text = text.replace(/#{vbank_num}/g, vbankNum);
      text = text.replace(/#{vbank_holder}/g, vbankHolder);
      if (vbankDate) {
        const m = moment(vbankDate).tz("Asia/Seoul");
        text = text.replace(/#{vbank_date}/g, m.format("YYYY-MM-DD HH:mm:ss"));
      }

      valid = true;
    } else {
      valid = false;
      reason.push("가상계좌 정보를 발급받은 내역을 확인할수 없습니다.");
    }
  } catch (error) {
    valid = false;
    reason.push("결제 정보를 조회할수 없습니다.");
  }

  return {
    valid,
    text,
    reason,

    vbank,
  };
};

// 결제정보 텍스트 변경 - oid -> 신청정보 하위
export const payCvt = (msg: IMsgMake, text: string) => {
  const { pay, url } = msg;
  const reason: any[] = [];

  let valid = true;
  if (text.indexOf("#{price}") > -1 || text.indexOf("#{paylink}") > -1) {
    const mpay = `${url.PAY}`;
    try {
      console.log(pay);
      if (pay) {
        text = text.replace(/#{price}/g, mComma(pay.amount));

        const paylink = `${mpay}/?id=${pay.payId}`;
        text = text.replace(/#{paylink}/g, paylink);

        if (pay.payId) {
          valid = false;
          reason.push("결제완료/환불 상태 입니다.");
        } else {
          valid = true;
        }
      } else {
        valid = false;
        reason.push("결제 정보가 존재하지 않습니다.");
      }
    } catch (error) {
      valid = false;
      reason.push("결제 정보를 불러오던 도중 문제가 발생하였습니다.");
    }
  }

  return {
    valid,
    text,
    reason,
    pay,
  };
};

export const makeCvt = (msg: IMsgMake, text: string) => {
  const { make, url } = msg;
  const reason: any[] = [];
  if (text.indexOf("#{makelink}") > -1) {
    const make = `${url.MAKE}/make`;

    text = text.replace(/#{makelink}/g, make);
  }

  return {
    valid: true,
    text,
    reason,
    make,
  };
};

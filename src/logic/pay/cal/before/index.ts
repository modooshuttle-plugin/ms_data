import { nullCheck } from "../../../../util";
import { IOutput, TPayOrg } from "../Cal";

type TMshuttleCal = { calObj: IOutput; payObj: TPayOrg };

export class BeforeCal {
  _obj: TMshuttleCal;

  constructor(obj: TMshuttleCal) {
    this._obj = obj;
  }
  public firstStep = () => {
    console.log(`firstStep`);
    const { payObj } = this._obj;
    const { orgUserAuth } = payObj;
    if (orgUserAuth?.methodCd === "a") {
      const obj = this.calA(this._obj);
      return obj;
    } else if (orgUserAuth?.methodCd === "b") {
      const obj = this.calB(this._obj);
      return obj;
    } else if (orgUserAuth?.methodCd === "c") {
      // 결제 맨마지막에 -만 해주면 되기때문에 할게 없다.
      const obj = this.calC(this._obj);
      return obj;
    } else {
      return this._obj.calObj;
    }
  };

  private calA = (obj: TMshuttleCal): IOutput => {
    const { calObj, payObj } = obj;
    const { payDc, payOrg } = payObj;
    // 결제할인(payDc)을 먼저 계산한다. 해당값은 없을수도 있다.

    if (payDc !== undefined) {
      if (calObj.output.paidDc !== undefined) {
        calObj.output.paidDc.amount = payDc.amount || 0;
      }
      calObj.amount.defaultAmount += payDc?.amount || 0;
    }
    calObj.amount.payOrgAmount =
      -1 * (calObj.amount.defaultAmount - (payOrg?.amount || 0));
    calObj.amount.defaultAmount = payObj.payOrg?.amount || 0;
    if (calObj.output.paidOrg !== undefined) {
      calObj.output.paidOrg.amount = calObj.amount.payOrgAmount;
    }

    return calObj;
  };

  private calB = (obj: TMshuttleCal): IOutput => {
    const { calObj, payObj } = obj;
    const { payOrg } = payObj;
    if (nullCheck(payOrg) === true) {
      calObj.ratio = payOrg?.ratio || "0"; // 일단 비율은 받아온다.
    }
    return this.calC({ calObj, payObj });
  };

  private calC = (obj: TMshuttleCal): IOutput => {
    const { calObj, payObj } = obj;
    const { payDc } = payObj;
    if (payDc !== undefined) {
      const { amount } = payDc;
      if (calObj.output.paidDc !== undefined) {
        calObj.output.paidDc.amount = amount;
      }
    }

    return calObj;
  };
}

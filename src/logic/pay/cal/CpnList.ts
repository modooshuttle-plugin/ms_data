// 쿠폰 조건

import { nullCheck } from "../../../util";
import { calCoupon } from "../pay_cal";

import { TCpn } from "./Cal";

//  쿠폰 계산
export class CalCpnList {
  private _cpnList: TCpn[] = [];
  private _defaultAmount: number = 0;

  constructor(cpnList: TCpn[], defaultAmount: number) {
    this._cpnList = cpnList;
    this._defaultAmount = defaultAmount;
  }

  get cpnList() {
    return this._cpnList;
  }

  get defaultAmount() {
    return this._defaultAmount;
  }

  // 쿠폰 계산
  get cal() {
    let amount = 0;
    const cpnUseList: any[] = [];
    const paidCpnList: any[] = [];
    this._cpnList.forEach((obj: TCpn) => {
      const calAmount = calCoupon(this._defaultAmount, obj.cpn);
      // rt_id, pay_id, board_id, orderNo, createdAt, updatedAt, adminId

      cpnUseList.push({ ...obj.cpn, amount: calAmount });
      if (nullCheck(obj.cpnUse)) {
        paidCpnList.push({ ...obj.cpnUse, cpnTplId: obj.cpn.cpnTplId });
      }

      amount += calAmount;
    });

    return {
      cpnUseList, // 쿠폰 사용 리스트 -> db에 바로 insert
      amount, // 계산에 사용하자
      paidCpnList,
    };
  }
}

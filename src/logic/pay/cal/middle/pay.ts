import { CalCpnList } from "../CpnList";
import { CalPayAdjustList } from "../PayAdjustList";
import { IOutput, TPayOrg } from "../Cal";

type TMshuttleCal = { calObj: IOutput; payObj: TPayOrg };

// 모두의셔틀 기본 결제 계산 방법
export class MshuttleCal {
  _obj: TMshuttleCal;

  constructor(obj: TMshuttleCal) {
    this._obj = obj;
  }

  get cal() {
    const { calObj, payObj } = this._obj;
    const { defaultAmount } = calObj.amount;

    const { cpnList, payAdjustList } = payObj;
    const {
      paidCpnList,
      cpnUseList,
      amount: cpnAmount,
    } = new CalCpnList(cpnList, defaultAmount).cal;

    const { paidAdjustList, amount: payAdjustAmount } = new CalPayAdjustList(
      payAdjustList
    ).cal;

    calObj.output.cpnUseList = cpnUseList;
    calObj.output.paidCpnList = paidCpnList;
    calObj.output.paidAdjustList = paidAdjustList;

    calObj.amount.cpnAmount = cpnAmount;
    calObj.amount.payAdjustAmount = payAdjustAmount;
    calObj.amount.totalAmount = defaultAmount + cpnAmount + payAdjustAmount; // 지금까지 총합계

    return calObj;
  }
}

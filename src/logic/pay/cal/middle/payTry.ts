import { ICpnUse, IPayAdjust } from "../../../../db";
import { CalCpnUseList } from "../CpnUseList";
import { CalPayAdjustList } from "../PayAdjustList";

type TMshuttleCal = {
  defaultAmount: number;
  paidId: number;
  cpnUseList: ICpnUse[];
  payAdjustList: IPayAdjust[];
};

// 모두의셔틀 기본 결제 계산 방법
export class MshuttleCalTry {
  _obj: TMshuttleCal;

  constructor(obj: TMshuttleCal) {
    this._obj = obj;
  }

  cal = () => {
    const { defaultAmount, cpnUseList, payAdjustList, paidId } = this._obj;

    const { paidCpnList, amount: cpnAmount } = new CalCpnUseList(
      cpnUseList || [],
      paidId
    ).cal;
    const { paidAdjustList, amount: payAdjustAmount } = new CalPayAdjustList(
      payAdjustList
    ).cal;

    return {
      paidCpnList,
      cpnUseList,
      cpnAmount,
      paidAdjustList,
      payAdjustAmount,
      totalAmount: defaultAmount + cpnAmount + payAdjustAmount, // 지금까지 총합계
    };
  };
}

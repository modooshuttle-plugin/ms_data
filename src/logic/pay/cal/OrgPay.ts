import { IOrgEmail, IOrgPayDcTpl, IOrgPayTpl, IOrgUserAuth } from "../../../db";
import { nullCheck } from "../../../util";

export type TOrg = {
  orgUserAuth?: IOrgUserAuth;
  orgPayTpl?: IOrgPayTpl;
  orgPayDcTpl?: IOrgPayDcTpl;
  orgEmailList: IOrgEmail[];
};

// 기업 인증 관련
//  -- 계산 방식
//  -- 인증
export class CalOrgPay {
  private _org;

  constructor(obj: TOrg) {
    this._org = obj;
  }

  get payOrgObj() {
    const { orgPayTpl, orgPayDcTpl, orgUserAuth } = this._org;

    let payDc;
    let payOrg;

    if (orgUserAuth?.methodCd === "a" || orgUserAuth?.methodCd === "b") {
      // 기업할인은 무조건 붙어야하고
      if (nullCheck(orgPayTpl) === true) {
        payOrg = {
          ...orgPayDcTpl,
        };
      }
      // 결제할인은 붙을수도 있고 아닐수도 있다.
      if (nullCheck(orgPayDcTpl) === true) {
        payDc = {
          ...orgPayDcTpl,
        };
      }
    } else if (orgUserAuth?.methodCd === "c") {
      // 결제 할인 하나만 붙는다.
      if (nullCheck(orgPayDcTpl) === true) {
        payDc = {
          ...orgPayDcTpl,
        };
      }
    }

    return {
      payDc,
      payOrg,
    };
  }

  // payOrg, payDc
  // getDefaultAmount = (amount: number) => {
  //   const { orgPay, orgPayTpl, orgPayDcTpl } = this._org;

  //   let defaultAmount: number = amount;

  //   let ratio;
  //   let payOrgAmount = 0;

  //   if (orgPay.orgPayCd === "a") {
  //     if (nullCheck(orgPayDcTpl) === true) {
  //       payDc =
  //       defaultAmount += orgPayDcTpl?.amount || 0;
  //     }

  //     if (nullCheck(orgPayTpl) === true) {
  //       payOrgAmount = defaultAmount - (orgPayTpl?.amount || 0);
  //       defaultAmount = orgPayTpl?.amount || 0;
  //     }
  //   } else if (orgPay.orgPayCd === "b") {
  //     if (orgPayTpl) {
  //       ratio = orgPayTpl?.func;
  //     }
  //   }

  //   return {
  //     orgPayCd: orgPay.orgPayCd,
  //     defaultAmount,
  //     payOrgAmount,
  //     ratio,
  //   };
  // };

  get validate() {
    return false;
  }

  emailAuth = (emailAddr: string) => {
    const { orgEmailList } = this._org;
    const arr = emailAddr.split("@");

    if (arr.length === 2) {
      const orgEmail = orgEmailList.find((c: IOrgEmail) => c.domain === arr[1]);
      if (nullCheck(orgEmail) === true) {
        return {
          emailAuth: {
            domain: orgEmail?.domain,
            emailAddr,
            useYn: 0,
          },
          // domain, rt_cd, email_addr ||  user_id, board_id, pay_id
          success: true,
          msg: "인증되었습니다.",
        };
      } else {
        return {
          msg: "등록되어있지 않은 이메일 주소입니다.",
          success: true,
        };
      }
    } else {
      return {
        success: false,
        msg: "이메일 형식이 아닙니다.",
      };
    }
  };
}

//

// 계산 사작전

// 계산 시작
// 기업인지가 중요함

//

import { ICpnUse, IPaidCpn } from "../../../db";

//  쿠폰 계산
export class CalCpnUseList {
  private _cpnUseList: ICpnUse[] = [];

  constructor(cpnUseList: ICpnUse[], paidId: number) {
    this._cpnUseList = cpnUseList;
  }

  get cpnUseList() {
    return this._cpnUseList;
  }

  // 쿠폰 계산
  get cal() {
    let amount = 0;
    const paidCpnList: IPaidCpn[] = [];
    this._cpnUseList.forEach((cpnUse: any) => {
      // paidId: 1, cpnTplId: 1
      paidCpnList.push({
        paidId: "?",
        ...cpnUse,
        useYn: 1,
        cpnTplId: 1, // 어디서 가져올까나? 아니면 지울까?
        // paidCpnId: 0,
      });
    });

    return {
      paidCpnList,
      amount, // 계산에 사용하자
    };
  }
}

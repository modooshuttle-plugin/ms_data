import {
  ICpn,
  ICpnConn,
  ICpnLimit,
  ICpnUse,
  IOrgUserAuth,
  IPaidDc,
  IPaidOrg,
  IPay,
  IPayAdjust,
  IPayDc,
  IPayOrg,
} from "../../../db";
import { nullCheck } from "../../../util";

import { AfterCal } from "./after";
import { BeforeCal } from "./before";
import { Cleanning } from "./final";
import { MshuttleCal } from "./middle";

export type TCpn = {
  cpn: ICpn;
  cpnUse?: ICpnUse;
  cpnConn?: ICpnConn;
  cpnLimit?: ICpnLimit;
};

export type TPayOrg = {
  orderNo?: string;

  pay: IPay; // 기본금액

  orgUserAuth?: IOrgUserAuth; // 기업 인증 여부 및 결제 할인 방식
  payOrg?: IPayOrg; // 기업할인
  payDc?: IPayDc; // 결제할인

  cpnList: TCpn[]; // 선택된 쿠폰 리스트
  payAdjustList: IPayAdjust[]; // 조정금액
  payTry: any;
};

export interface IOutput {
  methodCd: string | null;
  amount: {
    defaultAmount: number;
    payOrgAmount: number;
    cpnAmount: number;
    payAdjustAmount: number;
    totalAmount: number;
  };
  ratio: string;
  output: {
    payTry: any;
    paidOrg?: IPaidOrg;
    paidDc?: IPaidDc;
    cpnUseList: any[];
    paidCpnList: any[];
    paidAdjustList: any[];
  };
}

export class Cal {
  private _org;

  constructor(obj: TPayOrg) {
    this._org = obj;
  }

  // 기본 금액 결정하기
  get calculate() {
    const { orgUserAuth, payOrg, payDc, pay, payTry } = this._org;

    let calObj: IOutput = {
      methodCd: null,
      amount: {
        defaultAmount: pay.amount,
        payOrgAmount: 0,
        cpnAmount: 0,
        payAdjustAmount: 0,
        totalAmount: 0,
      },
      ratio: "0",
      output: {
        payTry,
        paidOrg: undefined,
        paidDc: undefined,
        cpnUseList: [],
        paidCpnList: [],
        paidAdjustList: [],
      },
    };

    if (nullCheck(orgUserAuth) === true) {
      if (payOrg !== null && payOrg !== undefined) {
        calObj.output.paidOrg = { ...payOrg, amount: payOrg.amount } as any;
      }

      if (nullCheck(payDc) === true) {
        calObj.output.paidDc = { ...payDc } as any;
      }

      console.log(1);
      console.log(this._org.payOrg);

      // 1차 계산 기본데이터를 수정한다.
      calObj = new BeforeCal({
        payObj: this._org,
        calObj: calObj,
      }).firstStep();
      console.log(`1step`);
      console.log(calObj);

      // 2차 계산 (일반)
      calObj = new MshuttleCal({ payObj: this._org, calObj }).cal;

      console.log(`2step`);
      console.log(calObj);
      calObj = new AfterCal({ payObj: this._org, calObj }).thirdStep;

      console.log(`3step`);
      console.log(calObj);
    } else {
      // 일반 계산
      calObj = new MshuttleCal({ payObj: this._org, calObj }).cal;

      console.log(`Normal Step`);
      console.log(calObj);
    }

    calObj = new Cleanning({ payObj: this._org, calObj }).clean;

    return calObj;
  }
}

import { IPaidAdjust, IPayAdjust } from "../../../db";

// 조정금액 계산  (비율 계산이 없으므로 무조건 +-만 적용된다.)
export class CalPayAdjustList {
  private _payAdjustList: IPayAdjust[] = [];

  constructor(obj: IPayAdjust[]) {
    this._payAdjustList = obj;
  }

  get payAdjustList() {
    return this._payAdjustList;
  }

  get cal() {
    let amount = 0;

    const paidAdjustList: IPaidAdjust[] = [];
    this._payAdjustList.map((c: IPayAdjust) => {
      amount += c.amount;
    });

    return {
      paidAdjustList,
      amount,
    };
  }
}

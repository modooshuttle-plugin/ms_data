import { IPaid, IPayTry } from "../../../../db";

type TPayDataCheck = {
  payTry: IPayTry;
  paidTemp: IPaid; // 아임포트에서 보내준 값 or 모두의셔틀 초기 셋팅값
};

export class PayDataCheck {
  _obj: TPayDataCheck;

  constructor(obj: TPayDataCheck) {
    this._obj = obj;
  }

  // 금액이 위조되었는지 체크함.
  // (payTry에 들어갈때는 계산된 값을 넣기때문에 문제 없음.
  // 그러나 아임포트에서 결제된 값은 payTry가 생성된 이후에 수정이 가능함. )
  get isForgery() {
    const { payTry, paidTemp } = this._obj;

    return payTry.amount !== paidTemp.amount ? true : false;
  }

  // 결제가 실패했는지 체크한다.
  get isFailed() {
    const { paidTemp } = this._obj;
    if (paidTemp.statusCd === "failed") {
      return true;
    } else {
      return false;
    }
  }

  get isVbank() {
    const { paidTemp } = this._obj;
    if (paidTemp.statusCd === "ready" && paidTemp.methodCd === "vbank") {
      return true;
    } else {
      return false;
    }
  }
}

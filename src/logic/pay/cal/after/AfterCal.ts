import { IOutput, TPayOrg } from "../Cal";

type TMshuttleCal = { calObj: IOutput; payObj: TPayOrg };

export class AfterCal {
  _obj: TMshuttleCal;

  constructor(obj: TMshuttleCal) {
    this._obj = obj;
  }

  // 3차 계산
  get thirdStep() {
    const { calObj, payObj } = this._obj;
    const { ratio } = calObj;
    const { orgUserAuth } = payObj;

    if (orgUserAuth?.methodCd === "b") {
      if (ratio !== null) {
        if (calObj.output.paidOrg !== undefined) {
          calObj.output.paidOrg.amount =
            calObj.amount.totalAmount -
            calObj.amount.totalAmount * parseFloat(ratio); // 기업할인
        }
        calObj.amount.totalAmount =
          calObj.amount.totalAmount * parseFloat(ratio);
      }
    } else if (orgUserAuth?.methodCd === "c") {
      if (calObj.output.paidDc !== undefined) {
        calObj.amount.totalAmount += calObj.output?.paidDc.amount || 0;
      }
      // 결제 맨마지막에 -만 해주면 되기때문에 할게 없다.
    }

    return calObj;
  }
}

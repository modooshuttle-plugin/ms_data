import { IOutput, TPayOrg } from "../Cal";

type TMshuttleCal = { calObj: IOutput; payObj: TPayOrg };

export class Cleanning {
  _obj: TMshuttleCal;

  constructor(obj: TMshuttleCal) {
    this._obj = obj;
  }

  // 3차 계산
  get clean() {
    const { calObj, payObj } = this._obj;
    const { pay } = payObj;

    if (calObj.output.cpnUseList.length > 0) {
      calObj.output.cpnUseList.map((c) => {
        c.payId = pay.payId;
        c.boardId = pay.boardId;
        c.rtId = pay.rtId;
        return c;
      });
    }

    if (calObj.output.paidCpnList.length > 0) {
      calObj.output.paidCpnList.map((c) => {
        c.payId = pay.payId;
        c.boardId = pay.boardId;
        c.rtId = pay.rtId;
        return c;
      });
    }

    calObj.output.payTry.amount = calObj.amount.totalAmount;

    return calObj;
  }
}

import { IPaid, IRefund } from "../../../db";

// 환불 계산
export class CalRefund {
  private _paid: IPaid;
  private _refundList: IRefund[];

  constructor(paid: IPaid, refundList: IRefund[]) {
    this._paid = paid;
    this._refundList = refundList;
  }

  get paid() {
    return this._paid;
  }

  get refundList() {
    return this._refundList;
  }

  get cal() {
    // 환불 총금액
    let amount = 0;

    this._refundList.map((c: IRefund) => {
      amount += c.amount;
    });

    let statusCd = "cancelled";
    // 결제한 금액과 환불한 총액이 0이 되지 않으면 부분환불이다.
    // 즉 위에서 데이터를 잘 보내시길
    if (this._paid.amount + amount !== 0) {
      statusCd = "partialcancelled";
    }

    return {
      amount,
      statusCd,
    };
  }
}

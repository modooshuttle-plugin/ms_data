import {
  ICpn,
  ICpnConn,
  IEmailAuth,
  IPay,
  IPayAdjust,
  IPayDc,
  IPayOrg,
} from "../../../db/auto/interface";

// 결제전 화면 및 데이터 준비

export type TBeforePayCal = {
  pay: IPay; // 결제 기본정보
  payOrg: IPayOrg; // 기업할인
  payDcList: IPayDc[]; // 할인 리스트
  payAdjustList: IPayAdjust[]; // 조정금액 리스트 복수 지정 가능
  cpnList: ICpn[]; // 쿠폰 리스트
  emailAuth: IEmailAuth;
  cpnConnList: ICpnConn[]; // 쿠폰 강제적용 리스트
};

class BeforePayCal {
  private _payCal;

  constructor(obj: TBeforePayCal) {
    this._payCal = obj;
  }

  get payCal() {
    return this._payCal;
  }
}

export default BeforePayCal;

import { IPaid } from "../../../db";

const naverUrlDefault = `https://pay.naver.com/payments/detail/`;
const naverUrlTest = `https://test-pay.naver.com/payments/detail/`;

export const openReceipt = (paid: IPaid, imp: any, stage: string) => {
  if (typeof window !== "undefined") {
    if (paid.providerCd === "naverpay") {
      // imp.pg_tid

      if (stage === "production") {
        window.open(`${naverUrlDefault}${imp.pg_tid}`);
      } else {
        window.open(`${naverUrlTest}${imp.pg_tid}`);
      }
    } else if (paid.providerCd === "kcp") {
      window.open(imp.receipt_url);
    }
  }
};

import { ICpn, IPay } from "../../db/auto/interface";

// 쿠폰은 죄다 -를 가진다.
export const calCoupon = (defaultAmount: number, cpn: ICpn) => {
  let text = "";

  if (cpn.calCd === 1) {
    // 수식 계산
    text = `-${cpn.func.replace(/v/g, `${defaultAmount}`)}`;
  } else if (cpn.calCd === 0) {
    // 금액적용
    text = `-${cpn.amount}`;
  }

  return new Function(`return ${text}`)();
};

// 결제가 가능한지 판단 필요
export const getValidate = (cpnList: ICpn[], cpnConnect: any) => {
  let dupeYn = 0;
  cpnList.map((cpn: ICpn) => {
    // 강제로 연결된 쿠폰은 계산에서 제외된다.

    if (cpn.dupeYn === 0) {
      dupeYn = 1;
    }
  });
  return {
    dupeYn,
  };
};

// 주문번호 생성기
export const getOrderNo = (pay: IPay) => {
  return `S${pay.rtId}-${new Date().getTime()}-${pay.payId}`;
};

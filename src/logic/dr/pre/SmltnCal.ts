import { IOfferDrPay } from "../../../db/auto/interface";

export const simltnKo = {
  prodsAmount: "월회비 (부가세 포함, 원)",
  discountAmount: "할인액 (부가세포함, 원)",
  discountProdsAmount: "할인월회비(부가세별도, 원)",
  paidCnt: "결제자수(명)",
  drPayAmount: "제안액(부가세별도, 원)",
  drAmount: "평일최소기사비(부가세별도, 원)",
  diffAmount: "차액(부가세별도, 원)",
  incntCnt: "5차_인센티브설정인원수(명)",
  incntCntPerUser: "5차_1인당인센티브(원)",
  incntCntPerUserPerDiscountProdsAmount: "5차_1인당인센티브율(%)",
  minPaidCnt: "평일 최소 기사비_총 필요 결제자수(명)",
};

export interface SimltnInput
  extends Pick<
    IOfferDrPay,
    | "prodsAmount" // 회비
    | "discountAmount" // 할인액
    | "paidCnt" // 결제자수
    | "incntCnt"
    | "drAmount"
  > {}

export class SmltnCal {
  _simltn: SimltnInput;
  constructor(simltn: SimltnInput) {
    this._simltn = simltn;
  }

  get calData() {
    const drAmount = this._simltn["drAmount"]; // # 최소기사비1(부가세별도)
    //?????????????????????
    const prodsAmount = this._simltn["prodsAmount"]; // # 월회비 (부가세 포함)
    const discountAmount = this._simltn["discountAmount"]; // # constant | 할인액 (부가세포함)
    const discountProdsAmount = prodsAmount / 1.1 - discountAmount / 1.1; // # 할인월회비(부가세별도)

    const paidCnt = this._simltn["paidCnt"]; // # 탑승현황_운행중

    const drPayAmount = Math.round(discountProdsAmount) * paidCnt; // # 제안액(부가세별도,원)

    const diffAmount = drAmount! - drPayAmount; // # 차액

    const minPaidCnt = Math.ceil(diffAmount / discountProdsAmount) + paidCnt;

    // # 5차_인센티브설정인원수
    const incntCnt = this._simltn["incntCnt"];

    // # 5차_1인당인센티브(오천원단위내림)
    const incntCntPerUser = diffAmount / incntCnt;

    // # 5차_1인당인센티브율
    const incntCntPerUserPerDiscountProdsAmount =
      incntCntPerUser / discountProdsAmount;

    // # 최소기사비1_넘어갈총인원수(올림)
    // const moveBoardCnt = diffAmount / incntBoardCntPerUser;

    return {
      prodsAmount,
      discountAmount,
      discountProdsAmount,
      paidCnt,
      drPayAmount,
      drAmount,

      diffAmount,
      minPaidCnt,

      incntCnt,
      incntCntPerUser,
      incntCntPerUserPerDiscountProdsAmount,
    };
  }
}

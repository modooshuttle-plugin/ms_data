import { runnConstant } from "./constant";

export interface InputEl {
  /**
   * 유가_경유(원/l)
   */
  oilAmount: number;
  /**
   * 톨게이트 비용(원)
   */
  tollgateAmount: number;
  /**
   * 월 운행일수(일)
   */

  runnDayCnt: number;
  /**
   * 주말 운행일수(일)
   */

  weekdayCnt: number;
  /**
   * 편동 운행횟수(일, 회)
   */

  repeatRunnCnt: number;
  /**
   * 편도 운행거리(일, km)
   */
  runnDist: number;
  /**
   * 편도 운행시간(일, 분)
   */

  runnTime: number;
  /**
   * 출근 도착시간
   */

  timeId: string;

  /**
   * 순환셔틀 여부
   */

  isCircular: boolean;
  /**
   * 연 할부이율(%)
   */

  interestRatio: number;
  /**
   * 운행지수(km/지수)
   */
  runnConstant: number;
  /**
   * 일 기타 추가비(원)
   */
  addAmountPerDay: number;

  /**
   * 주말 추가비(원)
   */
  weekDayAddAmount: number;
}

export class InputCal {
  _inputEl: InputEl;

  constructor(obj: InputEl) {
    this._inputEl = obj;

    this._inputEl.isCircular = obj.repeatRunnCnt > 1;

    this.getRunnConstant();
  }

  get inputEl() {
    return this._inputEl;
  }

  getRunnConstant = () => {
    this._inputEl.runnConstant = 0;
    if (this._inputEl.isCircular === true) {
      this._inputEl.runnConstant = 50;
    } else {
      if (this._inputEl.runnDist >= 0 && this._inputEl.runnDist <= 150) {
        this._inputEl.runnConstant = runnConstant[this._inputEl.runnDist] || 0;
      }
    }
  };
}

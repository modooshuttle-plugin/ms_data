import { SecondCal } from "./SecondCal";

interface IThirdResult {
  avg?: number;
  avgMin?: number;
  avgMax?: number;
  time?: number;
}

export class ThirdCal {
  _secondCal: SecondCal;
  constructor(secondCal: SecondCal) {
    this._secondCal = secondCal;
  }

  timeCal = (time: string) => {
    if (time == "t004" || time == "t005") {
      return 1;
    }

    return 1.1;
  };

  get calDrPay() {
    const firstCal = this._secondCal.firstCal;
    const input = firstCal.input;
    const vatRatio = firstCal.vatRatio;
    const secondCal = this._secondCal;

    const day: IThirdResult | undefined = {};
    const month: IThirdResult = {};
    const vat: IThirdResult = {};

    // # 1. 일평균 먼저 계산
    day["avg"] = secondCal.addAmount["day"] + secondCal.minusAmount["day"];

    // # 2. 일평균으로 월평균 구성
    month["avg"] = day["avg"]! * input.runnDayCnt;
    month["avgMin"] = month["avg"] - 50000; // # ?
    month["avgMax"] = month["avg"] + 100000; // # ?

    month["time"] = Math.round(month["avgMin"] / this.timeCal(input.timeId));

    // # 3-1. 월평균에 부가세 적용
    vat["avg"] = Math.round(month["avg"] * vatRatio);
    vat["avgMin"] = Math.round(month["avgMin"] * vatRatio);
    vat["avgMax"] = Math.round(month["avgMax"] * vatRatio);
    vat["time"] = Math.round(month["time"] * vatRatio);

    // # 3-2. 월평균을 다시 운행일로 나눠서 일평균 최소, 최대 구함.
    day["avgMin"] = Math.round(month["avgMin"] / input.runnDayCnt);
    day["avgMax"] = Math.round(month["avgMax"] / input.runnDayCnt);
    day["time"] = Math.round(month["time"] / input.runnDayCnt);

    return {
      month,
      day,
      vat,
    };
  }

  get calProdsAmount() {
    const { bep } = this._secondCal.firstCal.busCal;
    const { runnDayCnt } = this._secondCal.firstCal.input;

    const month: any = {};
    const day: any = {};
    const vat: any = {};
    month["avgMin"] = this.calDrPay["month"]["avgMin"]! / bep;
    month["avg"] = this.calDrPay["month"]["avg"]! / bep;
    month["avgMax"] = this.calDrPay["month"]["avgMax"]! / bep;

    day["avgMin"] = month["avgMin"]! / runnDayCnt;
    day["avg"] = month["avg"]! / runnDayCnt;
    day["avgMax"] = month["avgMax"]! / runnDayCnt;

    vat["avgMin"] = this.calDrPay["vat"]["avgMin"]! / bep;
    vat["avg"] = this.calDrPay["vat"]["avg"]! / bep;
    vat["avgMax"] = this.calDrPay["vat"]["avgMax"]! / bep;

    return {
      day,
      month,
      vat,
    };
  }

  get calDrPayCd() {
    return {
      month: {
        time: {
          ko: "시간대 인하_월",
          comment: "(평일 최소_일 x 월 운행일수 x 90%)_숫자",
        },
        avg: {
          ko: "평일 중간_월",
          comment: "(평일 중간_일 x 월 운행일수)_숫자",
        },
        avgMin: {
          ko: "평일 최소_월",
          comment: "(평일 중간_월 - 50,000)_숫자",
        },
        avgMax: {
          ko: "평일 최대_월",
          comment: "(평일 중간_월 + 100,000)_숫자",
        },
      },
      day: {
        time: {
          ko: "시간대 인하_일",
          comment: "(평일 최소_일 x 90%)_숫자",
        },
        avg: {
          ko: "평일 중간_일",
          comment: "(평일 중간_일 x 월 운행일수)_숫자",
        },
        avgMin: {
          ko: "평일 최소_월",
          comment: "",
        },
        avgMax: {
          ko: "평일 최대_일",
          comment: "",
        },
      },
      vat: {
        time: {
          ko: "시간대 인하_월부가세포함",
          comment:
            "산출 기사비_시간대 인하_월부가세포함	(평일 최소_일 x 월 운행일수 x 90% x 110%)_숫자",
        },
        avg: {
          ko: "평일 중간_월부가세포함",
          comment:
            "산출 기사비_평일 중간_월부가세포함	(평일 중간_일 x 월 운행일수 x 110%)_숫자",
        },
        avgMin: {
          ko: "평일 최소_월부가세포함",
          comment:
            "산출 기사비_평일 최소_월부가세포함	((평일 중간_월 - 50,000) x 110%)_숫자",
        },
        avgMax: {
          ko: "평일 최대_월부가세포함",
          comment:
            "산출 기사비_평일 최대_월부가세포함	((평일 중간_월 + 100,000) x 110%)_숫자",
        },
      },
    };
  }

  get calProdsAmountCd() {
    return {
      day: {
        avg: {
          ko: "평일 중간_일",
          comment:
            "(산출 기사비_평일 중간_월 / 차종별 BEP 설정 인원수 / 월 운행일수)_숫자",
        },
        avgMin: {
          ko: "평일 최소_일",
          comment:
            "(산출 기사비_평일 최소_월 / 차종별 BEP 설정 인원수 / 월 운행일수)_숫자",
        },
        avgMax: {
          ko: "평일 최대_일",
          comment:
            "(산출 기사비_평일 최대_월 / 차종별 BEP 설정 인원수 / 월 운행일수)_숫자",
        },
      },
      month: {
        avg: {
          ko: "평일 중간_월",
          comment: "(산출 기사비_평일 중간_월 / 차종별 BEP 설정 인원수)_숫자",
        },
        avgMin: {
          ko: "평일 최소_월",
          comment: "(산출 기사비_평일 최소_월 / 차종별 BEP 설정 인원수)_숫자",
        },
        avgMax: {
          ko: "평일 최대_월",
          comment: "(산출 기사비_평일 최대_월 / 차종별 BEP 설정 인원수)_숫자",
        },
      },
      vat: {
        avg: {
          ko: "평일 중간_월부가세포함",
          comment:
            "((산출 기사비_평일 중간_월 / 차종별 BEP 설정 인원수) x 110%)_숫자",
        },
        avgMin: {
          ko: "평일 최소_월부가세포함",
          comment:
            "((산출 기사비_평일 최소_월 / 차종별 BEP 설정 인원수) x 110%)_숫자",
        },
        avgMax: {
          ko: "평일 최대_월부가세포함",
          comment:
            "((산출 기사비_평일 최대_월 / 차종별 BEP 설정 인원수) x 110%)_숫자",
        },
      },
    };
  }
}

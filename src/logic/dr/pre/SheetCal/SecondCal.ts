import { FirstCal } from "./FirstCal";

export class SecondCal {
  _firstCal: FirstCal;
  constructor(firstCal: FirstCal) {
    this._firstCal = firstCal;
  }

  get firstCal() {
    return this._firstCal;
  }

  /**
   *  기사 고정 지출비
   */
  get minusAmount() {
    // # 아래 두개는 객관적 요소라고 판단하고 넘어감.
    const oil = this._firstCal.oil;
    const tollGate = this._firstCal.tollgateAmount;

    // # 아래 요소는 전부 계산식이 동일함. 전부 다른 요소인데 그게 가능함?
    const control = this._firstCal.controlAmount;
    const expend = this._firstCal.expendAmount;
    const insr = this._firstCal.insrAmount;
    const paper = this._firstCal.paperAmount;
    const interestRatio = this._firstCal.interestRatio;

    const day =
      oil["day"] +
      control["day"] +
      expend["day"] +
      insr["day"] +
      paper["day"] +
      interestRatio["day"] +
      tollGate["day"];
    const month = day * this._firstCal.input.runnDayCnt;

    return {
      month: month,
      day: day,
    };
  }

  /**
   *  평균 운행 추가비
   */
  get addAmount() {
    const bus = this._firstCal.busCal;
    const input = this._firstCal.input;

    // # 일
    const day =
      ((input.runnTime * input.repeatRunnCnt) / 2 + input.runnConstant) *
        bus.addAmount +
      input.addAmountPerDay;
    // # 주말
    const week =
      ((input.runnTime * input.repeatRunnCnt) / 2 + input.runnConstant) *
        bus.addAmount +
      input.weekDayAddAmount +
      input.addAmountPerDay;
    // # 월
    const month = day * input.runnDayCnt;
    // # 주말포함 월
    const monthWeek = month + week * input.weekdayCnt;

    return {
      day,
      week,
      month,
      monthWeek,
    };
  }

  get minusAmountCd() {
    return {
      day: { ko: "일", comment: "일 기사비 산출 계산결과_숫자" },
      month: {
        ko: "월",
        comment: "일 x 월 운행일수 기사비 산출 계산결과_숫자",
      },
    };
  }

  get addAmountCd() {
    return {
      day: { ko: "일", comment: "일 기사비 산출 계산결과_숫자" },
      week: {
        ko: "주말",
        comment: "(일 기사비 산출 계산결과 + 주말 추가비)_숫자",
      },
      month: {
        ko: "월평균",
        comment: "(일 기사비 산출 계산결과 x 월 운행일수)_숫자",
      },
      monthWeek: {
        ko: "주말포함",
        comment:
          "(일 기사비 산출 계산결과 x 월 운행일수)_숫자 + ((일 기사비 산출 계산결과 + 주말 추가비) x 월 운행일수)_숫자",
      },
    };
  }
}

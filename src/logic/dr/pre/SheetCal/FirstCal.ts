import { InputCal, InputEl } from "./Input";

import { BusCal, BusEl } from "./Bus";

export class FirstCal {
  private _input: InputEl;
  private _busCal: BusEl;
  private _vatRatio: number = 1.1;

  constructor(busCal: BusCal, input: InputCal) {
    this._busCal = busCal.busEl;
    this._input = input.inputEl;
  }

  get busCal() {
    return this._busCal;
  }

  get input() {
    return this._input;
  }

  get vatRatio() {
    return this._vatRatio;
  }

  get oil() {
    const day =
      ((this._input.oilAmount * this._input.runnDist) /
        this._busCal.fuelEfcnc) *
      this._input.repeatRunnCnt;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);

    return {
      day: day,
      month: null,
      monthWeek: monthWeek,
    };
  }

  get interestRatio() {
    const month = (this._busCal.amount * this._input.interestRatio) / 12;
    const day = month / 2 / 30;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      month: month,
      day: day,
      monthWeek: monthWeek,
    };
  }

  get expendAmount() {
    const month = this._busCal.expendAmount;
    const day = month / 2 / 30;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      month: month,
      day: day,
      monthWeek: monthWeek,
    };
  }

  get insrAmount() {
    const month = this._busCal.insrAmount;
    const day = month / 2 / 30;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      month: month,
      day: day,
      monthWeek: monthWeek,
    };
  }

  get paperAmount() {
    const month = this._busCal.paperAmount;
    const day = month / 2 / 30;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      month: month,
      day: day,
      monthWeek: monthWeek,
    };
  }

  get controlAmount() {
    const month = this._busCal.controlAmount;
    const day = month / 2 / 30;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      month,
      day,
      monthWeek,
    };
  }

  get tollgateAmount() {
    const day = this._input.tollgateAmount;
    const monthWeek = day * (this._input.runnDayCnt + this._input.weekdayCnt);
    return {
      day,
      monthWeek,
    };
  }
}

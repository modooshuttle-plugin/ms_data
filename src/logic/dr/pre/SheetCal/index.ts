import { BusCal, BusEl } from "./Bus";
import { InputCal, InputEl } from "./Input";
import { FirstCal } from "./FirstCal";
import { SecondCal } from "./SecondCal";
import { ThirdCal } from "./ThirdCal";

import { commuteCd, koDic, busList } from "./data";

export const sheetClass = {
  busCal: BusCal,
  inputCal: InputCal,
};
export type sheetType = { busEl: BusEl; inputEl: InputEl };

export const sheetData = {
  commuteCd,
  koDic,
  busList,
};

export class SheetCal {
  _firstCal: FirstCal;
  _secondCal: SecondCal;
  _thirdCal: ThirdCal;

  constructor(bus: BusCal, input: InputCal) {
    this._firstCal = new FirstCal(bus, input);
    this._secondCal = new SecondCal(this._firstCal);
    this._thirdCal = new ThirdCal(this._secondCal);
  }

  get firstCal() {
    return this._firstCal;
  }

  get secondCal() {
    return this._secondCal;
  }

  get thirdCal() {
    return this._thirdCal;
  }
}

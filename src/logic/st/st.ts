import { IBoard, IPath, ISt } from "../../db";

export type TBoardingSt = {
  start: ISt | undefined;
  end: ISt | undefined;
};

export type TIsMonday = {
  start: boolean;
  end: boolean;
  rt: boolean;
};

export type TStLogic = { path: IPath; stList: ISt[]; board?: IBoard };

export class StLogic {
  _data: TStLogic;

  constructor(obj: TStLogic) {
    this._data = obj;
  }

  /**
   * 탑승정류장
   */
  get boardingSt(): TBoardingSt {
    const { stList, board } = this._data;

    return {
      start: stList.find((c: ISt) => c.stCd === board?.startStCd),
      end: stList.find((c: ISt) => c.stCd === board?.endStCd),
    };
  }

  /**
   * 월요일 운핸여부
   */
  get isMonday(): TIsMonday {
    const { path } = this._data;
    return {
      start: path.startCd === "b",
      end: path.endCd === "b",
      rt: path.startCd === "b" || path.endCd === "b",
    };
  }
}

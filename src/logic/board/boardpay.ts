import { IApply, IBoard, IPaid, IPay, IRt, IServiceCd } from "../../db";
import { nullCheck } from "../../util";
import { mmt } from "../../util/mymoment";

type BoardPay = {
  rt: IRt;
  board: IBoard;
  pay: IPay;
  apply: IApply;
  paid?: IPaid;
  serviceCd: IServiceCd;
  runnDay?: string;
};

// 탑승 로직 관리
export class BoardPayLogic {
  _obj: BoardPay;

  constructor(obj: BoardPay) {
    this._obj = obj;
  }

  // 결제가 필요한가? -> 사실상 이전 내용이 될듯
  get isNeed() {
    const { rt, board } = this._obj;

    let res = false;
    // 운행중
    if (rt.rtCd === "b") {
      //  운행중
      if (board.boardCd === "a" || board.boardCd === "d") {
        // 탑승신청 | 결제대기
        res = true;
      }
    } else if (rt.rtCd === "c") {
      // 운행예정
      if (board.boardCd === "a" || board.boardCd === "d") {
        // 탑승신청 | 결제대기
        res = true;
      }
    }
    return res;
  }

  // 결제가 되었는지? -> pay - status_cd === 1이면서 paid가 없는 상황이 발생했다가 됨.
  get isPaid() {
    const { paid } = this._obj;
    return nullCheck(paid);
  }

  // 결제 완료 날짜 계산
  get payDay(): string | null {
    const { board, apply, runnDay } = this._obj;

    let time;

    if (apply.prodsCd === 1) {
      if (apply.applyCd === "b") {
        // 일반탑승 복사

        time = mmt.momentTz(board.startDay);
      } else if (apply.applyCd === "d") {
        // 무료탑승 복사
        // npday -> 탑승일 이전 마지막 운행일을 계산에 활용한다.
        time = mmt.momentTz(runnDay);
      }
    }

    if (nullCheck(time) === true) {
      return time
        .set({ hour: 16, minute: 0, second: 0 })
        .format("YYYYMMDDHHmm");
    } else {
      return null;
    }
  }
}

import { IBoard, IRt, IServiceCd } from "../../db";
import { nullCheck } from "../../util";
import { mFormat } from "../../util/mymoment";
export const periodCdBoard = [
  { key: "a", ko: "unknown", status: "unknown", detail: "알수 없음" },
  { key: "b", ko: "탑승중", status: "current", detail: "탑승중" },
  {
    key: "c",
    ko: "탑승예정",
    status: "before",
    detail: "탑승 시작일이 되지 않았음.",
  },
  { key: "d", ko: "탑승종료", status: "after", detail: "탑승 종료일이 지남." },
];

export const periodCdStop = [
  { key: "a", ko: "unknown", status: "unknown", detail: "알수 없음" },
  { key: "b", ko: "탑승중", status: "current", detail: "탑승중" },
  {
    key: "c",
    ko: "탑승예정",
    status: "before",
    detail: "탑승 시작일이 되지 않았음.",
  },
  { key: "d", ko: "탑승중단", status: "after", detail: "탑승 종료일이 지남." },
];

export const bizCd = [];

// 탑승 로직 관리
export class BoardLogic {
  _rt: IRt;
  _board: IBoard;
  _serviceCd: IServiceCd;

  constructor(rt: IRt, board: IBoard, serviceCd: IServiceCd) {
    this._rt = rt;
    this._board = board;
    this._serviceCd = serviceCd;
  }

  // 탑승 타입
  get boardCd() {
    const rt = this._rt;
    let res;
    if (rt.rtCd === "b") {
      res = this.runnCd;
    } else if (rt.rtCd === "c") {
      res = this.schdCd;
    }

    return res;
  }

  // 운행중 탑승 타입 및 기간 구분
  get runnCd(): { boardCd: any; periodCd?: any } {
    const { boardCd } = this._board;
    let res = this._serviceCd.value.find((c: any) => c.key === boardCd);

    let periodCd;
    if (boardCd === "b" || boardCd === "f") {
      periodCd = this.periodCd;
    }

    return {
      boardCd: res,
      periodCd,
    };
  }

  // 운행예정 탑승 타입
  get schdCd() {
    const { boardCd } = this._board;
    const boardCdObj = this._serviceCd.value.find(
      (c: any) => c.key === boardCd
    );
    return {
      boardCd: boardCdObj,
    };
  }

  // 탑승기간 판단
  get periodCd() {
    const { startDay } = this._board;

    const endDay = this.boardEndDay;

    const cur = mFormat(`YYYY-MM-DD`);
    const list = this.constList;

    let res = list[0];
    // 날짜 데이터가 존재할 경우
    if (startDay !== undefined && endDay !== undefined) {
      res = list[1];
      if (startDay > cur) {
        res = list[2];
      } else if (endDay < cur) {
        res = list[3];
      }
    }
    return res;
  }

  /**
   * 탑승 종료일 계산
   */
  get boardEndDay() {
    const { endDay } = this._board;
    const { closeDay, rtStatusCd } = this._rt;
    let res = endDay;
    if (nullCheck(closeDay) === true) {
      // 경로의 폐지일이 붙잡혔고 유저의 탑승 종료일이 그보다 뒤인경우
      if (rtStatusCd === "a" && closeDay < endDay) {
        res = closeDay;
      }
    }
    return res;
  }

  get constList() {
    if (this._board.boardCd === "b") {
      return [...periodCdBoard];
    } else if (this._board.boardCd === "f") {
      return [...periodCdStop];
    } else {
      return [];
    }
  }

  // 수정이 가능한 상황
  get isPossUpdate() {
    const rt = this._rt;
    const board = this._board;

    let res = periodCdBoard[0];
    if (board.boardCd === "b" || board.boardCd === "f") {
      const list = this.constList;
      res = list[1];

      // 운행중인 경로에 대해서는 날짜에 대해서 계산한다.
      if (rt.rtCd === "b") {
        res = this.periodCd;
      }
    }

    return res.status !== "after";
  }

  // 취소가 가능한 상황
  get isPossCancel(): boolean {
    const rt = this._rt;
    const board = this._board;

    let res = false;
    if (rt?.rtCd === "b") {
      // 운행중의 경우
      res = [`a`, `d`, `e`].indexOf(board?.boardCd) > -1;
    } else if (rt?.rtCd === "c") {
      // 운행예정의 경우
      res = board?.boardCd !== "c" ? true : false;
    }
    return res;
  }

  get isClose() {
    return this.periodCd?.key === "d";
  }
}

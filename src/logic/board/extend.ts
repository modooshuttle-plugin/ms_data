import { IApply, IBoard, IProds, IUser } from "../../db";
import { nullCheck } from "../../util";
import { mmt, mFormat } from "../../util/mymoment";

interface IExtendBoard {
  user: IUser;
  board: IBoard;
  apply: IApply;
  prods: IProds;
  crossBoard?: IBoard;
  crossProds?: IProds;
}

export class ExtendBoard {
  _obj: IExtendBoard;
  constructor(obj: IExtendBoard) {
    this._obj = obj;
  }

  makeExtendBoard = () => {
    const { apply, board, prods, crossProds, crossBoard } = this._obj;

    const { startDay, endDay } = mmt.makeBoardPeriod(board.endDay);

    const cur = mFormat(`YYYY-MM-DD HH:mm:ss`);

    let extend: any = {};

    extend.apply = {
      ...apply,
      startDay: startDay,
      initStartDay: startDay,
      endDay: endDay,
      createdAt: cur,
      updatedAt: cur,
      prodsCd: 1,
      applyCd: "d",
      prevApplyId: apply.applyId,
    };

    extend.board = {
      ...board,
      boardId: null,
      startDay,
      endDay,
      boardShapeCd: 0,
      applyId: "?",
      boardCd: "d",
      createdAt: cur,
      updatedAt: cur,
    };

    let amount = prods.amount;
    if (crossBoard !== undefined && crossProds !== undefined) {
      if (crossProds.amount > prods.amount) {
        amount = crossProds.amount;
      }
      extend.crossBoard = {
        ...crossBoard,
        startDay,
        endDay,
        boardShapeCd: 3,
      };
    }

    extend.pay = {
      boardId: "?",
      rtId: prods.rtId,
      amount,
      prodsId: prods.prodsId,
      createdAt: cur,
      updatedAt: cur,
      statusCd: "a",
    };

    return extend;
  };
}

// export const makePeriod = (day: string) => {
// 	const m = moment(day).tz('Asia/Seoul');
// 	m.add(1, 'day');
// 	const start_day = m.format('YYYY-MM-DD');
// 	const end_day = endDay(start_day);
// 	return {
// 			start_day,
// 			end_day
// 	};
// };

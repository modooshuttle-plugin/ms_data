import {
  ICpn,
  IPaidCpn,
  ICpnConn,
  ICpnLimit,
  IServiceCd,
  IRt,
  IPay,
} from "../../db";
import { nullCheck } from "../../util";
import { mFormat } from "../../util/mymoment";

export interface ICpnObj {
  cpn: ICpn;
  paidCpn?: IPaidCpn;
  cpnConn?: ICpnConn;
  cpnLimit?: ICpnLimit;
}

export interface IServiceCdObj {
  serviceCd: {
    limitCd: IServiceCd;
    rtCd: IServiceCd;
  };
}

export interface ICpnLogic extends ICpnObj, IServiceCdObj {}

// 우선순위가 필요
//  paidCpn객체의 유무
//  useCd의 값

const periodCd = [
  { key: "before", ko: "사용대기", use: false },
  { key: "current", ko: "사용가능", use: false },
  { key: "after", ko: "기간만료", use: false },
  { key: "not", ko: "무기한", use: false },
];

export class CpnLogic {
  _obj: ICpnLogic;

  constructor(obj: ICpnLogic) {
    this._obj = obj;
  }

  get cpnStatus() {
    const { cpn } = this._obj;

    if (cpn.useCd === 1) {
      return this.usePoss;
    } else if (cpn.useCd === 0) {
      return this.useImposs;
    }
  }

  get period() {
    const { cpn } = this._obj;
    const cur = mFormat(`YYYY-MM-DD HH:mm:ss`);
    if (nullCheck(cpn.startDay) && nullCheck(cpn.endDay)) {
      if (cpn.startDay > cur) {
        return periodCd[0];
      } else if (cpn.startDay <= cur && cpn.endDay >= cur) {
        return periodCd[1];
      } else {
        return periodCd[2];
      }
    } else {
      return periodCd[3];
    }
  }

  // 사용 가능 조건
  get usePoss() {
    // 기간값
    const obj = this.period;
    const { cpn } = this._obj;

    if (obj.key === "before") {
      return {
        ...obj,
        txt: `${dateFormatCpn(cpn.startDay + " 00:00:00")}부터 사용 가능`,
      };
    } else if (obj.key === "current") {
      return {
        ...obj,
        txt: `${dateFormatCpn(cpn.endDay + " 23:59:59")}까지`,
      };
    } else if (obj.key === "after") {
      return {
        ...obj,
        txt: `${dateFormatCpn(cpn.endDay + " 23:59:59")}까지`,
      };
    } else if (obj.key === "not") {
      return {
        ...obj,
        txt: `사용가능`,
      };
    }
  }

  // 쿠폰 사용 불가능할때
  get useImposs() {
    // 기간값
    const obj = this.period;
    const { cpn, paidCpn } = this._obj;
    const use: boolean = nullCheck(paidCpn) === true;

    if (obj.key === "before") {
      return {
        ...obj,
        txt: `${dateFormatCpn(cpn.startDay + " 00:00:00")}부터 사용 가능`,
        ko: "사용완료",
        use,
      };
    } else if (obj.key === "current") {
      return {
        ...obj,
        txt: `${dateFormatCpn(cpn.endDay + " 23:59:59")}까지`,
        use,
      };
    } else if (obj.key === "after") {
      return {
        txt: `${dateFormatCpn(cpn.endDay + " 23:59:59")}까지`,
        period: "after",
        ko: "기간만료",
        use,
      };
    } else if (obj.key === "not") {
      return { txt: `무기한`, period: null, ko: "사용가능" };
    }
  }

  get condition() {
    const { cpnConn, cpnLimit, serviceCd } = this._obj;

    let cpnLimitTemp: any = cpnLimit;

    if (nullCheck(cpnLimit)) {
      const limitCdobj = serviceCd.limitCd.value.find(
        (c: any) => c.key === cpnLimit?.limitCd
      );

      let where: any = "";
      if (cpnLimit?.limitCd === "rtCd") {
        const rtCdObj = serviceCd.rtCd.value.find(
          (c: any) => c.key === cpnLimit?.targetId
        );

        where = rtCdObj.ko;
      } else {
        where = cpnLimit?.targetId;
      }

      cpnLimitTemp = {
        ...cpnLimitTemp,
        ko: limitCdobj?.ko,
        txt: `${limitCdobj?.ko} ${where}일 경우만 사용 가능`,
      };
    }

    return {
      cpnConn,
      cpnLimit: cpnLimitTemp,
    };
  }

  get periodTxt() {
    const { cpn } = this._obj;
    const obj = this.period;

    if (obj.key === "not") {
      return "무기한";
    } else {
      return `${cpn.startDay} ~ ${cpn.endDay}`;
    }
  }

  // cpn selector에서 사용
  connDetail = (payId: number) => {
    const { cpn, cpnConn } = this._obj;
    if (nullCheck(cpnConn) === true) {
      if (cpnConn?.payId === payId) {
        return { isConn: false, msg: "해당쿠폰은 해제가 불가능합니다." };
      } else {
        return {
          isConn: false,
          msg: "해당쿠폰은 해당 결제에서 선택이 불가능합니다.",
        };
      }
    } else {
      // 강제 연결 쿠폰의 경우 선택 및 해제가 불가능 하다.
      return { isConn: true, msg: "" };
    }
  };

  limitDetail = (rt: IRt, pay: IPay) => {
    const { cpnLimit, serviceCd } = this._obj;
    let res = { isPush: true, msg: ``, title: "전체상품", type: "all" };
    if (nullCheck(cpnLimit) === true) {
      if (cpnLimit?.limitCd === "rt_cd") {
        if (cpnLimit?.targetId !== rt.rtCd) {
          const rtCdKo = serviceCd.rtCd.value.find(
            (c: any) => c.key === cpnLimit?.targetId
          );

          res = {
            isPush: false,
            msg: `${rtCdKo} 경로에서만 사용가능합니다.`,
            title: rtCdKo,
            type: "rtCd",
          };
        }
      } else if (cpnLimit?.limitCd === "rt_id") {
        if (cpnLimit?.targetId !== rt.rtId) {
          res = {
            isPush: false,
            msg: ` S${cpnLimit.targetId} 경로에서만 사용가능합니다. `,
            title: cpnLimit.targetId,
            type: "rtId",
          };
        }
      } else if (cpnLimit?.limitCd === "board_id") {
        if (parseInt(cpnLimit?.targetId) !== pay.boardId) {
          res = {
            isPush: false,
            msg: `${cpnLimit.targetId} 탑승정보에서 사용가능합니다.`,
            title: cpnLimit.targetId,
            type: "boardId",
          };
        }
      } else if (cpnLimit?.limitCd === "pay_id") {
        if (parseInt(cpnLimit?.targetId) !== pay.payId) {
          res = {
            isPush: false,
            msg: `${cpnLimit.targetId} 결제정보에서 사용가능합니다.`,
            title: cpnLimit.targetId,
            type: "payId",
          };
        }
      }
    }

    return res;
  };

  // get
}

const dateFormatCpn = (date: string) => {
  const c = date.split(" ");
  const dateList = c[0].split("-");
  const timeList = c[1].split(":");

  return `${dateList[0].substring(2)}.${dateList[1]}.${dateList[2]} ${
    timeList[0]
  }:${timeList[1]}`;
};

import { IRt } from "../../db";
import { nullCheck } from "../../util";
import { mFormat } from "../../util/mymoment";

export const rtRunnCd = [
  { key: "a", ko: "운행중단", detail: "경로 폐지 후" },
  { key: "b", ko: "운행중", detail: "경로 운행단계" },
  { key: "c", ko: "운행중단 예정", detail: "경로 폐지 전" },
  { key: "d", ko: "운행 전", detail: "운행시작 전" },
];

export const rtSchdCd = [
  { key: "a", ko: "운행중단", detail: "경로 폐지" },
  { key: "b", ko: "운행예정", detail: "운행예정 모집, 운행 직전" },
];

// 경로 로직 관리
export class RtLogic {
  _rt: IRt;

  constructor(rt: IRt) {
    this._rt = rt;
  }

  // 경로 타입 확장
  getRtCdEx = () => {
    const rt: IRt = this._rt;
    let res = [...rtRunnCd][0];
    if (nullCheck(rt) === true) {
      // 운행중
      if (rt.rtCd === "b") {
        res = this.runnStatus;
      } else if (rt.rtCd === "c") {
        // 운행예정
        res = this.schdStatus;
      }
    }

    return res;
  };

  // 경로 운행 타입 확장 (오늘기준)
  get runnStatus() {
    const rt = this._rt;
    const cd = [...rtRunnCd];

    // 운행중
    let res = cd[1];
    const cur = mFormat(`YYYY-MM-DD`);
    if (rt.rtStatusCd === "a") {
      if (nullCheck(rt.closeDay) === true) {
        if (rt.closeDay < cur) {
          // 폐지후
          res = cd[0];
        } else if (rt.closeDay >= cur) {
          // 폐지전
          res = cd[2];
        }
      }
    } else {
      // 운행전
      if (rt.startDay > cur) {
        res = cd[3];
      }
    }
    return res;
  }

  // 경로 운행예정 타입 확장
  get schdStatus() {
    const rt = this._rt;
    const cd = [...rtSchdCd];
    let res = cd[1];
    if (rt.rtStatusCd === "a") {
      res = cd[0];
    }

    return res;
  }

  // 경로가 중단 되었는지
  get isStop() {
    const rtCdEx = this.getRtCdEx();
    return rtCdEx.key === "a" ? true : false;
  }
}

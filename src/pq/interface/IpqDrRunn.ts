export interface IpqDrRunn {
  runnId: string;
  date: string;
  rtId: string;
  startedAt: string;
  endedAt: string;
  deviceId: string;
  drId: string;
}

export interface RunnHist {
  runnId: string;
  createdAt: string;
  drId: string;
  deviceId: string;
  accuracy: number;
  latitude: number;
  longitude: number;
}

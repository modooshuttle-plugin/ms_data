export interface PaymentPrepareAnnotation {
  merchant_uid: string; // 가맹점에서 전달한 거래 고유 UID ,
  amount: number; // 결제 예정 금액
}

export interface PaymentPrepareAnnotationDto {
  orderNo: string; // 가맹점에서 전달한 거래 고유 UID ,
  amount: number; // 결제 예정 금액
}

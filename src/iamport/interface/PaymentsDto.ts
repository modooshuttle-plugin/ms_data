// export interface PaymentPrepareAnnotation {
//   merchant_uid: string; // 가맹점에서 전달한 거래 고유 UID ,
//   amount: number; // 결제 예정 금액
// }

import { IUserBankAccount } from "../../db";

export interface PaymentsByImpUidDto {
  impUid: string;
}

export interface PaymentsByOrderNoDto {
  orderNo: string;
}

export interface PaymentsCancelDto {
  impUid: string;
  orderNo: string;
  amount: number;
  reason: string;

  bankAccount?: Pick<IUserBankAccount, "accountNo" | "bankCd" | "nm">;
}

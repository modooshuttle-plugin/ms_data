import { IPaid, IRefund, IUserBankAccount } from "../../db";

interface IBankServiceCd {
  key: string;
  ko: string;
  kcp: string;
}

export type TypeIamportRefundObj = {
  paid: IPaid;
  refund: IRefund;
  bank?: {
    userBankAccount?: IUserBankAccount;
    list: IBankServiceCd[]; // 서비스 코드에서 가져오자
  };
};

// 아래 데이터를 입력하고 아래 객체를 아임포트로 전송하자.
export const makeRefundObj = ({ paid, refund, bank }: TypeIamportRefundObj) => {
  console.log(paid);
  const params: any = {
    imp_uid: paid.impUid,
    merchant_uid: paid.orderNo,
    amount: refund.amount,
    reason: refund.comment,
  };

  //   가상계좌일 경우 - kcp 가상계좌의 경우만 해당됨.
  if (bank?.userBankAccount !== undefined) {
    const { userBankAccount, list } = bank;
    params.refund_holder = userBankAccount.nm; // 계좌주 명
    params.refund_account = userBankAccount.accountNo; // 계좌번호

    for (let i = 0; i < list.length; i++) {
      const el = list[i];
      if (el.key === userBankAccount.bankCd) {
        params.refund_bank = el.kcp; // 은행코드를 kcp 은행코드로 변경
      }
    }
  }

  if (paid.providerCd === "naverpay") {
    params.requester = "admin"; // 그냥 이래야한다고함.
  }
  return params;
};

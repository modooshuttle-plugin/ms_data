import { IBoard, IPay, IPayTry, IRt, IUser } from "../../db";
import { nullCheck } from "../../util";

// 아임포트 요청 객체
export interface IImp {
  pg: string; // provider_cd
  pay_method: string; // 결제 방법  method_cd
  merchant_uid: string; // 모두의셔틀 주문번호
  name: string; // 상품명
  amount: number;
  buyer_name: string; // 고객명
  buyer_tel: string; // 고객전화번호
  currency: "KRW";
  escrow: boolean; // 그냥 false
  notice_url: string; // 해당 사항에 대해서 호출할 webhook
  company: "주식회사 모두의셔틀";
  card_quota: any[]; // 할부개월수 선택 UI제어옵션
  custom_data: {
    mno: number; // pay_try_id
    no: number; // pay_id
  };
  vbank_due?: string;
  tax_free?: 0;
  naverProducts?: INaverProducts[];
  naverPopupMode?: boolean; // 모바일 false, pc true
}

// 네이버 페이 객체
interface INaverProducts {
  categoryType: "ETC";
  categoryId: "ETC";
  uid: string; // 상품코드
  name: string; // 상품명
  count: number; // 상품갯수 1
}

// 모두의셔틀 결제 객체
export type TypeImpRequestObj = {
  board: IBoard;
  payTry: IPayTry; // 결제 시도 정보 (모두의셔틀 주문번호가 발급된 후에 아임포트를 호출한다.)
  rt: IRt;
  user: IUser;
  webhookUrl: string;
  pay: IPay;
  vbankDue?: string; // YYYYMMDDHHmm
  isMobile: boolean;
};

// 모두의셔틀 데이터 객체 to 아임포트 객체 생성
export const makeRequestObj = (obj: TypeImpRequestObj) => {
  const { board, payTry, rt, user, webhookUrl, pay, vbankDue } = obj;

  let value: IImp = {
    // param
    pg: payTry.providerCd,
    pay_method: payTry.methodCd,
    merchant_uid: payTry.orderNo,
    name: `${
      payTry.providerCd === "naverpay"
        ? `${rt.rtCd === "b" ? `[${board.startDay}] ` : ""} S${rt.rtId}`
        : `S${rt.rtId}`
    }`,
    amount: payTry.amount,
    buyer_name: user.nm,
    buyer_tel: user.phone,
    currency: "KRW",
    escrow: false,
    notice_url: webhookUrl,
    company: "주식회사 모두의셔틀",
    card_quota: [], // 할부개월수 선택 UI제어옵션
    custom_data: {
      mno: payTry.payTryId,
      no: pay.payId,
    },
  };

  if (payTry.providerCd === "kcp" && payTry.methodCd === "vbank") {
    if (nullCheck(vbankDue) === true) {
      value.vbank_due = vbankDue;
    }
  }

  if (payTry.providerCd === "naverpay") {
    value = naverPay(obj, value);
  }

  return value;
};

// 네이버 페이 결제 처리시 추가 처리
export const naverPay = (obj: TypeImpRequestObj, value: any) => {
  const { board, rt, pay, isMobile } = obj;
  if (isMobile === true) {
    // 네이버 페이 모바일 - 페이지 이동
    value.naverPopupMode = false;
  } else {
    // 네이버 페이 데스크탑 - 팝업
    value.naverPopupMode = true;
  }

  value.tax_free = 0;
  value.naverProducts = [
    {
      categoryType: "ETC",
      categoryId: "ETC",
      uid: pay.prodsId,
      name: `${rt.rtCd === "b" ? `[${board.startDay}] ` : ""} S${rt.rtId}`,
      count: 1,
    },
  ];
  return value;
};

import { imp_url } from ".";
import axios from "axios";

interface IamportConfig {
  impKey: string;
  impSecret: string;
}

export class Iamport {
  _iamportConfig: any;

  constructor(obj: IamportConfig) {
    this._iamportConfig = {
      imp_key: obj.impKey,
      imp_secret: obj.impSecret,
    };
  }

  getData = async () => {
    const accessToken = await this.getToken();

    const data: any = {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true,
      data: this._iamportConfig,
    };

    if (accessToken) {
      data.headers.Authorization = accessToken;
    }

    return data;
  };

  getToken = async () => {
    const data: any = {
      headers: {
        "Content-Type": "application/json",
      },

      withCredentials: true,
      data: this._iamportConfig,
    };

    try {
      const res = await axios.post(`${imp_url}/users/getToken`, {}, data);
      if (res.data.code === 0) {
        const token = res.data.response;
        return token.access_token;
      } else {
        return null;
      }
    } catch (error) {
      console.log("토큰 발행 실패", error);
      return null;
    }

    // const req = await MPOST('users/getToken', {});
  };
}

// 싱글톤으로 리턴 (매번 새로운 객체를 생성 할 필요가 없다면 처음 부터 싱글톤으로 export)

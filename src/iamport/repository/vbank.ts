import axios from "axios";
import { Iamport, imp_url } from ".";
import { objToCamelCase } from "../../util";

export class VbankRepository extends Iamport {
  /**
   * 가상계좌 삭제
   * @param impUid
   */
  deleteVbank = async (impUid: string) => {
    const data = await this.getData();
    return await axios.get(`${imp_url}/vbanks/delete/${impUid}`, data);
  };

  /**
   * 은행코드/계좌번호로 통장 예금주를 확인한다.
   * @param accountNo
   * @param bankCd
   */
  vbankHolder = async (accountNo: string, bankCd: string) => {
    const data = await this.getData();
    const res = await axios.get(
      `${imp_url}/vbanks/holder?bank_num=${accountNo}&bank_code=${bankCd}`,
      data
    );
    return objToCamelCase(res.data);
  };
}

import axios from "axios";
import { Iamport, imp_url } from ".";
import { objToCamelCase } from "../../util";
import {
  PaymentsByImpUidDto,
  PaymentsByOrderNoDto,
  PaymentsCancelDto,
} from "../interface";

export class PaymentsRepsoitory extends Iamport {
  /**
   * imp_uid로 내용 검색
   * @param param0
   * @returns
   */
  findOneByImpUid = async ({ impUid }: PaymentsByImpUidDto) => {
    const data = await this.getData();
    const res = await axios.get(`${imp_url}/payments/${impUid}`, data);

    return objToCamelCase(res.data);
  };

  /**
   * merchant_uid (orderNo) 로 내용 검색
   * @param param0
   * @returns
   */
  findResultByOrderNo = async ({ orderNo }: PaymentsByOrderNoDto) => {
    const data = await this.getData();
    const res = await axios.get(`${imp_url}/payments/find/${orderNo}`, data);

    return objToCamelCase(res.data);
  };

  /**
   * 환불처리
   * @param obj
   * @returns
   */
  cancel = async (obj: PaymentsCancelDto) => {
    const params: any = {
      imp_uid: obj.impUid,
      merchant_uid: obj.orderNo,
      amount: obj.amount,
      reason: obj.reason, // '위변조로 인한 환불 처리'
    };
    this._iamportConfig;
    if (obj.bankAccount) {
      const { nm, accountNo, bankCd } = obj.bankAccount;
      params.refund_holder = nm;
      params.refund_bank = bankCd;
      params.refund_account = accountNo;
    }

    const data = await this.getData();
    return await axios.post(`${imp_url}/payments/cancel`, params, data);
  };
}

import axios from "axios";
import { Iamport, imp_url } from ".";
import { PaymentPrepareAnnotationDto } from "../interface";

export class ValidationRepository extends Iamport {
  /**
   * validation요청
   * @param params
   */
  prepare = async (params: PaymentPrepareAnnotationDto) => {
    const data = await this.getData();
    return await axios.post(`${imp_url}/payments/cancel`, params, data);
  };
}
